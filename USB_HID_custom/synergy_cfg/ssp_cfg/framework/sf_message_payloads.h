/* generated messaging header file - do not edit */
#ifndef SF_MESSAGE_PAYLOADS_H_
#define SF_MESSAGE_PAYLOADS_H_
#include "Includes\messages.h"
typedef union u_sf_message_payload {
	_DATE_TIME_MESSAGE_PAYLOAD date_time_message_payload;
	_SERIAL_DATA_OUT_PAYLOAD serial_data_out_payload;
	_SERIAL_DATA_IN_PAYLOAD serial_data_in_payload;
	_S485_DATALOGGER_PAYLOAD S485_datalogger_payload;
	_S485_DATALOGGER_IN_PAYLOAD s485_datalogger_in_payload;
	_FLASHEXTERNAL_PAYLOAD flashexternal_payload;
} sf_message_payload_t;
#endif /* SF_MESSAGE_PAYLOADS_H_ */
