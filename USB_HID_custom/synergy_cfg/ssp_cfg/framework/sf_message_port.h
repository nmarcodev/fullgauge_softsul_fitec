/* generated messaging header file - do not edit */
#ifndef SF_MESSAGE_PORT_H_
#define SF_MESSAGE_PORT_H_
typedef enum e_sf_message_event_class {
	SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE, /* Date Time Message */
	SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT, /* Serial Data Out */
	SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN, /* Serial Data In */
	SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER, /* S485_DataloggerOut */
	SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN, /* S485_Datalogger_In */
	SF_MESSAGE_EVENT_CLASS_FLASHEXTERNAL, /* FlashExternal */
} sf_message_event_class_t;
typedef enum e_sf_message_event {
	SF_MESSAGE_EVENT_UNUSED, /* Unused */
	SF_MESSAGE_EVENT_NEW_DATA, /* New Data */
	SF_MESSAGE_EVENT_S485_TX, /* S485_TX */
	SF_MESSAGE_EVENT_S485_RX, /* S485_RX */
} sf_message_event_t;
extern TX_QUEUE Thread_Datalogger_message_queue;
extern TX_QUEUE usb_hid_device_thread_message_queue;
extern TX_QUEUE Cellular_thread_message_queue;
extern TX_QUEUE RTC_thread_message_queue;
extern TX_QUEUE Serial_thread_message_queue;
extern TX_QUEUE MainManagerThread_message_queue;
#endif /* SF_MESSAGE_PORT_H_ */
