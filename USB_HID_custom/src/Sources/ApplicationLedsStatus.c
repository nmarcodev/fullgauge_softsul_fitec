/*
 * ApplicationLedsStatus.c
 *
 *  Created on: 18 de jul de 2019
 *      Author: leonardo.ceolin
 */

#include "Includes/TypeDefs.h"
#include "Includes/ApplicationLedsStatus.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/Define_IOs.h"

void DriversRgbLeds(_LED_RGB *LedRbg);
//----------------------------------------------------------------------------------------------------------------------
void DriversRgbLeds(_LED_RGB *LedRbg)
{
    uint8_t LedPosition = 0;
    ioport_port_pin_t BufferPinOutLedRgb[NUM_LEDS][NUM_PINS] = {{LED_ON_R, LED_ON_G, LED_ON_B}, {LED_ST_R, LED_ST_G, LED_ST_B}};

    for(LedPosition = 0; LedPosition < NUM_LEDS; LedPosition++)
    {
        if(LedRbg[LedPosition].LedsHardware.LedToggle == SET)
        {
            if(LedRbg[LedPosition].LedsHardware.LedState == LED_OFF)
            {
                LedRbg[LedPosition].LedsHardware.LedState = LED_ON;
                g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][0], LedRbg[LedPosition].LedsHardware.LedPinRed ? IOPORT_LEVEL_HIGH : IOPORT_LEVEL_LOW);
                g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][1], LedRbg[LedPosition].LedsHardware.LedPinGreen ? IOPORT_LEVEL_HIGH : IOPORT_LEVEL_LOW);
                g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][2], LedRbg[LedPosition].LedsHardware.LedPinBlue ? IOPORT_LEVEL_HIGH : IOPORT_LEVEL_LOW);
            }
            else
            {
                LedRbg[LedPosition].LedsHardware.LedState = LED_OFF;
                g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][0], IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][1], IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][2], IOPORT_LEVEL_HIGH);
            }
        }
        else
        {
            switch(LedRbg[LedPosition].LedsHardware.LedTurnOnOff)
            {
                case SET:
                {
                    g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][0], LedRbg[LedPosition].LedsHardware.LedPinRed ? IOPORT_LEVEL_HIGH : IOPORT_LEVEL_LOW);
                    g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][1], LedRbg[LedPosition].LedsHardware.LedPinGreen ? IOPORT_LEVEL_HIGH : IOPORT_LEVEL_LOW);
                    g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][2], LedRbg[LedPosition].LedsHardware.LedPinBlue ? IOPORT_LEVEL_HIGH : IOPORT_LEVEL_LOW);
                }break;
                case RESET:
                {
                    g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][0], IOPORT_LEVEL_HIGH);
                    g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][1], IOPORT_LEVEL_HIGH);
                    g_ioport.p_api->pinWrite(BufferPinOutLedRgb[LedPosition][2], IOPORT_LEVEL_HIGH);
                }break;
            }
        }
    }
}
