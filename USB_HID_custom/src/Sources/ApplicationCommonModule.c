/*
 * ApplicationCommunModule.c
 *
 *  Created on: 21 de mai de 2019
 *      Author: leonardo.ceolin
 */

#include "Includes/ApplicationCommonModule.h"
#include "Includes/Define_IOs.h"
#include "Includes/Thread_Monitor_entry.h"
#include "includes/internal_flash.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void RestartModule(void);
//void SortStruct(sf_wifi_scan_t *Sort, uint8_t Size);

void RestartModule()
{
    uint32_t TempTime = 0x05000000;

#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_RESET, 0, true);
#endif

    //ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING);
    tx_thread_sleep(100);

    int_storage_deinit(); //fecha a

    //pino do reset do modulo wifi
    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_15, IOPORT_LEVEL_LOW);    //se Board Custom fg

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO
    g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO

    g_ioport.p_api->pinWrite(LED_ON_R, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ON_G, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ON_B, IOPORT_LEVEL_LOW);

    g_ioport.p_api->pinWrite(LED_ST_R, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ST_G, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ST_B, IOPORT_LEVEL_LOW);


    do{
        TempTime--;
    }while(TempTime);


    NVIC_SystemReset();
}

//Ordena em ordem do rssi mais forte para o mais fraco
//void SortStruct(sf_wifi_scan_t *Sort, uint8_t Size)
//{
//   int i, j;
//   sf_wifi_scan_t Aux[1];
//
//   for (j=Size-1; j>0; j--){
//        for (i=0; i<j; i++){
//            if (Sort[i].rssi < Sort[i+1].rssi){
//                Aux[0] = Sort[i];
//                Sort[i] = Sort[i+1];
//                Sort[i+1] = Aux[0];
//            }
//        }
//    }
//}
