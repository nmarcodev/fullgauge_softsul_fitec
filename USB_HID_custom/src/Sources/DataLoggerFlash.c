/*
 * DataLoggerFlash.c
 *
 *  Created on: 27 de mai de 2019
 *      Author: leonardo.ceolin
 */

//--------------------- INCLUDES ---------------------//
#include "Includes/DataLoggerFlash.h"
#include "Includes/CRCModbus.h"
#include "Includes/ExternalFlash.h"
#include "Includes/DateTime.h"
#include "Includes/Thread_Datalogger_entry.h"
#include "Thread_Datalogger.h"
#include "Includes/Eeprom25AA02E48.h"
//#include "Includes/ConfDefault.h"
#include "Includes/Define_IOs.h"

#include "Includes/internal_flash.h"

//----------------- VARIABLES MODULE -----------------//
uint8_t                         MIndexInstrumentsToLog,
                                MEstateMachine = 0,
                                MOverrun = 0;
uint16_t                        MCounterLog = 0;
bool                            DataLogStart;
_LIST_INSTRUMENTS               MListInstruments;
_DATA_LOGGER_CONTROL            MDataloggerControl;
_DATA_LOGGER_EEPROM_CONTROL     MDataloggerEEpromControl;
_DATA_LOG_CONTROL_BURN          MDatallogControlBurn[QTD_INSTRUMENTS];
//----------------------------------------------------//

//----------------------------------------------------//

void        DataloggerFlashSetCmdSitrad(uint16_t Value, uint8_t *BufInstruments, uint8_t Size);
uint16_t    DataloggerGetTimeToLog(void);
uint16_t    DataloggerGetListInstruments(uint8_t *ListInstruments);
uint8_t     DataloggerGetListInstrumentsFG(uint8_t *ListInstruments);
void        DataloggerErase(_DATALOGGER_DATA *DataLoggerData);
uint16_t    DataloggerGetActualPage(void);
uint8_t     DataloggerRequisitaRS485(_DATALOGGER_DATA *DatalogData);
uint8_t     DataloggerTimeOut(_DATALOGGER_DATA *DatalogData);
void        DataLoggerMountPacket (_DATALOGGER_DATA *DatalogData);
void        DataLoggerMountVoidPacket (_DATALOGGER_DATA *DatalogData);
void        DataLoggerMountEventPacket (_DATALOGGER_DATA *DatalogData, const _DATA_LOGGER_DATA event);
void        SetDataDatalogger(_DATALOGGER_DATA *DatalogData);
uint8_t     DataloggerGetFlags(void);
uint8_t     Datalogger(_DATALOGGER_DATA *DatalogData);
void        SetLogInterval(uint16_t Value);
void        SetValueFacEEPROM(uint8_t *Data);
bool        IsFirstRecord(const uint8_t Address);

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao faz a inicializacao do Datalog,le a pagina da EEPROM que contem
*                       \n a escrita do ultimo setor que foi feito log e procura o mais atual, apos
*                       \n encontrar o setor na EEPROM, faz a leitura de pagina a pagina (256bytes) da
*                       \n memoria flash procurando uma sequencia de 4 0xFF, assim, encontra o ultimo
*                       \n endereço da memoria flash escrito
*
* @param nenhum
*
* @return nenhum
*
* @details <b>Historico:</b>
* \n Cleber Girardi     29/09/2017      Criação da função
*/
/*****************************************************************************************************/
void DataloggerInit(_DATALOGGER_DATA *DatalogData)
{
    uint8_t     ZEepromData[EEPROM_SIZE],
                BufferTemp[256];
    uint16_t    Index = 0,
                ActualSector = 0;
    uint32_t    AddressTemp = 0;

    memset(&ZEepromData[0], 0, sizeof(ZEepromData));
    memset(&MDataloggerControl, 0x00, sizeof(MDataloggerControl));
    MDataloggerEEpromControl.NextAddress = 0;

    int_storage_read((uint8_t*)&MListInstruments, sizeof(_LIST_INSTRUMENTS), FLASH_DATALOGGER_INSTR_LIST); //leitura dos instrumentos
    if(MListInstruments.CkCrc != CrcModbusCalc(&MListInstruments.AddressListInstruments[0], sizeof(MListInstruments.AddressListInstruments), false))
    {
        memset(&MListInstruments, 0xFF, sizeof(MListInstruments));
    }
    if(MListInstruments.NumberOfInst == 0xFF)
    {
        MListInstruments.NumberOfInst = 0;      //indica que memoria esta apagada
    }
    if(MListInstruments.LogInterval == 0xFFFF)
    {
        MListInstruments.LogInterval = 60;
    }

    //Seta as flags de controle de primeiro log de todos os instrumentos
    memset(MDataloggerControl.FirstRecordControl, 0xFF, sizeof MDataloggerControl.FirstRecordControl);

    MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = true; //indica que o datalogger esta vazio
    SPI_Read_EEPROM(&ZEepromData[0], (uint8_t)sizeof(ZEepromData));
    uint16_t *TempReadEeprom;
    for(uint8_t i=0; i < (EEPROM_SIZE - SIZE_ADDRESS_EEPROM); i = (uint8_t)(i + SIZE_ADDRESS_EEPROM))
    {
        TempReadEeprom = (uint16_t *)&ZEepromData[i];//(uint16_t)(((ZEepromData[i+1] << 8)&0xFF00) | (ZEepromData[i]));
        if(  (*TempReadEeprom >= ActualSector) && (*TempReadEeprom < NUMBER_OF_SECTORS_DATALOGGER) )
        {
            MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = false;
            ActualSector = *TempReadEeprom;//BufferSectors[Index];
            MDataloggerEEpromControl.NextAddress = (uint8_t)(i + SIZE_ADDRESS_EEPROM);
        }
    }
    DatalogData->MDataloggerEEpromControl.NextAddress = MDataloggerEEpromControl.NextAddress;
    DatalogData->OverrunFlash = ZEepromData[EEPROM_ADDR_OVERF]; //flag de sobrescrita na memoria flash no datalogger

    DatalogData->FAC = (bool)ZEepromData[EEPROM_ADDR_FLGS]; //flag de fac
    //SetEthWifiFAC(DatalogData->FAC); //Seta na ram flag de fac
    //TODO: Verificar item comentado

    MDataloggerControl.DataloggerPacketControl.Address = 0;
    if(MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty == false)
    {
        bool found = false;
        uint8_t Page ;
        for(Page = NUMBER_PAGES_OF_SECTOR ; Page > 0 && !found;)
        {
            Page--;
            AddressTemp = ((uint32_t)ActualSector * SECTOR_FLASH_SIZE) + ((uint32_t) Page * PAGE_FLASH_SIZE);        //é o endereço inicial do setor encontrado anteriormente
            FlashReadPageData( AddressTemp, 1, &BufferTemp[0] );          //le cada pagina dentro do setor encontrado antes

            for(Index = PAGE_FLASH_SIZE; Index > 0;)
            {
                Index--;
                if(BufferTemp[Index] != 0xFF)
                {
                    //MDataloggerControl.DataloggerPacketControl.Address = (U32)(AddressTemp + Index); //soma offset de endereço da pagina atual dentro do setor atual
                    found = true;
                    break;
                }
            }
        }
        MDataloggerControl.DataloggerPacketControl.Address = (U32)(AddressTemp + Index + 3); //Pula 2 bytes 0xFF para garantir que o dado nao sera sobrescrito
        if(Page == (NUMBER_PAGES_OF_SECTOR - 1) && Index == (PAGE_FLASH_SIZE - 1))
        {//proxixmo setor
            uint16_t NextSector = (uint16_t)(ActualSector + 1);
            if(NextSector >= NUMBER_OF_SECTORS_DATALOGGER)     //TESTE DO FIM DA MEMORIA
            {
                NextSector = 0;
                ClearSectorLogEEPROM();                 //Apaga eeprom parte dos setores da flash
                if(DatalogData->OverrunFlash)
                {
                    DatalogData->OverrunFlash = 0x00; //sinaliza que ja foi sobrescrita
                    SPI_Write_EEPROM(&DatalogData->OverrunFlash, EEPROM_WRSR, EEPROM_ADDR_OVERF); //Salva o flag que sinaliza sobrescrita de flash
                    DatalogData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = false;
                }
            }
            MDataloggerControl.DataloggerPacketControl.Address = ((uint32_t)NextSector * PAGE_FLASH_SIZE * NUMBER_PAGES_OF_SECTOR);
            ClearSectorFlashQSPI(NextSector);  //apaga o proximo setor
            DataloggerWriteSectorEEprom(DatalogData, NextSector); //memoria na eeprom
        }
        if (!found)
        {//inicio do setor
            MDataloggerControl.DataloggerPacketControl.Address = ((U32)ActualSector * SECTOR_FLASH_SIZE); //soma offset de endereço da pagina atual dentro do setor atual
        }
    }

//
//
    MCounterLog = 1;                                                     // inicia o contador do intervalo de registro
//
    if((MListInstruments.LogInterval != 0) && (MListInstruments.NumberOfInst != 0))
    {
        DataLogStart = true;
    }

    DatalogData->MListInstruments.LogInterval = MListInstruments.LogInterval;
    DatalogData->LogIntervalCounter = TIME_FOR_FIRST_LOG;
    DatalogData->MListInstruments.NumberOfInst = MListInstruments.NumberOfInst;
    DatalogData->MDataloggerControl.DataloggerPacketControl.Address = MDataloggerControl.DataloggerPacketControl.Address;
    DatalogData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty;

    DatalogData->MDataloggerEEpromControl.NextAddress = MDataloggerEEpromControl.NextAddress;
    MOverrun = DatalogData->OverrunFlash;   //Atualiza a variavel de sobrescrita da flash;
}

void SetValueFacEEPROM(uint8_t *Data)
{
    SPI_Write_EEPROM(Data, SIZE_ADDRESS_FAC, EEPROM_ADDR_FLGS);
}

uint8_t Datalogger(_DATALOGGER_DATA *DatalogData)
{

    uint8_t RetD;
    RetD = DataloggerRequisitaRS485(DatalogData);
    DataLoggerPost_485(DatalogData);
    // aguarda até que um evento ocorraTX_SEMAPHORE DataloggerSemaphore;
    tx_semaphore_get(&DataloggerSemaphore, TIME_1S); //Time out para recepcao da serial
    if(FG_SUCCESS_485 == DataLoggerPend(DatalogData))
    {
        //Completa com dados do Instrumento
        DataLoggerMountPacket(DatalogData);
    }
    else
    {   //vazio, usar packet Coringa quando o instrumento nao responde
        DataLoggerMountVoidPacket(DatalogData);
    }

    DataLoggerWriteData(DatalogData);

    SetDataDatalogger(DatalogData); //atualiza informacoes do Datalogger - variaveis do modulo
    return RetD;

}


/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao monta o pacote que devera ser escrito na memoria flash
*
* @param u8LAddInstruments endereco da rede RS485 da qual pertence o equipamento a ser feito o log
* @param u16LSizePayLoad tamanho dos dados recebido do comando 32 pela RS485
* @param pu8LPayLoad Ponteiro para a respota do comando 32 que sera logado
*
* @return nenhum
*
* @details <b>Historico:</b>
* \n Cleber Girardi     26/09/2017      Criação da função
*/
/*****************************************************************************************************/
//void DataloggerMountPacket(uint8_t AddInstruments, uint16_t SizePayLoad, uint8_t *pu8LPayLoad)
void DataLoggerMountPacket (_DATALOGGER_DATA *DatalogData)
{
    uint16_t    Index = 0,
                Crc = 0,
                SizeCrc = 0;
    uint32_t    SizePayLoad;
    uint8_t     Address = DatalogData->InstrumentAddress;//AddInstruments;

    DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket = DATALOGGER_HEADER_STD;//0x1B;
    if(IsFirstRecord(Address) == true)
    {
        DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket = DATALOGGER_HEADER_START;//0x1C;
    }

    //Atualiza data e hora
    GetDateTimerX(DatalogData->MDataloggerControl.MDataloggerRegister.DateAndTime);

    SizePayLoad = (uint32_t)DatalogData->SerialDataOutPayload.num_of_bytes;

    if(SizePayLoad > PAYLOAD_SIZE)
    {
        DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash =0;
        return;
    }
    DatalogData->MDataloggerControl.MDataloggerRegister.AddressRs485 = Address;
    DatalogData->MDataloggerControl.MDataloggerRegister.SizePacketMSB = (uint8_t)(((SizePayLoad + HEADER_DATALOG + SIZE_CKSUM_FRAME_DTL) >>  8) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.SizePacketLSB = (uint8_t)(((SizePayLoad + HEADER_DATALOG + SIZE_CKSUM_FRAME_DTL) >>  0) & 0xFF);

    memcpy(& DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[0],
             DatalogData->SerialDataOutPayload.pointer,
             SizePayLoad);

    SizeCrc = (uint16_t)(SizePayLoad + HEADER_DATALOG);
    Index = (uint16_t)(SizePayLoad);// + 1);
    Crc = CrcModbusCalc(&DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket, SizeCrc, false);

    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index] = (uint8_t)((Crc >>  8) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index + 1] = (uint8_t)((Crc >>  0) & 0xFF);

    DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash = (uint16_t)(SizeCrc + CRC_SIZE);
}

///*****************************************************************************************************/
///**
//* @brief <b>Descricao:</b> Esta funcao monta o pacote VAZIO quando instrumento nao retorna nada
//*
//* @param u8LAddInstruments endereco da rede RS485 da qual pertence o equipamento a ser feito o log
//*
//* @return nenhum
//*
//* @details <b>Historico:</b>
//* \n Cleber Girardi     27/03/2018      Criação da função
//*/
///*****************************************************************************************************/
void DataLoggerMountVoidPacket (_DATALOGGER_DATA *DatalogData)
{
    uint16_t    Index = 0,
                Crc = 0,
                SizeCrc = 0;
    uint8_t     Address = DatalogData->InstrumentAddress;//AddInstruments;

    DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket = DATALOGGER_HEADER_STD;//0x1B;

    GetDateTimerX(DatalogData->MDataloggerControl.MDataloggerRegister.DateAndTime);

    if(IsFirstRecord(Address) == true)
    {
        DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket = DATALOGGER_HEADER_START;//0x1C;
    }

    DatalogData->MDataloggerControl.MDataloggerRegister.AddressRs485 = Address;//AddInstruments;
    DatalogData->MDataloggerControl.MDataloggerRegister.SizePacketMSB = (uint8_t)(((HEADER_DATALOG + SIZE_CKSUM_FRAME_DTL) >>  8) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.SizePacketLSB = (uint8_t)(((HEADER_DATALOG + SIZE_CKSUM_FRAME_DTL) >>  0) & 0xFF);

    SizeCrc = (uint16_t)(HEADER_DATALOG);
    Crc = CrcModbusCalc(&DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket, SizeCrc, false);

    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index] = (uint8_t)((Crc >>  8) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index + 1] = (uint8_t)((Crc >>  0) & 0xFF);

    DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash = (uint16_t)(SizeCrc + CRC_SIZE);
}

#if RECORD_DATALOGGER_EVENTS
void DataLoggerMountEventPacket (_DATALOGGER_DATA *DatalogData, const _DATA_LOGGER_DATA event)
{
    uint16_t    Index = 0,
                Crc = 0,
                SizeCrc = 3;

    DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket = DATALOGGER_HEADER_EVENT;

    GetDateTimerX(DatalogData->MDataloggerControl.MDataloggerRegister.DateAndTime);

    DatalogData->MDataloggerControl.MDataloggerRegister.AddressRs485 = event.QntPg;
    DatalogData->MDataloggerControl.MDataloggerRegister.SizePacketMSB = (uint8_t)(((HEADER_DATALOG + SIZE_CKSUM_FRAME_DTL + SizeCrc) >>  8) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.SizePacketLSB = (uint8_t)(((HEADER_DATALOG + SIZE_CKSUM_FRAME_DTL + SizeCrc) >>  0) & 0xFF);

    SizeCrc = (uint16_t)(HEADER_DATALOG + SizeCrc);
    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index++] = (uint8_t)((event.SizeTemp >>  8) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index++] = (uint8_t)((event.SizeTemp  >>  0) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index++] = (uint8_t)(event.status);

    Crc = CrcModbusCalc(&DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket, SizeCrc, false);

    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index] = (uint8_t)((Crc >>  8) & 0xFF);
    DatalogData->MDataloggerControl.MDataloggerRegister.PayloadSave[Index + 1] = (uint8_t)((Crc >>  0) & 0xFF);

    DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash = (uint16_t)(SizeCrc + CRC_SIZE);
}
#endif

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao comunica e loga o instrumento da rede
*
* @param nenhum
*
* @return 1 quando chegou no fim da lista, e 0 quando ainda esta lendo os equipamentos
*
* @details <b>Historico:</b>
* \n Cleber Girardi     09/10/2017      Criação da função
*/
/*****************************************************************************************************/
uint8_t DataloggerRequisitaRS485(_DATALOGGER_DATA *DatalogData)
{
    uint8_t Ret = true;
    static uint8_t Send_485[2];
    Send_485[1] = COMMAND_32;

    if(MDatallogControlBurn[MIndexInstrumentsToLog].RequestSetup == true)
    {
        DatalogData->MListInstruments.AddressListInstruments[MIndexInstrumentsToLog] = MListInstruments.AddressListInstruments[MIndexInstrumentsToLog];
        DatalogData->MIndexInstrumentsToLog = MIndexInstrumentsToLog;
        DatalogData->InstrumentAddress = DatalogData->MListInstruments.AddressListInstruments[MIndexInstrumentsToLog];
        Send_485[0] = DatalogData->InstrumentAddress;

        DatalogData->SerialDataInPayload->pointer = (uint16_t *)&Send_485[0]; //passa o endereco do
        DatalogData->SerialDataInPayload->num_of_bytes = 2;
    }

    MDatallogControlBurn[MIndexInstrumentsToLog++].RequestSetup = false;
    if(MIndexInstrumentsToLog >= MListInstruments.NumberOfInst)
    {
        MIndexInstrumentsToLog = 0;
        Ret = false;
    }

    return Ret;
}

/////*****************************************************************************************************/
/////**
////* @brief <b>Descricao:</b> Esta e a funcao apenas atualiza as variaveis do modulo, para posteriormente,
////*                          \n serem obtidas por outras threads via get....
////*
////* @param nenhum
////*
////* @return nenhum
////*
////* @details <b>Historico:</b>
////* \n Leonardo Ceolin     16/07/2019      Criação da função
////*/
void SetDataDatalogger(_DATALOGGER_DATA *DatalogData)
{
    MDataloggerControl.DataloggerPacketControl.Address  = DatalogData->MDataloggerControl.DataloggerPacketControl.Address;
    MOverrun                                            = DatalogData->OverrunFlash;   //Atualiza a variavel de sobrescrita da flash;
    DataLogStart                                        = DatalogData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty;                   //sinaliza que a partir de agora, o log nao esta inciando um novo intervalo.
    DatalogData->MListInstruments.LogInterval           = MListInstruments.LogInterval;
}

//* @brief <b>Descricao:</b> Esta funcao chama a leitura da RS485 quando nao tiver socket aberto
//*
//* @param nenhum
//*
//* @return true or false
//*
//*/
///*****************************************************************************************************/
uint8_t DataloggerTimeOut(_DATALOGGER_DATA *DatalogData)
{
    uint16_t Index = 0;

    if(MListInstruments.LogInterval != 0)
    {
        for(Index = 0; Index < MListInstruments.NumberOfInst; Index++)
        {
            MDatallogControlBurn[Index].RequestSetup = false;
            DatalogData->MDatallogControlBurn[Index].RequestSetup = false;

            if((MListInstruments.AddressListInstruments[Index] > 0) && (MListInstruments.AddressListInstruments[Index] < 248))
            {
                MDatallogControlBurn[Index].RequestSetup = true;
                DatalogData->MDatallogControlBurn[Index].RequestSetup = true;
            }
        }
        MIndexInstrumentsToLog = 0;
    }
    else
    {
        for(Index = 0; Index < MListInstruments.NumberOfInst; Index++)
        {
            MDatallogControlBurn[Index].RequestSetup = false;
            DatalogData->MDatallogControlBurn[Index].RequestSetup = false;
        }
    }

    if(MListInstruments.NumberOfInst && MListInstruments.LogInterval)
    {
        return true; //com instrumentos para log
    }

    return false; //sem instrumentos para log
}


/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao retorna qual a pagina o log
*
* @param nenhum
*
* @return flags atuais
*
* @details <b>Historico:</b>
* \n Cleber Girardi     16/10/2017      Criação da função
*/
/*****************************************************************************************************/
uint16_t DataloggerGetActualPage(void)
{
    return ((uint16_t)(MDataloggerControl.DataloggerPacketControl.Address / PAGE_FLASH_SIZE));
}

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao APAGA o paginamento da EEPROM e APAGA toda a memoria flash por comando
*                       \n recebidos pelo Sitrad ou da Web
*
* @param nenhum
*
* @return nenhum
*
* @details <b>Historico:</b>
* \n Cleber Girardi     02/10/2017      Criação da função
*/
/*****************************************************************************************************/
void DataloggerErase(_DATALOGGER_DATA *DataLoggerData)
{
    DataLoggerData->MDataloggerEEpromControl.NextAddress = EEPROM_ADDR_STA;
    DataLoggerData->OverrunFlash = 0xFF; //limpa flag de sobrescrita da flash;
    DataLoggerData->FlagsEEprom.Bites.bOverrun = true;
    DataLoggerData->MDataloggerControl.DataloggerPacketControl.Address = 0;
    DataLoggerData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = true;
    DataLoggerData->OverrunFlash = 0xFF;

    //LIMPA VARIAVEIS DA RAM
    MDataloggerEEpromControl.NextAddress = EEPROM_ADDR_STA;
    MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = true;

    MDataloggerControl.DataloggerPacketControl.Address = 0;
    MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash = 0;
    //Seta as flags de controle de primeiro log de todos os instrumentos
    memset(MDataloggerControl.FirstRecordControl, 0xFF, sizeof MDataloggerControl.FirstRecordControl);

#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_DATALOGGER_ERASED, 0, false);
#endif

}

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao carrega o valor de intervalo de log na estrutura
*
* @param u8LValue valor recebido pelo SITRAD
*
* @return nenhum
*
* @details <b>Historico:</b>
* \n Cleber Girardi     29/11/2017      Criação da função
* \n Leonardo Ceolin    18/06/2019      Adaptado para a realidade atual
*/
/*****************************************************************************************************/
void DataloggerFlashSetCmdSitrad(uint16_t Value, uint8_t *BufInstruments, uint8_t Size)
{
    uint8_t     IndexByte = 0,
                IndexArray = 0,
                QtdInstruments = 0;


    if(!Value)
    {
        _LIST_INSTRUMENTS TempListInstruments;

        int_storage_read((uint8_t*)&TempListInstruments, sizeof(_LIST_INSTRUMENTS), FLASH_DATALOGGER_INSTR_LIST); //leitura dos instrumentos

        MListInstruments.LogInterval = TempListInstruments.LogInterval;
    }
    else
    {
        MListInstruments.LogInterval = Value;
    }

    for(IndexByte = 0; IndexByte < Size; IndexByte++)
    {
        if((*BufInstruments != 0) && (*BufInstruments <= QTD_INSTRUMENTS))
        {
            QtdInstruments++;
        }
        MListInstruments.AddressListInstruments[IndexArray++] = *BufInstruments++;
    }

    MListInstruments.NumberOfInst = QtdInstruments;

    MListInstruments.CkCrc = CrcModbusCalc(&MListInstruments.AddressListInstruments[0], sizeof(MListInstruments.AddressListInstruments), false);

    int_storage_write((uint8_t*)&MListInstruments, sizeof(_LIST_INSTRUMENTS), FLASH_DATALOGGER_INSTR_LIST);
}

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao carrega o valor de intervalo de log na estrutura
*
* @param u8LValue valor recebido pelo SITRAD
*
* @return nenhum
*
* @details <b>Historico:</b>
* \n Leonardo Ceolin    18/06/2019      Adaptado para a realidade atual
*/
/*****************************************************************************************************/
void SetLogInterval(uint16_t Value)
{
    _LIST_INSTRUMENTS TempListInstruments;

    int_storage_read((uint8_t*)&TempListInstruments, sizeof(_LIST_INSTRUMENTS), FLASH_DATALOGGER_INSTR_LIST); //leitura dos instrumentos

    TempListInstruments.LogInterval = Value;

    int_storage_write((uint8_t*)&TempListInstruments, sizeof(_LIST_INSTRUMENTS), FLASH_DATALOGGER_INSTR_LIST); //grava

    MListInstruments.LogInterval = TempListInstruments.LogInterval;
}


///*****************************************************************************************************/
///**
//* @brief <b>Descricao:</b> Esta funcao analisa quais instrumentos estão na rede
//*
//* @param u8LValue valor recebido pela WEB
//* @param u8LNibleRx numero do nibel recebido
//*
//* @return nenhum
//*
//* @details <b>Historico:</b>
//* \n Cleber Girardi     18/10/2017      Criação da função
//*/
///*****************************************************************************************************/
//void DataloggerSetListInstruments(uint8_t Value, uint8_t NibleRx)
//{
//    uint8_t Counter = 0,                                      //Counter posição que o endereço esta na lista de instrumentos
//            Index = 0,
//            NibbleInstruments = 0;
//
//    if(Value >= 0x30 && Value <= 0x39)
//        NibbleInstruments = (Value - 0x30);                   //numeros de 0 .. 9 em ASCII
//    else
//        NibbleInstruments = (Value - 0x37);                   //numeros de A .. F em ASCII
//
//    Counter = (NibleRx * 4);
//
//    for(Index = 1; Index < 5; Index++)
//    {
//        if(NibbleInstruments & (1 << (Index - 1)))
//            MListInstruments.AddressListInstruments[Counter++] = (Index + (NibleRx * 4));
//    }
//}

/************************************ EXT ********************************************/
/**
* @brief <b>Descricao:</b> Esta funcao retorna a lista de instrumentos cadastrados para ser enviada ao Sitrad
* \n pelo comando 0x17
*
* @param pu8LListInstruments Ponteiro para o buffer a ser copiado
*
* @return Retorna o numero de valores carregados
*
* @details <b>Historico:</b>
* \n Cleber Girardi     28/11/2017      Criação da função
*/
/***************************************************************************************************/
uint16_t DataloggerGetListInstruments(uint8_t *ListInstruments)
{
    uint16_t Index = 0;
    uint8_t  IndexH = 0; //Parte alta

    for(Index = 0; Index < QTD_INSTRUMENTS; Index++)
    {
        if(MListInstruments.AddressListInstruments[Index] > 0 && MListInstruments.AddressListInstruments[Index] <= QTD_INSTRUMENTS)
        {
            *ListInstruments++ = MListInstruments.AddressListInstruments[Index];
            IndexH++;
        }
        else
        {
            *ListInstruments++ = 0;
        }
    }

    return (uint16_t)(Index | (uint16_t)((IndexH << 8) & 0xFF00));
}

/************************************ EXT ********************************************/
/**
* @brief <b>Descricao:</b> Esta funcao retorna a lista de instrumentos cadastrados para ser enviada ao Sitrad
* \n pelo comando 0x17
*
* @param pu8LListInstruments Ponteiro para o buffer a ser copiado
*
* @return Retorna o numero de valores carregados
*
* @details <b>Historico:</b>
* \n Cleber Girardi     28/11/2017      Criação da função
*/
/***************************************************************************************************/
uint8_t DataloggerGetListInstrumentsFG(uint8_t *ListInstruments)
{
    uint16_t Index = 0;

    for(Index = 0; Index < MListInstruments.NumberOfInst; Index++)
    {
        if(MListInstruments.AddressListInstruments[Index] > 0 && MListInstruments.AddressListInstruments[Index] <= QTD_INSTRUMENTS)
        {
            *ListInstruments++ = MListInstruments.AddressListInstruments[Index];
        }
    }

    return (uint8_t)MListInstruments.NumberOfInst;
}
/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao retorna o tempo programado para log 0x16
*
* @param nenhum
*
* @return Retorna o tempo de log
*
* @details <b>Historico:</b>
* \n Cleber Girardi     28/11/2017      Criação da função
*/
/*****************************************************************************************************/
uint16_t  DataloggerGetTimeToLog(void)
{
    return (MListInstruments.LogInterval);
}

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao retorna o flag que sinaliza sobrescrita na flash, esta negada pois
*                          \n o sitrad le 0 para sem sobrescrita e FF para sobrescrita.
*
* @param nenhum
*
* @return flags atuais
*
* @details <b>Historico:</b>
* \n Cleber Girardi     16/10/2017      Criação da função
* \n Leonardo Ceolin    16/07/2019      Adaptada para a atual implementacao
*/
/*****************************************************************************************************/
uint8_t DataloggerGetFlags(void)
{
    return (!MOverrun);
}

/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Verifica se o instrumento ja teve seu primeiro log gravado.
*                          \n Retorna true apenas na primeira chamada. Depois sempre retorna false.
*
* @param Address Enredeco do instrumento
*
* @return True se o instrumento ainda nao teve seu log gravado
*           False se o instrumento ja teve seu log gravado
*
* @details <b>Historico:</b>
* \n Tiago Santos     23/03/2020      Criação da função
*/
/*****************************************************************************************************/
bool IsFirstRecord(const uint8_t Address)
{
    uint8_t Index = Address / 8;
    uint8_t Shift = Address % 8;
    bool First = MDataloggerControl.FirstRecordControl[Index] & (1 << (Shift));
    MDataloggerControl.FirstRecordControl[Index] = MDataloggerControl.FirstRecordControl[Index] & (uint8_t)(~(1 << (Shift)));
    return First;
}

