/*
 * DateTime.c
 *
 *  Created on: 10 de jan de 2019
 *      Author: felipe.pohren
 */
#include "common_data.h" // include obrigatório

#include "Includes/TypeDefs.h"
#include "Includes/DateTime.h"

void GetDateTimerX(uint8_t *DateTimeValue);
void SetDateTimerX(uint8_t *DateTimeValue);
void DateTimerX(uint8_t *DateTimeValue, _DATE_TIME_EVENT DataTimeEvent);
uint8_t StatusEcloRam(uint8_t FlagEclo, bool FlagWR);
void SetFlagEclo(uint8_t StatusEclo);
uint8_t GetFlagEclo(void);

const _PARAM_LIMITS DateTimeLimits[DATE_TIME_SIZE] =
{
 {1,  31},    // DATE_TIME_DAY    = 0,
 {1,  12},    // DATE_TIME_MONTH  = 1,
 {19, 99},    // DATE_TIME_YEAR   = 2,
 {0,  23},    // DATE_TIME_HOUR   = 3,
 {0,  59},    // DATE_TIME_MINUTE = 4,
 {0,  59},    // DATE_TIME_SECOND = 5,
};

void GetDateTimerX(uint8_t *DateTimeValue)
{
    DateTimerX(DateTimeValue, DT_GET );
}

void SetDateTimerX(uint8_t *DateTimeValue)
{
    DateTimerX(DateTimeValue, DT_SET );
}

void SetFlagEclo(uint8_t StatusEclo)
{
    StatusEcloRam(StatusEclo, true);
}

uint8_t GetFlagEclo(void)
{
    return (StatusEcloRam(0, false));
}

uint8_t StatusEcloRam(uint8_t FlagEclo, bool FlagWR)
{
    static uint8_t RamEclo = false;

    if(FlagWR)//Write true
    {
        RamEclo = FlagEclo;
    }

    return RamEclo;
}

//Esta funcao recebe a data/hora a cada segundo.
//quem posta nesta funcao e o rtc
//void GetDateTimerX(uint8_t *DateTimeValue, _DATE_TIME_EVENT DataTimeEvent)
void DateTimerX(uint8_t *DateTimeValue, _DATE_TIME_EVENT DataTimeEvent)
{
    static uint8_t DateTime[DATE_TIME_SIZE]; // MUX?

    //carrega o
    if(DataTimeEvent == DT_GET)
    {
        DateTimeValue[DATE_TIME_DAY]    = (uint8_t)DateTime[DATE_TIME_DAY];
        DateTimeValue[DATE_TIME_MONTH]  = (uint8_t)DateTime[DATE_TIME_MONTH];
        DateTimeValue[DATE_TIME_YEAR]   = (uint8_t)DateTime[DATE_TIME_YEAR];
        DateTimeValue[DATE_TIME_HOUR]   = (uint8_t)DateTime[DATE_TIME_HOUR];
        DateTimeValue[DATE_TIME_MINUTE] = (uint8_t)DateTime[DATE_TIME_MINUTE];
        DateTimeValue[DATE_TIME_SECOND] = (uint8_t)DateTime[DATE_TIME_SECOND];
    }

    else if(DataTimeEvent == DT_SET)
    {
        DateTime[DATE_TIME_DAY]    = (uint8_t)DateTimeValue[DATE_TIME_DAY];
        DateTime[DATE_TIME_MONTH]  = (uint8_t)DateTimeValue[DATE_TIME_MONTH];
        DateTime[DATE_TIME_YEAR]   = (uint8_t)DateTimeValue[DATE_TIME_YEAR];
        DateTime[DATE_TIME_HOUR]   = (uint8_t)DateTimeValue[DATE_TIME_HOUR];
        DateTime[DATE_TIME_MINUTE] = (uint8_t)DateTimeValue[DATE_TIME_MINUTE];
        DateTime[DATE_TIME_SECOND] = (uint8_t)DateTimeValue[DATE_TIME_SECOND];
    }

}
