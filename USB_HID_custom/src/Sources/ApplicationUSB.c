/*
 * ApplicationUSB.c
 *
 *  Created on: 27 de mai de 2021
 *      Author: rhenanbezerra
 */

#include "usb_hid_device_thread.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/TypeDefs.h"
#include "ux_api.h"
#include "ux_device_class_hid.h"
#include "common_data.h"
#include "Includes/Define_IOs.h"
#include "Includes/serial485_entry.h"
#include "Includes/ExternalFlash.h"
#include "stdio.h"

extern _USB_CONTROL USBControl;
#define TypeCom 2


_USB_RETURN ProcessPacketUSB(_USB_CONTROL *USBData);
void LoginDataResponse(_USB_CONTROL *USBData);
void GenericResponse(_USB_CONTROL *USBData);
void ConfigInfoResponse(_USB_CONTROL *USBData);
void TestResponse(_USB_CONTROL *USBData);
uint16_t FactoryTest(_USB_CONTROL *EthUSBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber);


UINT ux_hid_device_callback(UX_SLAVE_CLASS_HID* hid, UX_SLAVE_CLASS_HID_EVENT* hid_event)
{
    /* Does nothing. */
    SSP_PARAMETER_NOT_USED(hid);

    memcpy(&USBControl.BufferRx[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB + SIZE_PAYLOAD_USB);

	ProcessPacketUSB(&USBControl);

    piscaLed( LED_ST_R,  500U);

    return UX_SUCCESS;
}




_USB_RETURN ProcessPacketUSB(_USB_CONTROL *USBData)
{
	//int RNumber = rand();
	switch(USBData->BufferRx[0])
	{
		char senha_conversor[8];
		char data_hora[6];

		case CMD_LOGIN_CONVERSOR:
			print485("Realiza login no conversor.\r\n", LEN("Realiza login no conversor.\r\n") );
			memcpy(&senha_conversor[0],&USBData->BufferRx[2], USBData->BufferRx[1]);
			print485("A senha do conversor e:\r\n", LEN("A senha do conversor e:\r\n") );
			char rtc_writer[50];
			uint8_t n = 0;
			n=(uint8_t)sprintf(rtc_writer, "%d%d%d%d%d%d%d%d\r\n", (int)senha_conversor[0],(int)senha_conversor[1],(int)senha_conversor[2],(int)senha_conversor[3],(int)senha_conversor[4],(int)senha_conversor[5],(int)senha_conversor[6],(int)senha_conversor[7]);
			print485(rtc_writer, n);

			LoginDataResponse(USBData);
			break;

		case CMD_REINICIA_CONVERSOR:
			print485("Reiniciar conversor.\r\n", LEN("Reiniciar conversor.\r\n") );
			GenericResponse(USBData);
			RestartModule();
			break;

		case CMD_CONF_FABRICA:
			print485("Reset configuracoes para valores de fabrica.\r\n", LEN("Reset configuracoes para valores de fabrica.\r\n") );
//			RestoreFactorySetting();
			GenericResponse(USBData);
			break;

		case CMD_ATUALIZA_CONF:
			print485("Atualizar informacoes de configuracao.\r\n", LEN("Atualizar informacoes de configuracao.\r\n") );
			//AtualizaInfoConf(EthUSBData, &BufferRX[0], &BufferInput[0], (uint16_t)RNumber);
			GenericResponse(USBData);
			break;

		case CMD_CMD_REQ_CONF:
			print485("Requisita as informacoes de configuracao.\r\n", LEN("Requisita as informacoes de configuracao.\r\n") );
			//ConfigInfoResponse(USBData);
			break;

		case CMD_DESC_CONVERSOR:
			print485("Solicita desconexão do conversor.\r\n", LEN("Solicita desconexão do conversor.\r\n") );
			//AnswerDefault(&data[0], &BufferUSB->HeaderPayload[0], (uint16_t)RNumber);
		    //tx_event_flags_set (&USB_Flags, EW_FLAG_TCP_DISCONNECT, TX_OR);
			GenericResponse(USBData);
			break;

		case CMD_LIMPAR_DATALOGGER:
			print485("Comando para apagar datalogger do conversor.\r\n", LEN("SComando para apagar datalogger do conversor.\r\n") );
			//tx_event_flags_set (&DataloggerEventFlags, DL_FLAG_CLEAR, TX_OR);
			GenericResponse(USBData);
			break;

		case CMD_LEITURA_LOG:
			print485("Comando para fazer leitura de uma pagina completa de log do conversor.\r\n", LEN("Comando para fazer leitura de uma pagina completa de log do conversor.\r\n") );
			//ReadPgDatalogger(&BufferUSB[0], &data[0], (uint16_t)RNumber);
			break;

		case CMD_AJUSTE_DATA_HORA:
			print485("Comando para ajustar a data e hora do conversor.\r\n", LEN("Comando para ajustar a data e hora do conversor.\r\n") );
			memcpy(&data_hora[0],&USBData->BufferRx[2], USBData->BufferRx[1]);
			print485("A data e hora atual e:\r\n", LEN("A data e hora atual e:\r\n") );
			*rtc_writer = 0;
			n = 0;
			n=(uint8_t)sprintf(rtc_writer, "%d-%d-%d %d:%d:%d\r\n", (int)data_hora[0],(int)data_hora[1],(int)data_hora[2],(int)data_hora[3],(int)data_hora[4],(int)data_hora[5]);
			print485(rtc_writer, n);

			//UpdadeDateHour(&USBData->BufferRx, &USBData->Header, USBData, (uint16_t)RNumber);
			GenericResponse(USBData);
			break;

		case CMD_STATUS_CONVERSOR:
			print485("Comando que requisita o status do conversor.\r\n", LEN("Comando que requisita o status do conversor.\r\n") );
			//USBGetStatus(data[TypeCom], &BufferUSB->HeaderPayload[0], &ControlUSB, (uint16_t)RNumber);
			break;

		case CMD_ATUALIZACAO_FW:
			print485("Comando que envia atualizacao do firmware para o conversor.\r\n", LEN("Comando que envia atualizacao do firmware para o conversor.\r\n") );
			//DownloadFirmware(&ControlUSB, data, (uint16_t)RNumber); //todo declarar o usb_cotrol
			break;

		case CMD_TESTE_FABRICA:
			print485("Comando que realiza o teste de fobrica do modulo.\r\n", LEN("Comando que realiza o teste de fobrica do modulo.\r\n") );
			//FactoryTest(&USBData, &USBData->BufferUSB, &USBData->BufferRx, (uint16_t)RNumber);
			TestResponse(USBData);
			break;

		default:
			print485("Comando desconhecido.\r\n", LEN("Comando desconhecido.\r\n") );
		    break;
	}

	return 1;
}
//uint16_t AtualizaInfoConf(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber)
//{
//    //0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28
//    //32 92 04 00 18 26 0c 54 50 2d 4c 49 4e 4b 5f 44 36 38 32 27 08 38 30 31 30 31 30 36 32
//    //print_to_console("\n\r [DM] SITRAD TCP -> 0x04 - ATUALIZ_INFO_CONF!");
//    //0  1  2  3  4  5 6  7  8  9  10 11 12 13 14 15 ....
//    //0d 20 04 00 fd 16 02 00 0f 17 f7 1e 20 21 22 00
//    uint16_t Temp = 0;
//    uint16_t Size = 0;
//    //-------------------Header------------------------
//    BufferUSB->HeaderPayload[Size++] = HEADER_TX_TCP;                           //TYPE_IND
//    BufferUSB->HeaderPayload[Size++] = 0x00;                                    //SIZE1_IND
//    BufferUSB->HeaderPayload[Size++] = 0x10;//0x01;                             //SIZE2_IND
//    //-------------------Payload-----------------------
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber    >>     8);     //RANDOM1_IND
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber    ^   0xFF);     //RANDOM2_IND
//    BufferUSB->HeaderPayload[Size++] = BufferInput[TypeCom] |    0x80;          //COMMAND_IND
//    BufferUSB->HeaderPayload[Size++] = 0x00;          //SIZE
//    BufferUSB->HeaderPayload[Size++] = 0x00;          //SIZE
//
//    if(BufferInput[4] == 0xFD) //compatibilidade da versao antiga
//    {
//        Temp = (uint16_t)((BufferInput[7] <<  8) | (BufferInput[8]));           //captura o tempo de log
//        if(Temp < 15)//se for menor que 15 segundos, seta em 15s;
//            Temp = 15;
//
//        DataloggerFlashSetCmdSitrad(Temp, &BufferInput[11], BufferInput[10]);
//    }
//    else
//    {
//        uint16_t SizeRequest = (uint16_t)( BufferInput[3] << 8 | BufferInput[4]);
//        SetConfSitrad(&BufferInput[5], EthWifiData, SizeRequest); //envia os parametros recebidos do configurador para a memoria flash.
//    }
//
//    return Size;
//}
//
//void RestoreFactorySetting()
//{
//    _UDP_CONF ConfSitradUdp;
//    _LIST_INSTRUMENTS MListInstruments;
//
//    int_storage_read((uint8_t *)&ConfSitradUdp, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
//    ConfSitradUdp.ConfDefault = 0;
//
//    memset(&MListInstruments, 0xFF, sizeof(MListInstruments));//leitura dos instrumentos
//
//    int_storage_write((uint8_t *)&ConfSitradUdp, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
//    int_storage_write((uint8_t *)&MListInstruments, sizeof(_LIST_INSTRUMENTS), FLASH_DATALOGGER_INSTR_LIST);
//
//    LedsAllOnOff(false);
//
////    RestartModule();
//}

uint16_t FactoryTest(_USB_CONTROL *EthUSBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber)
{
    //ULONG               IpActualStatus  = 0;
    bool                FSend           = true;
    uint16_t            Size            = 0;



    //75 ce 0C 00 01 00 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A
    //-------------------+ Header +------------------------
    BufferUSB->HeaderPayload[Size++] = HEADER_TX_TCP;
    BufferUSB->HeaderPayload[Size++] = 0x00;
    BufferUSB->HeaderPayload[Size++] = 0x10;
    //------------------+ Payload +------------------------
    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber   >>  8   );
    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
    BufferUSB->HeaderPayload[Size++] = (uint8_t)BufferInput[TypeCom] | 0x80;
    BufferUSB->HeaderPayload[Size++] = 0x00;//06
    BufferUSB->HeaderPayload[Size++] = 0x02;//7
    BufferUSB->HeaderPayload[Size++] = BufferInput[5]; //8 sub id
    Size++;

    // verifica se pode entrar em modo teste
    if((!EthUSBData->FlagFAC))
    {
        return Size;
    }

    switch(BufferInput[5])
    {
        case FAC_TEST_START:
        {
        	EthUSBData->FlagFAC = true;

            //_DATA_LOGGER_DATA DataLoggerData;
            //DataLoggerData.OpCod = TEST_START; //Avisa a thread datalogger que uma atualizacao vai comecar.
            //tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger

            //ThreadMainManagerSetFlag(MM_FAC_MODE);

        }break;

        case FAC_TEST_SUPER_CAP_ON:
        {
            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_HIGH);

            tx_thread_sleep(TIME_200MS);

            //InitI2C_RTC();

            tx_thread_sleep(TIME_200MS);

            // inicializa o RTC Externo
            //RX8571LC_Init();

            // verifica estado de ECLO
            //if(FG_SUCCESS != RX8571LC_VerifyEclo())
            //{
                //FSend = false; //foi mal.
            //}

          //  ThreadMonitor_SendKeepAlive(T_MONITOR_START_MONITORING);
        }break;

        case FAC_TEST_SUPER_CAP_OFF:
        {
           // ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING);

            //CloseI2C_RTC();

            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_LOW);

        }break;

        case FAC_TEST_QSPI:
        {
//            FSend = TestQSPI(); //ret=true ok, ret false, nok

        }break;

        case FAC_TEST_LEDS:
        {
//            ThreadMainManagerSetLedColor(BufferInput[6]);
           // ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);

        }break;

        case FAC_TEST_485:
        {
//            FSend = TestSerial485(BufferInput[6]);

        }break;

        case FAC_TEST_KEY:
        {
//            FSend = ThreadMainManagerGetStatusButton();
        }break;

        case FAC_TEST_WIFI:
        {//6f   3a  8c  00  02  07  01  09  09  09  09  09  09  09  09  09
            //          06  07 08   09    10 11 12 13 14 15
            //xx xx 8C MSB LSB YY [00/01] 00 01 QR 01 WW SI...SI 02 01 IR -> FAC_TEST_WIFI [OK/NÃO_OK]
            //QR: Quantidade de redes WW: Tamanho do nome da rede mais forte IR: Intensidade da Rede mais forte
//        	BufferUSB->HeaderPayload[7] = (uint8_t)TestModuleWiFi(&BufferUSB->HeaderPayload[10]); //retorna o tamanho
            uint8_t TempSize = (uint8_t)(BufferUSB->HeaderPayload[7] + 5) / 16;

            TempSize++;

            BufferUSB->HeaderPayload[2] = (uint8_t)(0x10 * TempSize);

            if(!BufferUSB->HeaderPayload[11])
            {
                FSend = false;
            }

            Size = (uint16_t)(8 + BufferUSB->HeaderPayload[7]);
        }break;

        case FAC_TEST_MAC:
        {
//            Size = (uint16_t)((uint8_t)10 + TestMacAddress(&BufferUSB->HeaderPayload[10]));
            BufferUSB->HeaderPayload[2] = 0x20;
            BufferUSB->HeaderPayload[7] = 18;
        }break;

        case FAC_TEST_DO_FAC:
        {
           // _FLAGS_EEPROM FlagsEeprom;
         //   FlagsEeprom.Bites.bCalibration = true;
          //  SetValueFacEEPROM(&FlagsEeprom.Bytes);
            EthUSBData->TimerRestartModule = 3; //INICIALIZA A VARIAVEL QUE VAI RESETAR O CONVERSOR
        }break;

        case FAC_TEST_END:
        {
//            ThreadMainManagerSetLedColor(12);
            //ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);
        }break;

        default:
            break;
    }

    if(FSend)
    {
    	BufferUSB->HeaderPayload[9] = 0;
    }
    else
    {
    	BufferUSB->HeaderPayload[9] = 1;
    }

    return Size++;
}
//
//uint16_t ReadPgDatalogger(_BUFFER_USB *BufferUsb, uint8_t * BufferInput, uint16_t RandomNumber)
//{
//    /* Teste ...  08 00 03 00 00 01 08 08 08 08 08 08 08 08*/
//    /* Pagina(d39 = 0x27) 08 00 03 00 27 01 08 08 08 08 08 08 08 08
//    * */
//    //print_to_console("\n\r [DM] SITRAD TCP -> 0x08 - READ_PG_DATALOGGER!");
//    //if(S25FL064ChipEraseFinished() == 0)
//    // 0   1   2   3  4  5 6   7  ....
//    //RDM RDL 08  00  03 x y   z  (X || Y): Pagina a Ler  z: Qntidade de paginas.
//    uint16_t Size = 0;
//    uint16_t Temp;
//
//    Temp = (uint16_t)((BufferInput[5] <<  8) | (BufferInput[6]));         //captura o numero da pagina a ler
//    uint8_t QtdPages = BufferInput[7];
//
//        //-------------------Header------------------------
//    BufferUsb->HeaderPayload[Size++] = HEADER_TX_TCP;
//    BufferUsb->HeaderPayload[Size++] = 0x00;
//    BufferUsb->HeaderPayload[Size++] = 0x00;
//    //-------------------Payload-----------------------
//    BufferUsb->HeaderPayload[Size++] = (uint8_t)(RandomNumber >> 8   );
//    BufferUsb->HeaderPayload[Size++] = (uint8_t)(RandomNumber ^  0xFF);
//    BufferUsb->HeaderPayload[Size++] = BufferInput[TypeCom] | 0x80;
//    BufferUsb->HeaderPayload[Size++] = 0x00;
//    BufferUsb->HeaderPayload[Size++] = 0x00;
//
//    if((QtdPages > MAX_PAGES_TO_READ) || (QtdPages < 1))
//    {
//#if HANDLE_FLASH_ERRORS
//        BufferTcp->HeaderPayload[5] = BufferInput[TypeCom] | 0xA0;
//
//#endif
//        Size = 8;
//        BufferUsb->HeaderPayload[6] = 0; //ok
//        BufferUsb->HeaderPayload[7] = 0; //ok
//
//    }
//    else // tudo ok, pode ler as paginas
//    {
//
//
//        static _DATA_LOGGER_DATA DataLoggerData;
//        DataLoggerData.OpCod = REQUEST_PAG; //Sinaliza o Cod da operacao
//        DataLoggerData.QntPg = QtdPages; //numero de paginas que deve ser lida
//        DataLoggerData.SizeTemp = Temp; //primeira pagina a ser lida
//        DataLoggerData.PayloadTcp = &(BufferUsb->HeaderPayload[Size]);
//
//        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
//        tx_thread_relinquish();//TODO tratar o retorno da funcao da flash datalogger
//        tx_queue_receive(&EthWifi_Flash, &DataLoggerData, TX_WAIT_FOREVER); //recebe da thread Datalogger
//        //todo verificar variável EthWifi_Flash
//
//#if HANDLE_FLASH_ERRORS
//        if(!DataLoggerData.status)//erro de leitura da flasj qspi
//        {
//            BufferTcp->HeaderPayload[5] = BufferInput[TypeCom] | 0xC0;
//
//            Size = 8;
//            BufferTcp->HeaderPayload[6] = 0; //ok
//            BufferTcp->HeaderPayload[7] = 0; //ok
//        }
//        else
//#endif
//        {
//            uint16_t u16LQtd;
//            u16LQtd = (uint16_t)((uint16_t)(QtdPages*PAGE_SIZE) + (uint16_t)(CRC_SIZE));
//
//            BufferUsb->HeaderPayload[6] = (uint8_t)((u16LQtd >>  8) & 0xFF); //ok
//            BufferUsb->HeaderPayload[7] = (uint8_t)((u16LQtd >>  0) & 0xFF); //ok
//
//            u16LQtd = (uint16_t)(u16LQtd + (uint16_t)(HEADER_SIZE));
//
//            Size = (uint16_t)(u16LQtd - (uint16_t)CRC_SIZE);
//
//            Temp = (uint16_t)(CrcModbusCalc((uint8_t*)&BufferUsb->HeaderPayload[5], Size, false));
//
//            Size = (uint16_t)(Size + (uint16_t)5);
//
//            BufferUsb->HeaderPayload[Size++] = (uint8_t)((Temp >>  8) & 0xFF);
//            BufferUsb->HeaderPayload[Size++] = (uint8_t)((Temp >>  0) & 0xFF);
//        }
//    }
//
//    /*Ajusta o tamanho do payload*/
//    uint16_t DifTemp = (Size % 16);
//    BufferUsb->HeaderPayload[1] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  8) & 0xFF);
//    BufferUsb->HeaderPayload[2] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  0) & 0xFF);
//
//    return Size;
//}
//
//void UpdadeDateHour(uint8_t * BufferInput, uint8_t * GetConfData, _USB_CONTROL *EthUSBControl, uint16_t RandomNumber)
//{
//    //Pacote sitrad: 11 2d 09 00 07 19 02 13 02 11 39 0f 04 04 04 04
//    //                              D  M  Y  DW H  M  S  x  x  x  x
////    uint8_t    *AddInput, *AddOut ;
////
////    AddInput = GetConfData;
////
//    //-------------------Header------------------------
//    *GetConfData++ = HEADER_TX_TCP;
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x10;
//    //-------------------Payload-----------------------
//    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
//    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
//    *GetConfData++ = ( BufferInput[2] | 0x80 );
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x01;
//    *GetConfData++ = 0x00;
//
//    AddInput = GetConfData;
//
//    //-------------------Header------------------------
//    *GetConfData++ = HEADER_TX_TCP;
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x10;
//    //-------------------Payload-----------------------
//    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
//    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
//    *GetConfData++ = ( BufferInput[2] | 0x80 );
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x01;
//    *GetConfData++ = 0x00;
//
////    AddOut = GetConfData;
//
//    EthUSBControl->DateTimeValues[0] = BufferInput[3];
//    EthUSBControl->DateTimeValues[1] = BufferInput[4];
//    EthUSBControl->DateTimeValues[2] = BufferInput[5];
//    EthUSBControl->DateTimeValues[3] = BufferInput[6];
//    EthUSBControl->DateTimeValues[4] = BufferInput[7];
//    EthUSBControl->DateTimeValues[5] = BufferInput[8];
//
//}
//
//uint16_t DownloadFirmware(_USB_CONTROL *EthUSBData, uint8_t * BufferInput, uint16_t RandomNumber)
//{
//    bool                FSend       = false;
//    uint16_t            Size        = 0,
//                        SizePayload = 0,
//                        SizeFile    = 0,
//                        CRC_Calc    = 0,
//                        CompFile    = 0,
//                        FragFile    = 0,
//                        CRC_Recv    = 0;
//    static uint32_t     AddressFirmware = 0;
//    static uint16_t     ExpFragFile = 0;
//    EthUSBData->TimerTimeOutOTA = 8;
//
//    //ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING); //para o monitorando
//
//    EthUSBData->FlagOTARun = true;
//
//    SizePayload = (uint16_t)((BufferInput[3] <<  8) | (BufferInput[4])); //captura o tamanho do payload
//    SizeFile    = (uint16_t)(SizePayload - 6);
//
//    FragFile    = (uint16_t)((BufferInput[5] <<  8) | (BufferInput[6])); //captura o fragmento do arquivo
//    CompFile    = (uint16_t)((BufferInput[7] <<  8) | (BufferInput[8])); //captura o numero de partes do arqvivo
//
//    CRC_Calc = (uint16_t)(CrcModbusCalc((uint8_t*)&BufferInput[9], SizeFile, true));
//    CRC_Recv = (uint16_t)((BufferInput[SizePayload+3] <<  8) | (BufferInput[SizePayload+4]));
//
//    if(!FragFile) //Sinaliza que vai comecar a enviar o firmware;
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_FOTA_STATUS_CHANGE, 0, true);
//#endif
//
//        _DATA_LOGGER_DATA DataLoggerData;
//        DataLoggerData.OpCod = START_OTA; //Avisa a thread datalogger que uma atualizacao vai comecar.
//        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
//
//        ExpFragFile = 1;
//        AddressFirmware = 0;
//        ClearSectorFlashQSPIFirm((uint16_t)AddressFirmware);
//        if(!EthUSBData->FlagConnectSitrad)
//		{
//			FSend = true;
//		}
//    }
//
//    else if((CRC_Calc == CRC_Recv) && (FragFile <= CompFile))
//    {
//       if(FragFile != ExpFragFile)
//       {
//           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW);
//           FSend = false;
//       }
//       else
//       {
//           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);
//
//           EthUSBData->FlashExternalPaylod->SizeData = (uint16_t)SizeFile;
//           EthUSBData->FlashExternalPaylod->FragFile = (uint16_t)FragFile;
//           EthUSBData->FlashExternalPaylod->CompFile = (uint16_t)CompFile;
//           EthUSBData->FlashExternalPaylod->DataFlash = &BufferInput[9];
//
//           AddressFirmware = SaveFirmwareQSPI(EthUSBData->FlashExternalPaylod, AddressFirmware);
//           ExpFragFile ++;
//           FSend = true;
//       }
//
//       if(CompFile == FragFile)
//       {
//           if(!FSend)
//           {
//               EthUSBData->TimerTimeOutOTA = 3;
//           }
//           else
//           {
//               EthUSBData->TimerOTACheckCRC = 2;
//           }
//       }
//    }
//
//    //-------------------+ Header +------------------------
//    EthUSBData->BufferUSB.HeaderPayload[Size++] = HEADER_TX_TCP;
//    EthUSBData->BufferUSB.HeaderPayload[Size++] = 0x00;
//    EthUSBData->BufferUSB.HeaderPayload[Size++] = 0x10;
//    //------------------+ Payload +------------------------
//    EthUSBData->BufferUSB.HeaderPayload[Size++] = (uint8_t)(RandomNumber   >>  8   );
//    EthUSBData->BufferUSB.HeaderPayload[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
//    EthUSBData->BufferUSB.HeaderPayload[Size++] = (uint8_t)BufferInput[TypeCom] | 0x80;
//    EthUSBData->BufferUSB.HeaderPayload[Size++] = 0;//tamanho
//    //BufferTcp->HeaderPayload[Size++] = 1;//tamanho todo corrigir protocolo
//    if(FSend)
//    {
//    	EthUSBData->BufferUSB.HeaderPayload[Size++] = 0;
//        FSend = false;
//    }
//    else
//    {
//    	EthUSBData->BufferUSB.HeaderPayload[Size++] = 1;
//    	EthUSBData->BufferUSB.HeaderPayload[Size++] = (uint8_t)ExpFragFile;
//    }
//
//    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);
//
//    return Size;
//}
//
//uint16_t USBGetStatus(uint8_t id, uint8_t * GetConfData, _USB_CONTROL *usbControl, uint16_t RandomNumber)
//{
//    uint8_t     DateTimeValue[DATE_TIME_SIZE];
//    uint16_t    Temp;
//    uint8_t    *AddInput, *AddOut ;
//
//    AddInput = GetConfData;
//
//    GetDateTimerX(DateTimeValue);
//
//    //-------------------Header------------------------
//    *GetConfData++ = HEADER_TX_TCP;
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x20;
//    //-------------------Payload-----------------------
//    *GetConfData++ = (uint8_t)(RandomNumber >> 8);
//    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
//    *GetConfData++ = (uint8_t)(id | 0x80);
//    *GetConfData++ = (uint8_t)0x00;
//    *GetConfData++ = (uint8_t)0x10;
//    *GetConfData++ = (uint8_t)DataloggerGetFlags();
//    *GetConfData++ = (uint8_t)GetFlagEclo();
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_DAY];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_MONTH];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_YEAR];
//    *GetConfData++ = (uint8_t)0x00;//WeekOfTheMonth
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_HOUR];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_MINUTE];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_SECOND];
//    Temp = DataloggerGetActualPage();
//    *GetConfData++ = (uint8_t)((Temp >>  8) & 0xFF);
//    *GetConfData++ = (uint8_t)((Temp >>  0) & 0xFF);
//    *GetConfData++ = (uint8_t)((PAGE_FLASH_SIZE >>  8) & 0xFF);
//    *GetConfData++ = (uint8_t)((PAGE_FLASH_SIZE >>  0) & 0xFF);
//    *GetConfData++ = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  8) & 0xFF);
//    *GetConfData++ = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  0) & 0xFF);
//
//    *GetConfData++ = (uint8_t)(usbControl->FlagConnectSitrad); //novo
//
//    AddOut = GetConfData;
//
//    return (uint16_t)(AddOut - AddInput);
//}

void LoginDataResponse(_USB_CONTROL *USBData)
{
	USBData->BufferTx[0] = (UCHAR)(USBData->BufferRx[0] + 0x80);
	USBData->BufferTx[1]= 0x00;
	USBData->BufferTx[2]= 0x01;
	USBData->BufferTx[3]= 0x01; //senha válida
	print485("Senha valida\r\n", LEN("Senha valida\r\n") );
	SendPacketUSB(USBData,4);

}

void GenericResponse(_USB_CONTROL *USBData)
{
	USBData->BufferTx[0] = (UCHAR)(USBData->BufferRx[0] + 0x80);
	USBData->BufferTx[1]= 0x00;
	USBData->BufferTx[2]= 0x00;
	print485("Comando respondido\r\n", LEN("Comando respondido\r\n") );
	SendPacketUSB(USBData,3);
}


void ConfigInfoResponse(_USB_CONTROL *USBData)
{
	uint16_t length_payload = 300;
	char Modelo_conv[20] = "Teste de modelo";
	char Nome_conv[30] = "Teste de nome";
	char Versao[20] = "Versao 1";
	uint16_t baud_rate = 28800;
	uint16_t timeout = 10;
	char Senha[8] = "12345678";
	uint16_t TempoRegLog = 10;
	char ListaEquip[30] = "Lista de equipamentos";
	char ERB[8] = "TIM";
	char VersaoEXS82[20] = "Versao 1";
	char APNatual[100] = "Teste";
	char APNpadrao[100] = "Teste";
	char APNalt[100] = "Teste";
	char APNmanual[100] = "Teste";
	char APNusuario[30] = "Teste";
	char APNsenha[30] = "Teste";
	char IDSitrad[10] = "Teste";
	char IMEI[20] = "Teste";
	//uint16_t cont = 0;

	USBData->BufferTx[0] = (UCHAR)(USBData->BufferRx[0] + 0x80);


	memset(&USBData->BufferTx[1],0,2);
	memcpy(&USBData->BufferTx[1], &length_payload, sizeof(uint16_t));

	//Modelo do conversor - 20 bytes
	USBData->BufferTx[3] = 0x06; //ID
	USBData->BufferTx[4] = 0x14; //Tamanho
	memcpy(&USBData->BufferTx[5],&Modelo_conv[0], 20);

	//Nome do Conversor - 30 bytes
	USBData->BufferTx[25] = 0x07; //ID
	USBData->BufferTx[26] = 0x1E; //Tamanho
	memcpy(&USBData->BufferTx[27],&Nome_conv[0], 30);


	//Proteção de senha - 1 byte
	USBData->BufferTx[57] = 0x08; //ID
	USBData->BufferTx[58] = 0x01; //Tamanho
	USBData->BufferTx[59] = 0x01; // True

	//versão - 20 bytes
	USBData->BufferTx[60] = 0x0A;
	USBData->BufferTx[61] = 0x14; //Tamanho
	memcpy(&USBData->BufferTx[62],&Versao[0], 20);

	// Baud Rate - 4 bytes
	USBData->BufferTx[82] = 0x0F; //ID
	USBData->BufferTx[83] = 0x04; //Tamanho
	memset(&USBData->BufferTx[84],0,4);
	memcpy(&USBData->BufferTx[84], &baud_rate, sizeof(uint32_t));

	//Timeout retorno - 2 bytes
	USBData->BufferTx[88] = 0x10; //ID
	USBData->BufferTx[89] = 0x02; //Tamanho
	memset(&USBData->BufferTx[90],0,2);
	memcpy(&USBData->BufferTx[90], &timeout, sizeof(uint16_t));


	//Senha - 8 bytes
	USBData->BufferTx[92] = 0x12; //ID
	USBData->BufferTx[93] = 0x08; //Tamanho
	memcpy(&USBData->BufferTx[94],&Senha[0], 8);


	// Tempo registro log - 2 bytes
	USBData->BufferTx[102] = 0x16; //ID
	USBData->BufferTx[103] = 0x02; //Tamanho
	memset(&USBData->BufferTx[104],0,2);
	memcpy(&USBData->BufferTx[104], &TempoRegLog, sizeof(uint16_t));


	// Lista de equipamentos  serem logados - 247 bytes
	USBData->BufferTx[106] = 0x17; //ID
	USBData->BufferTx[107] = 0xF7; //Tamanho
	memcpy(&USBData->BufferTx[108],&ListaEquip[0], 8);

	// Status Sitrad - 1 byte
	USBData->BufferTx[365] = 0x29; //ID
	USBData->BufferTx[366] = 0x01; //Tamanho
	USBData->BufferTx[367] = 0x01; // Com conexão

	// Estação de Rádio Base  - 8 bytes
	USBData->BufferTx[368] = 0x30; //ID
	USBData->BufferTx[369] = 0x08; //Tamanho
	memcpy(&USBData->BufferTx[370],&ERB[0], 8);

	// Status da Rede Celular - 1 byte
	USBData->BufferTx[378] = 0x31; //ID
	USBData->BufferTx[379] = 0x01; //Tamanho
	USBData->BufferTx[380] = 0x01;

	//Versão do Módulo EXS82 - 20 bytes
	USBData->BufferTx[381] = 0x32; //ID
	USBData->BufferTx[382] = 0x14; //Tamanho
	memcpy(&USBData->BufferTx[383],&VersaoEXS82[0], 20);

	//Modo do APN - 1 byte
	USBData->BufferTx[403] = 0x33; //ID
	USBData->BufferTx[404] = 0x01; //Tamanho
	USBData->BufferTx[405] = 0x01; //Manual

	// APN Atual - 100 bytes
	USBData->BufferTx[406] = 0x34; //ID
	USBData->BufferTx[407] = 0x64; //Tamanho
	memcpy(&USBData->BufferTx[408],&APNatual[0], 100);

	//APN padrão - 100 bytes
	USBData->BufferTx[409] = 0x35; //ID
	USBData->BufferTx[410] = 0x64; //Tamanho
	memcpy(&USBData->BufferTx[411],&APNpadrao[0], 100);

	//APN alternativa - 100 bytes
	USBData->BufferTx[511] = 0x36; //ID
	USBData->BufferTx[512] = 0x64; //Tamanho
	memcpy(&USBData->BufferTx[513],&APNalt[0], 100);

	//APN - manual - 100 bytes
	USBData->BufferTx[613] = 0x37; //ID
	USBData->BufferTx[614] = 0x64; //Tamanho
	memcpy(&USBData->BufferTx[615],&APNmanual[0], 100);

	//APN – usuário - 30 bytes
	USBData->BufferTx[715] = 0x38; //ID
	USBData->BufferTx[716] = 0x1E; //Tamanho
	memcpy(&USBData->BufferTx[717],&APNusuario[0], 30);

	//APN - senha - 30 bytes
	USBData->BufferTx[747] = 0x39; //ID
	USBData->BufferTx[748] = 0x1E; //Tamanho
	memcpy(&USBData->BufferTx[749],&APNsenha[0], 30);

	//ID Sitrad - 10 bytes
	USBData->BufferTx[779] = 0x40; //ID
	USBData->BufferTx[780] = 0x0A; //Tamanho
	memcpy(&USBData->BufferTx[781],&IDSitrad[0], 10);

	// IMEI do Dispositivo 20 bytes
	USBData->BufferTx[791] = 0x41; //ID
	USBData->BufferTx[792] = 0x14; //Tamanho
			memcpy(&USBData->BufferTx[793],&IMEI[0], 20);

	SendPacketUSB(USBData,255);
}

void TestResponse(_USB_CONTROL *USBData)
{
	USBData->BufferTx[0] = (UCHAR)(USBData->BufferRx[0] + 0x80);
	USBData->BufferTx[1]= 0x00;
	USBData->BufferTx[2]= 0x01;
	USBData->BufferTx[3]= 0x00; //Teste Ok
	print485("Teste Ok\r\n", LEN("Teste Ok\r\n") );
	SendPacketUSB(USBData,4);

}

