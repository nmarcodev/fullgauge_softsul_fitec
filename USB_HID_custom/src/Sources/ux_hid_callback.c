/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/
#include "ux_api.h"
#include "ux_device_class_hid.h"
#include "common_data.h"
#include "Includes/Define_IOs.h"
#include "Includes/serial485_entry.h"
#include "Includes/ApplicationUSB.h"

UCHAR g_received;

/* This example callback shows how to interpret an event for a HID keyboard. */
//UINT ux_hid_device_callback(UX_SLAVE_CLASS_HID* hid, UX_SLAVE_CLASS_HID_EVENT* hid_event)
//{
//	uint8_t         BufferRx[SIZE_HEADER_TCP + SIZE_PAYLOAD_TCP]
//
//    /* Does nothing. */
//    SSP_PARAMETER_NOT_USED(hid);
//    //g_received = hid_event->ux_device_class_hid_event_buffer[0];
//	BufferRx = hid_event->ux_device_class_hid_event_buffer;
//
//    piscaLed( LED_ST_R,  500U);
//
//    return UX_SUCCESS;
//}

