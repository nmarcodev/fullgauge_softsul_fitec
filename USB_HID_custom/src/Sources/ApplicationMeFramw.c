/*
 * ApplicationMeFramw.c
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */

#include "common_data.h" // include obrigatório
//#include "Includes/Eth_Wifi_Thread_Entry.h"
//#include "Eth_Wifi_Thread.h"
//#include "Includes/ApplicationIp.h"
#include "Includes/ApplicationMeFramw.h"
#include "Includes/ApplicationUSB.h"

void RtcPost(_USB_CONTROL *USBData);
void SerialPost(_USB_CONTROL *USBData);
bool PendSerial485(_USB_CONTROL *USBData);

void RtcPost(_USB_CONTROL *USBData)
{
	USBData->DateTimeMessagePayload->header.event_b.class_instance = 0;
	USBData->DateTimeMessagePayload->header.event_b.class_code = SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE;
    USBData->DateTimeMessagePayload->header.event_b.code = SF_MESSAGE_EVENT_NEW_DATA;

    USBData->DateTimeMessagePayload->SetDateTime = true;
    memcpy(&USBData->DateTimeMessagePayload->DateTimeValues[0], &USBData->DateTimeValues[0], DATE_TIME_SIZE);

    g_sf_message0.p_api->post(g_sf_message0.p_ctrl,
                                           &USBData->DateTimeMessagePayload->header,
                                           &USBData->MessageCfg.postCfg,
                                           &USBData->MessageCfg.err_postcFG,
                                           300);
}

void SerialPost(_USB_CONTROL *USBData)
{
	USBData->SerialDataInPayload->header.event_b.class_instance = 0;
	USBData->SerialDataInPayload->header.event_b.class_code = SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN; //symbol da engrenagem
	USBData->SerialDataInPayload->header.event_b.code = SF_MESSAGE_EVENT_S485_TX; //Symbol Events da engrenagem

    g_sf_message0.p_api->post(g_sf_message0.p_ctrl,
                                           &USBData->SerialDataInPayload->header,//DataIpPayloadMessagePayload->header,
                                           &USBData->MessageCfg.postCfg,         //global que esta no incio desta thread;
                                           &USBData->MessageCfg.err_postcFG,     //global que esta no incio desta thread;
                                           TX_NO_WAIT);
}

bool PendSerial485(_USB_CONTROL *USBData)
{
    ssp_err_t SSP_status;
    bool RetTemp = true;

    _SERIAL_DATA_OUT_PAYLOAD         *SerialDataOutPayloadMP;

    SSP_status = g_sf_message0.p_api->pend(g_sf_message0.p_ctrl,
                                           &usb_hid_device_thread_message_queue,
                                           (sf_message_header_t **) &USBData->Header,
                                           TX_NO_WAIT);
    if(SSP_status == SSP_SUCCESS)
    {
        //recebeu menssagem de retorno da 485
        if(USBData->Header->event_b.class_code == SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT)
        {
            SerialDataOutPayloadMP = (_SERIAL_DATA_OUT_PAYLOAD*)USBData->Header;

            USBData->SerialDataOutPayload.num_of_bytes = SerialDataOutPayloadMP->num_of_bytes;
            USBData->SerialDataOutPayload.pointer = SerialDataOutPayloadMP->pointer;
            if(SerialDataOutPayloadMP->num_of_bytes)
            {
                RetTemp = false;
            }
        }
    }

    return RetTemp;
}
