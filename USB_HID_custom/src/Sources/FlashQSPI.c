/*
 * FlashQSPI.c
 *
 *  Created on: 13 de nov de 2019
 *      Author: leonardo.ceolin
 *
 * Trata da comunicacao com a flash externa, de 8MB. desse total, 7MB sao reservados para registros do datalogger
 * e 1MB e reservado para o armazenamento do firmware durante a atualizacao.
 */

#include "Includes/FlashQSPI.h"
#include "Thread_Datalogger.h"

void FlashReadFirmwareData(uint32_t Address, uint16_t QntPag, uint8_t *DataFlash);
uint32_t SaveFirmwareQSPI(_FLASHEXTERNAL_PAYLOAD *FlashExternalPayload, uint32_t AddsFirmware);
uint8_t CheckCRCFirmwareQSPI(void);
void ClearFirmwareFlashQSPI(void);

bool FlashWritePageData(uint32_t Address, uint16_t Lenght, uint8_t *DataFlash);
void ClearSectorFlashQSPIFirm(uint16_t Sector);

/* Esta funcao salva a flash externa o firmware depois do download */
uint32_t SaveFirmwareQSPI(_FLASHEXTERNAL_PAYLOAD *FlashExternalPayload, uint32_t AddsFirmware)
{
    uint16_t    QtdBytesToEndPage = 0,  ///> quantidade de bytes que restao para o fim da pagina
                NextSector = 0,         ///> numero do proximo setor para comparar com o anterior e ver se tem q alterar proximo endereço
                ActualSector = 0,
                NumPageToWrite = 0;     ///> numero da pagina a ser escrita
    uint32_t    FirstAddPage = 0;        ///> primeiro endereço da pagina vigente

    while(1)
    {
        NumPageToWrite      = (uint16_t)(AddsFirmware / PAGE_FLASH_SIZE);     //em qual pagina sera iniciado a escrita de 0 até 4096
        FirstAddPage        = (uint32_t)(NumPageToWrite * PAGE_FLASH_SIZE);      //calcula qual o endereço inicial da pagina que se deseja escrever
        QtdBytesToEndPage   = (uint16_t)(PAGE_FLASH_SIZE - (AddsFirmware - FirstAddPage));  //quantidade de bytes para acabar esta pagina, ESTE É O TAMANHO DA PRIMEIRA ESCRITA NA FLASH
        ActualSector        = (uint16_t)(NumPageToWrite / NUMBER_PAGES_OF_SECTOR);          //indica em qual setor será feito a gravação

        //CASO PRECISE DE MAIS DE UMA PAGINA PARA TERMINAR A ESCRITA
        if(FlashExternalPayload->SizeData >= QtdBytesToEndPage)
        {
            FlashWritePageData( AddsFirmware + QSPI_DEVICE_ADDRESS,
                                QtdBytesToEndPage,
                                FlashExternalPayload->DataFlash);

            FlashExternalPayload->DataFlash += QtdBytesToEndPage; //Incrementa o ponteiro contendo o dado
            AddsFirmware += QtdBytesToEndPage;

            //TAMANHO RESTANTE PARA GRAVAR NA OUTRA PAGINA
            FlashExternalPayload->SizeData = (uint16_t)(FlashExternalPayload->SizeData - QtdBytesToEndPage);
        }

        else
        {

            FlashWritePageData( AddsFirmware + QSPI_DEVICE_ADDRESS,
                                FlashExternalPayload->SizeData,
                                FlashExternalPayload->DataFlash);


            FlashExternalPayload->DataFlash += FlashExternalPayload->SizeData; //Incrementa o ponteiro contendo o dado

            AddsFirmware += FlashExternalPayload->SizeData;
            //TAMANHO RESTANTE PARA GRAVAR NA OUTRA PAGINA
            FlashExternalPayload->SizeData = (uint16_t)(FlashExternalPayload->SizeData - QtdBytesToEndPage);

            break;
        }

        NumPageToWrite++;
        NextSector = (uint16_t)(NumPageToWrite / NUMBER_PAGES_OF_SECTOR);
        //caso o setor encha na gravação pela troca de pagina
        if(ActualSector != NextSector)                 //TESTE PARA VER SE TROCOU DE SETOR E MANDA APAGAR
        {
            ClearSectorFlashQSPIFirm(NextSector);          //Limpa proximo setor da Flash
        }

    }

    FlashExternalPayload->DataFlash = 0;

    return AddsFirmware;
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao Escreve o conteúdo da memoria de um endereço específico
 *
 * @param uint32_t Address: 24bits de endereço
 * @param uint16_t Lenght: quantidade de bytes
 * @param uint8_t *DataFlash: Ponteiro contendo os dados da escrita
 *
 * @return Nenhum
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
bool FlashWritePageData(uint32_t Address, uint16_t Lenght, uint8_t *DataFlash)
{
    bool InProgress = true;
    ssp_err_t error = 0;

    tx_mutex_get(&QSPI_Mutex, TX_WAIT_FOREVER);
    error = g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);
    if(error)
    {
        tx_mutex_put(&QSPI_Mutex);
        return 1;
    }

    error = g_qspi0.p_api->pageProgram(g_qspi0.p_ctrl, (uint8_t *)Address, &DataFlash[0], (uint32_t)Lenght);
    if(error)
    {
        tx_mutex_put(&QSPI_Mutex);
        return 1;
    }

    do
    {
        g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &InProgress);
    }while (InProgress);

    error = g_qspi0.p_api->close(g_qspi0.p_ctrl);
    tx_mutex_put(&QSPI_Mutex);
    if(error)
    {
        return 1;
    }

    return 0;
}

/**
 * \brief Funcao que apaga um setor especifico da flash
 *
 *
 * \param uint16_t Sector -> Endereco do setor
 *
 * \return void
*/
void ClearSectorFlashQSPIFirm(uint16_t Sector)
{
    /* variables to store result/status */
    bool    in_progress = true;

    tx_mutex_get(&QSPI_Mutex, TX_WAIT_FOREVER);
    /* Open QSPI interface */
    g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);

    /* Erase first sector on QSPI flash memory */
    //test = g_qspi0.p_api->erase(g_qspi0.p_ctrl, (uint8_t *) QSPI_DEVICE_ADDRESS, FLASH_SIZE);SIZE_SECTOR
    g_qspi0.p_api->erase(g_qspi0.p_ctrl, (uint8_t *) (QSPI_DEVICE_ADDRESS+(SIZE_SECTOR*Sector)), SIZE_SECTOR);

    /* Wait until previous operation ends */
    do
    {
        g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &in_progress);
    } while (in_progress);

    /* Close QSPI interface */
    g_qspi0.p_api->close(g_qspi0.p_ctrl);
    tx_mutex_put(&QSPI_Mutex);
}


uint8_t CheckCRCFirmwareQSPI(void)
{
    uint16_t CalcCrc = 0;
    uint32_t start_address1 = (uint32_t)QSPI_DEVICE_ADDRESS;
    uint32_t    SizeMem;
    fl_image_header_t image_header;
    ssp_err_t error;

    tx_mutex_get(&QSPI_Mutex, TX_WAIT_FOREVER);

    g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);

    g_qspi0.p_api->read(g_qspi0.p_ctrl, (uint8_t *) (QSPI_DEVICE_ADDRESS + OFFSET_EXT_INFO_ADDRESS), (uint8_t *)&image_header, sizeof(image_header));


    error = g_crc0.p_api->open(g_crc0.p_ctrl,g_crc0.p_cfg);

    if(SSP_SUCCESS!=error)
    {
        g_qspi0.p_api->close(g_qspi0.p_ctrl);
        tx_mutex_put(&QSPI_Mutex);
        return 0;
    }

    SizeMem = (uint32_t)(APP_EXT_INFO_ADDRESS + offsetof(fl_image_header_t, raw_crc) - start_address1);
    /* Calculate CRC up to the location where the linker put the CRC value */
    error = g_crc0.p_api->calculate(g_crc0.p_ctrl, (uint8_t *)start_address1, (uint32_t)SizeMem, 0x1D0F, (uint32_t *)&CalcCrc); //1D0F

    if(SSP_SUCCESS!=error)
    {
        error = g_crc0.p_api->close(g_crc0.p_ctrl);
        g_qspi0.p_api->close(g_qspi0.p_ctrl);
        tx_mutex_put(&QSPI_Mutex);
        return 0;
    }

    /* Move start_address to right after 'raw_crc' */
    start_address1 = (uint32_t)APP_EXT_INFO_ADDRESS + offsetof(fl_image_header_t, raw_crc) + sizeof(image_header.raw_crc);

    SizeMem = (uint32_t)(APP_EXT_END  - start_address1);
    /* Calculate the rest of flash after the CRC in memory */
    error = g_crc0.p_api->calculate(g_crc0.p_ctrl, (uint8_t *)start_address1, (uint32_t)SizeMem, CalcCrc, (uint32_t *)&CalcCrc);

    if(SSP_SUCCESS!=error)
    {
        error = g_crc0.p_api->close(g_crc0.p_ctrl);
        g_qspi0.p_api->close(g_qspi0.p_ctrl);
        tx_mutex_put(&QSPI_Mutex);

        return 0;
    }

    error = g_crc0.p_api->close(g_crc0.p_ctrl);

    g_qspi0.p_api->close(g_qspi0.p_ctrl);
    tx_mutex_put(&QSPI_Mutex);

    if((CalcCrc == image_header.raw_crc) && (image_header.McuModel == MCU_MODEL))
    {
        uint8_t BootLoaderCode[4];
        BootLoaderCode[0] = ( BOOTLOADER_VALID_CODE         & 0xFF);
        BootLoaderCode[1] = ((BOOTLOADER_VALID_CODE >>  8)  & 0xFF);
        BootLoaderCode[2] = ((BOOTLOADER_VALID_CODE >> 16)  & 0xFF);
        BootLoaderCode[3] = ((BOOTLOADER_VALID_CODE >> 24)  & 0xFF);

        //Escreve numero magico na flash interna do micro. Sinaliza que vai atualizar firmware
        int_storage_write((uint8_t*)&BootLoaderCode[0], sizeof(uint32_t), FLASH_BOOTLOADER_CODE_ADDRRESS);
        return 0;
    }
    else
        return 1;
}

void FlashReadFirmwareData(uint32_t Address, uint16_t QntPag, uint8_t *DataFlash)
{
    bool InProgress = true;
    UINT ret;
    g_qspi0.p_api->close(g_qspi0.p_ctrl);

    ret = g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);
    if(ret != TX_SUCCESS)
        __BKPT(0);

    ret = g_qspi0.p_api->read(g_qspi0.p_ctrl, (uint8_t *) (QSPI_DEVICE_ADDRESS+Address) , DataFlash, (uint32_t)(BUFFER_LENGTH*QntPag));
    if(ret != TX_SUCCESS)
        __BKPT(0);
    do
    {
        ret = g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &InProgress);
        if(ret != TX_SUCCESS)
            __BKPT(0);
    } while (InProgress);

    g_qspi0.p_api->close(g_qspi0.p_ctrl);
}


/**
 * \brief Funcao que apaga a flash externa, parte onde fica o firmware salvo
 *
 *
 * \param uint16_t Sector -> Endereco do setor
 *
 * \return void
*/
void ClearFirmwareFlashQSPI(void)
{
    /* variables to store result/status */
    bool    in_progress = true;

    /* Open QSPI interface */
    g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);

    /* Erase first sector on QSPI flash memory */
    for(uint16_t i=0; i<256; i++)
    {
        g_qspi0.p_api->erase(g_qspi0.p_ctrl, (uint8_t *) QSPI_DEVICE_ADDRESS+(SIZE_SECTOR*i), SIZE_SECTOR);
        /* Wait until previous operation ends */
        do
        {
            g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &in_progress);
        } while (in_progress);
    }

    /* Close QSPI interface */
    g_qspi0.p_api->close(g_qspi0.p_ctrl);
}
