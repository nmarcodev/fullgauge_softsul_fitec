/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/
#include "usb_hid_device_thread.h"
#include "Includes/Define_IOs.h"
#include "Includes/serial485_entry.h"
#include "Includes/ApplicationUSB.h"

extern void g_ux_device_class_register_hid(void);

UX_SLAVE_DEVICE*    device;
UX_SLAVE_INTERFACE* interface;
UX_SLAVE_CLASS_HID* hid;

UX_SLAVE_CLASS_HID_EVENT hid_event;


_USB_RETURN SendPacketUSB(_USB_CONTROL *USBData, uint16_t len)
{
	if(device->ux_slave_device_state == UX_DEVICE_CONFIGURED)
	{
		/* Get the interface.  We use the first interface, this is a simple device.  */
		interface =  device->ux_slave_device_first_interface;

		/* Form that interface, derive the HID owner.  */
		hid = interface->ux_slave_interface_class_instance;

		tx_thread_sleep(20);

		/* Then insert a key into the keyboard event.  Length is fixed to 8.  */
		hid_event.ux_device_class_hid_event_length = 4;

		memcpy(&hid_event.ux_device_class_hid_event_buffer[0], &USBData->BufferTx[0], len);
		//R_BSP_SoftwareDelay(5000U, BSP_DELAY_UNITS_MILLISECONDS);
		ux_device_class_hid_event_set(hid, &hid_event);

		piscaLed( LED_ST_B,  500U);

		print485("Enviando pacote via USB.\r\n", LEN("Enviando pacote via USB.\r\n") );

		return USB_SEND;
	}
	else return USB_RELISTEN;



}

/* HID Keyboard Device Thread entry function */
void usb_hid_device_thread_entry(void)
{
    g_ux_device_class_register_hid();
    /* Get the pointer to the device.  */
    device =  &_ux_system_slave->ux_system_slave_device;

    /* reset the HID event structure.  */
    ux_utility_memory_set(&hid_event, 0, sizeof(UX_SLAVE_CLASS_HID_EVENT));

    print485("Inicializando USB.\r\n", LEN("Inicializando USB.\r\n") );

    while (1)
    {
        /* Is the device configured ? */
        while (device->ux_slave_device_state != UX_DEVICE_CONFIGURED)
        {
            /* Then wait.  */
            tx_thread_sleep(10);
        }

        /* Until the device stays configured.  */
//        while (device->ux_slave_device_state == UX_DEVICE_CONFIGURED)
//        {
//
//            /* Get the interface.  We use the first interface, this is a simple device.  */
//            interface =  device->ux_slave_device_first_interface;
//
//            /* Form that interface, derive the HID owner.  */
//            hid = interface->ux_slave_interface_class_instance;
//
//            tx_thread_sleep(20);
//
//            /* Then insert a key into the keyboard event.  Length is fixed to 8.  */
//            hid_event.ux_device_class_hid_event_length = 4;
//
//            /* First byte is a modifier byte.  */
//            hid_event.ux_device_class_hid_event_buffer[0] = g_sent;
//            hid_event.ux_device_class_hid_event_buffer[1] = g_sent;
//            hid_event.ux_device_class_hid_event_buffer[2] = g_sent;
//            hid_event.ux_device_class_hid_event_buffer[3] = g_sent;
//
//            /* Set the keyboard event.  */
//            ux_device_class_hid_event_set(hid, &hid_event);
//
//            g_sent++;
//            piscaLed( LED_ST_B,  500U);
//
//            print485("Enviando pacote via USB.\r\n", LEN("Enviando pacote via USB.\r\n") );
//        }
    }

}

void piscaLed(uint16_t LED, uint16_t Delay)
{
    R_BSP_SoftwareDelay(Delay, BSP_DELAY_UNITS_MILLISECONDS);
    g_ioport.p_api->pinWrite(LED, IOPORT_LEVEL_LOW);
    R_BSP_SoftwareDelay(Delay, BSP_DELAY_UNITS_MILLISECONDS);
    g_ioport.p_api->pinWrite(LED, IOPORT_LEVEL_HIGH);
}



