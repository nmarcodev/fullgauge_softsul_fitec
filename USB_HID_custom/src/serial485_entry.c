/*
 * serial_entry.c
 *
 *  Created on: 12 de mar de 2021
 *      Author: JJJunior
 */

#include "Includes/Define_IOs.h"
#include "hal_data.h"
#include "Includes/serial485_entry.h"

#define RX_BUFFER_LEN 256


char rxBuffer485[RX_BUFFER_LEN];
uint8_t index485 = 0;
uart_event_t uart_event485 = UART_EVENT_RX_CHAR;

volatile bool send_finish_status = false;
volatile bool receive_finish_status = false;

void UserUart0Callback(uart_callback_args_t * p_args)
{
    __NOP();
    if (2U == p_args->channel)
    {
        switch (p_args->event)
        {
            case UART_EVENT_RX_COMPLETE:
                receive_finish_status = true;
                break;
            case UART_EVENT_TX_COMPLETE:
                send_finish_status = true;
                g_ioport.p_api->pinWrite(UART0_TX_DIR,IOPORT_LEVEL_LOW);
                break;
            case UART_EVENT_ERR_OVERFLOW:
                break;
            case UART_EVENT_RX_CHAR:
                __NOP();
                break;

            default:
                break;
        }
    }
}



void print485(char * mensagem, uint8_t mensagemLen)
{
    g_uart0.p_api->open(g_uart0.p_ctrl, g_uart0.p_cfg);

    g_ioport.p_api->pinWrite(UART0_TX_DIR,IOPORT_LEVEL_HIGH);

    g_uart0.p_api->write(g_uart0.p_ctrl,
                                 (uint8_t *)(&mensagem[0]),
                                 mensagemLen);
    R_BSP_SoftwareDelay(100U, BSP_DELAY_UNITS_MILLISECONDS);

    g_uart0.p_api->close(g_uart0.p_ctrl);
}


