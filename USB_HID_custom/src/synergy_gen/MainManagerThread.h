/* generated thread header file - do not edit */
#ifndef MAINMANAGERTHREAD_H_
#define MAINMANAGERTHREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void MainManagerThread_entry(void);
#else
extern void MainManagerThread_entry(void);
#endif
#include "r_flash_hp.h"
#include "r_flash_api.h"
#include "r_gpt.h"
#include "r_timer_api.h"
#ifdef __cplusplus
extern "C" {
#endif
/* Flash on Flash HP Instance */
extern const flash_instance_t g_int_storageInst;
#ifndef flash_callback
void flash_callback(flash_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer0;
#ifndef Timer_IO_Callback
void Timer_IO_Callback(timer_callback_args_t *p_args);
#endif
extern TX_EVENT_FLAGS_GROUP g_main_event_flags;
extern TX_EVENT_FLAGS_GROUP g_int_storage_evt_grp;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* MAINMANAGERTHREAD_H_ */
