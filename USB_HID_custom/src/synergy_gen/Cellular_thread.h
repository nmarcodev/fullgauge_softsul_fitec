/* generated thread header file - do not edit */
#ifndef CELLULAR_THREAD_H_
#define CELLULAR_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void Cellular_thread_entry(void);
#else
extern void Cellular_thread_entry(void);
#endif
#ifdef __cplusplus
extern "C" {
#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* CELLULAR_THREAD_H_ */
