/* generated common header file - do not edit */
#ifndef COMMON_DATA_H_
#define COMMON_DATA_H_
#include <stdint.h>
#include "bsp_api.h"
#include "sf_message.h"
#include "sf_message_payloads.h"
#include "r_cgc.h"
#include "r_cgc_api.h"
#include "r_ioport.h"
#include "r_ioport_api.h"
#include "r_fmi.h"
#include "r_fmi_api.h"
#include "r_elc.h"
#include "r_elc_api.h"
#ifdef __cplusplus
extern "C" {
#endif
extern void g_message_init(void);

/* SF Message on SF Message Instance. */
extern const sf_message_instance_t g_sf_message0;
void g_sf_message0_err_callback(void *p_instance, void *p_data);
void sf_message_init0(void);

#include "ux_api.h"

/* USBX Host Stack initialization error callback function. User can override the function if needed. */
void ux_v2_err_callback(void *p_instance, void *p_data);

#if !defined(NULL)
/* User Callback for Host Event Notification (Only valid for USB Host). */
extern UINT NULL(ULONG event, UX_HOST_CLASS *host_class, VOID *instance);
#endif

#if !defined(NULL)
/* User Callback for Device Event Notification (Only valid for USB Device). */
extern UINT NULL(ULONG event);
#endif

#ifdef UX_HOST_CLASS_STORAGE_H
/* Utility function to get the pointer to a FileX Media Control Block for a USB Mass Storage device. */
UINT ux_system_host_storage_fx_media_get(UX_HOST_CLASS_STORAGE * instance, UX_HOST_CLASS_STORAGE_MEDIA ** p_storage_media, FX_MEDIA ** p_fx_media);
#endif
void ux_common_init0(void);
#include "ux_api.h"
#include "ux_dcd_synergy.h"
#include "sf_el_ux_dcd_fs_cfg.h"
void g_sf_el_ux_dcd_fs_0_err_callback(void *p_instance, void *p_data);
#include "ux_api.h"
#include "ux_dcd_synergy.h"

/* USBX Device Stack initialization error callback function. User can override the function if needed. */
void ux_device_err_callback(void *p_instance, void *p_data);
void ux_device_init0(void);
void ux_device_remove_compiler_padding(unsigned char *p_device_framework,
		UINT length);
#include "ux_api.h"
#include "ux_device_class_hid.h"
/* USBX Device Class HID Entry Function. */
extern UINT ux_device_class_hid_entry(UX_SLAVE_CLASS_COMMAND *command);

/* Function for USBX Device Class Keyboard HID Instance Activate/Deactivate Function Alias. */
#ifndef NULL
extern VOID NULL(VOID *hid_instance);
#endif
#ifndef NULL
extern VOID NULL(VOID *hid_instance);
#endif	
/* Function for USBX Device Class Mouse HID Instance Activate/Deactivate Function Alias. */
#ifndef NULL
extern VOID NULL(VOID *hid_instance);
#endif
#ifndef NULL
extern VOID NULL(VOID *hid_instance);
#endif
#ifndef ux_hid_device_callback
extern UINT ux_hid_device_callback(UX_SLAVE_CLASS_HID *hid,
		UX_SLAVE_CLASS_HID_EVENT *hid_event);
#endif
#ifndef NULL
extern UINT NULL(UX_SLAVE_CLASS_HID *hid, UX_SLAVE_CLASS_HID_EVENT *hid_event);
#endif
void ux_device_class_hid_init0(void);
/** CGC Instance */
extern const cgc_instance_t g_cgc;
/** IOPORT Instance */
extern const ioport_instance_t g_ioport;
/** FMI on FMI Instance. */
extern const fmi_instance_t g_fmi;
/** ELC Instance */
extern const elc_instance_t g_elc;
void g_common_init(void);
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* COMMON_DATA_H_ */
