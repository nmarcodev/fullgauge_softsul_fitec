/* generated common source file - do not edit */
#include "common_data.h"

#if defined(__ICCARM__)
#define g_sf_message0_err_callback_WEAK_ATTRIBUTE
#pragma weak g_sf_message0_err_callback  = g_sf_message0_err_callback_internal
#elif defined(__GNUC__)
#define g_sf_message0_err_callback_WEAK_ATTRIBUTE   __attribute__ ((weak, alias("g_sf_message0_err_callback_internal")))
#endif
void g_sf_message0_err_callback(void *p_instance, void *p_data)
g_sf_message0_err_callback_WEAK_ATTRIBUTE;
extern sf_message_subscriber_list_t *p_subscriber_lists[];
sf_message_instance_ctrl_t g_sf_message0_ctrl;
static uint8_t g_sf_message0_work_buffer[8192];
/* Configures the messaging framework */
sf_message_cfg_t g_sf_message0_cfg = { .p_work_memory_start =
		&g_sf_message0_work_buffer, .work_memory_size_bytes = 8192,
		.buffer_size = sizeof(sf_message_payload_t), .pp_subscriber_lists =
				p_subscriber_lists, .p_block_pool_name =
				(uint8_t *) "sf_msg_blk_pool" };
/* Instance structure to use this module. */
const sf_message_instance_t g_sf_message0 = { .p_ctrl = &g_sf_message0_ctrl,
		.p_cfg = &g_sf_message0_cfg, .p_api = &g_sf_message_on_sf_message };
/*******************************************************************************************************************//**
 * @brief      This is a weak example initialization error function.  It should be overridden by defining a user  function 
 *             with the prototype below.
 *             - void g_sf_message0_err_callback(void * p_instance, void * p_data)
 *
 * @param[in]  p_instance arguments used to identify which instance caused the error and p_data Callback arguments used to identify what error caused the callback.
 **********************************************************************************************************************/
void g_sf_message0_err_callback_internal(void *p_instance, void *p_data);
void g_sf_message0_err_callback_internal(void *p_instance, void *p_data) {
	/** Suppress compiler warning for not using parameters. */
	SSP_PARAMETER_NOT_USED(p_instance);
	SSP_PARAMETER_NOT_USED(p_data);

	/** An error has occurred. Please check function arguments for more information. */
	BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
}
/*******************************************************************************************************************//**
 * @brief     Initialization function that the user can choose to have called automatically during thread entry.
 *            The user can call this function at a later time if desired using the prototype below.

 *            - void sf_message_init0(void)
 **********************************************************************************************************************/
void sf_message_init0(void) {
	ssp_err_t ssp_err_g_sf_message0;

	/* Initializes Messaging Framework Queues */
	g_message_init();

	/* Opens the messaging framework */
	ssp_err_g_sf_message0 = g_sf_message0.p_api->open(g_sf_message0.p_ctrl,
			g_sf_message0.p_cfg);
	if (SSP_SUCCESS != ssp_err_g_sf_message0) {
		/* Error returns, check the cause. */
		g_sf_message0_err_callback((void *) &g_sf_message0,
				&ssp_err_g_sf_message0);
	}
}
/***********************************************************************************************************************
 * USB HID Descriotor definitions
 **********************************************************************************************************************/
/* Define the size of HID Descriptor to be used */
#ifndef USB_HID_DESCRIPTOR_TYPE
#define USB_HID_DESCRIPTOR_TYPE      (0x00) 
#endif
#if (USB_HID_DESCRIPTOR_TYPE == 0)
#define USB_HID_DESCRIPTOR_LENGTH    (0x0)
#elif (USB_HID_DESCRIPTOR_TYPE == 1)
#define USB_HID_DESCRIPTOR_LENGTH    (0x3F)   /* HID Descriptor for Keyboard Device */
#elif (USB_HID_DESCRIPTOR_TYPE == 2)
#define USB_HID_DESCRIPTOR_LENGTH    (0x32)   /* HID Descriptor for Mouse Device */
#elif (USB_HID_DESCRIPTOR_TYPE == 3)
#define USB_HID_DESCRIPTOR_LENGTH_KEYBOARD    (0x3F )   /* HID Descriptor for Keyboard + Mouse Device */
#define USB_HID_DESCRIPTOR_LENGTH_MOUSE       (0x32 )   /* HID Descriptor for Keyboard + Mouse Device */
#else
#define USB_HID_DESCRIPTOR_LENGTH    (0xFF)
#endif

#if (0) || (0)
#define USB_HID_DESCRIPTOR_NUM_OF_EP (2)   /* If enabled Interrupt Out endpoint */
#else
#define USB_HID_DESCRIPTOR_NUM_OF_EP (1)
#endif

/***********************************************************************************************************************
 * USB HID Interface Descriptor for FS mode g_usb_interface_descriptor_hid0
 **********************************************************************************************************************/
#if defined(__GNUC__)
static volatile const unsigned char g_usb_interface_descriptor_hid0_full_speed[] BSP_PLACE_IN_SECTION_V2(".usb_interface_desc_fs") BSP_ALIGN_VARIABLE_V2(1)
#else /* __ICCARM__ */
#pragma section = ".usb_interface_desc_fs" 1
__root static const unsigned char g_usb_interface_descriptor_hid0_full_speed[] BSP_PLACE_IN_SECTION_V2(".usb_interface_desc_fs")
#endif
= {
#if (USB_HID_DESCRIPTOR_TYPE == 1) || (USB_HID_DESCRIPTOR_TYPE == 3)

	/****************************************************************
	 * HID Class Keyboard Interface Descriptor Requirement 9 bytes           *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_INTERFACE_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x00, /* 2 bInterfaceNumber */
	0x00, /* 4 bAlternateSetting  */
#if (0)
	0x02, /* 5 bNumEndpoints      */
#else
	0x01, /* 5 bNumEndpoints      */
#endif
	UX_DEVICE_CLASS_HID_CLASS, /* 6 bInterfaceClass    : HID Class(0x3)  */
	UX_DEVICE_CLASS_HID_SUBCLASS, /* 7 bInterfaceSubClass : Not support boot interface FIXME */
	0x01, /* 8 bInterfaceProtocol : Protocol code(None(0)/Keyboard(1)/Mouse(2)/Keyboard+Mouse(3)) */
	0x00, /* 9 iInterface Index   */
	/****************************************************************
	 * HID Descriptor                                               *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_HID, /* 1 bDescriptorType : HID descriptor (0x21) */
	0x10, /* 2 bcdHID 0x0110 == 1.10 */
	0x01, /* 3 bcdHID  */
	0x21, /* 4 bCountryCode : Hardware target country */
	USB_HID_DESCRIPTOR_NUM_OF_EP, /* 5 bNumDescriptors */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_REPORT, /* 6 bDescriptorType (Report descriptor type) */
#if (USB_HID_DESCRIPTOR_TYPE == 1)				
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH >> 8), /* 8 wItemLength */
#else			
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_KEYBOARD), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_KEYBOARD >> 8), /* 8 wItemLength */
#endif				
	/****************************************************************
	 * Endpoint descriptor (Interrupt-In)                           *
	 ****************************************************************/
	0x7, /* 0 bLength */
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_IN | 1), /* 2 bEndpointAddress */
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes  */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#if (0)
	/****************************************************************
	 * Endpoint descriptor (Interrupt-Out)                          *
	 ****************************************************************/
	0x7, /* 0 bLength */
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_OUT | 3), /* 2 bEndpointAddress */
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#else
#endif
#endif

#if (USB_HID_DESCRIPTOR_TYPE == 2) || (USB_HID_DESCRIPTOR_TYPE == 3)
	/****************************************************************
	 * HID Class Mouse Interface Descriptor Requirement 9 bytes           *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_INTERFACE_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x01, /* 2 bInterfaceNumber  */
	0x00, /* 4 bAlternateSetting  */
#if (0)
	0x02, /* 5 bNumEndpoints      */
#else
	0x01, /* 5 bNumEndpoints      */
#endif
	UX_DEVICE_CLASS_HID_CLASS, /* 6 bInterfaceClass    : HID Class(0x3)  */
	UX_DEVICE_CLASS_HID_SUBCLASS, /* 7 bInterfaceSubClass : Not support boot interface FIXME */
	0x02, /* 8 bInterfaceProtocol : Protocol code(None(0)/Keyboard(1)/Mouse(2)/Keyboard+Mouse(3)) */
	0x00, /* 9 iInterface Index   */
	/****************************************************************
	 * HID Descriptor                                               *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_HID, /* 1 bDescriptorType : HID descriptor (0x21) */
	0x10, /* 2 bcdHID 0x0110 == 1.10 */
	0x01, /* 3 bcdHID  */
	0x21, /* 4 bCountryCode : Hardware target country */
	USB_HID_DESCRIPTOR_NUM_OF_EP, /* 5 bNumDescriptors */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_REPORT, /* 6 bDescriptorType (Report descriptor type) */
#if (USB_HID_DESCRIPTOR_TYPE == 2)				
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH >> 8), /* 8 wItemLength */
#else			
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_MOUSE), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_MOUSE >> 8), /* 8 wItemLength */
#endif
	/****************************************************************
	 * Endpoint descriptor (Interrupt-In)                           *
	 ****************************************************************/
	0x7, /* 0 bLength */
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_IN | 2), /* 2 bEndpointAddress */
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes  */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#if (0)
	/****************************************************************
	 * Endpoint descriptor (Interrupt-Out)                          *
	 ****************************************************************/
	0x7, /* 0 bLength */
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_OUT | 4), /* 2 bEndpointAddress */
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#else
#endif
#endif

};

#if defined (UX_DCD_SYNERY_USE_USBHS)
/***********************************************************************************************************************
 * USB HID Interface Descriptor for HS mode g_usb_interface_descriptor_hid0
 **********************************************************************************************************************/
#if defined(__GNUC__)
static volatile const unsigned char g_usb_interface_descriptor_hid0_high_speed[] BSP_PLACE_IN_SECTION_V2(".usb_interface_desc_hs") BSP_ALIGN_VARIABLE_V2(1)
#else /* __ICCARM__ */
#pragma section = ".usb_interface_desc_hs" 1
__root static const unsigned char g_usb_interface_descriptor_hid0_high_speed[] BSP_PLACE_IN_SECTION_V2(".usb_interface_desc_hs")
#endif
= {
#if (USB_HID_DESCRIPTOR_TYPE == 1) || (USB_HID_DESCRIPTOR_TYPE == 3)

	/****************************************************************
	 * HID Class Keyboard Interface Descriptor Requirement 9 bytes           *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_INTERFACE_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x00, /* 2 bInterfaceNumber */
	0x00, /* 4 bAlternateSetting  */
#if (0)
	0x02, /* 5 bNumEndpoints      */
#else
	0x01, /* 5 bNumEndpoints      */
#endif
	UX_DEVICE_CLASS_HID_CLASS, /* 6 bInterfaceClass    : HID Class(0x3)  */
	UX_DEVICE_CLASS_HID_SUBCLASS, /* 7 bInterfaceSubClass : Not support boot interface FIXME */
	0x01, /* 8 bInterfaceProtocol : Protocol code(None(0)/Keyboard(1)/Mouse(2)/Keyboard+Mouse(3)) */
	0x00, /* 9 iInterface Index   */
	/****************************************************************
	 * HID Descriptor                                               *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_HID, /* 1 bDescriptorType : HID descriptor (0x21) */
	0x10, /* 2 bcdHID 0x0110 == 1.10 */
	0x01, /* 3 bcdHID  */
	0x21, /* 4 bCountryCode : Hardware target country */
	USB_HID_DESCRIPTOR_NUM_OF_EP, /* 5 bNumDescriptors */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_REPORT, /* 6 bDescriptorType (Report descriptor type) */
#if (USB_HID_DESCRIPTOR_TYPE == 1)				
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH >> 8), /* 8 wItemLength */
#else			
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_KEYBOARD), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_KEYBOARD >> 8), /* 8 wItemLength */
#endif				
	/****************************************************************
	 * Endpoint descriptor (Interrupt-In)                           *
	 ****************************************************************/
	0x7, /* 0 bLength */
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_IN | 1), /* 2 bEndpointAddress */
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes  */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#if (0)
	/****************************************************************
	 * Endpoint descriptor (Interrupt-Out)                          *
	 ****************************************************************/
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_OUT | 3), /* 2 bEndpointAddress */
	UX_BULK_ENDPOINT,
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#else
#endif
#endif

#if (USB_HID_DESCRIPTOR_TYPE == 2) || (USB_HID_DESCRIPTOR_TYPE == 3)
	/****************************************************************
	 * HID Class Mouse Interface Descriptor Requirement 9 bytes           *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_INTERFACE_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x01, /* 2 bInterfaceNumber  */
	0x00, /* 4 bAlternateSetting  */
#if (0)
	0x02, /* 5 bNumEndpoints      */
#else
	0x01, /* 5 bNumEndpoints      */
#endif
	UX_DEVICE_CLASS_HID_CLASS, /* 6 bInterfaceClass    : HID Class(0x3)  */
	UX_DEVICE_CLASS_HID_SUBCLASS, /* 7 bInterfaceSubClass : Not support boot interface FIXME */
	0x02, /* 8 bInterfaceProtocol : Protocol code(None(0)/Keyboard(1)/Mouse(2)/Keyboard+Mouse(3)) */
	0x00, /* 9 iInterface Index   */
	/****************************************************************
	 * HID Descriptor                                               *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_HID, /* 1 bDescriptorType : HID descriptor (0x21) */
	0x10, /* 2 bcdHID 0x0110 == 1.10 */
	0x01, /* 3 bcdHID  */
	0x21, /* 4 bCountryCode : Hardware target country */
	USB_HID_DESCRIPTOR_NUM_OF_EP, /* 5 bNumDescriptors */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_REPORT, /* 6 bDescriptorType (Report descriptor type) */
#if (USB_HID_DESCRIPTOR_TYPE == 2)				
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH >> 8), /* 8 wItemLength */
#else			
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_MOUSE), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH_MOUSE >> 8), /* 8 wItemLength */
#endif
	/****************************************************************
	 * Endpoint descriptor (Interrupt-In)                           *
	 ****************************************************************/
	0x7, /* 0 bLength */
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_IN | 2), /* 2 bEndpointAddress */
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes  */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#if (0)
	/****************************************************************
	 * Endpoint descriptor (Interrupt-Out)                          *
	 ****************************************************************/
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_OUT | 4), /* 2 bEndpointAddress */
	UX_BULK_ENDPOINT,
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
#else
#endif
#endif	
};
#endif

/* Size of this USB Interface Descriptor */
#define USB_IFD_SIZE_0x00       (sizeof(g_usb_interface_descriptor_hid0_full_speed))
/* Number of Interface this USB Interface Descriptor has */
#if (0) || (0) || (USB_HID_DESCRIPTOR_TYPE == 3)
#define USB_IFD_NUM_0x00        (2)
#else
#define USB_IFD_NUM_0x00        (1)
#endif
#ifndef USB_IFD_NUM_0
#define USB_IFD_NUM_0  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_0 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_1
#define USB_IFD_NUM_1  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_1 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_2
#define USB_IFD_NUM_2  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_2 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_3
#define USB_IFD_NUM_3  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_3 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_4
#define USB_IFD_NUM_4  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_4 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_5
#define USB_IFD_NUM_5  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_5 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_6
#define USB_IFD_NUM_6  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_6 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_7
#define USB_IFD_NUM_7  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_7 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_8
#define USB_IFD_NUM_8  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_8 USB_IFD_SIZE_0x00
#else
#ifndef USB_IFD_NUM_9
#define USB_IFD_NUM_9  USB_IFD_NUM_0x00
#define USB_IFD_SIZE_9 USB_IFD_SIZE_0x00
#endif 
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
/***********************************************************************************************************************
 * USB HID Report Descriptor
 **********************************************************************************************************************/

#if (USB_HID_DESCRIPTOR_TYPE == 1) || (USB_HID_DESCRIPTOR_TYPE == 3) 
const UCHAR g_hid_report_descriptor_keyboard[] =
{
	0x05, 0x01, /* Usage Page (Generic Desktop)                    */
	0x09, 0x06, /* Usage (Keyboard)                                */
	0xA1, 0x01, /* Collection (Application)                        */
	0x05, 0x07, /* Usage Page (Key Codes)                          */
	0x19, 0xE0, /* Usage Minimum (Keyboard LeftControl)            */
	0x29, 0xE7, /* Usage Maximun (Keyboard Right GUI)              */
	0x15, 0x00, /* Logical Minimum (0)                             */
	0x25, 0x01, /* Logical Maximum (1)                             */
	0x75, 0x01, /* Report Size (1)                                 */
	0x95, 0x08, /* Report Count (8)                                */
	0x81, 0x02, /* Input (Data, Variable, Absolute); Modifier byte */
	0x95, 0x01, /* Report Count (1)                                */
	0x75, 0x08, /* Report Size (8)                                 */
	0x81, 0x03, /* Input (Constant); Reserved byte                 */
	0x95, 0x05, /* Report Count (5)                                */
	0x75, 0x01, /* Report Size (1)                                 */
	0x05, 0x08, /* Usage Page (Page# for LEDs)                     */
	0x19, 0x01, /* Usage Minimum (1)                               */
	0x29, 0x05, /* Usage Maximum (5)                               */
	0x91, 0x02, /* Output (Data, Variable, Absolute); LED report   */
	0x95, 0x01, /* Report Count (1)                                */
	0x75, 0x03, /* Report Size (3)                                 */
	0x91, 0x03, /* Output (Constant) ;LED report padding           */
	0x95, 0x06, /* Report Count (6)                                */
	0x75, 0x08, /* Report Size (8)                                 */
	0x15, 0x00, /* Logical Minimum (0)                             */
	0x25, 0x65, /* Logical Maximum(101)                            */
	0x05, 0x07, /* Usage Page (Key Codes)                          */
	0x19, 0x00, /* Usage Minimum (0)                               */
	0x29, 0x65, /* Usage Maximum (101)                             */
	0x81, 0x00, /* Input (Data, Array); Key arrays (6 bytes)       */
	0xc0 /* End Collection                                  */
};
#else
const UCHAR g_hid_report_descriptor_keyboard[] = { 0 };
#endif
#if (USB_HID_DESCRIPTOR_TYPE == 2) || (USB_HID_DESCRIPTOR_TYPE == 3)
const UCHAR g_hid_report_descriptor_mouse[] =
{
	0x05, 0x01, /* Usage Page (Generic Desktop)                    */
	0x09, 0x02, /* Usage (Mouse)                                   */
	0xA1, 0x01, /* Collection (Application)                        */
	0x09, 0x01, /* Usage (Pointer)                                 */
	0xA1, 0x00, /* Collection (Physical)                           */
	0x05, 0x09, /* Usage Page (Buttons)                            */
	0x19, 0x01, /* Usage Minimum (01)                              */
	0x29, 0x03, /* Usage Maximun (03)                              */
	0x15, 0x00, /* Logical Minimum (0)                             */
	0x25, 0x01, /* Logical Maximum (1)                             */
	0x95, 0x03, /* Report Count (3)                                */
	0x75, 0x01, /* Report Size (1)                                 */
	0x81, 0x02, /* Input (Data, Variable, Absolute); 3 button bits */
	0x95, 0x01, /* Report Count (1)                                */
	0x75, 0x05, /* Report Size (5)                                 */
	0x81, 0x01, /* Input (Constant), ;5 bit padding                */
	0x05, 0x01, /* Usage Page (Generic Desktop)                    */
	0x09, 0x30, /* Usage (X)                                       */
	0x09, 0x31, /* Usage (Y)                                       */
	0x15, 0x81, /* Logical Minimum (-127)                          */
	0x25, 0x7F, /* Logical Maximum (127)                           */
	0x75, 0x08, /* Report Size (8)                                 */
	0x95, 0x02, /* Report Count (2)                                */
	0x81, 0x06, /* Input (Data, Variable, Relative); 2position bytes(X & Y) */
	0xC0, /* End Collection                                  */
	0xC0, /* End Collection                                  */
};
#else
const UCHAR g_hid_report_descriptor_mouse[] = { 0 };
#endif
#if defined(__ICCARM__)
#define ux_v2_err_callback_WEAK_ATTRIBUTE
#pragma weak ux_v2_err_callback  = ux_v2_err_callback_internal
#elif defined(__GNUC__)
#define ux_v2_err_callback_WEAK_ATTRIBUTE   __attribute__ ((weak, alias("ux_v2_err_callback_internal")))
#endif
void ux_v2_err_callback(void *p_instance, void *p_data)
ux_v2_err_callback_WEAK_ATTRIBUTE;
/* Static memory pool allocation used by USBX system. */
static CHAR g_ux_pool_memory[18432];
/*******************************************************************************************************************//**
 * @brief      This is a weak example initialization error function.  It should be overridden by defining a user  function 
 *             with the prototype below.
 *             - void ux_v2_err_callback(void * p_instance, void * p_data)
 *
 * @param[in]  p_instance arguments used to identify which instance caused the error and p_data Callback arguments used to identify what error caused the callback.
 **********************************************************************************************************************/
void ux_v2_err_callback_internal(void *p_instance, void *p_data);
void ux_v2_err_callback_internal(void *p_instance, void *p_data) {
	/** Suppress compiler warning for not using parameters. */
	SSP_PARAMETER_NOT_USED(p_instance);
	SSP_PARAMETER_NOT_USED(p_data);

	/** An error has occurred. Please check function arguments for more information. */
	BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
}

#ifdef UX_HOST_INITIALIZE
/* Function prototype for the function to notify a USB event from the USBX Host system. */
static UINT ux_system_host_change_function(ULONG event, UX_HOST_CLASS * host_class, VOID * instance);

#ifdef USB_HOST_STORAGE_CLASS_REGISTER

/* A semaphore for synchronizing to the USB media storage mount timing. */
static TX_SEMAPHORE ux_host_storage_semaphore_insertion;

/* Temporary storage place for the pointer to a USBX Host Mass Storage Class Instance. */
static UX_HOST_CLASS_STORAGE * g_ux_new_host_storage_instance;

/* Function prototype for the function to register the USBX Host Class Mass Storage. */
static void ux_host_stack_class_register_storage(void);

/* Function prototype for the function to notify changes happened to the USBX Host Class Mass Storage. */
static UINT ux_system_host_storage_change_function(ULONG event, VOID * instance);

/*******************************************************************************************************************//**
 * @brief      This is the function to register the USBX Host Class Mass Storage.
 **********************************************************************************************************************/
static void ux_host_stack_class_register_storage(void)
{
	UINT status;
	status = ux_host_stack_class_register(_ux_system_host_class_storage_name, ux_host_class_storage_entry);
	if (UX_SUCCESS != status)
	{
		ux_v2_err_callback(NULL, &status);
	}

	/* Create semaphores used for the USB Mass Storage Media Initialization. */
	status = tx_semaphore_create(&ux_host_storage_semaphore_insertion, "ux_host_storage_semaphore_insertion", 0);
	if (TX_SUCCESS != status)
	{
		ux_v2_err_callback(NULL, &status);
	}
}

/*******************************************************************************************************************//**
 * @brief      This is the function to notify changes happened to the USBX Host Class Mass Storage.
 * @param[in]  event      Event code from happened to the USBX Host system.
 * @param[in]  instance   Pointer to a USBX Host class instance, which occurs a event.
 * @retval     USBX error code
 **********************************************************************************************************************/
static UINT ux_system_host_storage_change_function(ULONG event, VOID * instance)
{
	if (UX_DEVICE_INSERTION == event) /* Check if there is a device insertion. */
	{
		g_ux_new_host_storage_instance = instance;

		/* Put the semaphore for a USBX Mass Storage Media insertion. */
		tx_semaphore_put (&ux_host_storage_semaphore_insertion);
	}
	else if (UX_DEVICE_REMOVAL == event) /* Check if there is a device removal. */
	{
		g_ux_new_host_storage_instance = NULL;
	}
	return UX_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief      This is the function to get the FileX Media Control Block for a USB Mass Storage device.
 * @param[in]      new_instance     Pointer to a USBX Host Mass Storage Class instance.
 * @param[in/out]  p_storage_media  Pointer of the pointer to save an instance of the USBX Host Mass Storage Media.
 * @param[in/out]  p_fx_media       Pointer of the pointer to save an instance of FileX Media Control Block.
 * @retval     UX_HOST_CLASS_INSTANCE_UNKNOWN  The instance provided was not for a valid Mass Storage device.
 **********************************************************************************************************************/
UINT ux_system_host_storage_fx_media_get(UX_HOST_CLASS_STORAGE * instance, UX_HOST_CLASS_STORAGE_MEDIA ** p_storage_media,
		FX_MEDIA ** p_fx_media)
{
	UINT error = UX_SUCCESS;
	UX_HOST_CLASS * p_host_class;
	UX_HOST_CLASS_STORAGE_MEDIA * p_storage_media_local;
	FX_MEDIA * p_fx_media_local;
	int index;

	/** Get the USBX Host Mass Storage Class. */
	ux_host_stack_class_get(_ux_system_host_class_storage_name, &p_host_class);

	/** Get the pointer to a media attached to the class container for USB Host Mass Storage. */
	p_storage_media_local = (UX_HOST_CLASS_STORAGE_MEDIA *) p_host_class->ux_host_class_media;

	/** Get the pointer to a FileX Media Control Block. */
	for (index = 0; index < UX_HOST_CLASS_STORAGE_MAX_MEDIA; index++)
	{
		p_fx_media_local = &p_storage_media_local->ux_host_class_storage_media;
		if (p_fx_media_local->fx_media_driver_info != instance)
		{
			/* None of FileX Media Control Blocks matched to this USBX Host Mass Storage Instance. */
			p_storage_media_local++;
		}
		else
		{
			/* Found a FileX Media Control Block used for this USBX Host Mass Storage Instance. */
			break;
		}
	}
	if (UX_HOST_CLASS_STORAGE_MAX_MEDIA == index)
	{
		error = UX_HOST_CLASS_INSTANCE_UNKNOWN;
	}
	else
	{
		*p_storage_media = p_storage_media_local;
		*p_fx_media = p_fx_media_local;
	}

	return error;
}
#endif /* USB_HOST_STORAGE_CLASS_REGISTER */

/*******************************************************************************************************************//**
 * @brief      This is the function to notify a USB event from the USBX Host system.
 * @param[in]  event     Event code from happened to the USBX Host system.
 * @param[in]  usb_class Pointer to a USBX Host class, which occurs a event.
 * @param[in]  instance  Pointer to a USBX Host class instance, which occurs a event.
 * @retval     USBX error code
 **********************************************************************************************************************/
static UINT ux_system_host_change_function(ULONG event, UX_HOST_CLASS * host_class, VOID * instance)
{
	UINT status = UX_SUCCESS;
	SSP_PARAMETER_NOT_USED (event);
	SSP_PARAMETER_NOT_USED (host_class);
	SSP_PARAMETER_NOT_USED (instance);

#if !defined(NULL)
	/* Call user function back for USBX Host Class event notification. */
	status = NULL(event, host_class, instance);
	if (UX_SUCCESS != status)
	{
		return status;
	}
#endif

#ifdef USB_HOST_STORAGE_CLASS_REGISTER
	/* Check the class container if it is for a USBX Host Mass Storage class. */
	if (UX_SUCCESS == _ux_utility_memory_compare(_ux_system_host_class_storage_name, host_class, _ux_utility_string_length_get(_ux_system_host_class_storage_name)))
	{
		status = ux_system_host_storage_change_function(event, instance);
	}
#endif
	return status;
}
#endif

#ifdef USB_HOST_HID_CLASS_REGISTER
/* Function prototype to register USBX Host HID Clients to the USBX Host system. */
static void ux_host_class_hid_clients_register(void);
#endif

/*******************************************************************************************************************//**
 * @brief     Initialization function that the user can choose to have called automatically during thread entry.
 *            The user can call this function at a later time if desired using the prototype below.
 *            - void ux_common_init0(void)
 **********************************************************************************************************************/
void ux_common_init0(void) {
	UINT status_ux_init;

	/** Initialize the USBX system. */
	status_ux_init = ux_system_initialize((CHAR *) g_ux_pool_memory, 18432,
			UX_NULL, 0);
	if (UX_SUCCESS != status_ux_init) {
		ux_v2_err_callback(NULL, &status_ux_init);
	}

#ifdef UX_HOST_INITIALIZE
	/** Initialize the USBX Host stack. */
	status_ux_init = ux_host_stack_initialize(ux_system_host_change_function);
	if (UX_SUCCESS != status_ux_init)
	{
		ux_v2_err_callback(NULL,&status_ux_init);
	}

	/** Register USB Host classes. */
#ifdef USB_HOST_HUB_CLASS_REGISTER
	status_ux_init = ux_host_stack_class_register(_ux_system_host_class_hub_name, ux_host_class_hub_entry);
	if (UX_SUCCESS != status_ux_init)
	{
		ux_v2_err_callback(NULL,&status_ux_init);
	}
#endif

#ifdef USB_HOST_CDC_ACM_CLASS_REGISTER
	status_ux_init = ux_host_stack_class_register(_ux_system_host_class_cdc_acm_name, ux_host_class_cdc_acm_entry);
	if (UX_SUCCESS != status_ux_init)
	{
		ux_v2_err_callback(NULL,&status_ux_init);
	}
#endif

#ifdef USB_HOST_HID_CLASS_REGISTER
	status_ux_init = ux_host_stack_class_register(_ux_system_host_class_hid_name, ux_host_class_hid_entry);
	if (UX_SUCCESS != status_ux_init)
	{
		ux_v2_err_callback(NULL,&status_ux_init);
	}

	ux_host_class_hid_clients_register ();
#endif

#ifdef USB_HOST_STORAGE_CLASS_REGISTER
	ux_host_stack_class_register_storage();
#endif

#ifdef USB_HOST_VIDEO_CLASS_REGISTER
	status_ux_init = ux_host_stack_class_register(_ux_system_host_class_video_name, ux_host_class_video_entry);
	if (UX_SUCCESS != status_ux_init)
	{
		ux_v2_err_callback(NULL,&status_ux_init);
	}
#endif

#ifdef USB_HOST_AUDIO_CLASS_REGISTER
	status_ux_init = ux_host_stack_class_register(_ux_system_host_class_audio_name, ux_host_class_audio_entry);
	if (UX_SUCCESS != status_ux_init)
	{
		ux_v2_err_callback(NULL,&status_ux_init);
	}
#endif

#ifdef USB_HOST_PRINTER_CLASS_REGISTER
	status_ux_init = ux_host_stack_class_register(_ux_system_host_class_printer_name, _ux_host_class_printer_entry);
	if (UX_SUCCESS != status_ux_init)
	{
		ux_v2_err_callback(NULL,&status_ux_init);
	}
#endif

#endif /* UX_HOST_INITIALIZE */
}
#if defined(__ICCARM__)
#define g_sf_el_ux_dcd_fs_0_err_callback_WEAK_ATTRIBUTE
#pragma weak g_sf_el_ux_dcd_fs_0_err_callback  = g_sf_el_ux_dcd_fs_0_err_callback_internal
#elif defined(__GNUC__)
#define g_sf_el_ux_dcd_fs_0_err_callback_WEAK_ATTRIBUTE   __attribute__ ((weak, alias("g_sf_el_ux_dcd_fs_0_err_callback_internal")))
#endif
void g_sf_el_ux_dcd_fs_0_err_callback(void *p_instance, void *p_data)
g_sf_el_ux_dcd_fs_0_err_callback_WEAK_ATTRIBUTE;
#if (SF_EL_UX_CFG_FS_IRQ_IPL != BSP_IRQ_DISABLED)
/* USBFS ISR vector registering. */
#if !defined(SSP_SUPPRESS_ISR_g_sf_el_ux_dcd_fs_0) && !defined(SSP_SUPPRESS_ISR_USB)
SSP_VECTOR_DEFINE_UNIT(usbfs_int_isr, USB, FS, INT, 0);
#endif
#endif

/* Prototype function for USBX DCD Initializer. */
void ux_dcd_initialize(void);

#undef SYNERGY_NOT_DEFINED
#define SYNERGY_NOT_DEFINED (1)
/*******************************************************************************************************************//**
 * @brief      This is a weak example initialization error function.  It should be overridden by defining a user  function 
 *             with the prototype below.
 *             - void g_sf_el_ux_dcd_fs_0_err_callback(void * p_instance, void * p_data)
 *
 * @param[in]  p_instance arguments used to identify which instance caused the error and p_data Callback arguments used to identify what error caused the callback.
 **********************************************************************************************************************/
void g_sf_el_ux_dcd_fs_0_err_callback_internal(void *p_instance, void *p_data);
void g_sf_el_ux_dcd_fs_0_err_callback_internal(void *p_instance, void *p_data) {
	/** Suppress compiler warning for not using parameters. */
	SSP_PARAMETER_NOT_USED(p_instance);
	SSP_PARAMETER_NOT_USED(p_data);

	/** An error has occurred. Please check function arguments for more information. */
	BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
}
#if ((SYNERGY_NOT_DEFINED != SYNERGY_NOT_DEFINED) && (SYNERGY_NOT_DEFINED != SYNERGY_NOT_DEFINED))
/***********************************************************************************************************************
 * The definition of wrapper interface for USBX Synergy Port DCD Driver to get a transfer module instance.
 **********************************************************************************************************************/
static UINT g_sf_el_ux_dcd_fs_0_initialize_transfer_support(ULONG dcd_io)
{
	UX_DCD_SYNERGY_TRANSFER dcd_transfer;
	dcd_transfer.ux_synergy_transfer_tx = (transfer_instance_t *)&SYNERGY_NOT_DEFINED;
	dcd_transfer.ux_synergy_transfer_rx = (transfer_instance_t *)&SYNERGY_NOT_DEFINED;
	return (UINT)ux_dcd_synergy_initialize_transfer_support(dcd_io, (UX_DCD_SYNERGY_TRANSFER *)&dcd_transfer);
} /* End of function g_sf_el_ux_dcd_fs_0_initialize_transfer_support() */
#endif

/***********************************************************************************************************************
 * Initializes USBX Device Controller Driver.
 **********************************************************************************************************************/
void ux_dcd_initialize(void) {
	UINT status;
	/* Initializes the USB device controller, enabling DMA transfer if transfer module instances are given. */
#if ((SYNERGY_NOT_DEFINED == SYNERGY_NOT_DEFINED) || (SYNERGY_NOT_DEFINED == SYNERGY_NOT_DEFINED))
	status = (UINT) ux_dcd_synergy_initialize(R_USBFS_BASE);
#else
	status = g_sf_el_ux_dcd_fs_0_initialize_transfer_support(R_USBFS_BASE);
#endif
#undef SYNERGY_NOT_DEFINED
	if (UX_SUCCESS != status) {
		g_sf_el_ux_dcd_fs_0_err_callback(NULL, &status);
	}
} /* End of function ux_dcd_initialize() */
#if defined(__ICCARM__)
#define ux_device_err_callback_WEAK_ATTRIBUTE
#pragma weak ux_device_err_callback  = ux_device_err_callback_internal
#elif defined(__GNUC__)
#define ux_device_err_callback_WEAK_ATTRIBUTE   __attribute__ ((weak, alias("ux_device_err_callback_internal")))
#endif
void ux_device_err_callback(void *p_instance, void *p_data)
ux_device_err_callback_WEAK_ATTRIBUTE;
/* Definition for the special linker section for USB */
#if defined(__GNUC__)
extern uint32_t __usb_dev_descriptor_start_fs;
extern uint32_t __usb_descriptor_end_fs;
#if defined(UX_DCD_SYNERY_USE_USBHS)
extern uint32_t __usb_dev_descriptor_start_hs;
extern uint32_t __usb_descriptor_end_hs;
#endif
#endif

/* Counter to calculate number of Interfaces */
static uint8_t g_usbx_number_of_interface_count = 0;

/***********************************************************************************************************************
 * USB Device Descriptor for FS mode
 **********************************************************************************************************************/
#if defined(__GNUC__)
static volatile const unsigned char device_framework_full_speed[] BSP_PLACE_IN_SECTION_V2(".usb_device_desc_fs") BSP_ALIGN_VARIABLE_V2(1)
#else /* __ICCARM__ */
#pragma section = ".usb_device_desc_fs" 1
__root static const unsigned char device_framework_full_speed[] BSP_PLACE_IN_SECTION_V2(".usb_device_desc_fs")
#endif
= {
	0x12, /* 0 bLength */
	UX_DEVICE_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x00, /* 2 bcdUSB BCD(2.0) */
	0x02, /* 3 bcdUSB */
	0x03, /* 4 bDeviceClass    : Device Class */
#if (0x03 == 0x00) || (0x03 == 0x02) || (0x03 == 0x03) || (0x03 == 0x08)
	0x00, /* 5 bDeviceSubClass : none */
	0x00, /* 6 bDeviceProtocol : none */
#elif (0x03 == 0xEF)
	0x02, /* 5 bDeviceSubClass : Common Class(0x02) */
	0x01, /* 6 bDeviceProtocol :Interface Association Descriptor(IAD) */
#elif (0x03 == 0xFF)
	0x00, /* 5 bDeviceSubClass : none */
	0x00, /* 6 bDeviceProtocol : none */
#endif
	0x40, /* 7 bMaxPacketSize0 */
	(uint8_t)(0x045B), /* 8 idVendor */
	(uint8_t)(0x045B >> 8), /* 9 idVendor */
	(uint8_t)(0x5302), /* 10 idProduct */
	(uint8_t)(0x5302 >> 8), /* 11 idProduct */
	(uint8_t)(0x0000), /* 12 bcdDevice */
	(uint8_t)(0x0000 >> 8), /* 13 bcdDevice */
	0x00, /* 14 iManufacturer */
	0x00, /* 15 iProduct */
	0x00, /* 16 iSerialNumber */
	0x01, /* 17 bNumConfigurations */
};

#if defined(UX_DCD_SYNERY_USE_USBHS)
/***********************************************************************************************************************
 * USB Device Descriptor for HS Mode
 **********************************************************************************************************************/
#if defined(__GNUC__)
static volatile const unsigned char device_framework_high_speed[] BSP_PLACE_IN_SECTION_V2(".usb_device_desc_hs") BSP_ALIGN_VARIABLE_V2(1)
#else /* __ICCARM__ */
#pragma section = ".usb_device_desc_hs" 1
__root static const unsigned char device_framework_high_speed[] BSP_PLACE_IN_SECTION_V2(".usb_device_desc_hs")
#endif
= {
	0x12, /* 0 bLength */
	UX_DEVICE_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x00, /* 2 bcdUSB BCD(2.0) */
	0x02, /* 3 bcdUSB */
	0x03, /* 4 bDeviceClass    : Device Class */
#if (0x03 == 0x00) || (0x03 == 0x02) || (0x03 == 0x03) || (0x03 == 0x08)
	0x00, /* 5 bDeviceSubClass : none */
	0x00, /* 6 bDeviceProtocol : none */
#elif (0x03 == 0xEF) /* Composite device */
	0x02, /* 5 bDeviceSubClass : Common Class(0x02) */
	0x01, /* 6 bDeviceProtocol :Interface Association Descriptor(IAD) */
#elif (0x03 == 0xFF) /* Vendor specific device */
	0x00, /* 5 bDeviceSubClass : none */
	0x00, /* 6 bDeviceProtocol : none */
#endif
	0x40, /* 7 bMaxPacketSize0 */
	(uint8_t)(0x045B), /* 8 idVendor */
	(uint8_t)(0x045B >> 8), /* 9 idVendor */
	(uint8_t)(0x5302), /* 10 idProduct */
	(uint8_t)(0x5302 >> 8), /* 11 idProduct */
	(uint8_t)(0x0000), /* 12 bcdDevice */
	(uint8_t)(0x0000 >> 8), /* 13 bcdDevice */
	0x00, /* 14 iManufacturer */
	0x00, /* 15 iProduct */
	0x00, /* 16 iSerialNumber */
	0x01, /* 17 bNumConfigurations */
	/****************************************************************
	 * Device qualifier descriptor                                  *
	 ****************************************************************/
	0x0a, /* 0 bLength */
	UX_DEVICE_QUALIFIER_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x00, /* 2 bcdUSB BCD(2.0) */
	0x02, /* 3 bcdUSB */
	0x03, /* 4 bDeviceClass    : Device Class */
#if (0x03 == 0x00) || (0x03 == 0x02) || (0x03 == 0x03) || (0x03 == 0x08)
	0x00, /* 5 bDeviceSubClass : none */
	0x00, /* 6 bDeviceProtocol : none */
#elif (0x03 == 0xEF)
	0x02, /* 5 bDeviceSubClass : Common Class(0x02) */
	0x00, /* 6 bDeviceProtocol : none */
#elif (0x03 == 0xFF)
	0x00, /* 5 bDeviceSubClass : none */
	0x00, /* 6 bDeviceProtocol : none */
#endif
	0x40, /* 7 bMaxPacketSize0 */
	0x01, /* 8 bNumConfigs : 1 */
	0x00, /* 9 Reserve, fixed to 0 */
};
#endif

/***********************************************************************************************************************
 * Configuration Value Auto-calculation for USB Configuration Descriptor
 **********************************************************************************************************************/
#ifndef USB_IFD_NUM_0
#define USB_IFD_NUM_0  (0)
#define USB_IFD_SIZE_0 (0)
#endif
#ifndef USB_IFD_NUM_1
#define USB_IFD_NUM_1  (0)
#define USB_IFD_SIZE_1 (0)
#endif
#ifndef USB_IFD_NUM_2
#define USB_IFD_NUM_2  (0)
#define USB_IFD_SIZE_2 (0)
#endif
#ifndef USB_IFD_NUM_3
#define USB_IFD_NUM_3  (0)
#define USB_IFD_SIZE_3 (0)
#endif
#ifndef USB_IFD_NUM_4
#define USB_IFD_NUM_4  (0)
#define USB_IFD_SIZE_4 (0)
#endif
#ifndef USB_IFD_NUM_5
#define USB_IFD_NUM_5  (0)
#define USB_IFD_SIZE_5 (0)
#endif
#ifndef USB_IFD_NUM_6
#define USB_IFD_NUM_6  (0)
#define USB_IFD_SIZE_6 (0)
#endif
#ifndef USB_IFD_NUM_7
#define USB_IFD_NUM_7  (0)
#define USB_IFD_SIZE_7 (0)
#endif
#ifndef USB_IFD_NUM_8
#define USB_IFD_NUM_8  (0)
#define USB_IFD_SIZE_8 (0)
#endif
#ifndef USB_IFD_NUM_9
#define USB_IFD_NUM_9  (0)
#define USB_IFD_SIZE_9 (0)
#endif

#define USB_NUMBER_OF_INTERFACE USB_IFD_NUM_0 + USB_IFD_NUM_1 + USB_IFD_NUM_2 + USB_IFD_NUM_3 + USB_IFD_NUM_4 + USB_IFD_NUM_5 + USB_IFD_NUM_6 + USB_IFD_NUM_7 + USB_IFD_NUM_8 + USB_IFD_NUM_9
#define USB_TOTAL_LENGTH        USB_IFD_SIZE_0 + USB_IFD_SIZE_1 + USB_IFD_SIZE_2 + USB_IFD_SIZE_3 + USB_IFD_SIZE_4 + USB_IFD_SIZE_5 + USB_IFD_SIZE_6 + USB_IFD_SIZE_7 + USB_IFD_SIZE_8 + USB_IFD_SIZE_9

#if (0 != 0x00)
#define USB_CFG_DESCRIPTOR_bNumInterfaces     (0x00)
#else
#define USB_CFG_DESCRIPTOR_bNumInterfaces     (USB_NUMBER_OF_INTERFACE)
#endif
#if (0 != 0x22)
#define USB_CFG_DESCRIPTOR_wTotalLength       (0x22)
#else
#define USB_CFG_DESCRIPTOR_wTotalLength       (0x09 + USB_TOTAL_LENGTH)
#endif

/***********************************************************************************************************************
 * Consolidated USB Device Descriptor Framework on RAM for IAR build
 **********************************************************************************************************************/
#ifdef __ICCARM__
/* Memory area to complete USB device Descriptor Framework. */
static uint8_t device_framework_full_speed_ram[0x12 + USB_CFG_DESCRIPTOR_wTotalLength];
#if defined(UX_DCD_SYNERY_USE_USBHS)
static uint8_t device_framework_high_speed_ram[0x12 + 0x0a + USB_CFG_DESCRIPTOR_wTotalLength];
#endif
#endif

/***********************************************************************************************************************
 * USB Configuration Descriptor for FS mode
 **********************************************************************************************************************/
#if defined(__GNUC__)
static volatile const unsigned char ux_cfg_descriptor_full_speed[] BSP_PLACE_IN_SECTION_V2(".usb_config_desc_fs") BSP_ALIGN_VARIABLE_V2(1)
#else /* __ICCARM__ */
#pragma section = ".usb_config_desc_fs" 1
__root static const unsigned char ux_cfg_descriptor_full_speed[] BSP_PLACE_IN_SECTION_V2(".usb_config_desc_fs")
#endif
= {
	0x09, /* 0 bLength */
	UX_CONFIGURATION_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(uint8_t)(USB_CFG_DESCRIPTOR_wTotalLength), /* 2 wTotalLength : This will be calculated at run-time. */
	(uint8_t)(USB_CFG_DESCRIPTOR_wTotalLength >> 8), /* 3 wTotalLength : This will be calculated at run-time. */
	(uint8_t)(USB_CFG_DESCRIPTOR_bNumInterfaces), /* 4 bNumInterfaces */
	0x01, /* 5 bConfigurationValue : Fixed to 1 since only one configuration is supported. */
	0x00, /* 6 iConfiguration */
	0x80 | (1 << 6) | (0 << 5), /* 7 bmAttributes */
	50, /* 8 bMaxPower */
};

#if defined(UX_DCD_SYNERY_USE_USBHS)
/***********************************************************************************************************************
 * USB Configuration Descriptor for HS mode
 **********************************************************************************************************************/
#if defined(__GNUC__)
static volatile const unsigned char ux_cfg_descriptor_high_speed[] BSP_PLACE_IN_SECTION_V2(".usb_config_desc_hs") BSP_ALIGN_VARIABLE_V2(1)
#else /* __ICCARM__ */
#pragma section = ".usb_config_desc_hs" 1
__root static const unsigned char ux_cfg_descriptor_high_speed[] BSP_PLACE_IN_SECTION_V2(".usb_config_desc_hs")
#endif
= {
	0x09, /* 0 bLength */
	UX_CONFIGURATION_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(uint8_t)(USB_CFG_DESCRIPTOR_wTotalLength), /* 2 wTotalLength : This will be calculated at run-time. */
	(uint8_t)(USB_CFG_DESCRIPTOR_wTotalLength >> 8), /* 3 wTotalLength : This will be calculated at run-time. */
	(uint8_t)(USB_CFG_DESCRIPTOR_bNumInterfaces), /* 4 bNumInterfaces */
	0x01, /* 5 bConfigurationValue : Fixed to 1 since only one configuration is supported. */
	0x00, /* 6 iConfiguration */
	0x80 | (1 << 6) | (0 << 5), /* 7 bmAttributes */
	50, /* 8 bMaxPower */
};
#endif

/***********************************************************************************************************************
 * USB Language Framework (Default setting for US English)
 **********************************************************************************************************************/
#if(0 == 0) /* Create USB Language Framework with default setting if not defined by user. */
static volatile const UCHAR language_id_framework_default[] = { (uint8_t)(
		0x0409), /* Supported Language Code */
(uint8_t)(0x0409 >> 8) /* US English as the default */
};
#endif

/***********************************************************************************************************************
 * Function to get size of USB String Descriptor
 **********************************************************************************************************************/
#if (0)
extern const UCHAR NULL[];
static ULONG ux_device_string_descriptor_size_get(void)
{
	ULONG size = 0;
	UCHAR * ptr = (UCHAR *)NULL;
	if(NULL != ptr)
	{
		for(INT i = 0; i < 0; i++)
		{
			ptr = ptr + 3; /* bLength at byte offset 3 */
			/* Counts bLength + Language code(2bytes) + bLength(1byte) */
			size = size + *ptr + 4;
			ptr = ptr + (*ptr) + 1;
		}
	}
	return size;
}
#endif

#if (0)
extern const UCHAR NULL[];
#endif
/*******************************************************************************************************************//**
 * @brief      This is a weak example initialization error function.  It should be overridden by defining a user  function 
 *             with the prototype below.
 *             - void ux_device_err_callback(void * p_instance, void * p_data)
 *
 * @param[in]  p_instance arguments used to identify which instance caused the error and p_data Callback arguments used to identify what error caused the callback.
 **********************************************************************************************************************/
void ux_device_err_callback_internal(void *p_instance, void *p_data);
void ux_device_err_callback_internal(void *p_instance, void *p_data) {
	/** Suppress compiler warning for not using parameters. */
	SSP_PARAMETER_NOT_USED(p_instance);
	SSP_PARAMETER_NOT_USED(p_data);

	/** An error has occurred. Please check function arguments for more information. */
	BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
}

#ifdef __ICCARM__

/*******************************************************************************************************************//**
 * @brief      This function is called during USBX device initialization to remove any padding inserted by the compiler.
 **********************************************************************************************************************/
void ux_device_remove_compiler_padding(unsigned char * p_device_framework, UINT length)
{
	INT remaining, count = 0;
	unsigned char *index, *temp;
	remaining = length;
	index = p_device_framework;

	/* Parse the device framework and remove any padding inserted  by the compiler.  */
	while(index < (p_device_framework + length))

	{
		temp = index;

		if(*temp == 0)
		{
			/* Remove, if any padding inserted by the compiler. */
			count = 0;
			while( (*temp == 0 ) && (temp < (p_device_framework + length)) )
			{
				temp++;
				count++;
			}

			remaining -= count;
			if(remaining > 0)
			{
				memcpy(index, temp, remaining);
				count = 0;
			}

			else
			{
				index = p_device_framework + length;
			}
		}

		else
		{
			/* Jump to next offset of the descriptors. */
			remaining = remaining - (*index);
			index = index + (*index);

		}
	}
}

#endif
/*******************************************************************************************************************//**
 * @brief     Initialization function that the user can choose to have called automatically during thread entry.
 *            The user can call this function at a later time if desired using the prototype below.
 *            - void ux_device_init0(void)
 **********************************************************************************************************************/
void ux_device_init0(void) {

	UINT status_ux_device_init;

	/** Calculate the size of USBX String Framework. */
	ULONG string_framework_size;
	UCHAR *p_string_framework;

#if(0) /* Check if the number of index in USBX String Framework is greater than zero. */
	{
		p_string_framework = (UCHAR *)NULL;
		string_framework_size = ux_device_string_descriptor_size_get();
	}
#else
	{
		p_string_framework = NULL;
		string_framework_size = 0;
	}
#endif

	/** Calculate the size of USB Language Framework. */
	ULONG language_framework_size;
	UCHAR *p_language_framework;

#if(0) /* Check if the number of index in USB Language Framework is greater than zero. */
	{
		p_language_framework = (UCHAR *)NULL;
		language_framework_size = 0 * 2;
	}
#else
	{
		p_language_framework = (UCHAR *) language_id_framework_default;
		language_framework_size = 2;
	}
#endif

	/** Initialize the USB Device stack. */
#ifdef __GNUC__
	status_ux_device_init = ux_device_stack_initialize(
#if defined(UX_DCD_SYNERY_USE_USBHS)
			(UCHAR *)device_framework_high_speed, ((uint32_t)&__usb_descriptor_end_hs - (uint32_t)&__usb_dev_descriptor_start_hs),
#else /* If USB DCD runs in FS mode, USB Device Descriptor for HS mode is not required. */
			NULL, 0,
#endif
			(UCHAR *)device_framework_full_speed, ((uint32_t)&__usb_descriptor_end_fs - (uint32_t)&__usb_dev_descriptor_start_fs),
			p_string_framework, string_framework_size,
			p_language_framework, language_framework_size, NULL);
#else /* __ICCARM__ */

	uint8_t *p_device_framework;
	unsigned char *start, *end, interface_desc_fs_hs[0x12 + 0x0a
			+ USB_CFG_DESCRIPTOR_wTotalLength + 0x14];
	UINT length = 0;
#if defined(UX_DCD_SYNERY_USE_USBHS)
	start = (unsigned char *)(__section_begin (".usb_interface_desc_hs"));
	end = (unsigned char *)(__section_end (".usb_interface_desc_hs"));
	length = abs(end - start);
	memset(interface_desc_fs_hs, 0, sizeof(interface_desc_fs_hs));
	p_device_framework = (uint8_t *)interface_desc_fs_hs;
	/* Creates Device Descriptor Frameworks copying Descriptors from ROM. */
	memcpy (p_device_framework, device_framework_high_speed, sizeof(device_framework_high_speed));
	p_device_framework = p_device_framework + sizeof(device_framework_high_speed);
	/* Copy Configuration Descriptor */
	memcpy (p_device_framework, ux_cfg_descriptor_high_speed, sizeof(ux_cfg_descriptor_high_speed));
	p_device_framework = p_device_framework + sizeof(ux_cfg_descriptor_high_speed);
	/* Copy Interface Descriptors */
	memcpy (p_device_framework, (uint8_t *) __section_begin (".usb_interface_desc_hs"), length);
	ux_device_remove_compiler_padding(p_device_framework, length);
	/* Copy device frame work descriptor after removal of padding. */
	memcpy(device_framework_high_speed_ram, interface_desc_fs_hs, sizeof(device_framework_high_speed_ram));
#endif

	start = (unsigned char *) (__section_begin(".usb_interface_desc_fs"));
	end = (unsigned char *) (__section_end(".usb_interface_desc_fs"));
	length = abs(end - start);
	memset(interface_desc_fs_hs, 0, sizeof(interface_desc_fs_hs));
	p_device_framework = (uint8_t *) interface_desc_fs_hs;
	/* Creates Device Descriptor Frameworks copying Descriptors from ROM. */
	memcpy(p_device_framework, device_framework_full_speed,
			sizeof(device_framework_full_speed));
	p_device_framework = p_device_framework
			+ sizeof(device_framework_full_speed);
	/* Copy Configuration Descriptor */
	memcpy(p_device_framework, ux_cfg_descriptor_full_speed,
			sizeof(ux_cfg_descriptor_full_speed));
	p_device_framework = p_device_framework
			+ sizeof(ux_cfg_descriptor_full_speed);
	/* Copy Interface Descriptors */
	memcpy(p_device_framework,
			(uint8_t *) __section_begin(".usb_interface_desc_fs"), length);
	ux_device_remove_compiler_padding(p_device_framework, length);
	/* Copy device framework descriptor after removal of padding. */
	memcpy(device_framework_full_speed_ram, interface_desc_fs_hs,
			sizeof(device_framework_full_speed_ram));

	status_ux_device_init =
			ux_device_stack_initialize(
#if defined(UX_DCD_SYNERY_USE_USBHS)
					(UCHAR *)device_framework_high_speed_ram, (0x12 + 0x0a + USB_CFG_DESCRIPTOR_wTotalLength),
#else /* If USB DCD runs in FS mode, USB Device Descriptor for HS mode is not required. */
					NULL, 0,
#endif
					(UCHAR *) device_framework_full_speed_ram,
					(0x12 + USB_CFG_DESCRIPTOR_wTotalLength),
					p_string_framework, string_framework_size,
					p_language_framework, language_framework_size, NULL);
#endif
	if (UX_SUCCESS != status_ux_device_init) {
		ux_device_err_callback(NULL, &status_ux_device_init);
	}

}
static void g_ux_device_class_hid_setup(void);
/***********************************************************************************************************************
 * USBX HID Class Parameter Configuration Function.
 **********************************************************************************************************************/
static UX_SLAVE_CLASS_HID_PARAMETER g_ux_device_class_hid_parameter;
static void g_ux_device_class_hid_setup(void) {
#if (USB_HID_DESCRIPTOR_TYPE == 1) || (USB_HID_DESCRIPTOR_TYPE == 3)              
	/* Initialize the HID class parameters for a keyboard.  */
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_address = (UCHAR *)g_hid_report_descriptor_keyboard;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_length = sizeof(g_hid_report_descriptor_keyboard);
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_callback = ux_hid_device_callback;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_id = UX_FALSE;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_get_callback = NULL;

	/* Register user callback functions.  */
	g_ux_device_class_hid_parameter.ux_slave_class_hid_instance_activate = NULL;
	g_ux_device_class_hid_parameter.ux_slave_class_hid_instance_deactivate = NULL;

	/* Initialize the device HID class. This class owns both interfaces starting with 1. */
	ux_device_stack_class_register(_ux_system_slave_class_hid_name, ux_device_class_hid_entry, 1, 0x00, (VOID *)&g_ux_device_class_hid_parameter);
#endif		
#if (USB_HID_DESCRIPTOR_TYPE == 2) || (USB_HID_DESCRIPTOR_TYPE == 3)		
	/* Initialize the HID class parameters for a mouse.  */
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_address = (UCHAR *)g_hid_report_descriptor_mouse;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_length = sizeof(g_hid_report_descriptor_mouse);
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_callback = ux_hid_device_callback;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_id = UX_FALSE;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_get_callback = NULL;

	/* Register user callback functions.  */
	g_ux_device_class_hid_parameter.ux_slave_class_hid_instance_activate = NULL;
	g_ux_device_class_hid_parameter.ux_slave_class_hid_instance_deactivate = NULL;

	/* Initialize the device HID class. This class owns both interfaces starting with 1. */
	ux_device_stack_class_register(_ux_system_slave_class_hid_name, ux_device_class_hid_entry, 1, 0x01, (VOID *)&g_ux_device_class_hid_parameter);
#endif
	/* Counts up the number of Interfaces. */
#if (0) || (0) || (USB_HID_DESCRIPTOR_TYPE == 3)
	g_usbx_number_of_interface_count = (uint8_t)(g_usbx_number_of_interface_count + 2);
#else
	g_usbx_number_of_interface_count = (uint8_t)(
			g_usbx_number_of_interface_count + 1);
#endif
}
/*******************************************************************************************************************//**
 * @brief     Initialization function that the user can choose to have called automatically during thread entry.
 *            The user can call this function at a later time if desired using the prototype below.
 *            - void ux_device_class_hid_init0(void)
 **********************************************************************************************************************/
void ux_device_class_hid_init0(void) {
	/* Setups USB HID Class to create the instance (if required) */
	g_ux_device_class_hid_setup();

	/* USB DCD initialization (Only executed once after all the class registration completed). */
	if (g_usbx_number_of_interface_count == USB_NUMBER_OF_INTERFACE) {
		/* Initializes USBX Device Control Driver */
		ux_dcd_initialize();
	}
}
const cgc_instance_t g_cgc = { .p_api = &g_cgc_on_cgc, .p_cfg = NULL };
const ioport_instance_t g_ioport =
		{ .p_api = &g_ioport_on_ioport, .p_cfg = NULL };
/* Instance structure to use this module. */
const fmi_instance_t g_fmi = { .p_api = &g_fmi_on_fmi };
const elc_instance_t g_elc = { .p_api = &g_elc_on_elc, .p_cfg = NULL };
void g_common_init(void) {
	/** Call initialization function if user has selected to do so. */
#if (1)
	sf_message_init0();
#endif

	/** Call initialization function if user has selected to do so. */
#if (1)
	ux_common_init0();
#endif
	/** Call initialization function if user has selected to do so. */
#if (1)
	ux_device_init0();
#endif
	/** Call initialization function if user has selected to do so. */
#if (1)
	ux_device_class_hid_init0();
#endif
}
