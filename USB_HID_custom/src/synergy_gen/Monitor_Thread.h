/* generated thread header file - do not edit */
#ifndef MONITOR_THREAD_H_
#define MONITOR_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void Monitor_Thread_entry(void);
#else
extern void Monitor_Thread_entry(void);
#endif
#include "r_wdt.h"
#include "r_wdt_api.h"
#ifdef __cplusplus
extern "C" {
#endif
/** WDT on WDT Instance. */
extern const wdt_instance_t g_wdt0;
#ifndef NULL
void NULL(wdt_callback_args_t *p_args);
#endif
extern TX_EVENT_FLAGS_GROUP ThreadMonitor_EventFlag;
extern TX_SEMAPHORE g_Monitor_Semaphore;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* MONITOR_THREAD_H_ */
