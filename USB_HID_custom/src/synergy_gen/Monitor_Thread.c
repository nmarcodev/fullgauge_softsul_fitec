/* generated thread source file - do not edit */
#include "Monitor_Thread.h"

TX_THREAD Monitor_Thread;
void Monitor_Thread_create(void);
static void Monitor_Thread_func(ULONG thread_input);
static uint8_t Monitor_Thread_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.Monitor_Thread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
static wdt_instance_ctrl_t g_wdt0_ctrl;

static const wdt_cfg_t g_wdt0_cfg = { .start_mode = WDT_START_MODE_REGISTER,
		.autostart = true, .timeout = WDT_TIMEOUT_16384, .clock_division =
				WDT_CLOCK_DIVISION_8192, .window_start = WDT_WINDOW_START_100,
		.window_end = WDT_WINDOW_END_0,
		.reset_control = WDT_RESET_CONTROL_RESET, .stop_control =
				WDT_STOP_CONTROL_ENABLE, .p_callback = NULL, };

/* Instance structure to use this module. */
const wdt_instance_t g_wdt0 = { .p_ctrl = &g_wdt0_ctrl, .p_cfg = &g_wdt0_cfg,
		.p_api = &g_wdt_on_wdt };
TX_EVENT_FLAGS_GROUP ThreadMonitor_EventFlag;
TX_SEMAPHORE g_Monitor_Semaphore;

extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void Monitor_Thread_create(void) {
	/* Increment count so we will know the number of ISDE created threads. */
	g_ssp_common_thread_count++;

	/* Initialize each kernel object. */
	UINT err_ThreadMonitor_EventFlag;
	err_ThreadMonitor_EventFlag = tx_event_flags_create(
			&ThreadMonitor_EventFlag, (CHAR *) "Event Flags Monitor");
	if (TX_SUCCESS != err_ThreadMonitor_EventFlag) {
		tx_startup_err_callback(&ThreadMonitor_EventFlag, 0);
	}
	UINT err_g_Monitor_Semaphore;
	err_g_Monitor_Semaphore = tx_semaphore_create(&g_Monitor_Semaphore,
			(CHAR *) "Monitor Semaphore", 0);
	if (TX_SUCCESS != err_g_Monitor_Semaphore) {
		tx_startup_err_callback(&g_Monitor_Semaphore, 0);
	}

	UINT err;
	err = tx_thread_create(&Monitor_Thread, (CHAR *) "Monitor Thread",
			Monitor_Thread_func, (ULONG) NULL, &Monitor_Thread_stack, 1024, 1,
			1, 1, TX_AUTO_START);
	if (TX_SUCCESS != err) {
		tx_startup_err_callback(&Monitor_Thread, 0);
	}
}

static void Monitor_Thread_func(ULONG thread_input) {
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* Initialize common components */
	tx_startup_common_init();

	/* Initialize each module instance. */

	/* Enter user code for this thread. */
	Monitor_Thread_entry();
}
