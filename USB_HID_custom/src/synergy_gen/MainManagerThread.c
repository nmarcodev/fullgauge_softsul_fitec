/* generated thread source file - do not edit */
#include "MainManagerThread.h"

TX_THREAD MainManagerThread;
void MainManagerThread_create(void);
static void MainManagerThread_func(ULONG thread_input);
static uint8_t MainManagerThread_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.MainManagerThread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_int_storageInst) && !defined(SSP_SUPPRESS_ISR_FLASH)
SSP_VECTOR_DEFINE( fcu_frdyi_isr, FCU, FRDYI);
#endif
#endif
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_int_storageInst) && !defined(SSP_SUPPRESS_ISR_FLASH)
SSP_VECTOR_DEFINE( fcu_fiferr_isr, FCU, FIFERR);
#endif
#endif
flash_hp_instance_ctrl_t g_int_storageInst_ctrl;
const flash_cfg_t g_int_storageInst_cfg = { .data_flash_bgo = true,
		.p_callback = flash_callback, .p_context = &g_int_storageInst,
		.irq_ipl = (1), .err_irq_ipl = (1), };
/* Instance structure to use this module. */
const flash_instance_t g_int_storageInst = { .p_ctrl = &g_int_storageInst_ctrl,
		.p_cfg = &g_int_storageInst_cfg, .p_api = &g_flash_on_flash_hp };
#if (10) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer0) && !defined(SSP_SUPPRESS_ISR_GPT5)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 5);
#endif
#endif
static gpt_instance_ctrl_t g_timer0_ctrl;
static const timer_on_gpt_cfg_t g_timer0_extend = { .gtioca = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW }, .gtiocb = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
		.shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer0_cfg = { .mode = TIMER_MODE_PERIODIC, .period =
		10, .unit = TIMER_UNIT_PERIOD_MSEC, .duty_cycle = 50, .duty_cycle_unit =
		TIMER_PWM_UNIT_RAW_COUNTS, .channel = 5, .autostart = false,
		.p_callback = Timer_IO_Callback, .p_context = &g_timer0, .p_extend =
				&g_timer0_extend, .irq_ipl = (10), };
/* Instance structure to use this module. */
const timer_instance_t g_timer0 = { .p_ctrl = &g_timer0_ctrl, .p_cfg =
		&g_timer0_cfg, .p_api = &g_timer_on_gpt };
TX_EVENT_FLAGS_GROUP g_main_event_flags;
TX_EVENT_FLAGS_GROUP g_int_storage_evt_grp;
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void MainManagerThread_create(void) {
	/* Increment count so we will know the number of ISDE created threads. */
	g_ssp_common_thread_count++;

	/* Initialize each kernel object. */
	UINT err_g_main_event_flags;
	err_g_main_event_flags = tx_event_flags_create(&g_main_event_flags,
			(CHAR *) "Main Event Flags");
	if (TX_SUCCESS != err_g_main_event_flags) {
		tx_startup_err_callback(&g_main_event_flags, 0);
	}
	UINT err_g_int_storage_evt_grp;
	err_g_int_storage_evt_grp = tx_event_flags_create(&g_int_storage_evt_grp,
			(CHAR *) "Internal Storage Event Flags");
	if (TX_SUCCESS != err_g_int_storage_evt_grp) {
		tx_startup_err_callback(&g_int_storage_evt_grp, 0);
	}

	UINT err;
	err = tx_thread_create(&MainManagerThread, (CHAR *) "Main Manager Thread",
			MainManagerThread_func, (ULONG) NULL, &MainManagerThread_stack,
			1024, 1, 1, 1, TX_AUTO_START);
	if (TX_SUCCESS != err) {
		tx_startup_err_callback(&MainManagerThread, 0);
	}
}

static void MainManagerThread_func(ULONG thread_input) {
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* Initialize common components */
	tx_startup_common_init();

	/* Initialize each module instance. */

	/* Enter user code for this thread. */
	MainManagerThread_entry();
}
