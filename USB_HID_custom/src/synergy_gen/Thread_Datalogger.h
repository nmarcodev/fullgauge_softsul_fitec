/* generated thread header file - do not edit */
#ifndef THREAD_DATALOGGER_H_
#define THREAD_DATALOGGER_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void Thread_Datalogger_entry(void);
#else
extern void Thread_Datalogger_entry(void);
#endif
#include "r_crc.h"
#include "r_crc_api.h"
#include "r_gpt.h"
#include "r_timer_api.h"
#include "r_dtc.h"
#include "r_transfer_api.h"
#include "r_sci_spi.h"
#include "r_spi_api.h"
#include "r_qspi.h"
#include "r_qspi_api.h"
#ifdef __cplusplus
extern "C" {
#endif
extern const crc_instance_t g_crc0;
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_datalogger;
#ifndef TimerDataloggerCallback
void TimerDataloggerCallback(timer_callback_args_t *p_args);
#endif
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer3;
#ifndef NULL
void NULL(transfer_callback_args_t *p_args);
#endif
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer2;
#ifndef NULL
void NULL(transfer_callback_args_t *p_args);
#endif
extern const spi_cfg_t g_spi0_cfg;
/** SPI on SCI Instance. */
extern const spi_instance_t g_spi0;
extern sci_spi_instance_ctrl_t g_spi0_ctrl;
extern const sci_spi_extended_cfg g_spi0_cfg_extend;

#ifndef spi_callback
void spi_callback(spi_callback_args_t *p_args);
#endif

#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == g_transfer2)
#define g_spi0_P_TRANSFER_TX (NULL)
#else
#define g_spi0_P_TRANSFER_TX (&g_transfer2)
#endif
#if (SYNERGY_NOT_DEFINED == g_transfer3)
#define g_spi0_P_TRANSFER_RX (NULL)
#else
#define g_spi0_P_TRANSFER_RX (&g_transfer3)
#endif
#undef SYNERGY_NOT_DEFINED

#define g_spi0_P_EXTEND (&g_spi0_cfg_extend)
extern const qspi_instance_t g_qspi0;
extern TX_EVENT_FLAGS_GROUP SPI_FLAGS;
extern TX_EVENT_FLAGS_GROUP DataloggerEventFlags;
extern TX_QUEUE Datalogger_Flash;
extern TX_SEMAPHORE DataloggerSemaphore;
extern TX_MUTEX QSPI_Mutex;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* THREAD_DATALOGGER_H_ */
