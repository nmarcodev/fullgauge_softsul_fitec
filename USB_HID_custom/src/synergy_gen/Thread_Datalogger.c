/* generated thread source file - do not edit */
#include "Thread_Datalogger.h"

TX_THREAD Thread_Datalogger;
void Thread_Datalogger_create(void);
static void Thread_Datalogger_func(ULONG thread_input);
static uint8_t Thread_Datalogger_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.Thread_Datalogger") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
static crc_instance_ctrl_t g_crc0_ctrl;
const crc_cfg_t g_crc0_cfg = { .polynomial = CRC_POLYNOMIAL_CRC_CCITT,
		.bit_order = CRC_BIT_ORDER_LMS_MSB, .fifo_mode = false, };
/* Instance structure to use this module. */
const crc_instance_t g_crc0 = { .p_ctrl = &g_crc0_ctrl, .p_cfg = &g_crc0_cfg,
		.p_api = &g_crc_on_crc };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer_datalogger) && !defined(SSP_SUPPRESS_ISR_GPT1)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 1);
#endif
#endif
static gpt_instance_ctrl_t g_timer_datalogger_ctrl;
static const timer_on_gpt_cfg_t g_timer_datalogger_extend = { .gtioca = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW }, .gtiocb = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
		.shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer_datalogger_cfg = { .mode = TIMER_MODE_PERIODIC,
		.period = 1, .unit = TIMER_UNIT_PERIOD_SEC, .duty_cycle = 50,
		.duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 1, .autostart =
				false, .p_callback = TimerDataloggerCallback, .p_context =
				&g_timer_datalogger, .p_extend = &g_timer_datalogger_extend,
		.irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer_datalogger = {
		.p_ctrl = &g_timer_datalogger_ctrl, .p_cfg = &g_timer_datalogger_cfg,
		.p_api = &g_timer_on_gpt };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer3) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_SCI8_RXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_SCI8_RXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer3_ctrl;
transfer_info_t g_transfer3_info = { .dest_addr_mode =
		TRANSFER_ADDR_MODE_INCREMENTED, .repeat_area =
		TRANSFER_REPEAT_AREA_DESTINATION, .irq = TRANSFER_IRQ_END, .chain_mode =
		TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_FIXED,
		.size = TRANSFER_SIZE_1_BYTE, .mode = TRANSFER_MODE_NORMAL, .p_dest =
				(void *) NULL, .p_src = (void const *) NULL, .num_blocks = 0,
		.length = 0, };
const transfer_cfg_t g_transfer3_cfg = { .p_info = &g_transfer3_info,
		.activation_source = ELC_EVENT_SCI8_RXI, .auto_enable = false,
		.p_callback = NULL, .p_context = &g_transfer3, .irq_ipl =
				(BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer3 = { .p_ctrl = &g_transfer3_ctrl, .p_cfg =
		&g_transfer3_cfg, .p_api = &g_transfer_on_dtc };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer2) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_SCI8_TXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_SCI8_TXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer2_ctrl;
transfer_info_t g_transfer2_info = { .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED,
		.repeat_area = TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
		.chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode =
				TRANSFER_ADDR_MODE_INCREMENTED, .size = TRANSFER_SIZE_1_BYTE,
		.mode = TRANSFER_MODE_NORMAL, .p_dest = (void *) NULL, .p_src =
				(void const *) NULL, .num_blocks = 0, .length = 0, };
const transfer_cfg_t g_transfer2_cfg = { .p_info = &g_transfer2_info,
		.activation_source = ELC_EVENT_SCI8_TXI, .auto_enable = false,
		.p_callback = NULL, .p_context = &g_transfer2, .irq_ipl =
				(BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer2 = { .p_ctrl = &g_transfer2_ctrl, .p_cfg =
		&g_transfer2_cfg, .p_api = &g_transfer_on_dtc };
#if !defined(SSP_SUPPRESS_ISR_g_spi0) && !defined(SSP_SUPPRESS_ISR_SCI8)
SSP_VECTOR_DEFINE_CHAN(sci_spi_rxi_isr, SCI, RXI, 8);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_spi0) && !defined(SSP_SUPPRESS_ISR_SCI8)
SSP_VECTOR_DEFINE_CHAN(sci_spi_txi_isr, SCI, TXI, 8);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_spi0) && !defined(SSP_SUPPRESS_ISR_SCI8)
SSP_VECTOR_DEFINE_CHAN(sci_spi_tei_isr, SCI, TEI, 8);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_spi0) && !defined(SSP_SUPPRESS_ISR_SCI8)
SSP_VECTOR_DEFINE_CHAN(sci_spi_eri_isr, SCI, ERI, 8);
#endif
sci_spi_instance_ctrl_t g_spi0_ctrl;

/** SPI extended configuration */
const sci_spi_extended_cfg g_spi0_cfg_extend = { .bitrate_modulation = false };

const spi_cfg_t g_spi0_cfg = { .channel = 8, .operating_mode = SPI_MODE_MASTER,
		.clk_phase = SPI_CLK_PHASE_EDGE_ODD, .clk_polarity =
				SPI_CLK_POLARITY_LOW,
		.mode_fault = SPI_MODE_FAULT_ERROR_DISABLE, .bit_order =
				SPI_BIT_ORDER_MSB_FIRST, .bitrate = 100000,
#define SYNERGY_NOT_DEFINED (1)             
#if (SYNERGY_NOT_DEFINED == g_transfer2)
		.p_transfer_tx = NULL,
#else
		.p_transfer_tx = &g_transfer2,
#endif
#if (SYNERGY_NOT_DEFINED == g_transfer3)
		.p_transfer_rx = NULL,
#else
		.p_transfer_rx = &g_transfer3,
#endif
#undef SYNERGY_NOT_DEFINED	
		.p_callback = spi_callback, .p_context = (void *) &g_spi0, .rxi_ipl =
				(0), .txi_ipl = (0), .tei_ipl = (0), .eri_ipl = (0), .p_extend =
				&g_spi0_cfg_extend, };
/* Instance structure to use this module. */
const spi_instance_t g_spi0 = { .p_ctrl = &g_spi0_ctrl, .p_cfg = &g_spi0_cfg,
		.p_api = &g_spi_on_sci };
static qspi_instance_ctrl_t g_qspi0_ctrl;
const qspi_cfg_t g_qspi0_cfg = { .p_extend = NULL, .addr_mode =
		QSPI_3BYTE_ADDR_MODE, };
/** This structure encompasses everything that is needed to use an instance of this interface. */
const qspi_instance_t g_qspi0 = { .p_ctrl = &g_qspi0_ctrl,
		.p_cfg = &g_qspi0_cfg, .p_api = &g_qspi_on_qspi, };
TX_EVENT_FLAGS_GROUP SPI_FLAGS;
TX_EVENT_FLAGS_GROUP DataloggerEventFlags;
TX_QUEUE Datalogger_Flash;
static uint8_t queue_memory_Datalogger_Flash[200];
TX_SEMAPHORE DataloggerSemaphore;
TX_MUTEX QSPI_Mutex;
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void Thread_Datalogger_create(void) {
	/* Increment count so we will know the number of ISDE created threads. */
	g_ssp_common_thread_count++;

	/* Initialize each kernel object. */
	UINT err_SPI_FLAGS;
	err_SPI_FLAGS = tx_event_flags_create(&SPI_FLAGS,
			(CHAR *) "SPI_FLAGS Event Flags");
	if (TX_SUCCESS != err_SPI_FLAGS) {
		tx_startup_err_callback(&SPI_FLAGS, 0);
	}
	UINT err_DataloggerEventFlags;
	err_DataloggerEventFlags = tx_event_flags_create(&DataloggerEventFlags,
			(CHAR *) "Datalogger Event Flags");
	if (TX_SUCCESS != err_DataloggerEventFlags) {
		tx_startup_err_callback(&DataloggerEventFlags, 0);
	}
	UINT err_Datalogger_Flash;
	err_Datalogger_Flash = tx_queue_create(&Datalogger_Flash,
			(CHAR *) "Datalogger_Flash Queue", 5,
			&queue_memory_Datalogger_Flash,
			sizeof(queue_memory_Datalogger_Flash));
	if (TX_SUCCESS != err_Datalogger_Flash) {
		tx_startup_err_callback(&Datalogger_Flash, 0);
	}
	UINT err_DataloggerSemaphore;
	err_DataloggerSemaphore = tx_semaphore_create(&DataloggerSemaphore,
			(CHAR *) "Datalogger Semaphore", 0);
	if (TX_SUCCESS != err_DataloggerSemaphore) {
		tx_startup_err_callback(&DataloggerSemaphore, 0);
	}
	UINT err_QSPI_Mutex;
	err_QSPI_Mutex = tx_mutex_create(&QSPI_Mutex, (CHAR *) "QSPI Mutex",
			TX_INHERIT);
	if (TX_SUCCESS != err_QSPI_Mutex) {
		tx_startup_err_callback(&QSPI_Mutex, 0);
	}

	UINT err;
	err = tx_thread_create(&Thread_Datalogger, (CHAR *) "Datalogger Thread",
			Thread_Datalogger_func, (ULONG) NULL, &Thread_Datalogger_stack,
			1024, 1, 1, 1, TX_AUTO_START);
	if (TX_SUCCESS != err) {
		tx_startup_err_callback(&Thread_Datalogger, 0);
	}
}

static void Thread_Datalogger_func(ULONG thread_input) {
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* Initialize common components */
	tx_startup_common_init();

	/* Initialize each module instance. */

	/* Enter user code for this thread. */
	Thread_Datalogger_entry();
}
