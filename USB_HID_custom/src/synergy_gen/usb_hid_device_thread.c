/* generated thread source file - do not edit */
#include "usb_hid_device_thread.h"

TX_THREAD usb_hid_device_thread;
void usb_hid_device_thread_create(void);
static void usb_hid_device_thread_func(ULONG thread_input);
static uint8_t usb_hid_device_thread_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.usb_hid_device_thread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
TX_EVENT_FLAGS_GROUP USB_Flags;
TX_SEMAPHORE USBStatus_Semaphore;
TX_QUEUE USB_Flash;
static uint8_t queue_memory_USB_Flash[20];
TX_EVENT_FLAGS_GROUP UpFirmareEventFlags;
TX_SEMAPHORE g_SemaphoroTcp485;
TX_MUTEX TCP_MUTEX;
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void usb_hid_device_thread_create(void) {
	/* Increment count so we will know the number of ISDE created threads. */
	g_ssp_common_thread_count++;

	/* Initialize each kernel object. */
	UINT err_USB_Flags;
	err_USB_Flags = tx_event_flags_create(&USB_Flags,
			(CHAR *) "USB Event Flag");
	if (TX_SUCCESS != err_USB_Flags) {
		tx_startup_err_callback(&USB_Flags, 0);
	}
	UINT err_USBStatus_Semaphore;
	err_USBStatus_Semaphore = tx_semaphore_create(&USBStatus_Semaphore,
			(CHAR *) "USBStatus", 0);
	if (TX_SUCCESS != err_USBStatus_Semaphore) {
		tx_startup_err_callback(&USBStatus_Semaphore, 0);
	}
	UINT err_USB_Flash;
	err_USB_Flash = tx_queue_create(&USB_Flash, (CHAR *) "USB_Flash Queue", 5,
			&queue_memory_USB_Flash, sizeof(queue_memory_USB_Flash));
	if (TX_SUCCESS != err_USB_Flash) {
		tx_startup_err_callback(&USB_Flash, 0);
	}
	UINT err_UpFirmareEventFlags;
	err_UpFirmareEventFlags = tx_event_flags_create(&UpFirmareEventFlags,
			(CHAR *) "UpFirmare Event Flags");
	if (TX_SUCCESS != err_UpFirmareEventFlags) {
		tx_startup_err_callback(&UpFirmareEventFlags, 0);
	}
	UINT err_g_SemaphoroTcp485;
	err_g_SemaphoroTcp485 = tx_semaphore_create(&g_SemaphoroTcp485,
			(CHAR *) "SemaphoroTcp485", 0);
	if (TX_SUCCESS != err_g_SemaphoroTcp485) {
		tx_startup_err_callback(&g_SemaphoroTcp485, 0);
	}
	UINT err_TCP_MUTEX;
	err_TCP_MUTEX = tx_mutex_create(&TCP_MUTEX, (CHAR *) "Tcp Mutex",
			TX_NO_INHERIT);
	if (TX_SUCCESS != err_TCP_MUTEX) {
		tx_startup_err_callback(&TCP_MUTEX, 0);
	}

	UINT err;
	err = tx_thread_create(&usb_hid_device_thread,
			(CHAR *) "USB HID Device Thread", usb_hid_device_thread_func,
			(ULONG) NULL, &usb_hid_device_thread_stack, 1024, 1, 1, 1,
			TX_AUTO_START);
	if (TX_SUCCESS != err) {
		tx_startup_err_callback(&usb_hid_device_thread, 0);
	}
}

static void usb_hid_device_thread_func(ULONG thread_input) {
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* Initialize common components */
	tx_startup_common_init();

	/* Initialize each module instance. */

	/* Enter user code for this thread. */
	usb_hid_device_thread_entry();
}
