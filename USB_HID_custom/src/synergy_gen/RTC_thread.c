/* generated thread source file - do not edit */
#include "RTC_thread.h"

TX_THREAD RTC_thread;
void RTC_thread_create(void);
static void RTC_thread_func(ULONG thread_input);
static uint8_t RTC_thread_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.RTC_thread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer5) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_IIC1_RXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_IIC1_RXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer5_ctrl;
transfer_info_t g_transfer5_info = { .dest_addr_mode =
		TRANSFER_ADDR_MODE_INCREMENTED, .repeat_area =
		TRANSFER_REPEAT_AREA_DESTINATION, .irq = TRANSFER_IRQ_END, .chain_mode =
		TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_FIXED,
		.size = TRANSFER_SIZE_1_BYTE, .mode = TRANSFER_MODE_NORMAL, .p_dest =
				(void *) NULL, .p_src = (void const *) NULL, .num_blocks = 0,
		.length = 0, };
const transfer_cfg_t g_transfer5_cfg = { .p_info = &g_transfer5_info,
		.activation_source = ELC_EVENT_IIC1_RXI, .auto_enable = false,
		.p_callback = NULL, .p_context = &g_transfer5, .irq_ipl =
				(BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer5 = { .p_ctrl = &g_transfer5_ctrl, .p_cfg =
		&g_transfer5_cfg, .p_api = &g_transfer_on_dtc };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer4) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_IIC1_TXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_IIC1_TXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer4_ctrl;
transfer_info_t g_transfer4_info = { .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED,
		.repeat_area = TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
		.chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode =
				TRANSFER_ADDR_MODE_INCREMENTED, .size = TRANSFER_SIZE_1_BYTE,
		.mode = TRANSFER_MODE_NORMAL, .p_dest = (void *) NULL, .p_src =
				(void const *) NULL, .num_blocks = 0, .length = 0, };
const transfer_cfg_t g_transfer4_cfg = { .p_info = &g_transfer4_info,
		.activation_source = ELC_EVENT_IIC1_TXI, .auto_enable = false,
		.p_callback = NULL, .p_context = &g_transfer4, .irq_ipl =
				(BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer4 = { .p_ctrl = &g_transfer4_ctrl, .p_cfg =
		&g_transfer4_cfg, .p_api = &g_transfer_on_dtc };
#if !defined(SSP_SUPPRESS_ISR_g_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_rxi_isr, IIC, RXI, 1);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_txi_isr, IIC, TXI, 1);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_tei_isr, IIC, TEI, 1);
#endif
#if !defined(SSP_SUPPRESS_ISR_g_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC1)
SSP_VECTOR_DEFINE_CHAN(iic_eri_isr, IIC, ERI, 1);
#endif
riic_instance_ctrl_t g_i2c0_ctrl;
const riic_extended_cfg g_i2c0_extend = { .timeout_mode =
		RIIC_TIMEOUT_MODE_SHORT, };
const i2c_cfg_t g_i2c0_cfg = { .channel = 1, .rate = I2C_RATE_STANDARD, .slave =
		0x32, .addr_mode = I2C_ADDR_MODE_7BIT, .sda_delay = 0,
#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == g_transfer4)
		.p_transfer_tx = NULL,
#else
		.p_transfer_tx = &g_transfer4,
#endif
#if (SYNERGY_NOT_DEFINED == g_transfer5)
		.p_transfer_rx = NULL,
#else
		.p_transfer_rx = &g_transfer5,
#endif
#undef SYNERGY_NOT_DEFINED	
		.p_callback = NULL, .p_context = (void *) &g_i2c0, .rxi_ipl = (3),
		.txi_ipl = (3), .tei_ipl = (3), .eri_ipl = (3), .p_extend =
				&g_i2c0_extend, };
/* Instance structure to use this module. */
const i2c_master_instance_t g_i2c0 = { .p_ctrl = &g_i2c0_ctrl, .p_cfg =
		&g_i2c0_cfg, .p_api = &g_i2c_master_on_riic };
#if (6) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_rtc0) && !defined(SSP_SUPPRESS_ISR_RTC)
SSP_VECTOR_DEFINE( rtc_alarm_isr, RTC, ALARM);
#endif
#endif
#if (6) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_rtc0) && !defined(SSP_SUPPRESS_ISR_RTC)
SSP_VECTOR_DEFINE( rtc_period_isr, RTC, PERIOD);
#endif
#endif
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_rtc0) && !defined(SSP_SUPPRESS_ISR_RTC)
SSP_VECTOR_DEFINE( rtc_carry_isr, RTC, CARRY);
#endif
#endif
rtc_instance_ctrl_t g_rtc0_ctrl;
const rtc_cfg_t g_rtc0_cfg = { .clock_source = RTC_CLOCK_SOURCE_SUBCLK,
		.hw_cfg = true, .error_adjustment_value = 0, .error_adjustment_type =
				RTC_ERROR_ADJUSTMENT_NONE, .p_callback = RTC_callback,
		.p_context = &g_rtc0, .alarm_ipl = (6), .periodic_ipl = (6),
		.carry_ipl = (12), };
/* Instance structure to use this module. */
const rtc_instance_t g_rtc0 = { .p_ctrl = &g_rtc0_ctrl, .p_cfg = &g_rtc0_cfg,
		.p_api = &g_rtc_on_rtc };
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void RTC_thread_create(void) {
	/* Increment count so we will know the number of ISDE created threads. */
	g_ssp_common_thread_count++;

	/* Initialize each kernel object. */

	UINT err;
	err = tx_thread_create(&RTC_thread, (CHAR *) "RTC Thread", RTC_thread_func,
			(ULONG) NULL, &RTC_thread_stack, 1024, 1, 1, 1, TX_AUTO_START);
	if (TX_SUCCESS != err) {
		tx_startup_err_callback(&RTC_thread, 0);
	}
}

static void RTC_thread_func(ULONG thread_input) {
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* Initialize common components */
	tx_startup_common_init();

	/* Initialize each module instance. */

	/* Enter user code for this thread. */
	RTC_thread_entry();
}
