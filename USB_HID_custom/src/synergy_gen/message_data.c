/* generated messaging source file - do not edit */
#include "sf_message.h"
#ifndef SF_MESSAGE_CFG_QUEUE_SIZE
#define SF_MESSAGE_CFG_QUEUE_SIZE (16)
#endif
TX_QUEUE Thread_Datalogger_message_queue;
static uint8_t queue_memory_Thread_Datalogger_message_queue[SF_MESSAGE_CFG_QUEUE_SIZE];
TX_QUEUE usb_hid_device_thread_message_queue;
static uint8_t queue_memory_usb_hid_device_thread_message_queue[SF_MESSAGE_CFG_QUEUE_SIZE];
TX_QUEUE Cellular_thread_message_queue;
static uint8_t queue_memory_Cellular_thread_message_queue[SF_MESSAGE_CFG_QUEUE_SIZE];
TX_QUEUE RTC_thread_message_queue;
static uint8_t queue_memory_RTC_thread_message_queue[SF_MESSAGE_CFG_QUEUE_SIZE];
TX_QUEUE Serial_thread_message_queue;
static uint8_t queue_memory_Serial_thread_message_queue[SF_MESSAGE_CFG_QUEUE_SIZE];
TX_QUEUE MainManagerThread_message_queue;
static uint8_t queue_memory_MainManagerThread_message_queue[SF_MESSAGE_CFG_QUEUE_SIZE];
static sf_message_subscriber_t RTC_thread_message_queue_0_0 = { .p_queue =
		&RTC_thread_message_queue, .instance_range = { .start = 0, .end = 0 } };
static sf_message_subscriber_t Serial_thread_message_queue_0_0 =
		{ .p_queue = &Serial_thread_message_queue, .instance_range = { .start =
				0, .end = 0 } };
static sf_message_subscriber_t Thread_Datalogger_message_queue_0_0 = {
		.p_queue = &Thread_Datalogger_message_queue, .instance_range = {
				.start = 0, .end = 0 } };
static sf_message_subscriber_t usb_hid_device_thread_message_queue_0_0 = {
		.p_queue = &usb_hid_device_thread_message_queue, .instance_range = {
				.start = 0, .end = 0 } };
static sf_message_subscriber_t *gp_group_SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE[] =
		{ &RTC_thread_message_queue_0_0, };
static sf_message_subscriber_list_t g_list_SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE =
		{ .event_class = SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE,
				.number_of_nodes = 1, .pp_subscriber_group =
						gp_group_SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE };
static sf_message_subscriber_t *gp_group_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT[] =
		{ &usb_hid_device_thread_message_queue_0_0, };
static sf_message_subscriber_list_t g_list_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT =
		{ .event_class = SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT,
				.number_of_nodes = 1, .pp_subscriber_group =
						gp_group_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT };
static sf_message_subscriber_t *gp_group_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN[] =
		{ &Serial_thread_message_queue_0_0, };
static sf_message_subscriber_list_t g_list_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN =
		{ .event_class = SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN,
				.number_of_nodes = 1, .pp_subscriber_group =
						gp_group_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN };
static sf_message_subscriber_t *gp_group_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER[] =
		{ &Serial_thread_message_queue_0_0, };
static sf_message_subscriber_list_t g_list_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER =
		{ .event_class = SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER,
				.number_of_nodes = 1, .pp_subscriber_group =
						gp_group_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER };
static sf_message_subscriber_t *gp_group_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN[] =
		{ &Thread_Datalogger_message_queue_0_0, };
static sf_message_subscriber_list_t g_list_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN =
		{ .event_class = SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN,
				.number_of_nodes = 1, .pp_subscriber_group =
						gp_group_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN };
static sf_message_subscriber_t *gp_group_SF_MESSAGE_EVENT_CLASS_FLASHEXTERNAL[] =
		{ &Thread_Datalogger_message_queue_0_0, };
static sf_message_subscriber_list_t g_list_SF_MESSAGE_EVENT_CLASS_FLASHEXTERNAL =
		{ .event_class = SF_MESSAGE_EVENT_CLASS_FLASHEXTERNAL,
				.number_of_nodes = 1, .pp_subscriber_group =
						gp_group_SF_MESSAGE_EVENT_CLASS_FLASHEXTERNAL };
sf_message_subscriber_list_t *p_subscriber_lists[] = {
		&g_list_SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE,
		&g_list_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT,
		&g_list_SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN,
		&g_list_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER,
		&g_list_SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN,
		&g_list_SF_MESSAGE_EVENT_CLASS_FLASHEXTERNAL, NULL };
void g_message_init(void);
void g_message_init(void) {
	tx_queue_create(&Thread_Datalogger_message_queue,
			(CHAR *) "Datalogger Thread Message Queue", 1,
			&queue_memory_Thread_Datalogger_message_queue,
			sizeof(queue_memory_Thread_Datalogger_message_queue));
	tx_queue_create(&usb_hid_device_thread_message_queue,
			(CHAR *) "USB HID Device Thread Message Queue", 1,
			&queue_memory_usb_hid_device_thread_message_queue,
			sizeof(queue_memory_usb_hid_device_thread_message_queue));
	tx_queue_create(&Cellular_thread_message_queue,
			(CHAR *) "Cellular Thread Message Queue", 1,
			&queue_memory_Cellular_thread_message_queue,
			sizeof(queue_memory_Cellular_thread_message_queue));
	tx_queue_create(&RTC_thread_message_queue,
			(CHAR *) "RTC Thread Message Queue", 1,
			&queue_memory_RTC_thread_message_queue,
			sizeof(queue_memory_RTC_thread_message_queue));
	tx_queue_create(&Serial_thread_message_queue,
			(CHAR *) "Serial Thread Message Queue", 1,
			&queue_memory_Serial_thread_message_queue,
			sizeof(queue_memory_Serial_thread_message_queue));
	tx_queue_create(&MainManagerThread_message_queue,
			(CHAR *) "Main Manager Thread Message Queue", 1,
			&queue_memory_MainManagerThread_message_queue,
			sizeof(queue_memory_MainManagerThread_message_queue));
}
