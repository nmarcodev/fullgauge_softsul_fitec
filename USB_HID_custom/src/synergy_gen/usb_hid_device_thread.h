/* generated thread header file - do not edit */
#ifndef USB_HID_DEVICE_THREAD_H_
#define USB_HID_DEVICE_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void usb_hid_device_thread_entry(void);
#else
extern void usb_hid_device_thread_entry(void);
#endif
#ifdef __cplusplus
extern "C" {
#endif
extern TX_EVENT_FLAGS_GROUP USB_Flags;
extern TX_SEMAPHORE USBStatus_Semaphore;
extern TX_QUEUE USB_Flash;
extern TX_EVENT_FLAGS_GROUP UpFirmareEventFlags;
extern TX_SEMAPHORE g_SemaphoroTcp485;
extern TX_MUTEX TCP_MUTEX;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* USB_HID_DEVICE_THREAD_H_ */
