/*
 * qspi_programmer.c
 *
 *  Created on: May 14, 2018
 *      Author: LAfonso01
 */
#include "common_data.h"
#include "qspi_programmer.h"
#include "app_info.h"

/**********************************************************************************************************************
 * @brief  Erase serial flash
 *
 *
 **********************************************************************************************************************/
ssp_err_t spi_flash_erase0(const qspi_instance_t *p_qspi,uint32_t address, uint32_t block_size)
{
    ssp_err_t status = SSP_ERR_NOT_OPEN;
    bool wip = false;
    //uint32_t qspi_address = BSP_PRV_QSPI_DEVICE_PHYSICAL_ADDRESS;

    status = p_qspi->p_api->open(p_qspi->p_ctrl, p_qspi->p_cfg);
    if (status == SSP_SUCCESS)
    {
      //  for(uint8_t i = 0;i < block_size ;i++)
      //  {
            /* erase a 64K Block */
            status = p_qspi->p_api->erase(p_qspi->p_ctrl,(uint8_t *)address,block_size);
            if (status != SSP_SUCCESS)
            {
                p_qspi->p_api->close(p_qspi->p_ctrl);
                return status;
            }
            /* wait while it is busy*/
            wip = true;
            do{
                status = p_qspi->p_api->statusGet(p_qspi->p_ctrl,&wip);
            }while(wip);
            tx_thread_sleep(1);
       //     qspi_address +=  0x10000;
      //  }
    }
    p_qspi->p_api->close(p_qspi->p_ctrl);
    return status;
}

/**********************************************************************************************************************
 * @brief  Program extern serial flash. It assumes a 256 byte block.
 *
 *
 **********************************************************************************************************************/
ssp_err_t spi_flash_program(const qspi_instance_t *p_qspi, uint32_t spi_address, uint8_t *p_data, uint32_t size)
{
    ssp_err_t status = SSP_ERR_NOT_OPEN;
    bool            wip = false;
    uint32_t        local_buffer_index = 0;
    ULONG           remained_length;
    uint8_t * p_memory;

    status = p_qspi->p_api->open(p_qspi->p_ctrl, p_qspi->p_cfg);
    if (status == SSP_SUCCESS)
    {
        spi_address = spi_address + BSP_PRV_QSPI_DEVICE_PHYSICAL_ADDRESS;
        remained_length = (size - (size/256)*256);
        for(uint8_t i = 0;i < (size/256) ;i++)
        {
            p_memory = p_data + local_buffer_index;
            status = p_qspi->p_api->pageProgram(p_qspi->p_ctrl,(uint8_t *)spi_address,p_memory,256);
            if (status != SSP_SUCCESS)
            {
                p_qspi->p_api->close(p_qspi->p_ctrl);
                return status;
            }
            wip = true;
            do{
                p_qspi->p_api->statusGet(p_qspi->p_ctrl,&wip);
            }while(wip);
            local_buffer_index = local_buffer_index + 256;
            spi_address += 256;
        }
        if(remained_length > 0)
        {
            p_memory = p_data + local_buffer_index;
            status = p_qspi->p_api->pageProgram(p_qspi->p_ctrl,(uint8_t *)spi_address,p_memory,remained_length);
            if (status != SSP_SUCCESS)
            {
                p_qspi->p_api->close(p_qspi->p_ctrl);
                return status;
            }
            wip = true;
            do{
                p_qspi->p_api->statusGet(p_qspi->p_ctrl,&wip);
            }while(wip);
            spi_address += remained_length;
        }
        p_qspi->p_api->close(p_qspi->p_ctrl);
    }
    return status;
}
