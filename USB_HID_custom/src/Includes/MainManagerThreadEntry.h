/*
 * MainManagerThreadEntry.h
 *
 *  Created on: 27 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_MAINMANAGERTHREADENTRY_H_
#define INCLUDES_MAINMANAGERTHREADENTRY_H_

#include  "Includes/ApplicationLedsStatus.h"

typedef enum
{
    MM_FLAGS_NONE               = 0,
    // eventos
    MM_ETH_TYPE                 = 0x00000001,
    MM_ETH_WAITING_IP           = 0x00000002,
    MM_ETH_CONNECTED            = 0x00000004,
    MM_ETH_NOT_CONNECTED        = 0x00000008,

    MM_WIFI_TYPE                = 0x00000010,
    MM_WIFI_WAIT                = 0x00000020,
    MM_WIFI_CONNECTED           = 0x00000040,
    MM_WIFI_NOT_CONNECTED       = 0x00000080,

    MM_TIMER_IO                 = 0x00000100,
    MM_ERRO_PWD                 = 0x00000200,
    MM_ERRO_IP                  = 0x00000400,
    MM_ERRO_STATIC_IP           = 0x00000800,

    MM_CONNECTED                = 0x00001000,
    MM_DISCONNECTED             = 0x00002000,
    MM_CONNECTED_SITRAD         = 0x00004000,
    MM_DISCONNECTED_SITRAD      = 0x00008000,

    MM_READY_TO_CONNECT         = 0x00010000,
    MM_CONNECT_AND_READY        = 0x00020000,
    MM_NOT_READY                = 0x00040000,
    MM_SCAN_NET                 = 0x00080000,

    MM_S_WIFI_EXCELLENT         = 0x00100000, //VERDE
    MM_S_WIFI_GOOD              = 0x00200000, //AMARELO
    MM_S_WIFI_POOR              = 0x00400000, //VINHO_ROSE
    MM_S_WIFI_VERY_POOR         = 0x00800000, //VERMELHO

    MM_STATUS_WIFI              = 0x01000000,
    MM_S_WIFI_NO_SIGNAL         = 0x02000000,

    MM_WIFI_CLIENT              = 0x04000000,
    MM_WIFI_SERVER              = 0x08000000,

    MM_STATUS_OTA               = 0x10000000,
    MM_RESTORE_FACTORY          = 0x20000000,
    MM_FAC_MODE                 = 0x40000000,
    MM_FAC_TEST_LEDs            = 0x80000000,

    MM_ALL                      = 0xFFFFFFFF,
    MM_SIGNAL_WIFI              = 0x00000000,

}_MAIN_MANAGER_FLAGS;

typedef enum
{
    STATE_IDLE,

    STATE_WIFI_STARTING,
    STATE_WIFI_WAIT_LINK,
    STATE_WIFI_CONNECTING,
    STATE_WIFI_CONNECTED,
    STATE_WIFI_DISCONNECTED,

    STATE_ETH_STARTING,
    STATE_ETH_WAIT_LINK,
    STATE_ETH_CONNECTING,
    STATE_ETH_CONNECTED,
    STATE_ETH_DISCONNECTED,

}_MAIN_MANAGER_STATES;

//GSM Defines
#define MM_S_GSM_EXCELLENT		MM_S_WIFI_EXCELLENT
#define MM_S_GSM_GOOD			MM_S_WIFI_GOOD
#define MM_S_GSM_POOR			MM_S_WIFI_POOR
#define MM_S_GSM_VERY_POOR		MM_S_WIFI_VERY_POOR

#define MM_GSM_TYPE 			MM_WIFI_TYPE
#define MM_STATUS_GSM			MM_STATUS_WIFI
#define MM_ERRO_SIM_CARD		MM_ERRO_IP
#define MM_ERRO_NO_AUTH			MM_ERRO_STATIC_IP
#define MM_S_GSM_NO_SIGNAL		MM_S_WIFI_NO_SIGNAL

//----------------------------------------------------//

extern TX_EVENT_FLAGS_GROUP g_main_event_flags;

extern void ThreadMainManagerSetFlag(_MAIN_MANAGER_FLAGS EventFlag);
extern void ThreadMainManagerSetLedColor(uint8_t LedColorTest);
extern bool ThreadMainManagerGetStatusButton(void);

#endif /* INCLUDES_MAINMANAGERTHREADENTRY_H_ */
