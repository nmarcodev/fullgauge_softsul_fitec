/*
 * messages.h
 *
 *  Created on: 18 de jun de 2018
 *      Author: felipe.pohren
 */

#ifndef __MESSAGES_H_
#define __MESSAGES_H_

#include "Includes/DateTime.h"
#include "Includes/Eth_thread_entry.h"
#include "Includes/UdpTcpTypes.h"
//#include "DataManager_thread_entry.h"

//#include "Includes/Serial_Thread_entry.h"
//#include "nx_api.h"

//*************************************************************************************************
// estruturas de mensagens enviadas via queue


//*************************************************************************************************
// estruturas de mensagens enviadas via message framework

// Declaração do payload de mensagem Datalogger (Salvar na flash externa)
typedef struct
{
    sf_message_header_t  header;
    uint16_t SizeData;
    uint16_t FragFile; ///> fragmento do arquivo
    uint16_t CompFile; ///> Arquivo completo
    uint8_t *DataFlash;
}_FLASHEXTERNAL_PAYLOAD;

// Declaração do payload de mensagem de data e hora
typedef struct
{
    sf_message_header_t  header;
    bool SetDateTime;
    uint8_t DateTimeValues[DATE_TIME_SIZE];
}_DATE_TIME_MESSAGE_PAYLOAD;

// Declaração do payload de mensagem da tabela de configurações
typedef struct
{
    sf_message_header_t  header;
    // ponteiro para a tabela de parametros
    //_CFG_TABLE* table;

}_PARAM_TABLE_MESSAGE_PAYLOAD;


//typedef struct
//{
//    sf_message_header_t  header;
//    // ponteiro para a tabela de parametros
//    //_CFG_TABLE* table;
//    NX_PACKET *NxPacketTCP;
//    _IPSTRUCT *PonterIpStruct;
//    uint8_t NumCtrl;
//
//}_DATA_IP_PAYLOAD_MESSAGE_PAYLOAD;

//typedef struct
//{
//    sf_message_header_t  header;
//    // ponteiro para a tabela de parametros
//    NX_PACKET *NxPacketTCP;
//    _IPSTRUCT *PointerIpStruct;
//    uint8_t NumCtrl;
//}_DATA_IP_PAYLOAD_WIFI;

//typedef struct
//{
//    sf_message_header_t  header;
//    // ponteiro para a tabela de parametros
//    NX_PACKET *NxPacketTCP;
//    _IPSTRUCT *PointerIpStruct;
//    uint8_t NumCtrl;
//}_DATA_IP_PAYLOAD_WIFI_OUT;

/*Resposta com datamanagercontrol*/
typedef struct
{
    sf_message_header_t  header;
    _PACKET_UDP *PacketUdp;
    _BUFFER_TCP *BufferTcp;
    uint8_t NumCtrlTxPayload;
}_DATA_TX_PAYLOAD_MESSAGE_PAYLOAD;

//Serial data payload declaration
typedef struct{
    sf_message_header_t header;
    //data_byte rx_data;
    uint32_t timestamp;
}_SERIAL_RX_DATA_PAYLOAD;

typedef struct{
    sf_message_header_t header;
    uint16_t *pointer;
    uint32_t num_of_bytes;
}_SERIAL_DATA_OUT_PAYLOAD;

typedef struct{
    sf_message_header_t header;
    uint16_t *pointer;
    uint32_t num_of_bytes;
}_SERIAL_DATA_IN_PAYLOAD;

typedef struct{
    sf_message_header_t header;
    uint16_t *pointer;
    uint32_t num_of_bytes;
}_S485_DATALOGGER_PAYLOAD;

typedef struct{
    sf_message_header_t header;
    uint16_t *pointer;
    uint32_t num_of_bytes;
}_S485_DATALOGGER_IN_PAYLOAD;


typedef enum
{
    MESSAGE_TYPE_ETH,
    MESSAGE_TYPE_ETH_VOID,
    MESSAGE_TYPE_WIFI,
    MESSAGE_TYPE_WIFI_VOID,

}_MESSAGE_TYPE;


#endif /* __MESSAGES_H_ */
