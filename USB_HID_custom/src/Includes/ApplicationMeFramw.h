/*
 * ApplicationMeFramw.h
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONMEFRAMW_H_
#define INCLUDES_APPLICATIONMEFRAMW_H_

#include "Includes/ApplicationUSB.h"

extern void RtcPost(_USB_CONTROL *USBData);
extern void SerialPost(_USB_CONTROL *USBData);
extern bool PendSerial485(_USB_CONTROL *USBData);

#endif /* INCLUDES_APPLICATIONMEFRAMW_H_ */
