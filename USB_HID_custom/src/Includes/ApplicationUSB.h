/*
 * usb.h
 *
 *  Created on: 27 de mai de 2021
 *      Author: rhenanbezerra
 */

#ifndef INCLUDES_APPLICATIONUSB_H_
#define INCLUDES_APPLICATIONUSB_H_

#include "Includes/TypeDefs.h"
#include "messages.h"

#define SIZE_PAYLOAD_USB        898    ///> Tamanho maximo de Payload da transmissao usb
#define SIZE_HEADER_USB         2    ///> Tamanho de Header USB (Comando + comprimento do pacote)

//Lista de comandos especiais

#define CMD_LOGIN_CONVERSOR			 	0x01
#define CMD_REINICIA_CONVERSOR			0x02
#define CMD_CONF_FABRICA 				0x03
#define CMD_ATUALIZA_CONF               0x04
#define CMD_CMD_REQ_CONF 				0x05
#define CMD_DESC_CONVERSOR 				0x06
#define CMD_LIMPAR_DATALOGGER 			0x07
#define CMD_LEITURA_LOG 				0x08
#define CMD_AJUSTE_DATA_HORA 			0x09
#define	CMD_STATUS_CONVERSOR 			0x0A
#define CMD_ATUALIZACAO_FW 				0x0B
#define CMD_TESTE_FABRICA 				0x0C

typedef union{
    uint8_t      HeaderPayload[SIZE_HEADER_USB + SIZE_PAYLOAD_USB];
    struct{
        uint8_t      Header[SIZE_HEADER_USB];
        uint8_t      Payload[SIZE_PAYLOAD_USB];
    }HP;
}_BUFFER_USB;


typedef enum
{
   USB_NONE,
   // eventos
   USB_SEND,
   USB_REQUESTED,
   USB_REQUESTED_CONFIG,
   USB_REQUESTED_485,
   USB_REQUESTED_CONF,
   USB_RELISTEN,

}_USB_RETURN;

typedef struct
{
	UCHAR BufferTx[SIZE_HEADER_USB + SIZE_PAYLOAD_USB];
	UCHAR BufferRx[SIZE_HEADER_USB + SIZE_PAYLOAD_USB];
	
    uint8_t                             TimerBigFrame;           ///> TimerBigFrameTCP
    uint8_t                             TimerRestartModule;             ///> TimerResetConv ... Contagem para resetar o modulo conversor.
    uint8_t                             TimerOTACheckCRC;           ///> TimerOTACheckCRC ... Contagem para verificar o CRC da flash.
    uint8_t                             TimerTimeOutOTA;            ///> TimeOut para atualizar o firmware.
    uint8_t                             TimerRestoreFactory;        ///> Timer responsavel por determinar a hora de restaurar as confs de fabrica.
    uint8_t                             TimerConnectSitrad;         ///> Timer para timeout sitrad
    uint8_t                             TimerInactivity;            ///> Timer para monitoramento de inatividade
    bool                                FlagOTAFinished;            ///> Utilziado para sinalizar que uma atualização esta  finalizando.
    bool                                FlagOTARun;                 ///> Utilizado para sinalizar que a atualizacao esta em curso.
    bool                                FlagFAC;                    ///> Utilizado para determinar se está em fac ou nao.
    bool                                FlagConnectSitrad;  ///>Utilzado para verificar se o Sitrad está conectado
    bool                                BigFrame;                ///> Utilizado em USBPayload

    uint8_t                             DateTimeValues[DATE_TIME_SIZE]; ///> Utilizado para atualizar o RTC

    sf_message_header_t                 *Header;
    _MESSAGE_FRAMEWORK_CFG              MessageCfg;
    _FLASHEXTERNAL_PAYLOAD              *FlashExternalPaylod;
	_BUFFER_USB	BufferUSB;
	_DATE_TIME_MESSAGE_PAYLOAD 			*DateTimeMessagePayload;    ///> Messagem Payload - Atualiza o RTC Envio
	_SERIAL_DATA_IN_PAYLOAD             *SerialDataInPayload;       ///> Messagem Payload - Serial 485 Recebimento
    _SERIAL_DATA_OUT_PAYLOAD            SerialDataOutPayload;       ///> Messagem Payload - Serial 485 Envio
}_USB_CONTROL;

typedef enum
{

    FAC_TEST_START          = 0,

    FAC_TEST_SUPER_CAP_ON,  // 1
    FAC_TEST_SUPER_CAP_OFF, // 2
    FAC_TEST_QSPI,          // 3

    FAC_TEST_LEDS,          // 4
    FAC_TEST_485,           // 5
    FAC_TEST_WIFI,          // 6
    FAC_TEST_MAC,           // 7
    FAC_TEST_KEY,           // 8

    FAC_TEST_END,

    FAC_TEST_DO_FAC         = 0x66, //Esse id coloca o modulo em modo FAC.
    FAC_TEST_WAIT_STEP,     // deve ser o penultimo
    FAC_TEST_NONE           = 0xFF,

}_FACTORY_TEST_STEP;

typedef struct
{
    unsigned    FlagStartUdpTcp:1,
                FlagSitradConnected:1,
                StateSitrad:1,
                FlagNetConnected:1,
                FlagNetScan:1,
                FlagNumAttempts:1,
                FlagFree01:1,
                FlagFree02:1,
                FlagFree03:1,

                FlagErrorPassWrd:1,
                FlagErrorIp:1,
                FlagErrorInvalidStaticIp:1,
                FlagErrorFree03:1,
                FlagErrorFree04:1,
                FlagErrorFree05:1,
                FlagErrorFree06:1,
                FlagErrorFree07:1;
}_STATUS_FLAGS;

_USB_CONTROL USBControl;

_USB_RETURN SendPacketUSB(_USB_CONTROL *USBData, uint16_t len);

#endif /* INCLUDES_APPLICATIONUSB_H_ */
