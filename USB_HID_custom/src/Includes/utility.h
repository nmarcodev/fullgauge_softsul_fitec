/*
 * utility.h
 *
 *  Created on: July 27, 2017
 *      Author: Xianghui Wang
 */

#ifndef INCLUDES_UTILITY_H_
#define INCLUDES_UTILITY_H_

#include "Includes/console_config.h"
//#include "Includes/wifi_app_thread_entry.h"

#define NUM_MAX_NETS_MEM                1
#define NUM_MAX_TIMES_DEFAULT           2
#define NUM_ATTEMPTS                    2
#define WIFI_KEY_LENGTH                 64

#define SF_GSM_SITRAD_ID_SIZE 						(10U)
#define SF_GSM_RADIO_BASE_STATION_SIZE 				(8U)
#define SF_GSM_EXS82_VERSION_SIZE					(20U)
#define SF_GSM_DEVICE_IMEI_SIZE						(20U)
#define SF_GSM_APN_NAME_SIZE						(100U)
#define SF_GSM_APN_USER_SIZE						(30U)
#define SF_GSM_APN_PASSWORD_SIZE					(30U)


//typedef struct
//{
//    uint8_t *ssid;
//    uint8_t *pwd;
//}wifi_ap_info_t;

//typedef struct
//{
//    uint8_t *ap_ssid;
//    uint8_t *ap_pwd;
//}wifi_queue_payload_t;

//typedef struct
//{
//    uint8_t ValCod;//1 -> A5
//    uint8_t DateTimeValues[DATE_TIME_SIZE];//6
//    uint8_t ApSsid[SF_WIFI_SSID_LENGTH+1];//32+1
//    uint8_t ApPwd[WIFI_KEY_LENGTH];//32
//}_WIFI_AP_INFO; //total 71 bytes

/** Module Operation Mode */
//typedef enum
//{
//    MODE_AP         = 0,        ///< Operando em modo AP, onde eh provedor de Acesso e de DHCP.
//    MODE_CLIENT     = 1,        ///< Operando em modo Cliente. Neste modo, o equipamento eh cliente.
//}_OP_MODE_WIFI;

//typedef struct
//{
//    uint8_t ValCod;//1 -> A5
//    uint8_t ApSsid[SF_WIFI_SSID_LENGTH+1];//32+1
//    uint8_t ApPwd[WIFI_KEY_LENGTH];//64
//    uint8_t SecurityType;//1
//}_NET_WIFI_CONF;

//typedef struct
//{
//    uint8_t ValCod;//1 -> A6
//    uint8_t ApSsid[SF_WIFI_SSID_LENGTH+1];//32+1
//    uint8_t ApPwd[WIFI_KEY_LENGTH];//64
//}_NET_WIFI_CONF_AP;

typedef struct NetifStaticMode
{
    uint32_t AddIP;
    uint32_t AddMask;
    uint32_t AddGw;
    uint32_t AddDNS;    ///> DNS Primario
    uint32_t AddDNS2;   ///> DNS Secundario
}_NET_STATIC_MODE;

//typedef struct
//{
//    uint8_t                     ValCod; //1 -> A6
//    uint8_t                     Ssid[SF_WIFI_SSID_LENGTH + 1];  ///< SSID //33
//    uint8_t                     Key[WIFI_KEY_LENGTH];           ///< Pre-shared key //64
//    uint8_t                     Channel;                        ///< 1 ao 13 RF Channel number //1
//    sf_wifi_security_type_t     Security;                       ///< Security type //1
//    sf_wifi_encryption_type_t   Encryption;                     ///< Encryption type  //1
//    _NET_STATIC_MODE            ApStaticIP;                     ///> End Ip em Modo AP //32*4 = 128
//    uint32_t                    StartIpAddList;                 ///< Inicio da faixa de IP //32
//    uint32_t                    EndIpAddList;                   ///< Fim da faixa de IP //32
//}_WIFI_AP_MODE_CONF; //

/** GSM Structs */
typedef struct
{
	uint8_t StatusSitrad;										///< Status Sitrad // 1 Byte
	uint8_t IDStrad[SF_GSM_SITRAD_ID_SIZE];					    ///< ID Sitrad // 10 Bytes

}_SITRAD_CONF;

typedef struct
{
	uint8_t StatusCellularNet;									///< Status Cellular Net // 1 Byte
	uint8_t RadioBaseStation[SF_GSM_RADIO_BASE_STATION_SIZE];   ///< Radio Base Station // 8 Bytes

}_CELLULAR_CONF;

typedef struct
{
	uint8_t EXS82Version[SF_GSM_EXS82_VERSION_SIZE];			///< EXS82 Version // 20 Bytes
	uint8_t DeviceIMEI[SF_GSM_DEVICE_IMEI_SIZE];				///< Device IMEI // 20 Bytes

}_DEVICE_CONF;

typedef enum
{
    AUT            = 0,    ///< Automatic
    MAN        	   = 1,    ///< Manual
}_APN_MODE_TYPE;

typedef struct
{
	_APN_MODE_TYPE ModeAPN;										///< APN Mode // 1 Byte
	uint8_t CurrentAPN[SF_GSM_APN_NAME_SIZE];					///< Current APN // 100 Bytes
	uint8_t DefaultAPN[SF_GSM_APN_NAME_SIZE];					///< Default APN // 100 Bytes
	uint8_t AlternativeAPN[SF_GSM_APN_NAME_SIZE];				///< Alternative APN // 100 Bytes
	uint8_t ManualAPN[SF_GSM_APN_NAME_SIZE];					///< Manual APN // 100 Bytes
	uint8_t UserAPN[SF_GSM_APN_USER_SIZE];						///< User APN // 30 Bytes
	uint8_t PassWordAPN[SF_GSM_APN_PASSWORD_SIZE];				///< Password APN // 30 Bytes

}_APN_CONF;

/** Network interface types **/
typedef enum
{
    WIFI            = 0,    ///< Wi-Fi
    ETHERNET        = 1,    ///< Ethernet
}_INTERFACE_TYPE;

/** IP Address Mode */
typedef enum
{
    MODE_DHCP           = 0,        ///< Obtain address via DHCP
    MODE_STATIC         = 1,        ///< Static IP address
}_ADDR_MODE;


typedef struct
{
    uint8_t             CodMagic;           ///> Numero coringa para validar configuracao //1 Bytes
    _INTERFACE_TYPE     InterfaceType;      ///> Wifi ou Eth //1 Bytes
    //_OP_MODE_WIFI       OpModeWifi;         ///> Modo de Operacao em Wifi (AP ou Client) //1 Bytes
    uint8_t             FlagDNS;            ///> Flag que verifica se tem DNS (0-Nao, 1-Sim)// 1Byte
    _ADDR_MODE          AddrMode;           ///> Configura do modo (0-Dhcp, 1-Static)// 1Byte
    uint8_t             HiddenNet;          ///> Flag da rede -> caso oculta
    _INTERFACE_TYPE     OldInterfaceType;   ///> Wifi ou Eth  Valor antes do reset //1 Bytes
}_CONF_NET_BASIC;

//typedef struct
//{
//    uint8_t                     StatusRet;
//    uint8_t                     Index;
//    _WIFI_STATE                 WifiState;
//    uint8_t                     TimesCounter; //Contador de Tentativas de conexao
//    uint8_t                     *ap_ssid;
//    uint8_t                     *ap_pwd;
//    sf_wifi_interface_mode_t    mode;
//    uint8_t                     channel;
//    sf_wifi_security_type_t     security;
//    sf_wifi_encryption_type_t   encryption;
//}_WIFI_QUEUE_PAYLOAD;

typedef struct
{
    //_ETH_STATE                  EthState;

}_ETH_QUEUE_PAYLOAD;

void print_to_console(char* msg);   /* print user information to console */

#define APP_ERR_TRAP(a)     if(a) {__asm("BKPT #0\n");} /* trap the error location */
#define BUFFER_SIZE (64)    /* assume SSID and password are 64 character or less */
#define MAX_RETRY_CNT       (3)
#endif /* INCLUDES_UTILITY_H_ */
