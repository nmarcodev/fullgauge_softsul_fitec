/*
 * EepromMacAddress.h
 *
 *  Created on: 24 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_EEPROMMACADDRESS_H_
#define INCLUDES_EEPROMMACADDRESS_H_

#define CS_EEPROM      IOPORT_PORT_01_PIN_07

///> Microchip EEPROM GET MAC_ADDRESS 25AA02E48
#define EEPROM_READ    0x03 ///> Read data from memory array beginning at selected address
#define EEPROM_WRITE   0x02 ///>Write data to memory array beginning at selected address
#define EEPROM_WRDI    0x04 ///>Reset the write enable latch (disable write operations)
#define EEPROM_WREN    0x06 ///>Set the write enable latch (enable write operations)
#define EEPROM_RDSR    0x05 ///>Read STATUS register
#define EEPROM_WRSR    0x01 ///>Write STATUS register
#define EEPROM_ADDR    0xFA ///>Posicao de memoria da eeprom onde inicia o MAC_ADDRESS

#endif /* INCLUDES_EEPROMMACADDRESS_H_ */
