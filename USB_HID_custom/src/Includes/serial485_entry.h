/*
 * serial485_entry.h
 */

#ifndef INCLUDES_SERIAL485_ENTRY_H_
#define INCLUDES_SERIAL485_ENTRY_H_

void print485(char * mensagem, uint8_t mensagemLen);
void sendWaitResponse485(char * sendMSG,uint8_t sendLen ,  char * recvMSG, uint8_t recvLen );
#endif /* INCLUDES_SERIAL485_ENTRY_H_ */
