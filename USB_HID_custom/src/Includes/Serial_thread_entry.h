/*
 * SerialData.h
 *
 *  Created on: 14 de jan de 2019
 *      Author: rafael.macedo
 */

#ifndef INCLUDES_SERIALDATA_H_
#define INCLUDES_SERIALDATA_H_

#include "common_data.h" // include obrigatório
//#include "Includes/TypeDefs.h"
//#include "r_uart_api.h"
//#include "stdint.h"
#include "Define_IOs.h"

#define SYSTEM_CLOCK        240000000U

#define BROADCAST           248

#define MAX_BUF_RX_LEN      512
#define MAX_BUF_TX_485      MAX_BUF_RX_LEN/2

#define DELAY_TURN_OFF_LED  30  ///> Utilzado para fazer uma animacao do led de envio

#define MAX_OF_SERIAL_RETRYS 2
typedef union
{
    // tempo de 3.5 caracteres para detecção de início e fim de frame
    uint16_t Mb_35_charTime_US;
    // tempo de 1.5 caracteres para time out intra frame
    uint16_t Mb_15_charTime_US;

   // uart_cfg_t uart_cfg;

    uint8_t* BufferDisplay;
}_SERIAL_CONTROL;


typedef union
{
    uint16_t data_word;
    struct
    {
       uint8_t data_lsb;
       uint8_t data_msb;
    }data_t;

}data_byte;

typedef struct
{
    uint8_t     RxDevice[MAX_BUF_RX_LEN];
    uint16_t    IndexPtr;
    uint32_t    timestamp;
    uint32_t    time_elapsed_min;
    uint32_t    time_elapsed_max;

}_SERIAL_RECV;

typedef enum
{
    UART_0 = 0,

}_UART_NAMES;

typedef enum
{
    SERIAL_FLAGS_NONE       = 0,
    // eventos
    SERIAL_MSFW             = 0x00000001,
    SERIAL_IN_RX            = 0x00000002,
    SERIAL_OUT              = 0x00000004,
    SERIAL_OUT_TX           = 0x00000008,
    SERIAL_RCV_OK           = 0x00000010,
    SERIAL_OUT_TX_VOID      = 0x00000020,
    SERIAL_FREE2            = 0x00000040,
    SERIAL_TEST_485         = 0x00000080,
    SERIAL_ALL              = 0x000000FF,
}_SERIAL_FLAGS;

typedef enum
{
    SERIAL_NONE  = 0,
    SERIAL_POST_TRUE,
    SERIAL_POST_VOID,
}_SERIAL_RETURN;

//extern _FG_ERR SerialConfigure(_SERIAL_CONTROL *SerialCtrl, _UART_NAMES UartNames);
extern TX_QUEUE QueueTest485;
extern TX_EVENT_FLAGS_GROUP EventFlagsSerialTestF;

//TODO FITEC Serial funcs add para teste
void Force_Serial_RCV_OK();
#endif /* INCLUDES_SERIALDATA_H_ */
