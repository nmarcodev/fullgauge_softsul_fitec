/*
 * console_config.h
 *
 *  Created on: Dec 10, 2017
 *      Author: Rajkumar.Thiagarajan
 */

#ifndef INCLUDES_CONSOLE_CONFIG_H_
#define INCLUDES_CONSOLE_CONFIG_H_

//#include "Includes/TypeDefs.h"
#include "common_data.h" // include obrigatório

#define DEFAULT_APN_NAME            "internet"
#define DEFAULT_CONTEXT_ID          (1)
#define DEFAULT_PDP_TYPE            (SF_CELLULAR_PDP_TYPE_IP)


///** Network interface types */
//typedef enum
//{
//    NONE_INTEFACE = 0,
//    ETHERNET = 1,         ///< Ethernet
//    WIFI    ,                 ///< Wi-Fi
//    EXIT_NET_MENU,
//    NETIF_MAX,              ///< Maximum index, used to size arrays
//}e_netif;

///** Module Operation Mode */
//typedef enum
//{
//    MODE_NONE       = 0x00,
//    MODE_AP         = 0xAA,        ///< Operando em modo AP, onde eh provedor de Acesso e de DHCP.
//    MODE_CLIENT     = 0xAF,        ///< Operando em modo Cliente. Neste modo, o equipamento eh cliente.
//}_OP_MODE;

/** IP Address Mode */
enum e_addr_mode
{
    IOTKIT_ADDR_MODE_DHCP,  ///< Obtain address via DHCP
    IOTKIT_ADDR_MODE_STATIC,///< Static IP address
    IOTKIT_ADDR_MODE_NOTCONFIG, ///< Mode not selected/configure
};

typedef enum
{
    STATE_NETIF_SELECTION = 1,       ///< Selection of Network Interface
    STATE_WIFI_CONFIG,               ///< Wi-Fi Configuration
    STATE_CELLULAR_CONFIG,           ///< Cellular Configuration
    STATE_ADDR_MODE_CONFIG,          ///< IP address mode Configuration Menu
    STATE_CONFIG_EXIT,               ///< Exit from the state machine
}e_config_state;

typedef enum
{
    CONF_CONV = 0,
    NET_CONFIG,
    MEMORIA_FLASH,
    VIEW_IP_ETH,
    VIEW_IP_WIFI,
    RESTART_MODULE,
    MAIN_CONFIG_EXIT,
}e_main_cfg_state;

typedef enum
{
    FLASH_PREVIEW = 1,      ///< VISUALIZAR REDES SALVAS
    FLASH_REMOVE,           ///< REMOVER ALGUMA REDE SALVA
    FLASH_CLEAR,            ///< LIMPAR FLASH

}_CONSOLE_FLASH;

typedef enum
{
    AWS = 1,
    EXIT_IOT_MENU,
    CLOUD_MAX,
}e_cloud_provider_type;

typedef enum
{
    CLOUD_CONFIG_ENTER,
    CLOUD_CONFIG_EXIT = 1,
}e_cloud_cfg_state;

typedef struct aws_cfg_info_t
{
    uint8_t endp_address[128];
    uint8_t thing_name[64];
}aws_cfg_info;

typedef struct aws_cert_cfg_t
{
    unsigned int rootCA_len;
    unsigned int devCert_len;
    unsigned int priKey_len;
}aws_cert_cfg;

typedef struct iot_input_cfg
{
    uint8_t iotserv_valid;
    aws_cfg_info aws_info;
    aws_cert_cfg cert_info;
}iot_input_cfg_t;

typedef struct netif_static_mode
{
    ULONG address;
    ULONG mask;
    ULONG gw;
    ULONG dns;
}netif_static_mode_t;

typedef struct net_input_cfg
{
    uint8_t netif_valid;
    char netif_select[8];
    uint8_t netif_addr_mode;
    netif_static_mode_t netif_static;
    //sf_wifi_provisioning_t wifi_prov;
  //  sf_cellular_provisioning_t cell_prov;
}net_input_cfg_t;

typedef enum
{
    AT_CMD_STRING,             ///< AT command string
    AT_CMD_RESP_STRING,        ///< AT command response string
    AT_CMD_RESP_WAIT_TIME,     ///< AT command response wait time
    AT_CMD_RETRY_COUNT,        ///< AT command retry count
    AT_CMD_RETRY_DELAY,        ///< AT command retry delay
    AT_CMD_SAVE                ///< AT command save
}at_cmd_save_stage_t;

typedef struct
{
    char    cmd[128];          ///< Command buffer to save AT command
    char    resp[128];         ///< AT command response
    uint8_t retry_cnt;         ///< Retry count
    uint16_t retry_delay;       ///< AT command retry delay
    uint32_t resp_waittime;    ///< AT command response wait time in milliseconds
}at_cmd_t;

typedef enum
{
    USA_ATT = 1,
    IND_VODAFONE,
    SINGTEL_NBIOT,
    SINGTEL_CATM1,
    EUR_VODAFONE,
    FAREASTONE,
    CHUNGHWA_TELECOM,
    CAN_BELL,
    MAX_CELL_CARRIERS,
}cell_prequalified_list_t;

/* Info type IDs */
typedef enum
{
    NUM_NETS,
    NUM_TIMES, //Numero de
}_INDEX_NUM_NETS;

#endif /* INCLUDES_CONSOLE_CONFIG_H_ */
