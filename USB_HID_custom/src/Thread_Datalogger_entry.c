/*
 * Thread Datalogger
 *
 * Thread responsavel por controlar a comunicacao com os instrumentos quando nao ha conexao com o SITRAD e armazenar
 * os registros na flash, assim como recuperar os dados para envia-los ao SITRAD ou apaga-los quando for requisitado.
 *
 * Recebe informacoes da thread Eth_Wifi atraves da queue Datalogger_Flash. As informacoes podem ser para inibir a acao
 * do datalogger ou para requisitar a leitura de uma pagina. No caso de leitura de pagina, a pagina lida e enviada para
 * a thread Eth_Wifi pela queue EthWifi_Flash.
 *
 * Quando habilitado, controla os logs atraves do TimerDatalogger, cujo tempo e definido pelos parametros configurados
 * pela thread Eth_Wifi. A comunicacao com a thread Serial Thread e feita atraves do Message Framework, com as mensagens
 * SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER, requisitando a comunicacao com o instrumento, e SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN,
 * que ao ser recebida indica a recepcao de dados do instrumento pela serial ou o timeout da operacao.
 *
 * Os logs sao salvos na memoria flash de 7MB, dividida em paginas de 256 bytes e setores de 16 paginas. O setor atual e
 * armazenado na EEPROM.
 */
#include "Thread_Datalogger.h"
#include "Includes/Typedefs.h"
#include "Includes/messages.h"
#include "Includes/Thread_Datalogger_entry.h"
//#include "Includes/Eth_Wifi_Thread_Entry.h"
#include "usb_hid_device_thread.h"
#include "Includes/internal_flash.h"
#include "Includes/DataLoggerFlash.h"
#include "Includes/Eeprom25AA02E48.h"
#include "Includes/SPI_Application.h"
#include "Includes/Define_IOs.h"
#include "Includes/FlashQSPI.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/ApplicationCommonModule.h"

void Datalogger_MessageQueue_NotifyCallback(TX_QUEUE *USB_Flash_message_queue);
void Datalogger_Message_Framework_NotifyCallback(TX_QUEUE *Datalogger_thread_message);
void Datalogger_Open(_DATALOGGER_DATA *DataLoggerData);
_FG_ERR DataLoggerPend(_DATALOGGER_DATA *DataLoggerData);

void TimerDatalogger_Open(void);
void TimerDatalogger_Start(void);
void TimerDatalogger_SetPeriod(uint16_t period);
void TimerDatalogger_Stop(void);
void TimerExpired(_DATALOGGER_DATA *DataLoggerDataX);

_FG_ERR SPI_MAC_GetValue(uint8_t *EepromMacAdress, uint8_t SizeEepromMacAdress);
_FG_ERR SPI_Read_EEPROM(uint8_t *EepromData, uint8_t SizeEepromData);
_FG_ERR SPI_Write_EEPROM(uint8_t *EepromData, uint8_t SizeEepromData, uint8_t Address);
void ClearAllEEPROM(void);
void ClearSectorLogEEPROM(void);
void ClearAddEEPROM(uint8_t AddressEEprom);

bool FlashReadPageData(uint32_t Address, uint16_t QntPag, uint8_t *DataFlash);
bool FlashWritePageData(uint32_t Address, uint16_t Lenght, uint8_t *DataFlash);
void ClearFlashQSPI(void);
void ClearPagFlashQSPIFirm(uint16_t Pag);
void ClearBlockFlashQSPI(uint16_t Block);
void ClearSectorFlashQSPI(uint16_t Sector);
void DataLoggerWriteData(_DATALOGGER_DATA *DatalogData);

bool FirstTimerExpiration = false;

void Datalogger_ProcessFlags(_DATALOGGER_DATA *DataloggerData)
{
    _DATA_LOGGER_DATA DataLoggerReceived;
    while(TX_SUCCESS == tx_queue_receive(&Datalogger_Flash, &DataLoggerReceived, TX_NO_WAIT)) //recebe para a thread EthWifi
    {
        switch(DataLoggerReceived.OpCod)
        {
            case    START_OTA:
            case    TEST_START:
                    DataloggerData->UpdateFirmware = true;
                    break;

            case    END_OTA:
                    DataloggerData->UpdateFirmware = false;
                    break;

            case    CONNECTED_SITRAD:
                    if((bool)DataLoggerReceived.SizeTemp != DataloggerData->ConnectSitrad)
                    {
                        if((bool)DataLoggerReceived.SizeTemp)
                        {
                            TimerDatalogger_Stop();
                        }
                        else
                        {
                            DataloggerData->LogIntervalCounter = TIME_FOR_FIRST_LOG_AFTER_SITRAD_DISCONNECT;
                            TimerDatalogger_Start();
                        }
                    }
                    DataloggerData->ConnectSitrad = (bool)DataLoggerReceived.SizeTemp;

#if RECORD_DATALOGGER_EVENTS
                    DataLoggerRegisterEvent(DATALOGGER_EVENT_SITRAD_STATUS_CHANGE, 0, DataloggerData->ConnectSitrad);
#endif

                    break;

            case    REQUEST_PAG:
                    DataLoggerReceived.status = FlashReadPageData( (uint32_t)(DataLoggerReceived.SizeTemp*PAGE_FLASH_SIZE), DataLoggerReceived.QntPg, &DataLoggerReceived.PayloadTcp[0] );
                    tx_queue_send(&USB_Flash, &DataLoggerReceived, TX_WAIT_FOREVER); //Envio da thread USB
                    break;

#if RECORD_DATALOGGER_EVENTS
            case    REGISTER_EVENT:
                    //vazio, usar packet Coringa quando o instrumento nao responde
                    DataLoggerMountEventPacket(DataloggerData, DataLoggerReceived);

                    DataLoggerWriteData(DataloggerData);

                    SetDataDatalogger(DataloggerData); //atualiza informacoes do Datalogger - variaveis do modulo
                    break;
#endif

            default:
                    break;
        }
    }
}

/* Datalogger Thread entry function */
void Thread_Datalogger_entry(void)
{
//    _FG_ERR Status;
//    uint8_t ZEepromMacAdress[6];
//    uint32_t ActualFlags;
//    _DATALOGGER_DATA DataloggerData;
//
//    DataloggerData.ConnectSitrad            = false; //Flag em Ram que controla o Log
//    DataloggerData.UpdateFirmware           = false; //Flag em Ram que controla o updatefirmware
//    DataloggerData.FlagFirwmareDownload     = false;
//    DataloggerData.isLogging                = false;
//    DataloggerData.startLog                 = false;
//
//    /*
//      * TODO FITEC MODIFICACOES
//      * */
//
//         DataloggerData.FAC            = false;
//         DataloggerData.startLog       = true;
//     /*
//      * TODO FITEC MODIFICACOES
//      * */
//
//    DataloggerInit(&DataloggerData); //Inicializa o Datalogger
//
//    Datalogger_Open(&DataloggerData);
//
//    //Inicializa notify callback para MessageQueue
//    tx_queue_send_notify (&Datalogger_Flash , Datalogger_MessageQueue_NotifyCallback);
//
//    //Le da eeprom externa o endereco MAC
//    Status = SPI_MAC_GetValue(&ZEepromMacAdress[0], (uint8_t)sizeof(ZEepromMacAdress));
//    if(FG_SUCCESS == Status)
//    {
//        //Armazena na ram o endereco mac da interface eth
//        SetMACValue(&ZEepromMacAdress[0]);
//    }
//
//    TimerDatalogger_Open();
//
//    while (1)
//    {
//        //ThreadMonitor_SendKeepAlive(T_MONITOR_DATALOGGER_THREAD);
//    	//TODO: descomentar função
///*
//        if(!tx_event_flags_get(&DataloggerEventFlags, DL_FLAG_ALL, TX_OR_CLEAR, &ActualFlags, DataloggerData.isLogging ? TX_NO_WAIT : TIME_750MS))
//        {
//            if(DL_FLAG_CLEAR & ActualFlags)
//            {
//                ClearAllEEPROM();                   //Limpa EEPROM
//                ClearSectorFlashQSPI(0);            //Limpa Primeiro setor da Flash
//                DataloggerErase(&DataloggerData);   //Limpa RAM
//                SetDataDatalogger(&DataloggerData);
//            }
//
//            if(DL_FLAG_QUEUE & ActualFlags)
//            {
//                Datalogger_ProcessFlags(&DataloggerData);
//            }
//            //controle do log
//            if(DL_FLAG_TIMER_EXP & ActualFlags)
//            {
//                SetDataDatalogger(&DataloggerData);
//                TimerExpired(&DataloggerData);
//            }
//        }*/
//        if(DataloggerData.isLogging)
//        {
//            if(DataloggerData.ConnectSitrad || DataloggerData.UpdateFirmware || DataloggerData.FAC) //verifica se esta conectado ao sitrad ou esta em download firmware
//            {
//                DataloggerData.isLogging = false;
//            }
//            else if (GetFlagEclo())//Sinaliza que esta em Eclo
//            {
//                g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW);
//                g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW);
//            }
//            else
//            {
//                if(!Datalogger(&DataloggerData))
//                {
//                    DataloggerData.isLogging=false;
//                }
//            }
//        }
//        else if(DataloggerData.startLog)
//        {
//            DataloggerData.startLog = false;
//            if(!DataloggerData.ConnectSitrad && !DataloggerData.UpdateFirmware && !DataloggerData.FAC) //verifica se esta conectado ao sitrad ou esta em download firmware
//            {
//                if(DataloggerTimeOut(&DataloggerData))//controle dos instrumetos
//                {
//                    DataloggerData.isLogging = true;
//                }
//            }
//        }
//    }
}

void TimerExpired(_DATALOGGER_DATA *DataLoggerDataX)
{
    if(DataLoggerDataX->LogIntervalCounter)
    {
        DataLoggerDataX->LogIntervalCounter--;
        if(!DataLoggerDataX->LogIntervalCounter)
        {
            DataLoggerDataX->LogIntervalCounter = DataLoggerDataX->MListInstruments.LogInterval;
            DataLoggerDataX->startLog = true;
        }
    }
}

//Faz a leitura dos bytes contendo o Mac address.
_FG_ERR SPI_MAC_GetValue(uint8_t *EepromMacAdress, uint8_t SizeEepromMacAdress)
{
    ssp_err_t   err;
    uint32_t    ActualFlags = 0;
    _FG_ERR     Ret = FG_SUCCESS;

    /* Instrucao e endereco para leitura da eeprom */
    const uint8_t  InstructionAndEnd[] =
    {
         EEPROM_READ, EEPROM_ADDR_MAC
    };

    err = g_spi0.p_api ->open(g_spi0.p_ctrl, g_spi0.p_cfg);
    if (SSP_SUCCESS != err)
    {
        Ret = FG_ERROR;
    }

    //Puxa o CS
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_LOW);
    err = g_spi0.p_api->write(g_spi0.p_ctrl, (void *) &InstructionAndEnd[0], sizeof(InstructionAndEnd), SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err)
    {
        Ret = FG_ERROR;
    }
    // Aguarda SPI Completar a Operacao
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000))
    {
        Ret = FG_ERROR;
    }

    err = g_spi0.p_api->read(g_spi0.p_ctrl, (void *) EepromMacAdress, SizeEepromMacAdress, SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err)
    {
        Ret = FG_ERROR;
    }
    // Aguarda SPI Completar a Operacao
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000))
    {
        Ret = FG_ERROR;
    }

    //Solta o CS
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_HIGH);


    g_spi0.p_api ->close(g_spi0.p_ctrl);
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_HIGH);

    return Ret;
}

_FG_ERR SPI_Read_EEPROM(uint8_t *EepromData, uint8_t SizeEepromData)
{
    ssp_err_t   err;
    uint32_t    ActualFlags = 0;
    _FG_ERR     Ret = FG_SUCCESS;

    /* Instrucao e endereco para leitura da eeprom */
    const uint8_t  InstructionAndEnd[] =
    {
         EEPROM_READ, EEPROM_ADDR_STA
    };

    err = g_spi0.p_api ->open(g_spi0.p_ctrl, g_spi0.p_cfg);
    if (SSP_SUCCESS != err){Ret = FG_ERROR;}

    //Puxa o CS
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_LOW);
    err = g_spi0.p_api->write(g_spi0.p_ctrl, (void *) &InstructionAndEnd[0], sizeof(InstructionAndEnd), SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err){Ret = FG_ERROR;}
    // Aguarda SPI Completar a Operacao
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000))
    {
        Ret = FG_ERROR;
    }

    err = g_spi0.p_api->read(g_spi0.p_ctrl, (void *) EepromData, SizeEepromData, SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err){Ret = FG_ERROR;}
    // Aguarda SPI Completar a Operacao
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000))
    {
        Ret = FG_ERROR;
    }

    //Solta o CS
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_HIGH);


    g_spi0.p_api ->close(g_spi0.p_ctrl);
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_HIGH);

    return Ret;
}

_FG_ERR SPI_Write_EEPROM(uint8_t *EepromData, uint8_t SizeEepromData, uint8_t Address)
{
    ssp_err_t   err;
    uint32_t    ActualFlags = 0;
    _FG_ERR     Ret = FG_SUCCESS;

    /* Instrucao que Habilita a escrita */
    const uint8_t  InstructionEnableW[] =
    {
         EEPROM_WREN
    };

    /* Instrucao que habilita escrita, sinaliza que quer escrever e passa o end. e endereco para escrita */
    uint8_t  InstructionAndEnd[] =
    {
         EEPROM_WRITE, Address
    };

    /* Instrucao que Desabilita a escrita */
    const uint8_t  InstructionDesabilitaW[] =
    {
         EEPROM_WRDI
    };

    err = g_spi0.p_api ->open(g_spi0.p_ctrl, g_spi0.p_cfg);
    if (SSP_SUCCESS != err)
    {
        Ret = FG_ERROR;
    }

    /******************* Envia Instrucao de Habilita Escrita **************/
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_LOW);
    err = g_spi0.p_api->write(g_spi0.p_ctrl, (void *) &InstructionEnableW[0], sizeof(InstructionEnableW), SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err){Ret = FG_ERROR;}
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000)){Ret = FG_ERROR;}
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_HIGH);
    /**********************************************************************/

    tx_thread_sleep(1);

    /**************** Envia Instrucao de Escrita + End + Dado **************/

    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_LOW);
    err = g_spi0.p_api->write(g_spi0.p_ctrl, (void *) &InstructionAndEnd[0], sizeof(InstructionAndEnd), SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err){Ret = FG_ERROR;}
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000)){Ret = FG_ERROR;}

    err = g_spi0.p_api->write(g_spi0.p_ctrl, (void *) EepromData, SizeEepromData, SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err){Ret = FG_ERROR;}
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000)){Ret = FG_ERROR;}
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_HIGH);

    tx_thread_sleep(1);

    /**************** Envia Instrucao de Desabilita Escrita ***************/
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_LOW);
    err = g_spi0.p_api->write(g_spi0.p_ctrl, (void *) &InstructionDesabilitaW[0], sizeof(InstructionDesabilitaW), SPI_BIT_WIDTH_8_BITS);
    if (SSP_SUCCESS != err){Ret = FG_ERROR;}
    if(tx_event_flags_get(&SPI_FLAGS, ETH_SPI_DONE, TX_OR_CLEAR, &ActualFlags, 1000)){Ret = FG_ERROR;}
    g_ioport.p_api->pinWrite(CS_EEPROM, IOPORT_LEVEL_HIGH);
    /**********************************************************************/

    g_spi0.p_api ->close(g_spi0.p_ctrl);

    return Ret;
}

void ClearAllEEPROM()
{
    uint8_t  Buffer[EEPROM_SIZE_PAGE];

    for(uint8_t i=0; i<EEPROM_SIZE_PAGE; i++)
    {
        Buffer[i] = 0xFF;
    }

    //LIMPA VARIAVEIS DA FLASH
    for(uint8_t Index = 0; Index < ((EEPROM_SIZE/EEPROM_SIZE_PAGE) - 1); Index++)
    {
            SPI_Write_EEPROM(&Buffer[0], (uint8_t)(EEPROM_SIZE_PAGE), (uint8_t)(Index*EEPROM_SIZE_PAGE)); //CLEAR EEPROM
    }

    //LIMPA O ULTIMO BLOCO DA EEPROM MENOS O ULTIMO BYTE
    SPI_Write_EEPROM(&Buffer[0], (uint8_t)(EEPROM_SIZE_PAGE-1), (uint8_t)(((EEPROM_SIZE/EEPROM_SIZE_PAGE) - 1) * EEPROM_SIZE_PAGE)); //CLEAR EEPROM
}

/**
 * \brief Funcao que apaga todo_ o espaco da memoria Eeprom que registra os sectores
 *
 *
 * \param void
 *
 * \return void
*/
void ClearSectorLogEEPROM(void)
{
    uint8_t  Buffer[EEPROM_SIZE_PAGE],
             Index = 0;

    for(uint8_t i=0; i<EEPROM_SIZE_PAGE; i++)
    {
        Buffer[i] = 0xFF;
    }

    //LIMPA VARIAVEIS DA FLASH
    for(Index = 0; Index < ((EEPROM_SIZE/EEPROM_SIZE_PAGE)-1); Index++)
    {
        SPI_Write_EEPROM(&Buffer[0], (uint8_t)(EEPROM_SIZE_PAGE), (uint8_t)(Index*EEPROM_SIZE_PAGE)); //CLEAR EEPROM
    }

    SPI_Write_EEPROM(&Buffer[0], (uint8_t)(EEPROM_SIZE_PAGE-2), (uint8_t)(Index*EEPROM_SIZE_PAGE)); //CLEAR EEPROM
}

/**
 * \brief Funcao que apaga um endereco especifico da memoria Eeprom
 *
 *
 * \param uint8_t AddressEEprom -> Endereco valido na eeprom
 *
 * \return void
*/
void ClearAddEEPROM(uint8_t AddressEEprom)
{
    uint8_t  Buffer[] = {0xFF};
    if(AddressEEprom < EEPROM_SIZE) //se o addressEEprom vor menor do que o ultimo endereco.
    {
        SPI_Write_EEPROM(&Buffer[0], (uint8_t)(EEPROM_WRSR), (uint8_t)(AddressEEprom)); //CLEAR EEPROM
    }
}

/**
 * \brief Funcao que apaga um block da flash
 *
 *
 * \param uint16_t Block-> Endereco do block
 *
 * \return void
*/
void ClearBlockFlashQSPI(uint16_t Block)
{
    /* variables to store result/status */
    bool    in_progress = true;

    /* Open QSPI interface */
    g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);

    /* Erase first sector on QSPI flash memory */
    if(!g_qspi0.p_api->sectorErase(g_qspi0.p_ctrl, (uint8_t *) (QSPI_DATALOGGER_ADDR + (Block*SIZE_SECTOR))))
    {
        /* Wait until previous operation ends */
        do
        {
            g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &in_progress);
        } while (in_progress);
    }
    else
    {
        while(1);
    }

    /* Close QSPI interface */
    g_qspi0.p_api->close(g_qspi0.p_ctrl);
}

/**
 * \brief Funcao que apaga um setor especifico da flash
 *
 *
 * \param uint16_t Sector -> Endereco do setor
 *
 * \return void
*/
void ClearSectorFlashQSPI(uint16_t Sector)
{
    /* variables to store result/status */
    bool    in_progress = true;
    tx_mutex_get(&QSPI_Mutex, TX_WAIT_FOREVER);
    /* Open QSPI interface */
    g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);

    /* Erase first sector on QSPI flash memory */
    //test = g_qspi0.p_api->erase(g_qspi0.p_ctrl, (uint8_t *) QSPI_DEVICE_ADDRESS, FLASH_SIZE);SIZE_SECTOR
    g_qspi0.p_api->erase(g_qspi0.p_ctrl, (uint8_t *) (QSPI_DATALOGGER_ADDR+(SIZE_SECTOR*Sector)), SIZE_SECTOR);

    /* Wait until previous operation ends */
    do
    {
        g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &in_progress);
    } while (in_progress);

    /* Close QSPI interface */
    g_qspi0.p_api->close(g_qspi0.p_ctrl);
    tx_mutex_put(&QSPI_Mutex);
}

/**
 * \brief Funcao que apaga um setor especifico da flash
 *
 *
 * \param uint16_t Sector -> Endereco do setor
 *
 * \return void
*/
void ClearPagFlashQSPIFirm(uint16_t Pag)
{
    /* variables to store result/status */
    bool    in_progress = true;

    /* Open QSPI interface */
    g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);

    /* Erase first sector on QSPI flash memory */
    //test = g_qspi0.p_api->erase(g_qspi0.p_ctrl, (uint8_t *) QSPI_DEVICE_ADDRESS, FLASH_SIZE);SIZE_SECTOR
    g_qspi0.p_api->erase(g_qspi0.p_ctrl, (uint8_t *) (QSPI_DEVICE_ADDRESS+(PAGE_FLASH_SIZE*Pag)), PAGE_FLASH_SIZE);

    /* Wait until previous operation ends */
    do
    {
        g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &in_progress);
    } while (in_progress);

    /* Close QSPI interface */
    g_qspi0.p_api->close(g_qspi0.p_ctrl);
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao le o conteúdo da memoria de um endereço específico
 *
 * @param uint32_t Address: 24bits de endereço
 * @param uint16_t QntPag: quantidade de b
 * @param uint8_t *DataFlash: Ponteiro contendo os dados da leitura
 *
 * @return Nenhum
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
bool FlashReadPageData(uint32_t Address, uint16_t QntPag, uint8_t *DataFlash)
{
    bool InProgress;

    memset(DataFlash, 0x00, QntPag * (size_t)PAGE_FLASH_SIZE);

    g_qspi0.p_api->open(g_qspi0.p_ctrl, g_qspi0.p_cfg);


    ssp_err_t result = g_qspi0.p_api->read(g_qspi0.p_ctrl, (uint8_t *) (QSPI_DATALOGGER_ADDR + Address) , &DataFlash[0], (uint32_t)(BUFFER_LENGTH*QntPag));
    if(result != SSP_SUCCESS)
    {
        g_qspi0.p_api->close(g_qspi0.p_ctrl);
        return false;
    }

    do
    {
        g_qspi0.p_api->statusGet(g_qspi0.p_ctrl, &InProgress);
    } while (InProgress);


    g_qspi0.p_api->close(g_qspi0.p_ctrl);
    return true;
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao inicializa alguns recursos da thread Datalogger
 *
 * @param _DATALOGGER_DATA *DataLoggerData: Ponteiro da strutura Datalogger_data
 *
 * @return Nenhum
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
void Datalogger_Open(_DATALOGGER_DATA *DataLoggerData)
{
    //Message Framework: Used in Acquire
    DataLoggerData->MessageCfg.acquireCfg.buffer_keep = true;   // não desaloca o buffer

    //Message Framework: Used in Post
    DataLoggerData->MessageCfg.postCfg.priority = SF_MESSAGE_PRIORITY_NORMAL;
    DataLoggerData->MessageCfg.postCfg.p_callback = NULL; // não cria callback

    //Message Framework: Cria BufferAcquire para postar para Serial 485
    g_sf_message0.p_api->bufferAcquire( g_sf_message0.p_ctrl,
                                        (sf_message_header_t **) &DataLoggerData->SerialDataInPayload,
                                        &DataLoggerData->MessageCfg.acquireCfg,
                                        300);

    tx_queue_send_notify (&Thread_Datalogger_message_queue, Datalogger_Message_Framework_NotifyCallback);
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao inicializa o timerDatalogger
 *
 * @param Nenhum
 *
 * @return Nenhum
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
void TimerDatalogger_Open(void)
{
    g_timer_datalogger.p_api->open(g_timer_datalogger.p_ctrl, g_timer_datalogger.p_cfg);
    TimerDatalogger_Start();

}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao inicia o timerDatalogger
 *
 * @param uint16_t period: Periodo em segundos
 *
 * @return Nenhum
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
void TimerDatalogger_Start(void)
{
    g_timer_datalogger.p_api->periodSet( g_timer_datalogger.p_ctrl, (timer_size_t)1, TIMER_UNIT_PERIOD_SEC );
    g_timer_datalogger.p_api->start(g_timer_datalogger.p_ctrl);
    FirstTimerExpiration = true;
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao seta o periodo do timerDatalogger
 *
 * @param uint16_t period: Periodo em segundos
 *
 * @return Nenhum
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
void TimerDatalogger_SetPeriod(uint16_t period)
{
    g_timer_datalogger.p_api->periodSet( g_timer_datalogger.p_ctrl, (timer_size_t)period, TIMER_UNIT_PERIOD_SEC );
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao para o timerDatalogger
 *
 * @param Nenhum
 *
 * @return Nenhum
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
void TimerDatalogger_Stop(void)
{
    g_timer_datalogger.p_api->stop( g_timer_datalogger.p_ctrl);
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao envia menssagem para a thread 485
 *
 * @param _DATALOGGER_DATA *DataLoggerData: Ponteiro da strutura Datalogger_data
 *
 * @return FG_ERR
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
_FG_ERR DataLoggerPost_485(_DATALOGGER_DATA *DataLoggerData)
{
    ssp_err_t status;
    DataLoggerData->SerialDataInPayload->header.event_b.class_instance = 0;

    DataLoggerData->SerialDataInPayload->header.event_b.class_code = SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER;//SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER; //symbol da engrenagem
    DataLoggerData->SerialDataInPayload->header.event_b.code = SF_MESSAGE_EVENT_S485_RX; //Symbol Events da engrenagem

    status = g_sf_message0.p_api->post(  g_sf_message0.p_ctrl,
                                         &DataLoggerData->SerialDataInPayload->header,//DataIpPayloadMessagePayload->header,
                                         &DataLoggerData->MessageCfg.postCfg,         //global que esta no incio desta thread;
                                         &DataLoggerData->MessageCfg.err_postcFG,     //global que esta no incio desta thread;
                                         TIME_1S);
    if(status == SSP_SUCCESS)
    {
        tx_thread_relinquish();
        return FG_SUCCESS;
    }
    else
    {
        return status;
    }
}

/*****************************************************************************************************/
/**
 * @brief <b>Descricao:</b> Esta funcao recebe o pacote da thread 485, contendo a resposta do instrumento
 *
 * @param _DATALOGGER_DATA *DataLoggerData: Ponteiro da strutura Datalogger_data
 *
 * @return _FG_ERR: FG_SUCCESS ou FG_ERROR
 *
 * @details <b>Historico:</b>
 * \n
 */
/*****************************************************************************************************/
_FG_ERR DataLoggerPend(_DATALOGGER_DATA *DataLoggerData)
{
    ssp_err_t SSPErr;

    _SERIAL_DATA_IN_PAYLOAD *SDataInPayload;

    DataLoggerData->FlashExternalPayload.DataFlash = 0;

    SSPErr = g_sf_message0.p_api->pend( g_sf_message0.p_ctrl,
                                        &Thread_Datalogger_message_queue,
                                        (sf_message_header_t **) &DataLoggerData->header,
                                        TX_NO_WAIT);
    if (SSPErr == SSP_SUCCESS)
    {
        if(DataLoggerData->header->event_b.class_code == SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN)
        {
            SDataInPayload = (_SERIAL_DATA_IN_PAYLOAD*)DataLoggerData->header;

            DataLoggerData->SerialDataOutPayload.num_of_bytes = SDataInPayload->num_of_bytes;
            DataLoggerData->SerialDataOutPayload.pointer = SDataInPayload->pointer;
            if(!SDataInPayload->num_of_bytes) //Retornou zero da thread Serial
            {
                return FG_NOTHING_TO_BE_DONE;
            }
            return FG_SUCCESS_485;
        }
    }
    return FG_ERROR;
}


/*****************************************************************************************************/
/**
* @brief <b>Descricao:</b> Esta funcao faz a gravacao do setor na eerpom, ela utiliza todo_ o espaco
*                       \n da eeprom (190 bytes) para evitar danos a mesma, esta funcao tambem
*                       \n identifica o fim da pagina para comecar a sobreescrever a primeira posicao
*
* @param uint16_t NextSector: O proximo setor que sera utilizado na memoria flash
*
* @return nenhum
*
* @details <b>Historico:</b>
*/
/*****************************************************************************************************/
void DataloggerWriteSectorEEprom(_DATALOGGER_DATA *DatalogData, uint16_t NextSector)
{
    if(DatalogData->MDataloggerEEpromControl.NextAddress > EEPROM_ADDR_LAS)
    {
        DatalogData->MDataloggerEEpromControl.NextAddress =  EEPROM_ADDR_STA;
        ClearSectorLogEEPROM(); //apaga todas as areas da eeprom, os quais, salvam o numero do setor da flash que o log estah usando.
    }

    SPI_Write_EEPROM((uint8_t *)&NextSector, SIZE_ADDRESS_EEPROM, DatalogData->MDataloggerEEpromControl.NextAddress);

    DatalogData->MDataloggerEEpromControl.NextAddress = (uint8_t)(DatalogData->MDataloggerEEpromControl.NextAddress + SIZE_ADDRESS_EEPROM);
}

///*****************************************************************************************************/
///**
//* @brief <b>Descricao:</b> Esta funcao faz o gerenciamento das memorias, flash e eeprom
//*                       \n ela recebe o endereco de escrita na flash e testa tamanho para
//*                       \n verificar se cabe na pagina da flash, caso nao caiba, já muda de
//*                       \n pagina e termina a escrita, verifica o fim da memoria flash para
//*                       \n fazer a virada, escreve o setor atual na eeprom para controle de posicao
//*                       \n faz a limpeza da pagina que sera sobre escrita, e ainda aponta para o proximo
//*                       \n endereco a ser escrito
//*
//* @param _DATALOGGER_DATA *DatalogData
//* endereco da da memoria flash que se deseja gravar os dados
//*
//* @return nenhum
//*
//* @details <b>Historico:</b>
//*/
///*****************************************************************************************************/
void DataLoggerWriteData(_DATALOGGER_DATA *DatalogData)
{

    uint16_t    SizeBurnOtherPage = 0,  ///> quantidade de bytes que ficaram para a proxima pagina
                QtdBytesToEndPage = 0,  ///> quantidade de bytes que restao para o fim da pagina
                NumPageToWrite = 0,     ///> numero da pagina a ser escrita
                ActualSector = 0,       ///> numero do setor atual a ser escrito
                NextSector = 0;         ///> numero do proximo setor para comparar com o anterior e ver se tem q alterar proximo endereço
    uint32_t    FirstAddPage = 0,       ///> primeiro endereço da pagina vigente
                NextAddressToWrite = 0; ///> retorno da função com o proximo endereço a ser gravado

    if(DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash == PAYLOAD_SIZE_ERROR)
    {
        return;
    }
    if(DatalogData->MDataloggerControl.DataloggerPacketControl.Address >= SIZE_MAX_FLASH_MEMORY)                                            //IDENTIFICA VIRADA DA MEMÓRIA
    {
        DatalogData->MDataloggerControl.DataloggerPacketControl.Address = 0; //posiciona o endereco para a posicao zero
        ClearSectorLogEEPROM();             //Apaga eeprom parte que salva os setores
        ClearSectorFlashQSPI(ActualSector); //limpa o primeiro setor da flash

        //verifica se a flash ja foi sobrescrita
        if(DatalogData->OverrunFlash)
        {
            DatalogData->OverrunFlash = 0x00; //sinaliza que ja foi sobrescrita
            SPI_Write_EEPROM(&DatalogData->OverrunFlash, EEPROM_WRSR, EEPROM_ADDR_OVERF); //Salva o flag que sinaliza sobrescrita de flash
        }
    }

    if(DatalogData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty == true) //indica que vai fazer o primeir logo da sua historia
    {
        DataloggerWriteSectorEEprom(DatalogData, 0);
        ClearSectorFlashQSPI(0);
        DatalogData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = false;
    }

    NumPageToWrite = (uint16_t)(DatalogData->MDataloggerControl.DataloggerPacketControl.Address / PAGE_FLASH_SIZE); //em qual pagina sera iniciado a escrita de 0 até 32767
    ActualSector = (uint16_t)(NumPageToWrite / NUMBER_PAGES_OF_SECTOR);          //indica em qual setor será feito a gravação
    FirstAddPage = (uint32_t)(NumPageToWrite * PAGE_FLASH_SIZE);                 //calcula qual o endereço inicial da pagina que se deseja escrever
    QtdBytesToEndPage = (uint16_t)(PAGE_FLASH_SIZE - (DatalogData->MDataloggerControl.DataloggerPacketControl.Address - FirstAddPage));  //quantidade de bytes para acabar esta pagina, ESTE É O TAMANHO DA PRIMEIRA ESCRITA NA FLASH

    //CASO PRECISE DE MAIS DE UMA PAGINA PARA TERMINAR A ESCRITA
    if(DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash > QtdBytesToEndPage)
    {
        SizeBurnOtherPage = (uint16_t)(DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash - QtdBytesToEndPage);//TAMANHO RESTANTE PARA GRAVAR NA OUTRA PAGINA

        FlashWritePageData( (DatalogData->MDataloggerControl.DataloggerPacketControl.Address + QSPI_DATALOGGER_ADDR),
                            QtdBytesToEndPage,
                            &DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket);

        do//laco que controla a gravacao de dados maiores que o tamanho da pagina da flash
        {
            NumPageToWrite++;
            //todo verificar onde termina o log e comeca o firmware,se nao sobrescreve o firmware
            if(NumPageToWrite >= NUMBER_OF_PAGES_DATALOGGER) //IDENTIFICA VIRADA DA MEMÓRIA QUANDO SIZE MAIOR QUE O ESPAÇO DA ULTIMA PAGINA
            {
                NumPageToWrite = 0;
                ClearSectorLogEEPROM();                 //Apaga eeprom parte dos setores da flash
                ClearSectorFlashQSPI(NumPageToWrite);   //Limpa Primeiro setor da Flash

                //verifica se a flash ja foi sobrescrita
                if(DatalogData->OverrunFlash)
                {
                    DatalogData->OverrunFlash = 0x00; //sinaliza que ja foi sobrescrita
                    SPI_Write_EEPROM(&DatalogData->OverrunFlash, EEPROM_WRSR, EEPROM_ADDR_OVERF); //Salva o flag que sinaliza sobrescrita de flash
                    DatalogData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = false;//Salva o flag que sinaliza sobrescrita de flash
                }
            }

            DatalogData->MDataloggerControl.DataloggerPacketControl.Address = (uint32_t)(NumPageToWrite * PAGE_FLASH_SIZE); //AJUSTA O NOVO ENDEREÇO PARA O INICIO DA PROXIMA PAGINA

            NextSector = (uint16_t)(NumPageToWrite / NUMBER_PAGES_OF_SECTOR);
            //caso o setor encha na gravação pela troca de pagina
            if(NextSector != ActualSector)                 //TESTE PARA VER SE TROCOU DE SETOR E MANDA APAGAR
            {
                ClearSectorFlashQSPI(NextSector);          //Limpa proximo setor da Flash
                DataloggerWriteSectorEEprom(DatalogData, NextSector); //memoria na eeprom
                ActualSector = NextSector;
            }

            //se q auantidade de bytes restante exceder o tamanho da pagina, grava apenas o tamanho da pagina
            uint16_t SizeToBurnNow = (SizeBurnOtherPage > PAGE_FLASH_SIZE ? (uint16_t)PAGE_FLASH_SIZE: (uint16_t)SizeBurnOtherPage);
            //DESLOCA O PONTEIRO PARA GRAVAÇÃO DO RESTANTE DO FRAME EM OUTRA PAGINA
            FlashWritePageData( (DatalogData->MDataloggerControl.DataloggerPacketControl.Address + QSPI_DATALOGGER_ADDR),
                                SizeToBurnNow,
                                &(DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket) + QtdBytesToEndPage);

            QtdBytesToEndPage = (uint16_t)(QtdBytesToEndPage + SizeToBurnNow); //recalcula o indice
            NextAddressToWrite = (DatalogData->MDataloggerControl.DataloggerPacketControl.Address + SizeToBurnNow);                 //APONTA PARA O PROXIMO ENDEREÇO DE REGISTRO
            SizeBurnOtherPage = (uint16_t)(SizeBurnOtherPage - SizeToBurnNow); // atualiza o numero de bytes restantes
        }while((SizeBurnOtherPage) > 0);
    }

    else //CASO A ESCRITA DO REGISTRO UTILIZE APENAS UMA PAGINA
    {
        FlashWritePageData( (DatalogData->MDataloggerControl.DataloggerPacketControl.Address + QSPI_DATALOGGER_ADDR),
                            DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash,
                            &DatalogData->MDataloggerControl.MDataloggerRegister.InitPacket);

        //Calcula o proximo endereco para registro
        NextAddressToWrite = (  DatalogData->MDataloggerControl.DataloggerPacketControl.Address +
                                DatalogData->MDataloggerControl.DataloggerPacketControl.SizeToBurnFlash );
    }
    NextSector = (uint16_t)((NextAddressToWrite / PAGE_FLASH_SIZE) / NUMBER_PAGES_OF_SECTOR);
    //verifica se mudou de setor
    if(NextSector != ActualSector)              //TESTE PARA VER SE TROCOU DE SETOR E MANDA APAGAR
    {
        if(NextSector >= NUMBER_OF_SECTORS_DATALOGGER)     //TESTE DO FIM DA MEMORIA
        {
            NextSector = 0;
            ClearSectorLogEEPROM();                 //Apaga eeprom parte dos setores da flash
            if(DatalogData->OverrunFlash)
            {
                DatalogData->OverrunFlash = 0x00; //sinaliza que ja foi sobrescrita
                SPI_Write_EEPROM(&DatalogData->OverrunFlash, EEPROM_WRSR, EEPROM_ADDR_OVERF); //Salva o flag que sinaliza sobrescrita de flash
                DatalogData->MDataloggerControl.MDataloggerFlags.Bites.DataloggerEmpty = false;
            }
        }
        NextAddressToWrite = (uint32_t)(NextSector * PAGE_FLASH_SIZE * NUMBER_PAGES_OF_SECTOR);
        ClearSectorFlashQSPI(NextSector);  //apaga o proximo setor
        DataloggerWriteSectorEEprom(DatalogData, NextSector); //memoria na eeprom
    }

    DatalogData->MDataloggerControl.DataloggerPacketControl.Address = NextAddressToWrite;
}

/***********************************************************************************************************************
* Function Name: spi_callback
* Description  : A callback function, called by SPI interrupts.
*                This function name must match the name specified in the SSP Configurator setting for "g_spi SPI Driver
*                on r_sci_spi
* Arguments    : p_args - Pointer to arguments that provide information when this callback is executed.
* Return Value : None
***********************************************************************************************************************/
void spi_callback (spi_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);

    tx_event_flags_set (&SPI_FLAGS, ETH_SPI_DONE, TX_OR);
}

///> Notificacao de recebimento de menssagem de origem queue
void Datalogger_MessageQueue_NotifyCallback(TX_QUEUE *USB_Flash_message_queue)
{
    tx_event_flags_set (&DataloggerEventFlags, DL_FLAG_QUEUE, TX_OR);

    SSP_PARAMETER_NOT_USED(USB_Flash_message_queue);
}

///> Notificacao de recebimento de menssagem de origem Framework
void Datalogger_Message_Framework_NotifyCallback(TX_QUEUE *Datalogger_thread_message)
{
    tx_semaphore_put(&DataloggerSemaphore);

    SSP_PARAMETER_NOT_USED(Datalogger_thread_message);
}

//Callback que sinaliza que o timer estourou Timer-> Auto reload;
void TimerDataloggerCallback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&DataloggerEventFlags, DL_FLAG_TIMER_EXP, TX_OR); //SET FLAG
    }
}

#if RECORD_DATALOGGER_EVENTS
void DataLoggerRegisterEvent(const _DATALOGGER_EVENTS event, const uint16_t info, const bool status)
{
    _DATA_LOGGER_DATA DataLoggerData;
    DataLoggerData.OpCod = REGISTER_EVENT;
    DataLoggerData.QntPg = event;
    DataLoggerData.SizeTemp = info;
    DataLoggerData.status = status;
    tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
}
#endif
