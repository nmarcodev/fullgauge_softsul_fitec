#ifndef __TYPEDEFS_H
#define __TYPEDEFS_H

//#include "common_data.h" // include obrigatório

#include <stdio.h>

//#include "Includes/messages.h"

//#include "RTT/SEGGER_RTT.h"

//extern void initialise_monitor_handles(void);


//#define PINCFG_PROTOTYPE_01
//#define PINCFG_PROTOTYPE_02

//#define DISPLAY_I2C
//#define DISPLAY_SPI


//*************************************************************************************************
// Defines


#define FIRMWARE_VERSION 1
#define FIRMWARE_RELEASE 0



//#define	FALSE		0
//#define	TRUE		1

//*************************************************************************************************
// Enumerations


// definições de temporização levando em conta o TICK do RTOS de 10ms
//typedef enum
//{
//    ZERO_MS_ONE_SHOT_TIMER  =  0,
//    TIME_10MS               =  1,
//    TIME_20MS               =  2,
//    TIME_40MS               =  4,
//    TIME_50MS               =  5,
//    TIME_100MS              =  10,
//    TIME_200MS              =  20,
//    TIME_250MS              =  25,
//    TIME_300MS              =  30,
//    TIME_500MS              =  50,
//    TIME_1S                 = 100,
//    TIME_2S                 = 200,
//    TIME_3S                 = 300,
//    TIME_5S                 = 500,
//    TIME_10S                = 1000,
//
//} _TIME_PARAM;

//typedef struct {
//    int16_t Min;
//    int16_t Max;
//} _PARAM_LIMITS;

//typedef enum
//{
//	STATE_OFF			= 0,
//	STATE_ON			= 1,
//} _STATE;

//typedef enum
//{
//	CELSIUS_UNIT 		= 0,
//	FAHRENHEIT_UNIT		= 1,
//} _TEMPERATURE_UNIT;

//typedef enum
//{
//    IO_NTC     = 0,
//    IO_4_20    = 1,
//}_NTC_4_20_TYPE;
//
//typedef enum
//{
//    SUCTION   = 0,
//    DISCHARGE = 1,
//
//}_LINE_TYPE;

// USB FLAGS
// cada flag deve representar um bit.
//
//typedef enum
//{
//    REFRIGERANT_TYPE_R12    = 0,
//    REFRIGERANT_TYPE_R22    = 1,
//    REFRIGERANT_TYPE_R32    = 2,
//    REFRIGERANT_TYPE_R134A  = 3,
//    REFRIGERANT_TYPE_R290   = 4,
//   // REFRIGERANT_TYPE_R401A  = 4,
//    REFRIGERANT_TYPE_R404A  = 5,
//    REFRIGERANT_TYPE_R407A  = 6,
//    REFRIGERANT_TYPE_R407C  = 7,
//    REFRIGERANT_TYPE_R407F  = 8,
//    REFRIGERANT_TYPE_R410A  = 9,
//    REFRIGERANT_TYPE_R422A  = 10,
//    REFRIGERANT_TYPE_R422D  = 11,
//    REFRIGERANT_TYPE_R441A  = 12,
//    REFRIGERANT_TYPE_R507A  = 13,
//    REFRIGERANT_TYPE_R513A  = 14,
//    REFRIGERANT_TYPE_R600A  = 15,
//    REFRIGERANT_TYPE_R717A  = 16, //NH3
//    REFRIGERANT_TYPE_R744A  = 17, // CO2
//   // REFRIGERANT_TYPE_R1270  = 15,
//    REFRIGERANT_TYPE_CUSTOM = 18,
//    REFRIGERANT_TYPE_SIZE   = 19,
//}_CFG_TYPE_GAS_TYPE;
////
//typedef enum
//{
//    USB_FLAGS_NONE               = 0,
//    USB_INSERT_FLAG              = 0x0001,
//    USB_REMOVE_FLAG              = 0x0002,
//    USB_IMPORT_PARAM_TABLE_FLAG  = 0x0004,
//    USB_EXPORT_PARAM_TABLE_FLAG  = 0x0008,
//    USB_ALL_FLAGS                = 0x00FF,
//
//    USB_CMD_OK_ACK               = 0x0100,
//    USB_CMD_FAIL_ACK             = 0x0200,
//    USB_NO_MEDIA_ACK             = 0x0400,
//    USB_NO_FILE_ACK              = 0x0800,
//    USB_NO_SPACE_ACK             = 0x1000,
//    USB_ALL_ACKS                 = 0xFF00,
//}_USB_FLAGS;


//*************************************************************************************************
// Defines




//*************************************************************************************************
// Typedefs

//
//typedef	int8_t				    S8;
//typedef	uint8_t			        U8;
//
//typedef	int16_t			        S16;
//typedef	uint16_t			    U16;
//
//typedef	int32_t			    	S32;
//typedef	uint32_t	    		U32;
//
//typedef int64_t			        S64;
//typedef uint64_t		        U64;
//
//typedef struct
//{
//    unsigned  bit0:1, bit1:1, bit2:1, bit3:1, bit4:1, bit5:1, bit6:1, bit7:1, bit8:1, bit9:1, bit10:1,
//              bit11:1, bit12:1, bit13:1, bit14:1, bit15:1, bit16:1, bit17:1, bit18:1, bit19:1, bit20:1,
//              bit21:1, bit22:1, bit23:1, bit24:1, bit25:1, bit26:1, bit27:1, bit28:1, bit29:1, bit30:1, bit31:1;
//}_BITS32;
//
//typedef struct
//{
//    unsigned  bit0:1, bit1:1, bit2:1, bit3:1, bit4:1, bit5:1, bit6:1, bit7:1, bit8:1;
//}_BITS8;
//
//typedef union
//{
//    U32      u32data;
//    U16      u16data[2];
//    _BITS32  bdata;
//
//}_U32DATA;
//
//typedef union
//{
//    U8      u8data;
//    _BITS8  bdata;
//
//}_U8DATA;

//
//typedef struct
//{
//    sf_message_acquire_cfg_t acquireCfg;
//    sf_message_post_cfg_t    postCfg;
//    sf_message_post_err_t    err_postcFG;
//
//}_MESSAGE_FRAMEWORK_CFG;
//
//
//typedef struct
//{
//    S16 Pressure;       //ponto de pressao na curva de gas
//    S16 Temperature;    //ponto de temperatura na curva de gas
//}_GAS_PROP_TYPE;



//*************************************************************************************************
//*
//*
//*************************************************************************************************
//
//typedef enum
//{
//    KEY_01          = 0,
//    KEY_02          = 1,
//    KEY_BACK_MENU   = 2,
//    KEY_UP          = 3,
//    KEY_DOWN        = 4,
//    KEY_ENTER       = 5,
//    KEY_01_02       = 6,
//    KEY_MAX_ADDRESS = 7,
//    INVALID_KEY,
//} _KEY_ADDRESS;
//
//typedef enum
//{
//    KEY_EVENT_TO_VALIDATE = 0,
//    KEY_EVENT_OK          = 1,
//    KEY_EVENT_NO_EVENT    = 2,
//    KEY_EVENT_ERROR       = 3,
//
//}_KEY_EVENT_STATUS;
//
//typedef enum
//{
//    KEY_RELEASED            = 0,
//    KEY_PRESSED             = 1,
//    KEY_HOLD_FOR_1S         = 2,
//    KEY_HOLD_FOR_2S         = 3,
//    KEY_HOLD_FOR_5S         = 4,
//    KEY_HOLD_FOR_10S        = 5,
//    KEY_EVENT_NONE,
//} _KEY_EVENT;
//
//typedef enum
//{
//    USB_INSERT          = 0,
//    USB_REMOVE          = 1,
//    USB_EXPORT          = 2,
//    USB_IMPORT          = 3,
//    BUZZER_ON           = 4,
//    BUZZER_OFF          = 5,
//} _BUZZER_EVENT;
//
//typedef enum
//{
//    OFF = 0,
//    ON  = 1,
//
//}_ON_OFF;
//
//typedef enum
//{
//    DISPLAY_ERASE    = 0,
//    DISPLAY_DRAW_L1  = 1,
//    DISPLAY_DRAW_L2  = 2,
//    DISPLAY_DRAW_L3  = 3,
//    DISPLAY_DRAW_L4  = 4,
//    DISPLAY_DRAW_L5  = 5,
//    DISPLAY_DRAW_L6  = 6,
//    DISPLAY_DRAW_L7  = 7,
//    DISPLAY_DRAW_L8  = 8,
//    DISPLAY_DRAW_ALL = 1,
//
//}_DISPLAY_COMMANDS;


//*************************************************************************************************
//
typedef enum
{
    // OK
    FG_SUCCESS              = 0,
    FG_ERROR                = 1,
    FG_NOTHING_TO_BE_DONE   = 2,
    FG_INVALID_PARAM        = 3,

    // usado para entradas e saídas
    FG_INVALID_ADDRESS      = 10,
    FG_INVALID_NUMBER       = 11,
    FG_INVALID_TYPE         = 12,
    FG_INVALID_QUANTITY     = 13,

    // usado para parametros
    FG_OUT_OF_MIN_RANGE     = 20,
    FG_OUT_OF_MAX_RANGE     = 21,
    FG_INVALID_OFFSET       = 22,
    FG_INVALID_DATA_TYPE    = 23,

    // usado para timers
    FG_TIMER_ACTIVE         = 30,
    FG_TIMER_EXPIRED        = 31,

    // usado para USB
    FG_USB_IN_PROGRESS      = 40,
    FG_USB_NO_SPACE         = 41,
    FG_USB_NO_MEDIA         = 42,
    FG_USB_FILE_NOT_FOUND   = 43,
    FG_USB_INVALID_FILE     = 44,
    FG_USB_INVALID_PATH     = 45,
    FG_USB_INVALID_PARAM    = 46,
    FG_USB_LOCKED_PARAM     = 47,
    FG_USB_WRITE_PROTECTED  = 48,

    // usado no módulo IOManager
    FG_NO_UPDATE_NEEDED     = 50,
    FG_UPDATE_NEEDED        = 51,


    // usado para habilitação de edição de parâmetro

    FG_LOCKED               = 60,
    FG_UNLOCKED             = 61,

    // usado para relógio
    FG_ECLO                 = 70,



} _FG_ERR;
//
//typedef enum
//{
//    // usado para status de IO
//    IO_OK           = 0,
//    IO_ERROR        = 1,
//    IO_DISABLE      = 2,
//    IO_MAINTENANCE  = 3,
//    IO_IN_ALARM     = 4,
//    IO_OUT_OF_RANGE = 5,
//
//}_IO_ERR;
//
//typedef struct
//{
//    U16 MainOffset;
//    U16 GroupOffset;
//    U8  GroupIndex;
//    U8  SubGroupIndex;
//    U16 SubGroupOffset;
//    U16 GroupSize;
//    U16 SubGroupSize;
//    U16 PropertyIndex;
//
//
//}_CFG_TABLE_OFFSETS;
//
//
//
//extern TX_QUEUE TimeCounter_queue;
//extern TX_QUEUE Buzzer_queue;
//extern TX_QUEUE keyboard_queue;
//extern TX_QUEUE Configs_queue;
//extern TX_QUEUE IOManager_queue;

//*************************************************************************************************

#endif
