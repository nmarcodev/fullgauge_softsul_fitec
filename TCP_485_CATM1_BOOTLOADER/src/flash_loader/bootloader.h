/*
 * bootloader.h
 *
 *  Created on: Dec 19, 2016
 *      Author: LAfonso01
 */

#ifndef FLASH_LOADER_BOOTLOADER_H_
#define FLASH_LOADER_BOOTLOADER_H_


#define CPU_S5D9E



//#define BOOTMAGICNUM            0xA5A55A5A
#define APPMAGICNUM                             0xAAAAAAAA
#define APP_RESET                               0x00010000
#define APP_INFO_ADDRESS                        (APP_RESET + 0x300)

#ifdef CPU_S5D5
#define APP_END                 0x0100000
#define APP_FLASH_END           0x000FFFFF  ///<   Last block address
#define APP_BLOCK_START         8
#define APP_BLOCK_END           38
#define APP_BLOCK_SIZE          0x8000
#define APP_LAST_BLOCK_ADDRESS  0x0F0000

#endif

#ifdef CPU_S5D9E
#define APP_END                                 0x0090000//0x0200000
#define APP_FLASH_END                           0x000FFFFF//0x001FFFFF  ///<   Last block address
#define APP_BLOCK_START                         8
#define APP_BLOCK_END                           38//70
#define APP_BLOCK_SIZE                          0x8000UL
#define APP_LAST_BLOCK_ADDRESS                  0x0F0000//0x1F0000


#endif

#define FLASH_DF_BLOCK_SIZE                     (64)          ///< Tamanho de um bloco da dataflash em bytes

#define FLASH_DF_BOOTLOADER_CODE_ADDRRESS       (0x40107F00)
#define BOOTLOADER_VALID_CODE                   0xA5A55A5A
#define APP_EXT_RESET                           BSP_PRV_QSPI_DEVICE_PHYSICAL_ADDRESS
#define APP_EXT_INFO_ADDRESS                    (BSP_PRV_QSPI_DEVICE_PHYSICAL_ADDRESS + 0x300)
#define APP_EXT_END                             (APP_EXT_RESET + APP_END - APP_RESET)


typedef struct
{
    // codigo de firmware valido  0x10300
    uint16_t     valid_mask;
    // versao do firmware           0x10302
    uint8_t     FirmwareVersion;
    // release do firmware          0x10303
    uint8_t     FirmwareRelease;
    //MCU Model S1 = 1, S3 = 3, 4, S5-500k, S5 = 5-1M, S7 = 7       0x10304
    uint8_t    McuModel;
    //Modelo de produto 0x10305
    uint8_t    ProductModel;
    // versão da tabela de parâmetros  0x10306
    uint8_t   ParameterTableHi;
    uint8_t   ParameterTableLo;

    uint8_t   Free01;// 0x10308
    uint8_t   Free02;// 0x10309
    uint8_t   Free03;// 0x1030A
    uint8_t   Free04;// 0x1030B
    uint8_t   Free05;// 0x1030C
    uint8_t   Free06;// 0x1030D
    /* CRC-16 CCITT of image as in MCU flash */
    uint16_t    raw_crc; // 0x1030E
} fl_image_header_t;

typedef enum
{
    CODE_USER   = 0,
    CODE_CRC    = 1,
    CODE_MASK   = 2,
}_ERROR_CODE;

void jumptoApplication(_ERROR_CODE* errorCode);
void bootloader(_ERROR_CODE errorCode);

extern void Leds(bool LedOnR, bool LedOnG, bool LedOnB, bool LedStR, bool LedStG, bool LedStB);

#endif /* FLASH_LOADER_BOOTLOADER_H_ */
