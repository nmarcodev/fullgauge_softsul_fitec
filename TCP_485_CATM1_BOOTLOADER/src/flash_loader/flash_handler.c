#include "hal_data.h"
#include "bootloader.h"
#include "flash_handler.h"



void error(ssp_err_t *err);
static uint32_t address;

ssp_err_t flashInit(void)
{
    ssp_err_t err = SSP_SUCCESS;
    address = (uint32_t)APP_RESET;
    err = g_flash0.p_api->open(g_flash0.p_ctrl,g_flash0.p_cfg);
    g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
    __disable_irq();
    for (uint32_t i = 0; i < (APP_BLOCK_END - APP_BLOCK_START); i++)
    {
        err |= g_flash0.p_api->erase(g_flash0.p_ctrl, (uint32_t)(address + APP_BLOCK_SIZE * i), 1);
        g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
        Leds(true, true, true, i&1, i&1, false);
    }
    __enable_irq();
    if(err != SSP_SUCCESS)
        return err;

    return err;
}

int flashWrite(unsigned char *buffer, int size)
{
    ssp_err_t ret = SSP_SUCCESS;
    __disable_irq();

    for (uint32_t i = 0; i < (APP_BLOCK_END - APP_BLOCK_START); i++)
    {
        ret |= g_flash0.p_api->write(g_flash0.p_ctrl, (uint32_t)(buffer + APP_BLOCK_SIZE * i), (uint32_t)(address + APP_BLOCK_SIZE * i), APP_BLOCK_SIZE);
        g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
        Leds(true, true, true, i&1, i&1, false);
    }

    /*ret = g_flash0.p_api->write(g_flash0.p_ctrl,(uint32_t)buffer,address,(uint32_t)size / 2);
    g_wdt0.p_api->refresh(g_wdt0.p_ctrl);

    buffer += size / 2;
    ret = g_flash0.p_api->write(g_flash0.p_ctrl,(uint32_t)buffer,address + size / 2,(uint32_t)size / 2);
    g_wdt0.p_api->refresh(g_wdt0.p_ctrl);*/
    __enable_irq();
    address += (uint32_t)size;
    return ret;
}

void error(ssp_err_t *err)
{
    if (SSP_SUCCESS != *err)
    {

        while(1)
        {

        }
    }
}


