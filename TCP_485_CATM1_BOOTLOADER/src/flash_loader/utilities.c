/*
 * utilities.c
 *
 *  Created on: Dec 19, 2016
 *      Author: LAfonso01
 */
#include "bsp_api.h"
#include "hal_data.h"
#include "utilities.h"
#include "./flash_loader/bootloader.h"

/******************************************************************************
* Function Name: fl_check_application
* Description  : Does a CRC on MCU flash to make sure current image is valid
* Arguments    : none
* Return value : CRC16-CCITT value of image in MCU flash
******************************************************************************/
uint16_t fl_check_application(void)
{
    uint16_t calc_crc = 0;
    uint32_t start_address1;

    start_address1 = (uint32_t)APP_RESET;

    g_crc0.p_api->open(g_crc0.p_ctrl,g_crc0.p_cfg);
    /* Calculate CRC up to the location where the linker put the CRC value */
    g_crc0.p_api->calculate(g_crc0.p_ctrl,
                            (uint8_t *)start_address1,
                            (uint32_t)APP_INFO_ADDRESS + \
                            offsetof(fl_image_header_t, raw_crc) - \
                            start_address1,
                            0x1D0F,
                            (uint32_t *)&calc_crc);
    /* Move start_address to right after 'raw_crc' */
    start_address1 = (uint32_t)APP_INFO_ADDRESS + \
                    offsetof(fl_image_header_t, raw_crc) + \
                    sizeof(((fl_image_header_t *) 0)->raw_crc);

    /* Calculate the rest of flash after the CRC in memory */
    g_crc0.p_api->calculate(g_crc0.p_ctrl,
                            (uint8_t *)start_address1,
                            (APP_END  - start_address1),
                            calc_crc,
                            (uint32_t *)&calc_crc);
    g_crc0.p_api->close(g_crc0.p_ctrl);

    return calc_crc;
}

uint16_t fl_check_ext_application(void)
{
    uint16_t calc_crc;
    uint32_t start_address1;
    start_address1 = (uint32_t)APP_EXT_RESET;

    g_crc0.p_api->open(g_crc0.p_ctrl,g_crc0.p_cfg);

    /* Calculate CRC up to the location where the linker put the CRC value */
    g_crc0.p_api->calculate(g_crc0.p_ctrl, //crt>dsadta
                            (uint8_t *)start_address1,
                            (uint32_t)APP_EXT_INFO_ADDRESS + \
                            offsetof(fl_image_header_t, raw_crc) - \
                            start_address1,
                            0x1D0F,
                            (uint32_t *)&calc_crc);
    /* Move start_address to right after 'raw_crc' */
    start_address1 = (uint32_t)APP_EXT_INFO_ADDRESS + \
                    offsetof(fl_image_header_t, raw_crc) + \
                    sizeof(((fl_image_header_t *) 0)->raw_crc);

    /* Calculate the rest of flash after the CRC in memory */
    g_crc0.p_api->calculate(g_crc0.p_ctrl,
                            (uint8_t *)start_address1,
                            (APP_EXT_END  - start_address1),
                            calc_crc,
                            (uint32_t *)&calc_crc);
    g_crc0.p_api->close(g_crc0.p_ctrl);

    return calc_crc;
}
