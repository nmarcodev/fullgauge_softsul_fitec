
#ifndef FLASH_HANDLER_H_
#define FLASH_HANDLER_H_

ssp_err_t flashInit(void);
int flashWrite(unsigned char *buffer, int size);

#endif /* FLASH_HANDLER_H_ */
