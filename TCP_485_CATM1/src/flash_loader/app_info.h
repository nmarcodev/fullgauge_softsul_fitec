/*
 * app_info.h
 *
 *  Created on: Dec 21, 2016
 *      Author: LAfonso01
 */

#ifndef FLASH_LOADER_APP_INFO_H_
#define FLASH_LOADER_APP_INFO_H_

//#define MAGICNUM  0xA5A55A5A
#define APP_INFO_ADDRESS 0x10300

typedef struct
{
    // codigo de firmware valido  0x10300
    uint16_t     valid_mask;
    // versao do firmware           0x10302
    uint8_t     FirmwareVersion;
    // release do firmware          0x10303
    uint8_t     FirmwareRelease;
    //MCU Model S1 = 1, S3 = 3, 4= S5D5 500k, S5D5 1M= 5, 6= S5D9, S7 = 7      0x10304
    uint8_t    McuModel;
    //Modelo de produto 0x10305
    uint8_t    ProductModel;
    // versão da tabela de parâmetros  0x10306
    uint8_t   ParameterTableHi;
    uint8_t   ParameterTableLo;

    uint8_t   Free01;// 0x10308
    uint8_t   Free02;// 0x10309
    uint8_t   Free03;// 0x1030A
    uint8_t   Free04;// 0x1030B
    uint8_t   Free05;// 0x1030C
    uint8_t   Free06;// 0x1030D
    /* CRC-16 CCITT of image as in MCU flash */
    uint16_t    raw_crc; // 0x1030E
} fl_image_header_t;


extern fl_image_header_t __attribute__((section (".app_info"))) AppInfo;

#endif /* FLASH_LOADER_APP_INFO_H_ */
