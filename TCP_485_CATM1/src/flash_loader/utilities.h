/*
 * utilities.h
 *
 *  Created on: Dec 26, 2016
 *      Author: LAfonso01
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_

//#include "r_crc.h"
#include "r_flash_hp.h"
#include "Includes/TypeDefs.h"

_FG_ERR boot_request(const flash_instance_t *p_flash, uint8_t *p_data, uint32_t size);

#endif /* UTILITIES_H_ */
