/*
 * qspi_programmer.h
 *
 *  Created on: May 14, 2018
 *      Author: LAfonso01
 */

#ifndef FLASH_LOADER_QSPI_PROGRAMMER_H_
#define FLASH_LOADER_QSPI_PROGRAMMER_H_

#include "r_qspi.h"

ssp_err_t spi_flash_erase0(const qspi_instance_t *p_qspi,uint32_t address, uint32_t block_size);
ssp_err_t spi_flash_program(const qspi_instance_t *p_qspi, uint32_t spi_address, uint8_t *p_data, uint32_t size);

#endif /* FLASH_LOADER_QSPI_PROGRAMMER_H_ */
