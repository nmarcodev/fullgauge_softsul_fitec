/*
 * ApplicationUSB.c
 *
 *  Created on: 27 de mai de 2021
 *      Author: rhenanbezerra
 */

#include "usb_hid_device_thread.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/TypeDefs.h"
#include "ux_api.h"
#include "Includes/FlashQSPI.h"
//#include "ux_device_class_hid.h"
#include "common_data.h"
#include "Includes/Define_IOs.h"
//#include "Includes/serial485_entry.h"
#include "Includes/ExternalFlash.h"
#include "stdio.h"
#include "Includes/CRCModbus.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/Thread_Datalogger_entry.h"

#define HANDLE_FLASH_ERRORS (0)
extern _USB_CONTROL USBControl;

TX_THREAD   USB_Thread;

#if FIRMWARE_VERSION == 99
const uint8_t aes_key2[]   = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
#else
#error no firmware version defined!
#endif
const uint8_t IVc2[]   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

uint16_t AES_Enc2(uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets);
uint16_t AES_Dec2(uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets);
//uint16_t AES_Dec (_USB_CONTROL *USBData, uint16_t InputOctets);


uint16_t ProcessPacketUSB(uint8_t *BufferInput, _BUFFER_USB *BufferUSB, _USB_CONTROL *USBData);
//uint16_t GetConfSitrad(uint8_t id, uint8_t * GetConfData, _USB_CONTROL *USBData, bool FlagReadConf);
//void     SetConfSitrad(uint8_t * SetConfData, _USB_CONTROL *USBData, uint16_t SizeRequest);
//void 	 StartTimerReset(void);

_USB_RETURN AppPacketUSB(_USB_CONTROL *USBData);

//uint16_t RequesitaInfoConf(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber);
uint16_t DownloadFirmware(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber);
//uint16_t AtualizaInfoConf(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber);
//uint16_t UpdadeDateHour(uint8_t * BufferInput, uint8_t * GetConfData, _USB_CONTROL *USB_Control, uint16_t RandomNumber);
//uint16_t ReadPgDatalogger(_BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber);
//uint16_t DisconnectConv(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber);
//uint16_t EraseDatalogger(uint8_t Id, uint8_t * GetConfData, uint16_t RandomNumber);
//uint16_t GetStatus(uint8_t id, uint8_t * GetConfData, _USB_CONTROL *USB_Control, uint16_t RandomNumber);
uint16_t AnswerDefault(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber);
//bool     TestQSPI(void);
//bool     TestSerial485(uint8_t AddressInst485);
//uint8_t  TestStatusRssid(uint8_t Rssid_In);

uint16_t FactoryTest(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber);
uint16_t LoginDataResponse(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber);

uint16_t lastBuffer = 0;

UINT ux_hid_device_callback(UX_SLAVE_CLASS_HID* hid, UX_SLAVE_CLASS_HID_EVENT* hid_event)
{
    /* Does nothing. */
    SSP_PARAMETER_NOT_USED(hid);

    memcpy(&USBControl.BufferUSB.HeaderPayload[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB + SIZE_PAYLOAD_USB);
    memcpy(&USBControl.BufferUSB.HP.Header[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB);
    memcpy(&USBControl.BufferUSB.HP.Payload[0],&hid_event->ux_device_class_hid_event_buffer[3], SIZE_PAYLOAD_USB);

    AppPacketUSB(&USBControl);

    //piscaLed( LED_ST_R,  500U);

    return UX_SUCCESS;
}


/**
 * Trata um pacote USB recebido.
 */
_USB_RETURN AppPacketUSB(_USB_CONTROL *USBData)
{
	_BUFFER_USB     *BufferUSB;
	ConvBytesToInt  CmdSize;
	uint8_t         BufferRxUSBSitradOut[SIZE_HEADER_USB + SIZE_PAYLOAD_USB]; ///> Buffer de envio
	uint16_t        PayloadLength,
					SizeRxFrame = 0;

	BufferUSB = &USBData->BufferUSB;

	if(BufferUSB->HP.Header[0] >= DATA_UNKNOWN) //comando invalido;
	{
		USBData->BigFrame = 0;
		return USB_RELISTEN;
	}

	CmdSize.Bytes[0] = BufferUSB->HP.Header[2];
	CmdSize.Bytes[1] = BufferUSB->HP.Header[1];
	PayloadLength    = CmdSize.Word16;
	if(!PayloadLength) //se o payload tem tamanho zero
	{
		return USB_RELISTEN;
	}

	// Todos os pacotes sao criptografados com uma chave AES
	memset(&BufferRxUSBSitradOut[0], 0, sizeof(BufferRxUSBSitradOut));
	SizeRxFrame = AES_Dec2(&BufferUSB->HP.Payload[0], &BufferRxUSBSitradOut[0], PayloadLength);
	//SizeRxFrame = AES_Dec(USBData, PayloadLength);
	if(!SizeRxFrame)
	{
		return USB_RELISTEN;//return EW_TCP_NONE;//return EW_TCP_RELISTEN;
	}

	switch(BufferUSB->HP.Header[0])
	{
	   case DATA_RS485_SPACE://implementacao futura para modbus (8 bits)
		break;
		//Comandos requisitando comunicacao serial somente podem ser enviados pela porta de comunicacao (SITRAD)
	   case DATA_RS485_MARK:
		   memcpy(&USBData->BufferUSB.HP.Payload, BufferRxUSBSitradOut+2, (size_t)(SizeRxFrame-2));
		   //tamanho menos os numeros randomicos
		   USBData->SerialDataInPayload->num_of_bytes = (uint32_t)(SizeRxFrame-2);
		   //payload menos os 2 bytes de num. randomico.
		   USBData->SerialDataInPayload->pointer = (uint16_t *)USBData->BufferUSB.HP.Payload;
		   return USB_REQUESTED_485;
	   break;

	   // Comandos de configuracao / status podem ser enviados por ambas as portas, de preferencia pela porta de configuracao
	   case DATA_USB_MODULE:
		   PayloadLength = ProcessPacketUSB(&BufferRxUSBSitradOut[0], BufferUSB, USBData);

		  if(PayloadLength) // Payload len zero indica que nenhuma resposta deve ser enviada.
		  {
			  if (( PayloadLength % 16U ) != 0 )
			  {
				  uint8_t tmp;
				  tmp = (uint8_t)((uint8_t)(PayloadLength / 16U) + (uint8_t)1U);
			      PayloadLength = (uint8_t)(tmp*16U);
			  }

			  memset(&BufferRxUSBSitradOut[0], 0, sizeof(BufferRxUSBSitradOut));

			  // Todos os pacotes sao criptografados com uma chave AES
			  SizeRxFrame = AES_Enc2(&BufferUSB->HP.Payload[0], &BufferRxUSBSitradOut[0], PayloadLength);

			  memcpy(&BufferUSB->HP.Payload[0], &BufferRxUSBSitradOut[0], SizeRxFrame);

			  memcpy(&USBData->BufferUSB, BufferUSB, SizeRxFrame+3U);

			  SendPacketUSB(USBData,(uint16_t)(PayloadLength+3U));

			  return USB_SEND;
		  }
	   break;
	}

	return USB_SEND;
}

uint16_t ProcessPacketUSB(uint8_t *BufferInput, _BUFFER_USB *BufferUSB, _USB_CONTROL *USBData)
{
	#define TypeCom 2
    int RNumber = rand();
    uint16_t Size = 0;
	uint16_t    SizeTemp0,
				SizeTemp1 = 0;
	uint8_t     Dif = 0;


	switch(BufferInput[TypeCom])
	{
//		char senha_conversor[8];
//		char data_hora[6];

		case CMD_LOGIN_CONVERSOR:
//			print485("Realiza login no conversor.\r\n", LEN("Realiza login no conversor.\r\n") );
//			memcpy(&senha_conversor[0],&USBData->BufferRx[4], USBData->BufferRx[3]);
//			print485("A senha do conversor e:\r\n", LEN("A senha do conversor e:\r\n") );
//			char rtc_writer[50];
//			uint8_t n = 0;
//			n=(uint8_t)sprintf(rtc_writer, "%d%d%d%d%d%d%d%d\r\n", (int)senha_conversor[0],(int)senha_conversor[1],(int)senha_conversor[2],(int)senha_conversor[3],(int)senha_conversor[4],(int)senha_conversor[5],(int)senha_conversor[6],(int)senha_conversor[7]);
//			print485(rtc_writer, n);
			Size = LoginDataResponse(&BufferInput[0], &BufferUSB->HeaderPayload[0], (uint16_t)RNumber);
			break;

		case CMD_REINICIA_CONVERSOR: // REINICIA O CONVERSOR (REBOOT)
			//print485("Reiniciar conversor.\r\n", LEN("Reiniciar conversor.\r\n") );
			Size = AnswerDefault(&BufferInput[0], &BufferUSB->HeaderPayload[0], (uint16_t)RNumber);
			//USBData->TimerRestartModule = 3; //INICIALIZA A VARIAVEL QUE VAI RESETAR O CONVERSOR
			break;

		case CMD_CONF_FABRICA:
			//print485("Reset configuracoes para valores de fabrica.\r\n", LEN("Reset configuracoes para valores de fabrica.\r\n") );
			Size = AnswerDefault(&BufferInput[0], &BufferUSB->HeaderPayload[0], (uint16_t)RNumber);
			//USBData->TimerRestoreFactory = 3; //Carrega contador para Restaurar o padrao de fabrica
			break;

		case CMD_ATUALIZA_CONF:
			//print485("Atualizar informacoes de configuracao.\r\n", LEN("Atualizar informacoes de configuracao.\r\n") );
			//AtualizaInfoConf(USBData, &BufferUSB[0], &BufferInput[0], (uint16_t)RNumber);
			break;

		case CMD_CMD_REQ_CONF:
			//print485("Requisita as informacoes de configuracao.\r\n", LEN("Requisita as informacoes de configuracao.\r\n") );
			//RequesitaInfoConf(USBData, &BufferUSB[0], &BufferInput[0], (uint16_t)RNumber);
			break;

		case CMD_DESC_CONVERSOR:
			//print485("Solicita desconexão do conversor.\r\n", LEN("Solicita desconexão do conversor.\r\n") );
			Size = AnswerDefault(&BufferInput[0], &BufferUSB->HeaderPayload[0], (uint16_t)RNumber);
		    //tx_event_flags_set (&USB_Flags, FLAG_USB_DISCONNECTED, TX_OR);
			break;

		case CMD_LIMPAR_DATALOGGER:
			//print485("Comando para apagar datalogger do conversor.\r\n", LEN("SComando para apagar datalogger do conversor.\r\n") );
			//tx_event_flags_set (&DataloggerEventFlags, DL_FLAG_CLEAR, TX_OR); // Seta a flag para a thread datalogger apagar o registro de logs.
			Size = AnswerDefault(&BufferInput[0], &BufferUSB->HeaderPayload[0], (uint16_t)RNumber);
			break;

		case CMD_LEITURA_LOG:
			//print485("Comando para fazer leitura de uma pagina completa de log do conversor.\r\n", LEN("Comando para fazer leitura de uma pagina completa de log do conversor.\r\n") );
			//ReadPgDatalogger(&BufferUSB[0], &BufferInput[0], (uint16_t)RNumber);
			break;

		case CMD_AJUSTE_DATA_HORA:
			//print485("Comando para ajustar a data e hora do conversor.\r\n", LEN("Comando para ajustar a data e hora do conversor.\r\n") );
			//memcpy(&data_hora[0],&USBData->BufferRx[4], USBData->BufferRx[3]);
			//print485("A data e hora atual e:\r\n", LEN("A data e hora atual e:\r\n") );
			//*rtc_writer = 0;
			//n = 0;
			//n=(uint8_t)sprintf(rtc_writer, "%d-%d-%d %d:%d:%d\r\n", (int)data_hora[0],(int)data_hora[1],(int)data_hora[2],(int)data_hora[3],(int)data_hora[4],(int)data_hora[5]);
			//print485(rtc_writer, n);

			//UpdadeDateHour(&BufferInput[0], &BufferUSB->HeaderPayload[0], USBData, (uint16_t)RNumber);
			//seta o flag primeiro.
			//tx_event_flags_set (&USB_Flags, FLAG_RTC_SEND, TX_OR); // Seta a flag para a thread do RTC ajustar o horario
			break;

		case CMD_STATUS_CONVERSOR:
			//print485("Comando que requisita o status do conversor.\r\n", LEN("Comando que requisita o status do conversor.\r\n") );
			//GetStatus(BufferInput[TypeCom], &BufferUSB->HeaderPayload[0], USBData, (uint16_t)RNumber);
			break;

		case CMD_ATUALIZACAO_FW:
			//print485("Comando que envia atualizacao do firmware para o conversor.\r\n", LEN("Comando que envia atualizacao do firmware para o conversor.\r\n") );
			Size = DownloadFirmware(USBData, &BufferUSB[0], &BufferInput[0], (uint16_t)RNumber);
			break;

		case CMD_TESTE_FABRICA:
			//print485("Comando que realiza o teste de fobrica do modulo.\r\n", LEN("Comando que realiza o teste de fobrica do modulo.\r\n") );
			//FactoryTest(USBData, &BufferUSB[0], &BufferInput[0], (uint16_t)RNumber);;
			break;

		default:
			//print485("Comando desconhecido.\r\n", LEN("Comando desconhecido.\r\n") );
			BufferUSB->HeaderPayload[1] = 0;
			BufferUSB->HeaderPayload[2] = 0;
			Size = 0;
		    break;
	}

	SizeTemp0 = BufferUSB->HeaderPayload[1];
	SizeTemp0 = (uint16_t)((SizeTemp0 << 8) | BufferUSB->HeaderPayload[2]);
	if((Size > 6) && (BufferUSB->HeaderPayload[0] == HEADER_TX_USB))
	{
		SizeTemp1 = BufferUSB->HeaderPayload[6];
		SizeTemp1 = (uint16_t)((SizeTemp1 << 8) | BufferUSB->HeaderPayload[7]);

		Dif = (uint8_t)(SizeTemp0 - SizeTemp1);
		if(Dif >= 5)
		{
			Dif = (uint8_t)(Dif - 5);
		}
	}
	else if(Size > 0)
	{
		Dif = (uint8_t)SizeTemp0;
		Dif = (uint8_t)(Dif - 3);
	}
	else
	{
		Dif = 0;
		SizeTemp0 = 0;
	}

	for(uint16_t i=0; i<=Dif; i++)
	{
		BufferUSB->HeaderPayload[i+Size] = Dif;
	}

	return SizeTemp0;

}

//uint16_t ReadPgDatalogger(_BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber)
//{
//    /* Teste ...  08 00 03 00 00 01 08 08 08 08 08 08 08 08*/
//    /* Pagina(d39 = 0x27) 08 00 03 00 27 01 08 08 08 08 08 08 08 08
//    * */
//    //print_to_console("\n\r [DM] SITRAD TCP -> 0x08 - READ_PG_DATALOGGER!");
//    //if(S25FL064ChipEraseFinished() == 0)
//    // 0   1   2   3  4  5 6   7  ....
//    //RDM RDL 08  00  03 x y   z  (X || Y): Pagina a Ler  z: Qntidade de paginas.
//    uint16_t Size = 0;
//    uint16_t Temp;
//
//    Temp = (uint16_t)((BufferInput[5] <<  8) | (BufferInput[6]));         //captura o numero da pagina a ler
//    uint8_t QtdPages = BufferInput[7];
//
//        //-------------------Header------------------------
//    BufferUSB->HeaderPayload[Size++] = HEADER_TX_USB;
//    BufferUSB->HeaderPayload[Size++] = 0x00;
//    BufferUSB->HeaderPayload[Size++] = 0x00;
//    //-------------------Payload-----------------------
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber >> 8   );
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber ^  0xFF);
//    BufferUSB->HeaderPayload[Size++] = BufferInput[TypeCom] | 0x80;
//    BufferUSB->HeaderPayload[Size++] = 0x00;
//    BufferUSB->HeaderPayload[Size++] = 0x00;
//
//    if((QtdPages > MAX_PAGES_TO_READ) || (QtdPages < 1))
//    {
//#if HANDLE_FLASH_ERRORS
//        BufferTcp->HeaderPayload[5] = BufferInput[TypeCom] | 0xA0;
//
//#endif
//        Size = 8;
//        BufferUSB->HeaderPayload[6] = 0; //ok
//        BufferUSB->HeaderPayload[7] = 0; //ok
//
//    }
//    else // tudo ok, pode ler as paginas
//    {
//
//
//        static _DATA_LOGGER_DATA DataLoggerData;
//        DataLoggerData.OpCod = REQUEST_PAG; //Sinaliza o Cod da operacao
//        DataLoggerData.QntPg = QtdPages; //numero de paginas que deve ser lida
//        DataLoggerData.SizeTemp = Temp; //primeira pagina a ser lida
//        DataLoggerData.PayloadTcp = &(BufferUSB->HeaderPayload[Size]);
//
//        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
//        tx_thread_relinquish();//TODO tratar o retorno da funcao da flash datalogger
//        tx_queue_receive(&USB_Flash, &DataLoggerData, TX_WAIT_FOREVER); //recebe da thread Datalogger
//        //todo verificar variável EthWifi_Flash
//
//#if HANDLE_FLASH_ERRORS
//        if(!DataLoggerData.status)//erro de leitura da flasj qspi
//        {
//            BufferTcp->HeaderPayload[5] = BufferInput[TypeCom] | 0xC0;
//
//            Size = 8;
//            BufferTcp->HeaderPayload[6] = 0; //ok
//            BufferTcp->HeaderPayload[7] = 0; //ok
//        }
//        else
//#endif
//        {
//            uint16_t u16LQtd;
//            u16LQtd = (uint16_t)((uint16_t)(QtdPages*PAGE_SIZE) + (uint16_t)(CRC_SIZE));
//
//            BufferUSB->HeaderPayload[6] = (uint8_t)((u16LQtd >>  8) & 0xFF); //ok
//            BufferUSB->HeaderPayload[7] = (uint8_t)((u16LQtd >>  0) & 0xFF); //ok
//
//            u16LQtd = (uint16_t)(u16LQtd + (uint16_t)(HEADER_SIZE));
//
//            Size = (uint16_t)(u16LQtd - (uint16_t)CRC_SIZE);
//
//            Temp = (uint16_t)(CrcModbusCalc((uint8_t*)&BufferUSB->HeaderPayload[5], Size, false));
//
//            Size = (uint16_t)(Size + (uint16_t)5);
//
//            BufferUSB->HeaderPayload[Size++] = (uint8_t)((Temp >>  8) & 0xFF);
//            BufferUSB->HeaderPayload[Size++] = (uint8_t)((Temp >>  0) & 0xFF);
//        }
//    }
//
//    /*Ajusta o tamanho do payload*/
//    uint16_t DifTemp = (Size % 16);
//    BufferUSB->HeaderPayload[1] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  8) & 0xFF);
//    BufferUSB->HeaderPayload[2] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  0) & 0xFF);
//
//    return Size;
//}


uint16_t DownloadFirmware(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber)
{
    bool                FSend       = false;
    uint16_t            Size        = 0,
                        SizePayload = 0,
                        SizeFile    = 0,
                        CRC_Calc    = 0,
                        CompFile    = 0,
                        FragFile    = 0,
                        CRC_Recv    = 0;
    static uint32_t     AddressFirmware = 0;
    static uint16_t     ExpFragFile = 0;
    USBData->TimerTimeOutOTA = 8;

    //ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING); //para o monitorando

    USBData->FlagOTARun = true;
    UINT OldPrority;
    tx_thread_priority_change(&USB_Thread, 1, &OldPrority);

    SizePayload = (uint16_t)((BufferInput[3] <<  8) | (BufferInput[4])); //captura o tamanho do payload
    SizeFile    = (uint16_t)(SizePayload - 6);

    FragFile    = (uint16_t)((BufferInput[5] <<  8) | (BufferInput[6])); //captura o fragmento do arquivo
    CompFile    = (uint16_t)((BufferInput[7] <<  8) | (BufferInput[8])); //captura o numero de partes do arqvivo

    CRC_Calc = (uint16_t)(CrcModbusCalc((uint8_t*)&BufferInput[9], SizeFile, true));
    CRC_Recv = (uint16_t)((BufferInput[SizePayload+3] <<  8) | (BufferInput[SizePayload+4]));

    if(!FragFile) //Sinaliza que vai comecar a enviar o firmware;
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_FOTA_STATUS_CHANGE, 0, true);
#endif

        _DATA_LOGGER_DATA DataLoggerData;
        DataLoggerData.OpCod = START_OTA; //Avisa a thread datalogger que uma atualizacao vai comecar.
        tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger

        ExpFragFile = 1;
        AddressFirmware = 0;
        ClearSectorFlashQSPIFirm((uint16_t)AddressFirmware);
        if(!USBData->FlagConnectSitrad)
		{
			FSend = true;
		}
    }

    else if((CRC_Calc == CRC_Recv) && (FragFile <= CompFile))
    {
       if(FragFile != ExpFragFile)
       {
           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW);
           FSend = false;
       }
       else
       {
           g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);

           USBData->FlashExternalPaylod->SizeData = (uint16_t)SizeFile;
           USBData->FlashExternalPaylod->FragFile = (uint16_t)FragFile;
           USBData->FlashExternalPaylod->CompFile = (uint16_t)CompFile;
           USBData->FlashExternalPaylod->DataFlash = &BufferInput[9];

           AddressFirmware = SaveFirmwareQSPI(USBData->FlashExternalPaylod, AddressFirmware);
           ExpFragFile ++;
           FSend = true;
       }

       if(CompFile == FragFile)
       {
           if(!FSend)
           {
               USBData->TimerTimeOutOTA = 3;
           }
           else
           {
               USBData->TimerOTACheckCRC = 2;
           }
       }
    }

    //-------------------+ Header +------------------------
    BufferUSB->HeaderPayload[Size++] = HEADER_TX_USB;
    BufferUSB->HeaderPayload[Size++] = 0x00;
    BufferUSB->HeaderPayload[Size++] = 0x10;
    //------------------+ Payload +------------------------
    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber   >>  8   );
    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
    BufferUSB->HeaderPayload[Size++] = (uint8_t)BufferInput[TypeCom] | 0x80;
    BufferUSB->HeaderPayload[Size++] = 0;//tamanho
    //BufferUSB->HeaderPayload[Size++] = 1;//tamanho todo corrigir protocolo
    if(FSend)
    {
    	BufferUSB->HeaderPayload[Size++] = 0;
        FSend = false;
    }
    else
    {
    	BufferUSB->HeaderPayload[Size++] = 1;
    	BufferUSB->HeaderPayload[Size++] = (uint8_t)ExpFragFile;
    }

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);

    return Size;
}


//uint16_t FactoryTest(_USB_CONTROL *EthUSBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber)
//{
//    //ULONG               IpActualStatus  = 0;
//    bool                FSend           = true;
//    uint16_t            Size            = 0;
//
//
//
//    //75 ce 0C 00 01 00 0A 0A 0A 0A 0A 0A 0A 0A 0A 0A
//    //-------------------+ Header +------------------------
//    BufferUSB->HeaderPayload[Size++] = HEADER_TX_USB;
//    BufferUSB->HeaderPayload[Size++] = 0x00;
//    BufferUSB->HeaderPayload[Size++] = 0x10;
//    //------------------+ Payload +------------------------
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber   >>  8   );
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber   ^   0xFF);
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)BufferInput[TypeCom] | 0x80;
//    BufferUSB->HeaderPayload[Size++] = 0x00;//06
//    BufferUSB->HeaderPayload[Size++] = 0x02;//7
//    BufferUSB->HeaderPayload[Size++] = BufferInput[5]; //8 sub id
//    Size++;
//
//    // verifica se pode entrar em modo teste
//    if((!EthUSBData->FlagFAC))
//    {
//        return Size;
//    }
//
//    switch(BufferInput[5])
//    {
//        case FAC_TEST_START:
//        {
//        	EthUSBData->FlagFAC = true;
//
//            //_DATA_LOGGER_DATA DataLoggerData;
//            //DataLoggerData.OpCod = TEST_START; //Avisa a thread datalogger que uma atualizacao vai comecar.
//            //tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger
//
//            //ThreadMainManagerSetFlag(MM_FAC_MODE);
//
//        }break;
//
//        case FAC_TEST_SUPER_CAP_ON:
//        {
//            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_HIGH);
//
//            tx_thread_sleep(TIME_200MS);
//
//            //InitI2C_RTC();
//
//            tx_thread_sleep(TIME_200MS);
//
//            // inicializa o RTC Externo
//            //RX8571LC_Init();
//
//            // verifica estado de ECLO
//            //if(FG_SUCCESS != RX8571LC_VerifyEclo())
//            //{
//                //FSend = false; //foi mal.
//            //}
//
//          //  ThreadMonitor_SendKeepAlive(T_MONITOR_START_MONITORING);
//        }break;
//
//        case FAC_TEST_SUPER_CAP_OFF:
//        {
//           // ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING);
//
//            //CloseI2C_RTC();
//
//            g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_LOW);
//
//        }break;
//
//        case FAC_TEST_QSPI:
//        {
////            FSend = TestQSPI(); //ret=true ok, ret false, nok
//
//        }break;
//
//        case FAC_TEST_LEDS:
//        {
////            ThreadMainManagerSetLedColor(BufferInput[6]);
//           // ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);
//
//        }break;
//
//        case FAC_TEST_485:
//        {
////            FSend = TestSerial485(BufferInput[6]);
//
//        }break;
//
//        case FAC_TEST_KEY:
//        {
////            FSend = ThreadMainManagerGetStatusButton();
//        }break;
//
//        case FAC_TEST_WIFI:
//        {//6f   3a  8c  00  02  07  01  09  09  09  09  09  09  09  09  09
//            //          06  07 08   09    10 11 12 13 14 15
//            //xx xx 8C MSB LSB YY [00/01] 00 01 QR 01 WW SI...SI 02 01 IR -> FAC_TEST_WIFI [OK/NÃO_OK]
//            //QR: Quantidade de redes WW: Tamanho do nome da rede mais forte IR: Intensidade da Rede mais forte
////        	BufferUSB->HeaderPayload[7] = (uint8_t)TestModuleWiFi(&BufferUSB->HeaderPayload[10]); //retorna o tamanho
//            uint8_t TempSize = (uint8_t)(BufferUSB->HeaderPayload[7] + 5) / 16;
//
//            TempSize++;
//
//            BufferUSB->HeaderPayload[2] = (uint8_t)(0x10 * TempSize);
//
//            if(!BufferUSB->HeaderPayload[11])
//            {
//                FSend = false;
//            }
//
//            Size = (uint16_t)(8 + BufferUSB->HeaderPayload[7]);
//        }break;
//
//        case FAC_TEST_MAC:
//        {
////            Size = (uint16_t)((uint8_t)10 + TestMacAddress(&BufferUSB->HeaderPayload[10]));
//            BufferUSB->HeaderPayload[2] = 0x20;
//            BufferUSB->HeaderPayload[7] = 18;
//        }break;
//
//        case FAC_TEST_DO_FAC:
//        {
//           // _FLAGS_EEPROM FlagsEeprom;
//         //   FlagsEeprom.Bites.bCalibration = true;
//          //  SetValueFacEEPROM(&FlagsEeprom.Bytes);
//            EthUSBData->TimerRestartModule = 3; //INICIALIZA A VARIAVEL QUE VAI RESETAR O CONVERSOR
//        }break;
//
//        case FAC_TEST_END:
//        {
////            ThreadMainManagerSetLedColor(12);
//            //ThreadMainManagerSetFlag(MM_FAC_TEST_LEDs);
//        }break;
//
//        default:
//            break;
//    }
//
//    if(FSend)
//    {
//    	BufferUSB->HeaderPayload[9] = 0;
//    }
//    else
//    {
//    	BufferUSB->HeaderPayload[9] = 1;
//    }
//
//    return Size++;
//}
//
//
//bool TestSerial485(uint8_t AddressInst485)
//{
//    ULONG SerialFlag = 0;
//    uint32_t AddressInst_485;
//    AddressInst_485 = AddressInst485;
//
//    //tx_queue_send(&QueueTest485, &AddressInst_485, TX_WAIT_FOREVER);   //envia para a thread Serial
//    tx_thread_relinquish();
//
//    //tx_event_flags_get(&EventFlagsSerialTestF, 0x01, TX_OR_CLEAR, &SerialFlag, TIME_2S);
//
//    return (bool)SerialFlag;
//}
//
//bool TestQSPI(void)
//{
//    uint8_t Tve[BUFFER_LENGTH-1];
//    uint8_t InTve[BUFFER_LENGTH];
//    bool FlagTestMem = false;
//
//    memset(&InTve[0], 0xFF, BUFFER_LENGTH);
//
//    FlashReadFirmwareData(0, 1, &InTve[0]);
//    for(uint8_t i=0; i < sizeof(Tve); i++)
//    {
//        if(InTve[i] == 0xFF)
//        {
//            FlagTestMem = true; //se a flash vazia
//        }
//    }
//
//    if(FlagTestMem)
//    {
//        ClearSectorFlashQSPIFirm(0);
//        memset(&InTve, 0, sizeof(InTve));
//
//        for(uint8_t i=0; i < sizeof(Tve); i++)
//        {
//            Tve[i] = (uint8_t)rand();
//        }
//
//        FlashWritePageData(QSPI_DEVICE_ADDRESS, sizeof(Tve), &Tve[0]);
//        FlashReadFirmwareData(0, 1, &InTve[0]);
//        for(uint8_t i=0; i < sizeof(Tve); i++)
//        {
//            if(InTve[i] != Tve[i])
//            {
//                return false;
//            }
//        }
//    }
//
//    return true;
//}
//
//
//uint16_t AtualizaInfoConf(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber)
//{
//	uint8_t Size = 0,
//			SizePayload = 0,
//			SizeFile    = 0,
//			CompFile    = 0,
//			FragFile    = 0;
//
//    //-------------------Header------------------------
//    BufferUSB->HeaderPayload[Size++] = HEADER_TX_USB;                           //TYPE_IND
//    BufferUSB->HeaderPayload[Size++] = 0x00;                                    //SIZE1_IND
//    BufferUSB->HeaderPayload[Size++] = 0x10;//0x01;                             //SIZE2_IND
//    //-------------------Payload-----------------------
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber    >>     8);     //RANDOM1_IND
//    BufferUSB->HeaderPayload[Size++] = (uint8_t)(RandomNumber    ^   0xFF);     //RANDOM2_IND
//    BufferUSB->HeaderPayload[Size++] = BufferInput[TypeCom] |    0x80;          //COMMAND_IND
//    BufferUSB->HeaderPayload[Size++] = 0x00;          //SIZE
//    BufferUSB->HeaderPayload[Size++] = 0x00;          //SIZE
//
//    SizePayload = BufferInput[3];
//    FragFile    = BufferInput[4];
//    CompFile    = BufferInput[5];
//    SizeFile    = (uint8_t)(SizePayload - 2);
//
//    if(FragFile < CompFile)
//    {
//    	memcpy(&USBData->BufferInfoConf[lastBuffer], &BufferInput[6], SizeFile);
//    }
//    else if (FragFile == CompFile)
//    {
//    	memcpy(&USBData->BufferInfoConf[lastBuffer], &BufferInput[6], SizeFile);
//    	lastBuffer = 0;
//    	//SetConfSitrad(USBData); //envia os parametros recebidos do configurador para a memoria flash.
//    }
//
//    return Size;
//}
//
//
//uint16_t RequesitaInfoConf(_USB_CONTROL *USBData, _BUFFER_USB *BufferUSB, uint8_t * BufferInput, uint16_t RandomNumber)
//{
//    // exemplo 85 00 fd 16 02 00 1e 17 f7
//    //A principio o sitrad deve enviar o range dos comandos que o conversor de responder.
//    //Caso A: 0x00 0x00 = Respondo tudo, estilo UDP
//    //Caso B: 0x01 0x05 = Respondo o intervalo, do 0x01 ao 0x05
//    //Caso C: 0x03 0x03 = Respondo apenas o 0x03
//
//    uint16_t    Size = 0;
//
//    bool        FlagReadConf = false;
//
//    //-------------------Header------------------------
//    BufferUSB->HeaderPayload[Size++]  = HEADER_TX_USB;
//    BufferUSB->HeaderPayload[Size++]  = 0;
//    BufferUSB->HeaderPayload[Size++]  = 0;
//    //-------------------Payload-----------------------
//    BufferUSB->HeaderPayload[Size++]  = (uint8_t)(RandomNumber >> 8  );
//    BufferUSB->HeaderPayload[Size++]  = (uint8_t)(RandomNumber ^ 0xFF);
//    BufferUSB->HeaderPayload[Size++]  = BufferInput[2] | 0x80;
//    BufferUSB->HeaderPayload[Size++]  = 0x00;
//    BufferUSB->HeaderPayload[Size++]  = 0x00;
//
//
//    if(!BufferInput[3])
//    {
//        //varre o intervalo
//        uint16_t    TmpRet,
//                    SizePayload = 0;
//        for(uint8_t i = 0x16; i <= 0x17; i++)
//        {
//            //TmpRet = (uint16_t)GetConfSitrad(i, &BufferUSB->HeaderPayload[Size], USBData, FlagReadConf);
//            FlagReadConf = true;
//            if(TmpRet)
//            {
//                SizePayload = (uint16_t)(SizePayload + TmpRet);
//                Size = (uint16_t)(TmpRet + Size);
//            }
//        }
//
//        BufferUSB->HeaderPayload[6] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF);
//        BufferUSB->HeaderPayload[7] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF);
//
//        Size = (uint16_t)(SizePayload  + (uint16_t)5); //ajustar o payload
//
//        uint8_t TempSize = (uint8_t)(Size / 16);
//
//        TempSize++;
//
//        BufferUSB->HeaderPayload[2] = (uint8_t)(0x10 * TempSize);
//
//        Size = (uint16_t)(Size + (uint16_t)3);
//    }
//    else
//    {
//        //varre o intervalo
//        uint16_t    TmpRet,
//                    SizePayload = 0,
//                    SizeRequest;
//        SizeRequest = (uint16_t)(BufferInput[3]);
//
//        if(!BufferInput[5]) //verifica se eh range de IDs!
//        {
//            if(!BufferInput[6] && !BufferInput[7]) // 00 00 :> que todos os IDS
//            {
//                BufferInput[6] = ID_MAC_ADDRESS;    //Primeiro ID
//                BufferInput[7] = ID_END;            //Ultimo ID
//            }
//
//            for(uint8_t i = BufferInput[6]; i <= BufferInput[7]; i++)
//            {
//                //TmpRet = (uint16_t)GetConfSitrad(i, &BufferUSB->HeaderPayload[Size], USBData, FlagReadConf);
//                FlagReadConf = true;
//                if(TmpRet)
//                {
//                    SizePayload = (uint16_t)(SizePayload + TmpRet);
//                    Size = (uint16_t)(TmpRet + Size);
//                }
//            }
//        }
//        else //Neste caso, o Sitrad quer os IDs individuais.
//        {
//            if(SizeRequest)
//            {
//                SizeRequest = (uint16_t)(SizeRequest - 1); //remove o sinalizador
//
//                uint8_t IndexBufferInput = 6;
//                for(uint8_t i = IndexBufferInput; i <= (SizeRequest+IndexBufferInput); i++)
//                {
//                    //TmpRet = (uint16_t)GetConfSitrad(BufferInput[i], &BufferUSB->HeaderPayload[Size], USBData, FlagReadConf);
//                    FlagReadConf = true;
//                    if(TmpRet)
//                    {
//                        SizePayload = (uint16_t)(SizePayload + TmpRet);
//                        Size = (uint16_t)(TmpRet + Size);
//                    }
//                }
//            }
//        }
//
//
//        BufferUSB->HeaderPayload[6] = (uint8_t)(    ((SizePayload) >>  8) & 0xFF); //CMD_SIZE1_IND
//        BufferUSB->HeaderPayload[7] = (uint8_t)(    ((SizePayload) >>  0) & 0xFF); //CMD_SIZE2_IND
//
//        Size = (uint16_t)(SizePayload + (uint16_t)5); //ajustar o payload
//
//        uint16_t DifTemp = (Size % 16);
//        BufferUSB->HeaderPayload[1] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  8) & 0xFF);
//        BufferUSB->HeaderPayload[2] = (uint8_t)(((uint16_t)(Size + (16 - DifTemp)) >>  0) & 0xFF);
//        Size = (uint16_t)(Size + (uint16_t)3);
//    }
//
//    return Size;
//}
//
//uint16_t UpdadeDateHour(uint8_t * BufferInput, uint8_t * GetConfData, _USB_CONTROL *USB_Control, uint16_t RandomNumber)
//{
//    //Pacote sitrad: 11 2d 09 00 07 19 02 13 11 39 0f 04 04 04 04
//    //                              D  M  Y  H  M  S  x  x  x  x
//    uint8_t    *AddInput, *AddOut ;
//
//    AddInput = GetConfData;
//
//    //-------------------Header------------------------
//    *GetConfData++ = HEADER_TX_USB;
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x10;
//    //-------------------Payload-----------------------
//    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
//    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
//    *GetConfData++ = ( BufferInput[2] | 0x80 );
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x01;
//    *GetConfData++ = 0x00;
//
//    AddInput = GetConfData;
//
//    //-------------------Header------------------------
//    *GetConfData++ = HEADER_TX_USB;
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x10;
//    //-------------------Payload-----------------------
//    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
//    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
//    *GetConfData++ = ( BufferInput[2] | 0x80 );
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x01;
//    *GetConfData++ = 0x00;
//
//    AddOut = GetConfData;
//
//    USB_Control->DateTimeValues[0] = BufferInput[3];
//    USB_Control->DateTimeValues[1] = BufferInput[4];
//    USB_Control->DateTimeValues[2] = BufferInput[5];
//    USB_Control->DateTimeValues[3] = BufferInput[6];
//    USB_Control->DateTimeValues[4] = BufferInput[7];
//    USB_Control->DateTimeValues[5] = BufferInput[8];
//
//    return (uint16_t)(AddOut - AddInput);
//}


uint16_t AnswerDefault(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber)
{
    uint8_t    *AddInput, *AddOut ;

    AddInput = GetConfData;

    //-------------------Header------------------------
    *GetConfData++ = HEADER_TX_USB;
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x10;
    //-------------------Payload-----------------------
    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
    *GetConfData++ = ( BufferInput[2] | 0x80 );
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x01;
    *GetConfData++ = 0x00;

    AddOut = GetConfData;

    //print485("Comando respondido\r\n", LEN("Comando respondido\r\n") );

    return (uint16_t)(AddOut - AddInput);
}


//uint16_t GetStatus(uint8_t id, uint8_t * GetConfData, _USB_CONTROL *USB_Control, uint16_t RandomNumber)
//{
//    uint8_t     DateTimeValue[DATE_TIME_SIZE];
//    uint16_t    Temp;
//    uint8_t    *AddInput, *AddOut ;
//
//    AddInput = GetConfData;
//
//    GetDateTimerX(DateTimeValue);
//
//    //-------------------Header------------------------
//    *GetConfData++ = HEADER_TX_USB;
//    *GetConfData++ = 0x00;
//    *GetConfData++ = 0x20;
//    //-------------------Payload-----------------------
//    *GetConfData++ = (uint8_t)(RandomNumber >> 8);
//    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
//    *GetConfData++ = (uint8_t)(id | 0x80);
//    *GetConfData++ = (uint8_t)0x00;
//    *GetConfData++ = (uint8_t)0x10;
//    *GetConfData++ = (uint8_t)DataloggerGetFlags();
//    *GetConfData++ = (uint8_t)GetFlagEclo();
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_DAY];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_MONTH];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_YEAR];
//    *GetConfData++ = (uint8_t)0x00;//WeekOfTheMonth
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_HOUR];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_MINUTE];
//    *GetConfData++ = (uint8_t)DateTimeValue[DATE_TIME_SECOND];
//    Temp = DataloggerGetActualPage();
//    *GetConfData++ = (uint8_t)((Temp >>  8) & 0xFF);
//    *GetConfData++ = (uint8_t)((Temp >>  0) & 0xFF);
//    *GetConfData++ = (uint8_t)((PAGE_FLASH_SIZE >>  8) & 0xFF);
//    *GetConfData++ = (uint8_t)((PAGE_FLASH_SIZE >>  0) & 0xFF);
//    *GetConfData++ = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  8) & 0xFF);
//    *GetConfData++ = (uint8_t)((NUMBER_OF_PAGES_DATALOGGER >>  0) & 0xFF);
//
//    *GetConfData++ = (uint8_t)(USB_Control->FlagConnectSitrad); //novo
//
//    AddOut = GetConfData;
//
//    return (uint16_t)(AddOut - AddInput);
//}
//
//uint16_t GetConfSitrad(uint8_t id, uint8_t * GetConfData, _USB_CONTROL *USBData, bool FlagReadConf) //(_PACKET_UDP *PacketUdp, _IPSTRUCT *PonterIpStruct)
//{
////    static _UDP_CONF ConfSitradUdp;
//    uint16_t SizeReturn = 0;
////
////    static _NET_STATIC_MODE NetStaticWifi;
////    static _NET_STATIC_MODE NetStaticEth;
////    static _CONF_NET_BASIC ConfNetBasic;
////    static _NET_WIFI_CONF NetWifiConf;
////    static _WIFI_AP_MODE_CONF WifiApModeConf;
////
////    if(!FlagReadConf)
////    {
////        ReadPacketUdpCuston(&ConfSitradUdp);
////
////        int_storage_read((uint8_t *)&NetStaticWifi, sizeof(_NET_STATIC_MODE), CONF_WIFI_STATIC_IP);
////        int_storage_read((uint8_t *)&NetStaticEth, sizeof(_NET_STATIC_MODE), CONF_ETH_STATIC_IP);
////        int_storage_read((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
////        int_storage_read((uint8_t *)&NetWifiConf, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
////        int_storage_read((uint8_t *)&WifiApModeConf, sizeof(_WIFI_AP_MODE_CONF), CONF_WIFI_AP);
////    }
////
////    switch(id)
////    {
////        case ID_MAC_ADDRESS://              = 1,
////            *GetConfData++ = id;
////            *GetConfData++ = MAC_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMacAdressMs >> 8 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMacAdressMs      ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMacAdressLs >> 24) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMacAdressLs >> 16) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMacAdressLs >> 8 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMacAdressLs      ) & 0xFF;
////            SizeReturn = (uint16_t) (MAC_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_IP_ADDRESS://               = 2,
////            *GetConfData++ = id;
////            *GetConfData++ = IP_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpAddress >>24     ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpAddress >>16     ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpAddress >>8      ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpAddress          ) & 0xFF;
////            SizeReturn = (uint16_t) (IP_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_MASK_ADDRESS://             = 3,
////            *GetConfData++ = id;
////            *GetConfData++ = MASK_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMask >>24        ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMask >>16        ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMask >>8         ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpMask             ) & 0xFF;
////            SizeReturn = (uint16_t) (MASK_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_GETWAY_ADDRESS://           = 4,
////            *GetConfData++ = id;
////            *GetConfData++ = GTW_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpGateway >>24     ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpGateway >>16     ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpGateway >>8      ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.IpGateway          ) & 0xFF;
////            SizeReturn = (uint16_t) (GTW_ADDRESS_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_MODEL://                    = 6,
////            *GetConfData++ = id;
////            *GetConfData++ = (uint8_t)strlen((const char *)MODEL);
////            strcpy((char *)GetConfData, (const char *)MODEL);
////            SizeReturn = (uint16_t) (strlen((const char *)MODEL) + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_CONVERTER_NAME://               = 7,
////            *GetConfData++ = id;
////            *GetConfData++ = (uint8_t)strlen((const char *)ConfSitradUdp.ModelName);
////            strcpy((char *)GetConfData, (const char *)ConfSitradUdp.ModelName);
////            SizeReturn = (uint16_t) (strlen((const char *)ConfSitradUdp.ModelName) + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_ENABLE_PASSWORD://          = 8,
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_PASSWORD_SIZE;
////            *GetConfData++ = (uint8_t)ConfSitradUdp.EnablePassword;
////            SizeReturn = (uint16_t) (FLAG_PASSWORD_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_VERSION://                  = 0x0A,
////            *GetConfData++ = id;
////            *GetConfData++ = (uint8_t)strlen((const char *)VERSION);//VERSION_SIZE;
////            strcpy((char *)GetConfData, (const char *)VERSION);
////            SizeReturn = (uint16_t) ((uint8_t)strlen((const char *)VERSION) + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_SETUP_PORT://               = 0x0C,
////            *GetConfData++ = id;
////            *GetConfData++ = SETUP_PORT_SIZE;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.Tcp_Port_Conf  >> 8 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.Tcp_Port_Conf       ) & 0xFF;
////            SizeReturn =     (uint16_t) (SETUP_PORT_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_COMUNITATION_PORT://        = 0x0E,
////            *GetConfData++ = id;
////            *GetConfData++ = COMUN_PORT_SIZE;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.Tcp_Port_Com  >> 8  ) & 0xFF;
////            *GetConfData++ = (uint8_t)(EthWifiData->IpStruct.Tcp_Port_Com        ) & 0xFF;
////            SizeReturn =     (uint16_t) (COMUN_PORT_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_BAUD_RATE://                = 0x0F,
////            *GetConfData++ = id;
////            *GetConfData++ = BAUD_RATE_485_SIZE;
////            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485  >> 8 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(BAUD_RATE_RS485       ) & 0xFF;
////            SizeReturn = (uint16_t) (BAUD_RATE_485_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_TIME_OUT_RS485://           = 0x10,
////            *GetConfData++ = id;
////            *GetConfData++ = TIME_OUT_485_SIZE;
////            *GetConfData++ = (uint8_t)(TIMER_OUT_WAIT_RX_RS485 >> 8 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(TIMER_OUT_WAIT_RX_RS485      ) & 0xFF;
////            SizeReturn = (uint16_t)(TIME_OUT_485_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_IP_MODE://                  = 0x11,
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_IP_MODE_SIZE;
////            *GetConfData++ = ConfNetBasic.AddrMode; ///ver isso depois
////            SizeReturn = (uint16_t)(FLAG_IP_MODE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_ENABLE_IP_FILTER://         = 0x13,
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_ENABLE_IP_FILTER;
////            *GetConfData++ = ConfSitradUdp.EnableIpFilter;
////            SizeReturn = (uint16_t)(FLAG_ENABLE_IP_FILTER + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_INIT_IP://                  = 0x14,
////            *GetConfData++ = id;
////            *GetConfData++ = INIT_FILTER_IP_SIZE;
////            *GetConfData++ = ConfSitradUdp.InitFilterIp[0];
////            *GetConfData++ = ConfSitradUdp.InitFilterIp[1];
////            *GetConfData++ = ConfSitradUdp.InitFilterIp[2];
////            *GetConfData++ = ConfSitradUdp.InitFilterIp[3];
////            SizeReturn = (uint16_t)(INIT_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_END_IP://                   = 0x15,
////            *GetConfData++ = id;
////            *GetConfData++ = END_FILTER_IP_SIZE;
////            *GetConfData++ = ConfSitradUdp.EndFilterIp[0];
////            *GetConfData++ = ConfSitradUdp.EndFilterIp[1];
////            *GetConfData++ = ConfSitradUdp.EndFilterIp[2];
////            *GetConfData++ = ConfSitradUdp.EndFilterIp[3];
////            SizeReturn = (uint16_t)(END_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_TIME_LOG://                 = 0x16,
////            *GetConfData++ = id;
////            *GetConfData++ = TIME_OUT_LOG_SIZE;
////            uint16_t TempS;
////            TempS = DataloggerGetTimeToLog();
////            *GetConfData++ = (uint8_t)(TempS  >> 8 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(TempS       ) & 0xFF;
////            SizeReturn = (uint16_t)(TIME_OUT_LOG_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_LIST_INSTRUMENTS://         = 0x17,
////            *GetConfData++ = id;
////            uint8_t DataTemp[0xFF],
////                    SizeTemp = 0;
////            SizeTemp = (uint8_t)DataloggerGetListInstrumentsFG(&DataTemp[0]);
////            if(!SizeTemp)
////            {
////                SizeTemp++;
////                *GetConfData++ = SizeTemp;
////                *GetConfData++ = 0;
////            }
////            else
////            {
////                *GetConfData++ = SizeTemp;
////                memcpy(GetConfData, &DataTemp[0], SizeTemp);
////            }
////            SizeReturn = (uint16_t)(SizeTemp + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////            /*** IDS NOVOS ****/
////        case ID_NET_INTERFACE: //   0x18  Interface de Rede (0-Wi-Fi 1-Eth)
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_T_INTERFACE_SIZE;
////            *GetConfData++ = ConfNetBasic.InterfaceType; ///ver isso depois
////            SizeReturn = (uint16_t)(FLAG_IP_MODE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_FLAG_DNS://  0x19  DNS
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_ONE_BYTE_SIZE;
////            *GetConfData++ = ConfNetBasic.FlagDNS; ///ver isso depois
////            SizeReturn = (uint16_t)(FLAG_ONE_BYTE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_DNS://  0x1A  DNS Primario
////            *GetConfData++ = id;
////            *GetConfData++ = END_DNS_IP_SIZE;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS >> 24   )   & 0xFF;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS >> 16   )   & 0xFF;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS >> 8    )   & 0xFF;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS         )   & 0xFF;
////            SizeReturn = (uint16_t)(END_DNS_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_DNS2://  0x1B  DNS Secundario
////            *GetConfData++ = id;
////            *GetConfData++ = END_DNS_IP_SIZE;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS2 >> 24  )  & 0xFF;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS2 >> 16  )  & 0xFF;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS2 >> 8   )  & 0xFF;
////            *GetConfData++ = (uint8_t)(NetStaticWifi.AddDNS2        )  & 0xFF;
////            SizeReturn = (uint16_t)(END_DNS_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_MODE_WIFI:// 0x1C  Modo Wifi (0-AcessPoint(AP) 1-Client)
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_T_WIFI_SIZE;
////            *GetConfData++ = ConfNetBasic.OpModeWifi; ///ver isso depois
////            SizeReturn = (uint16_t)(FLAG_IP_MODE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////            //Wifi Server
////
////        case ID_SSID_WIFI_AP:// 0x1D    Nome do AccessPoint (SSID Max 30 carac)
////            *GetConfData++ = id;
////            *GetConfData++ = (uint8_t)strlen((const char *)WifiApModeConf.Ssid);
////            strcpy((char *)GetConfData, (const char *)WifiApModeConf.Ssid);
////            SizeReturn = (uint16_t) (strlen((const char *)WifiApModeConf.Ssid) + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_TYPE_PWD_WIFI_AP:// 0x1E  Tipo de Senha (0-Aberta, 1-WEP, 2-WPA, 3-WPA2)
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_ONE_BYTE_SIZE;
////            *GetConfData++ = WifiApModeConf.Security; ///ver isso depois
////            SizeReturn = (uint16_t)(FLAG_ONE_BYTE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_CHANNEL:// 0x20 Canal (valor entre 3 e 12, 0-Automatico)
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_ONE_BYTE_SIZE;
////            *GetConfData++ = WifiApModeConf.Channel;
////            SizeReturn = (uint16_t)(FLAG_ONE_BYTE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_IP_SERV_DHCP:// 0x21    IP do Servidor DHCP - modo AP
////            *GetConfData++ = id;
////            *GetConfData++ = IP_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddIP >>24 );
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddIP >>16 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddIP >>8  ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddIP      ) & 0xFF;
////            SizeReturn = (uint16_t)(END_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////        case ID_IP_MASK_SERV_DHCP:// 0x22    IP Mascara de rede - modo AP
////            *GetConfData++ = id;
////            *GetConfData++ = MASK_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddMask >>24 );
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddMask >>16 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddMask >>8  ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.ApStaticIP.AddMask      ) & 0xFF;
////            SizeReturn = (uint16_t)(END_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_IP_RANGE_INIT_SERV_DHCP:// 0x23  IP incial do DHCP - modo AP
////            *GetConfData++ = id;
////            *GetConfData++ = IP_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.StartIpAddList >>24 );
////            *GetConfData++ = (uint8_t)(WifiApModeConf.StartIpAddList >>16 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.StartIpAddList >>8  ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.StartIpAddList      ) & 0xFF;
////            SizeReturn = (uint16_t)(END_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_IP_RANGE_END_SERV_DHCP:// 0x24  IP final do DHCP - modo AP
////            *GetConfData++ = id;
////            *GetConfData++ = IP_ADDRESS_SIZE;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.EndIpAddList >>24 );
////            *GetConfData++ = (uint8_t)(WifiApModeConf.EndIpAddList >>16 ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.EndIpAddList >>8  ) & 0xFF;
////            *GetConfData++ = (uint8_t)(WifiApModeConf.EndIpAddList      ) & 0xFF;
////            SizeReturn = (uint16_t)(END_FILTER_IP_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_HIDDEN_NET:
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_ONE_BYTE_SIZE;
////            *GetConfData++ = ConfNetBasic.HiddenNet;
////            SizeReturn = (uint16_t)(FLAG_ONE_BYTE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////            //wifi client
////        case ID_SSID_WIFI_CLIENT:// 0x26  Nome Rede Wifi - Modo Client NetWifiConf
////            *GetConfData++ = id;
////            *GetConfData++ = (uint8_t)strlen((const char *)NetWifiConf.ApSsid);
////            strcpy((char *)GetConfData, (const char *)NetWifiConf.ApSsid);
////            SizeReturn = (uint16_t) (strlen((const char *)NetWifiConf.ApSsid) + HEADER_CONF_SIZE); //size do comando + 2bytes header
////            break;
////
////        case ID_TYPE_PWD_WIFI_CLIENT:
////        {
////            *GetConfData++ = id;
////            *GetConfData++ = FLAG_ONE_BYTE_SIZE;
////            *GetConfData++ = NetWifiConf.SecurityType;
////            SizeReturn = (uint16_t) (FLAG_ONE_BYTE_SIZE + HEADER_CONF_SIZE); //size do comando + 2bytes header
////        }break;
////
////        default:
////            SizeReturn = 0;
////            break;
////    }
//
//    return SizeReturn;
//}
//
//void SetConfSitrad(uint8_t * SetConfData, _USB_CONTROL *USBData, uint16_t SizeRequest) //
//{
////    _UDP_CONF            ConfSitradUdp;
////    _NET_STATIC_MODE     NetStatic;
////    _CONF_NET_BASIC      ConfNetBasic;
////    _NET_WIFI_CONF       NetWifiConf;
////    _WIFI_AP_MODE_CONF   WifiApModeConf;
////
////    int_storage_read((uint8_t *)&ConfSitradUdp, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
////    int_storage_read((uint8_t *)&NetStatic, sizeof(_NET_STATIC_MODE), CONF_WIFI_STATIC_IP);
////    int_storage_read((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
////    int_storage_read((uint8_t *)&NetWifiConf, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
////    int_storage_read((uint8_t *)&WifiApModeConf, sizeof(_WIFI_AP_MODE_CONF), CONF_WIFI_AP);
////
////
////    uint16_t    SizeCmd = 0,
////                SizeCmdHeader,
////                TimeDatalogger = 0;
////    uint8_t     Id,
////                Temp,
////                BufInstruments[MAX_NUM_INSTRUMENTS];
////
////    NetStatic.AddIP = EthWifiData->IpStruct.IpAddress;
////    NetStatic.AddMask = EthWifiData->IpStruct.IpMask;
////    NetStatic.AddGw = EthWifiData->IpStruct.IpGateway;
////
////    //0  1  2   3  4  5 6  7  8  9  10 11 12 13 14 15 ....
////    //0d 20 04 00 15 02 04 c0 a8 0a 01 03 04 ff ff ff 00 04 04 c0 a8 0a 01 08 01 00
////
////    //0  1   2  3  4 5  6   7  8 9  10 11 12 13 14 15 16 17 18 19 20 21 22....
////    //00 15 02 04 c0 a8 0a 01 03 04 ff ff ff 00 04 04 c0 a8 0a 01 08 01 00
////    //0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28
////   //32 92 04 00 18 26 0c 54 50 2d 4c 49 4e 4b 5f 44 36 38 32 27 08 38 30 31 30 31 30 36 32
////    for(SizeCmdHeader = 0; SizeCmdHeader < SizeRequest; )
////    {
////        //    i = (uint8_t)(i + SetConfSitrad(&BufferInput[i]));
////        Id = *SetConfData++;
////        SizeCmd = *SetConfData++;
////        switch(Id)
////        {
////            case ID_IP_ADDRESS://    - 0x02  Endereço IP
////                NetStatic.AddIP = 0;
////                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | (uint32_t)(*SetConfData++ << 24));
////                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | (uint32_t)(*SetConfData++ << 16));
////                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | (uint32_t)(*SetConfData++ << 8));
////                NetStatic.AddIP =  (uint32_t)(NetStatic.AddIP | *SetConfData++);
////                break;
////            case ID_MASK_ADDRESS://   - 0x03  Máscara de Rede,
////                NetStatic.AddMask = 0;
////                NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | (uint32_t)(*SetConfData++ << 24));
////                NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | (uint32_t)(*SetConfData++ << 16));
////                NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | (uint32_t)(*SetConfData++ << 8));
////                NetStatic.AddMask =  (uint32_t)(NetStatic.AddMask | *SetConfData++);
////                break;
////            case ID_GETWAY_ADDRESS://    - 0x04  Gateway
////                NetStatic.AddGw = 0;
////                NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | (uint32_t)(*SetConfData++ << 24));
////                NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | (uint32_t)(*SetConfData++ << 16));
////                NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | (uint32_t)(*SetConfData++ << 8));
////                NetStatic.AddGw =  (uint32_t)(NetStatic.AddGw | *SetConfData++);
////                break;
////            case ID_CONVERTER_NAME://    - 0x07  Nome do Conversor (Max 30car)
////                memset(ConfSitradUdp.ModelName,'\0', MODEL_NAME_SIZE );
////                memcpy((char *)ConfSitradUdp.ModelName, (const char *)SetConfData, SizeCmd);
////                SetConfData = SetConfData + SizeCmd;
////                break;
////            case ID_ENABLE_PASSWORD://    - 0x08  Protegido por senha?
////                ConfSitradUdp.EnablePassword =  *SetConfData++;
////                break;
////            case ID_SETUP_PORT://    - 0x0C  Porta de Configuracao
////                ConfSitradUdp.ConfigurationPort[1] = (uint8_t)*SetConfData++;
////                ConfSitradUdp.ConfigurationPort[0] = (uint8_t)*SetConfData++;
////                break;
////            case ID_COMUNITATION_PORT://    - 0x0E  Porta de Comunicacao
////                ConfSitradUdp.ComunicationPort[1] = (uint8_t)*SetConfData++;
////                ConfSitradUdp.ComunicationPort[0] = (uint8_t)*SetConfData++;
////                break;
////            case ID_BAUD_RATE://    - 0x0F  Baud Rate
////                ConfSitradUdp.BaudRate485[1] = (uint8_t)*SetConfData++;
////                ConfSitradUdp.BaudRate485[0] = (uint8_t)*SetConfData++;
////                break;
////            case ID_TIME_OUT_RS485://           = 0x10,
////                ConfSitradUdp.TimeOutRs485[1] = (uint8_t)*SetConfData++;
////                ConfSitradUdp.TimeOutRs485[0] = (uint8_t)*SetConfData++;
////                break;
////            case ID_IP_MODE://                  = 0x11,
////                ConfSitradUdp.IpMode = (uint8_t)*SetConfData++;
////                ConfNetBasic.AddrMode = ConfSitradUdp.IpMode;
////                break;
////            case ID_PASSWORD://                 = 0x12,
////                memset(ConfSitradUdp.Password, 0 , PASSWORD_SIZE );
////                memcpy((char *)ConfSitradUdp.Password, (const char *)SetConfData, (uint8_t)SizeCmd);
////                SetConfData = SetConfData + SizeCmd;
////                break;
////            case ID_ENABLE_IP_FILTER://         = 0x13,
////                ConfSitradUdp.EnableIpFilter = *SetConfData++;
////                break;
////            case ID_INIT_IP://                  = 0x14,
////                ConfSitradUdp.InitFilterIp[0] =  (uint8_t)*SetConfData++;
////                ConfSitradUdp.InitFilterIp[1] =  (uint8_t)*SetConfData++;
////                ConfSitradUdp.InitFilterIp[2] =  (uint8_t)*SetConfData++;
////                ConfSitradUdp.InitFilterIp[3] =  (uint8_t)*SetConfData++;
////                break;
////            case ID_END_IP://                   = 0x15,
////                ConfSitradUdp.EndFilterIp[0] =  (uint8_t)*SetConfData++;
////                ConfSitradUdp.EndFilterIp[1] =  (uint8_t)*SetConfData++;
////                ConfSitradUdp.EndFilterIp[2] =  (uint8_t)*SetConfData++;
////                ConfSitradUdp.EndFilterIp[3] =  (uint8_t)*SetConfData++;
////                break;
////
////            case ID_TIME_LOG://                 = 0x16,
////                TimeDatalogger = (uint16_t)(*SetConfData++ << 8);
////                TimeDatalogger = (uint16_t)(TimeDatalogger | (uint8_t)*SetConfData++);
////                if(TimeDatalogger < 15) //tempo minimo para log. em segundos
////                    TimeDatalogger = 15;
////                SetLogInterval(TimeDatalogger);
////                break;
////
////            case ID_LIST_INSTRUMENTS://         = 0x17,
////                Temp = (uint8_t)SizeCmd; //numero de instrumentos para log
////
////                if(Temp > MAX_NUM_INSTRUMENTS)
////                    Temp = MAX_NUM_INSTRUMENTS;
////
////                memset(&BufInstruments[0], 0xFF, MAX_NUM_INSTRUMENTS);
////
////                for(uint8_t i=0; i < Temp; i++)
////                {
////                    BufInstruments[i] = *SetConfData++;
////                }
////
////                DataloggerFlashSetCmdSitrad(0, &BufInstruments[0], Temp);
////                break;
////
////                /*** IDS NOVOS ****/
////            case ID_NET_INTERFACE: //   0x18  Interface de Rede (0-Wi-Fi 1-Eth)
////                ConfNetBasic.InterfaceType = *SetConfData++;
////                if(ConfNetBasic.InterfaceType == ETHERNET)
////                {
////                    ConfNetBasic.OpModeWifi = MODE_CLIENT;
////                }
////                break;
////            case ID_FLAG_DNS://  0x19  DNS? FLAG
////                ConfNetBasic.FlagDNS = *SetConfData++;
////                break;
////            case ID_DNS://  0x1A  DNS PRIMARIO
////                NetStatic.AddDNS = 0;
////                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | (uint32_t)(*SetConfData++ << 24));
////                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | (uint32_t)(*SetConfData++ << 16));
////                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | (uint32_t)(*SetConfData++ << 8));
////                NetStatic.AddDNS =  (uint32_t)(NetStatic.AddDNS | *SetConfData++);
////                break;
////            case ID_DNS2://  0x1B  DNS2 SECUNDARIO
////                NetStatic.AddDNS2 = 0;
////                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | (uint32_t)(*SetConfData++ << 24));
////                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | (uint32_t)(*SetConfData++ << 16));
////                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | (uint32_t)(*SetConfData++ << 8));
////                NetStatic.AddDNS2 =  (uint32_t)(NetStatic.AddDNS2 | *SetConfData++);
////                break;
////
////            case ID_MODE_WIFI:// 0x1A  Modo Wifi (0-AcessPoint(AP) 1-Client)
////                ConfNetBasic.OpModeWifi = *SetConfData++;
////                break;
////
////            case ID_HIDDEN_NET:
////                ConfNetBasic.HiddenNet = *SetConfData++; //(0-NaoOculta 1-Oculta)
////                break;
////
////            case ID_SSID_WIFI_CLIENT:// 0x1B  Nome Rede Wifi - Modo Client
////                memset(NetWifiConf.ApSsid, 0, SSID_WIFI_SIZE);
////                memcpy((char *)NetWifiConf.ApSsid, (const char *)SetConfData, (uint8_t)SizeCmd);
////                SetConfData = SetConfData + SizeCmd;
////                break;
////
////            case ID_PWD_WIFI_CLIENT:// 0x1c  Senha Rede Wifi - Modo Client
////                memset(NetWifiConf.ApPwd, 0, PWD_WIFI_SIZE);
////                memcpy((char *)NetWifiConf.ApPwd, (const char *)SetConfData, (uint8_t)SizeCmd);
////                SetConfData = SetConfData + SizeCmd;
////                break;
////
////            case ID_SSID_WIFI_AP:// 0x1D    Nome do AcessPoint (SSID Max 30 carac)
////                memset(WifiApModeConf.Ssid, 0, SSID_WIFI_SIZE);
////                memcpy((char *)WifiApModeConf.Ssid, (const char *)SetConfData, (uint8_t)SizeCmd);
////                SetConfData = SetConfData + SizeCmd;
////                break;
////
////            case ID_TYPE_PWD_WIFI_AP:// 0x1E  Tipo de Senha (0-Aberta, 1-WEP, 2-WPA, 3-WPA2)
////                WifiApModeConf.Security = *SetConfData++;
////                if(WifiApModeConf.Security)
////                    WifiApModeConf.Security = 3; //permitido apenas 0 ou 3.
////                break;
////
////            case ID_PWD_AP:// 0x1F  Senha (Max 64 caracteres),
////                if((uint8_t)SizeCmd > 7)
////                {
////                    memset(WifiApModeConf.Key, 0, PWD_WIFI_SIZE);
////                    memcpy((char *)WifiApModeConf.Key, (const char *)SetConfData, (uint8_t)SizeCmd);
////                }
////                SetConfData = SetConfData + SizeCmd;
////                break;
////
////            case ID_CHANNEL:// 0x20 Canal (valor entre 3 e 12, 0-Automatico)
////                WifiApModeConf.Channel = *SetConfData++;
////                if(!WifiApModeConf.Channel)
////                    WifiApModeConf.Channel = 11; //1,6,11
////                break;
////
////            case ID_IP_SERV_DHCP:// 0x21    IP do Servidor DHCP - modo AP
////                WifiApModeConf.ApStaticIP.AddIP = 0;
////                WifiApModeConf.ApStaticIP.AddIP =  (uint32_t)(WifiApModeConf.ApStaticIP.AddIP | (uint32_t)(*SetConfData++ << 24 ));
////                WifiApModeConf.ApStaticIP.AddIP =  (uint32_t)(WifiApModeConf.ApStaticIP.AddIP | (uint32_t)(*SetConfData++ << 16 ));
////                WifiApModeConf.ApStaticIP.AddIP =  (uint32_t)(WifiApModeConf.ApStaticIP.AddIP | (uint32_t)(*SetConfData++ << 8  ));
////                WifiApModeConf.ApStaticIP.AddIP =  (uint32_t)(WifiApModeConf.ApStaticIP.AddIP | (uint32_t)(*SetConfData++       ));
////                break;
////            case ID_IP_MASK_SERV_DHCP:// 0x22    IP Mascara de rede - modo AP
////                WifiApModeConf.ApStaticIP.AddMask = 0;
////                WifiApModeConf.ApStaticIP.AddMask =  (uint32_t)(WifiApModeConf.ApStaticIP.AddMask | (uint32_t)(*SetConfData++ << 24 ));
////                WifiApModeConf.ApStaticIP.AddMask =  (uint32_t)(WifiApModeConf.ApStaticIP.AddMask | (uint32_t)(*SetConfData++ << 16 ));
////                WifiApModeConf.ApStaticIP.AddMask =  (uint32_t)(WifiApModeConf.ApStaticIP.AddMask | (uint32_t)(*SetConfData++ << 8  ));
////                WifiApModeConf.ApStaticIP.AddMask =  (uint32_t)(WifiApModeConf.ApStaticIP.AddMask | (uint32_t)(*SetConfData++       ));
////                break;
////            case ID_IP_RANGE_INIT_SERV_DHCP:// 0x23  IP incial do DHCP - modo AP
////                WifiApModeConf.StartIpAddList = 0;
////                WifiApModeConf.StartIpAddList =  (uint32_t)(WifiApModeConf.StartIpAddList | (uint32_t)(*SetConfData++ << 24 ));
////                WifiApModeConf.StartIpAddList =  (uint32_t)(WifiApModeConf.StartIpAddList | (uint32_t)(*SetConfData++ << 16 ));
////                WifiApModeConf.StartIpAddList =  (uint32_t)(WifiApModeConf.StartIpAddList | (uint32_t)(*SetConfData++ << 8  ));
////                WifiApModeConf.StartIpAddList =  (uint32_t)(WifiApModeConf.StartIpAddList | (uint32_t)(*SetConfData++       ));
////                break;
////            case ID_IP_RANGE_END_SERV_DHCP:// 0x24  IP final do DHCP - modo AP
////                WifiApModeConf.EndIpAddList = 0;
////                WifiApModeConf.EndIpAddList =  (uint32_t)(WifiApModeConf.EndIpAddList | (uint32_t)(*SetConfData++ << 24 ));
////                WifiApModeConf.EndIpAddList =  (uint32_t)(WifiApModeConf.EndIpAddList | (uint32_t)(*SetConfData++ << 16 ));
////                WifiApModeConf.EndIpAddList =  (uint32_t)(WifiApModeConf.EndIpAddList | (uint32_t)(*SetConfData++ << 8  ));
////                WifiApModeConf.EndIpAddList =  (uint32_t)(WifiApModeConf.EndIpAddList | (uint32_t)(*SetConfData++       ));
////                break;
////            default:
////            //            SizeReturn = 0;
////                break;
////        }
////
////        SizeCmdHeader = (uint16_t)(SizeCmdHeader + (uint16_t)(SizeCmd + HEADER_CONF_SIZE));
////
////        if(     (Id == ID_CONVERTER_NAME)           ||
////                (Id == ID_IP_MODE)              ||
////                (Id == ID_PASSWORD)             ||
////                (Id == ID_ENABLE_PASSWORD)      ||
////                (Id == ID_SETUP_PORT)           ||
////                (Id == ID_COMUNITATION_PORT)    ||
////                (Id == ID_BAUD_RATE)            ||
////                (Id == ID_TIME_OUT_RS485)       ||
////                (Id == ID_ENABLE_IP_FILTER)     ||
////                (Id == ID_INIT_IP)              ||
////                (Id == ID_END_IP))
////        {
////            ConfSitradUdp.ConfDefault = FLASH_NUMBER_CUSTON;
////        }
////
////    }
////
////    int_storage_write((uint8_t *)&ConfSitradUdp,    sizeof(_UDP_CONF),          CONF_UDP_SITRAD);
////    int_storage_write((uint8_t *)&NetStatic,        sizeof(_NET_STATIC_MODE),   CONF_WIFI_STATIC_IP);
////    int_storage_write((uint8_t *)&ConfNetBasic,     sizeof(_CONF_NET_BASIC),    CONF_NET_BASIC);
////    int_storage_write((uint8_t *)&NetWifiConf,      sizeof(_NET_WIFI_CONF),     CONF_WIFI_CLIENT);
////    int_storage_write((uint8_t *)&WifiApModeConf,   sizeof(_WIFI_AP_MODE_CONF), CONF_WIFI_AP);
//}


uint16_t LoginDataResponse(uint8_t * BufferInput, uint8_t * GetConfData, uint16_t RandomNumber)
{
    uint8_t    *AddInput, *AddOut ;

    AddInput = GetConfData;

    //-------------------Header------------------------
    *GetConfData++ = HEADER_TX_USB;
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x10;
    //-------------------Payload-----------------------
    *GetConfData++ = (uint8_t)(RandomNumber >> 0x08);
    *GetConfData++ = (uint8_t)(RandomNumber ^ 0xFF);
    *GetConfData++ = ( BufferInput[2] | 0x80 );
    *GetConfData++ = 0x00;
    *GetConfData++ = 0x01;
    *GetConfData++ = 0x00;

    AddOut = GetConfData;

    //print485("Comando respondido\r\n", LEN("Comando respondido\r\n") );

	//print485("Senha valida\r\n", LEN("Senha valida\r\n") );
    //memcpy(&USBControl.BufferUSB.HP.Header[0],&hid_event->ux_device_class_hid_event_buffer[0], SIZE_HEADER_USB);
	//SendPacketUSB(&GetConfData,9);

	return (uint16_t)(AddOut - AddInput);
}






uint16_t AES_Enc2 (uint8_t * PMessage, uint8_t *EncMessage, uint16_t InputOctets)
{
    uint32_t  MessageLen  = 0;

    if (( EncMessage == NULL ) || ( InputOctets <= 0 ))
    {
       return 0;
    }

    if (( InputOctets % 16U ) != 0 )
    {
       return 0;
    }

    MessageLen = (uint32_t)(InputOctets / 4U);

    g_sce_aes_1.p_api->open(g_sce_aes_1.p_ctrl, g_sce_aes_1.p_cfg); //Open AES_0

    g_sce_aes_1.p_api->encrypt(g_sce_aes_1.p_ctrl, (uint32_t *)aes_key2, (uint32_t *)IVc2, MessageLen, (uint32_t *)PMessage, (uint32_t *)EncMessage);

    // Close AES driver
    g_sce_aes_1.p_api->close(g_sce_aes_1.p_ctrl);

    return InputOctets;
}


uint16_t AES_Dec2 (uint8_t * PMessage, uint8_t *DecMessage, uint16_t InputOctets)
{
   //uint16_t NumBlocks;
   uint32_t  MessageLen  = 0;

   if (( DecMessage == NULL ) || ( InputOctets <= 0 ))
   {
      return 0;
   }

   if (( InputOctets % 16U ) != 0 )
   {
      return 0;
   }

   //NumBlocks = ( InputOctets / 16U );//280219

   MessageLen = (uint32_t)(InputOctets / 4U);

   g_sce_aes_1.p_api->open(g_sce_aes_1.p_ctrl, g_sce_aes_1.p_cfg); //Open AES_0

   g_sce_aes_1.p_api->decrypt(g_sce_aes_1.p_ctrl, (uint32_t *)aes_key2, (uint32_t *)IVc2, MessageLen, (uint32_t *)PMessage, (uint32_t *)DecMessage);//(uint32_t *)tempOut);

   /* Close AES driver */
   g_sce_aes_1.p_api->close(g_sce_aes_1.p_ctrl);

//   uint16_t padLen = DecMessage[(NumBlocks*16) - 1];
//
//   if (( padLen <= 0 ) || ( padLen > 16 ))
//   {
//       return 0;
//   }
//
//   //Verifica o padding do pacote
//   for ( uint16_t i = (uint16_t)(16 - padLen); i < 16; i++ )
//   {
//       if(DecMessage[((NumBlocks*16)-16) + i] != padLen)
//       {
//             return 0;
//       }
//   }
//
//   uint16_t OutSizePayload;
//   OutSizePayload = (uint16_t)(( NumBlocks*16 ) - padLen);
//
//   return OutSizePayload;
   return InputOctets;
}

//uint16_t AES_Dec (_USB_CONTROL *USBData, uint16_t InputOctets)
//{
//   uint16_t NumBlocks;
//   uint32_t  MessageLen  = 0;
//
//   if (( USBData->BufferRxUSBSitradOut == NULL ) || ( InputOctets <= 0 ))
//   {
//      return 0;
//   }
//
//   if (( InputOctets % 16U ) != 0 )
//   {
//      return 0;
//   }
//
//   NumBlocks = ( InputOctets / 16U );//280219
//
//   MessageLen = (uint32_t)(InputOctets / 4U);
//
//   g_sce_aes_0.p_api->open(g_sce_aes_0.p_ctrl, g_sce_aes_0.p_cfg); //Open AES_0
//
//   g_sce_aes_0.p_api->encrypt(g_sce_aes_0.p_ctrl, (uint32_t *)aes_key, (uint32_t *)IVc, MessageLen, (uint32_t *)USBData->BufferUSB.HP.Payload, (uint32_t *)USBData->BufferRxUSBSitradOut);//(uint32_t *)tempOut);
//
//   /* Close AES driver */
//   g_sce_aes_0.p_api->close(g_sce_aes_0.p_ctrl);
//
//   uint16_t padLen = USBData->BufferRxUSBSitradOut[(NumBlocks*16) - 1];
//
//   if (( padLen <= 0 ) || ( padLen > 16 ))
//   {
//       return 0;
//   }
//
//   //Verifica o padding do pacote
//   for ( uint16_t i = (uint16_t)(16 - padLen); i < 16; i++ )
//   {
//       if(USBData->BufferRxUSBSitradOut[((NumBlocks*16)-16) + i] != padLen)
//       {
//             return 0;
//       }
//   }
//
//   uint16_t OutSizePayload;
//   OutSizePayload = (uint16_t)(( NumBlocks*16 ) - padLen);
//
//   return OutSizePayload;
//}






