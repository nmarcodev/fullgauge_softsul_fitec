/*
 * internal_flash_api.c
 *
 *  Created on: Dec 13, 2017
 *      Author: Rajkumar.Thiagarajan
 */

#include <Includes/common_util.h>
#include "Includes/internal_flash.h"
#include "MainManagerThread.h"

ssp_err_t int_storage_init(void);
ssp_err_t int_storage_deinit(void);
ssp_err_t int_storage_read(uint8_t *data_buffer, unsigned int data_size, unsigned int info_type);
ssp_err_t int_storage_write(uint8_t *data_buffer, unsigned int data_size, unsigned int info_type);

static UINT wait_for_flash_idle(int event_type)
{
    UINT retval;
    ULONG actual_events;

    if( event_type == ERASE_COMPLETE_EVT)
    {
        retval = tx_event_flags_get(&g_int_storage_evt_grp, ERASE_COMPLETE_EVT, TX_AND_CLEAR, &actual_events, TX_WAIT_FOREVER);
    }
    else if (event_type == WRITE_COMPLETE_EVT)
    {
        retval = tx_event_flags_get(&g_int_storage_evt_grp, (WRITE_COMPLETE_EVT | ERR_FAILURE_EVT), TX_OR_CLEAR, &actual_events, TX_WAIT_FOREVER);
    }
    else
    {
        retval = SSP_ERR_INVALID_ARGUMENT;
    }

    return retval;
}

void flash_callback(flash_callback_args_t * p_args)
{
    if (p_args->event == FLASH_EVENT_ERASE_COMPLETE)
    {
        tx_event_flags_set(&g_int_storage_evt_grp, ERASE_COMPLETE_EVT, TX_OR);
    }
    else if (p_args->event == FLASH_EVENT_WRITE_COMPLETE)
    {
        tx_event_flags_set(&g_int_storage_evt_grp, WRITE_COMPLETE_EVT, TX_OR);
    }
    else if ((p_args->event == FLASH_EVENT_ERR_DF_ACCESS) || (p_args->event == FLASH_EVENT_ERR_CMD_LOCKED) ||
            (p_args->event == FLASH_EVENT_ERR_FAILURE))
    {
        tx_event_flags_set(&g_int_storage_evt_grp, ERR_FAILURE_EVT, TX_OR);
    }
}

ssp_err_t int_storage_init(void)
{
    ssp_err_t ssp_err;

    ssp_err = (g_int_storageInst.p_api->open(g_int_storageInst.p_ctrl, g_int_storageInst.p_cfg));

    return ssp_err;
}

ssp_err_t int_storage_read(uint8_t *data_buffer, unsigned int data_size, unsigned int info_type)
{
    unsigned int _dataflash_addr;

    switch(info_type)
    {
        case CONF_NET_BASIC:
            _dataflash_addr = CONF_NET_BASIC_ADDR;
            break;
        case CONF_WIFI_STATIC_IP:
            _dataflash_addr = CONF_WIFI_STATIC_IP_ADDR;
            break;
        case CONF_ETH_STATIC_IP:
            _dataflash_addr = CONF_ETH_STATIC_IP_ADDR;
            break;
        case CONF_WIFI_AP:
            _dataflash_addr = CONF_WIFI_AP_ADDR;
            break;
        case CONF_WIFI_CLIENT:
            _dataflash_addr = CONF_WIFI_CLIENT_ADDR;
            break;
        case CONF_ETH:
            _dataflash_addr = CONF_ETH_ADDR;
            break;
        case CONF_UDP_SITRAD:
            _dataflash_addr = CONF_UDP_SITRAD_ADDR;
            break;
        case FLASH_ALL_FLAGS_256:
            _dataflash_addr = ALL_FLAGS_256;
            break;
        case FLASH_DATALOGGER_INSTR_LIST:
            _dataflash_addr = DATALOGGER_INSTR_LIST;
            break;
        default:
            return SSP_ERR_INVALID_ARGUMENT;
    }

    return (g_int_storageInst.p_api->read(g_int_storageInst.p_ctrl,data_buffer, _dataflash_addr, data_size));
}

ssp_err_t int_storage_write(uint8_t *data_buffer , unsigned int data_size, unsigned int info_type)
{
    ssp_err_t ret_val = SSP_SUCCESS, j;
    unsigned int _dataflash_addr;
    unsigned int _num_of_blocks = 1;
    uint32_t copy_size = 0;

    switch(info_type)
    {
        case CONF_NET_BASIC:
            _dataflash_addr = CONF_NET_BASIC_ADDR;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(CONF_NET_BASIC_LEN);
            break;
        case CONF_WIFI_STATIC_IP:
            _dataflash_addr = CONF_WIFI_STATIC_IP_ADDR;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(CONF_WIFI_STATIC_IP_LEN);
            break;
        case CONF_ETH_STATIC_IP:
            _dataflash_addr = CONF_ETH_STATIC_IP_ADDR;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(CONF_ETH_STATIC_IP_LEN);
            break;
        case CONF_WIFI_AP:
            _dataflash_addr = CONF_WIFI_AP_ADDR;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(CONF_WIFI_AP_LEN);
            break;
        case CONF_WIFI_CLIENT:
            _dataflash_addr = CONF_WIFI_CLIENT_ADDR;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(CONF_WIFI_CLIENT_LEN);
            break;
        case CONF_ETH:
            _dataflash_addr = CONF_ETH_ADDR;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(CONF_ETH_LEN);
            break;
        case CONF_UDP_SITRAD:
            _dataflash_addr = CONF_UDP_SITRAD_ADDR;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(CONF_UDP_SITRAD_LEN);
            break;
        case FLASH_ALL_FLAGS_256:
            _dataflash_addr = ALL_FLAGS_256;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(ALL_FLAGS_256_LEN);
            break;
        case FLASH_DATALOGGER_INSTR_LIST:
            _dataflash_addr = DATALOGGER_INSTR_LIST;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(DATALOGGER_INSTR_LIST_LEN);
            break;
        case FLASH_BOOTLOADER_CODE_ADDRRESS: //memoriza flag de ok para atualizar firmware - lido apos reset
            _dataflash_addr = FLASH_DF_BOOTLOADER_CODE_ADDRRESS;
            _num_of_blocks  = (unsigned int)NUM_OF_BLOCKS(FLASH_BOOTLOADER_CODE_ADDR_LEN);
            break;
        default:
            return SSP_ERR_INVALID_ARGUMENT;
    }

    if( data_size > (_num_of_blocks * FLASH_BLOCK_SIZE))
    {
        //print_to_console("Input data buffer size is greater than allocated size\r\n");
        return SSP_ERR_INVALID_ARGUMENT;
    }

    /* Erase flash memory location */
    j = g_int_storageInst.p_api->erase(g_int_storageInst.p_ctrl, _dataflash_addr, _num_of_blocks);
    if(j == TX_SUCCESS)
    {
        /* wait for idle  */

        wait_for_flash_idle(ERASE_COMPLETE_EVT);

        while (data_size > 0)
        {
            if (data_size > FLASH_BLOCK_SIZE)
            {
                copy_size = FLASH_BLOCK_SIZE;
            }
            else
            {
                copy_size = data_size;
            }

            ret_val = g_int_storageInst.p_api->write(g_int_storageInst.p_ctrl, (uint32_t)data_buffer, _dataflash_addr, FLASH_BLOCK_SIZE);
            if ( ret_val == SSP_SUCCESS)
            {
                /* wait for idle */
                wait_for_flash_idle(WRITE_COMPLETE_EVT);

                data_size -= (copy_size);
                data_buffer += (copy_size);
                _dataflash_addr += FLASH_BLOCK_SIZE;
            }
            else
            {

                ret_val = SSP_ERR_WRITE_FAILED;
                break;
            }
        }
    }
    else
    {
        /* Flash erase failure */
//        print_to_console("Failed to erase given block of internal flash memory \r\n");
        ret_val = SSP_ERR_ERASE_FAILED;
    }

    return ret_val;
}

/**
 * Grava codigo de solicitacao de bootloader
 * @return sucesso = FG_SUCCESS, erro = FG_ERROR
 */
_FG_ERR Flash_BootRequest(void)
{
    ssp_err_t result = SSP_ERR_ASSERTION;
    flash_result_t blank_check_result;
    uint32_t *booMagicNum = (uint32_t *)FLASH_DF_BOOTLOADER_CODE_ADDRRESS;
    uint32_t BootLoaderValidCode = BOOTLOADER_VALID_CODE;

    g_int_storageInst.p_api->open(g_int_storageInst.p_ctrl, g_int_storageInst.p_cfg);
    g_int_storageInst.p_api->blankCheck(g_int_storageInst.p_ctrl, FLASH_DF_BOOTLOADER_CODE_ADDRRESS,sizeof( uint32_t ),&blank_check_result);
    if (blank_check_result == FLASH_RESULT_BLANK)
    {
        g_int_storageInst.p_api->write(g_int_storageInst.p_ctrl, (uint32_t)&BootLoaderValidCode,FLASH_DF_BOOTLOADER_CODE_ADDRRESS,sizeof( uint32_t ));
        result = SSP_SUCCESS;
    }
    else
    {
        if (memcmp(&BootLoaderValidCode,booMagicNum, sizeof( uint32_t )))
        {
            g_int_storageInst.p_api->erase(g_int_storageInst.p_ctrl, FLASH_DF_BOOTLOADER_CODE_ADDRRESS,1);
            g_int_storageInst.p_api->write(g_int_storageInst.p_ctrl, (uint32_t)&BootLoaderValidCode,FLASH_DF_BOOTLOADER_CODE_ADDRRESS,sizeof( uint32_t ));
            result = SSP_SUCCESS;
        }
        else
        {
            result = SSP_SUCCESS;
        }
    }
    g_int_storageInst.p_api->close(g_int_storageInst.p_ctrl);

    if(result == SSP_SUCCESS)
    {
        return FG_SUCCESS;
    }
    else
    {
        return FG_ERROR;
    }
    return FG_SUCCESS;
}


ssp_err_t int_storage_deinit(void)
{
    return (g_int_storageInst.p_api->close(g_int_storageInst.p_ctrl));
    //return 0;
}
