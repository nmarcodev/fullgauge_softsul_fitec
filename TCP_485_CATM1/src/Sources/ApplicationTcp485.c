/*
 * ApplicationTcp485.c
 *
 *  Created on: 4 de nov de 2019
 *      Author: leonardo.ceolin
 *
 * Modulo principal da aplicacao, junto com ApplicationPayload.c
 *
 * Faz o controle das interfaces de rede, e do estado do equipamento, informando-o MainManagerThread.
 *
 * A configuracao do equipamento esta armazenada na memoria flash interna.
 */

#include "Includes/ApplicationTcp485.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/ApplicationIp.h"
#include "Includes/ApplicationUSB.h"
#include "USB_HID_Device_Thread.h"

#if RECORD_DATALOGGER_EVENTS
#include "includes/Thread_Datalogger_entry.h"
#endif

//void ChangeMode(_USB_CONTROL *USBControl);
//void InitEth(_USB_CONTROL *USBControl);
//void WaitLinkEth(_USB_CONTROL *USBControl);
//void InitAp(_USB_CONTROL *USBControl);
void StatusSitrad(_STATUS_FLAGS *StatusFlagsX);
//void ReadFlashNetDefault(_USB_CONTROL *USBControl);

//void InitDhcp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
//void IdleInit(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
//void InitWifi(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
//void WifiScan(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
void RestartIp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
//void StaticIp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
void StatusIp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
void CxAnyFlags(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
//void NetStateMachine(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
void WifiProvision(_USB_CONTROL *USBControl, uint8_t *NumAttmptsPwdX);
void WifiStatusSignalWifi(_USB_CONTROL *USBControl, _MAIN_MANAGER_FLAGS *MainManagerFlag);
//_MAIN_MANAGER_FLAGS NetStatusModule(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX);
void WifiProvisionFail(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX, uint8_t *NumAttmptsPwdX);

void CheckConnectivity(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlags);

bool RamStatusSitrad(bool In, bool SetTGetF);
bool GetStatusSitrad(void);
void SetStatusSitrad(bool StatusSitradCon);



void StatusSitrad(_STATUS_FLAGS *StatusFlagsX)
{
    if(StatusFlagsX->StateSitrad != StatusFlagsX->FlagSitradConnected)
    {
        //Envia para a thread DataLoggerFlash a sinalizacao quanto ao status do Sitrad.
        _DATA_LOGGER_DATA FlagSitrad;
        FlagSitrad.OpCod = CONNECTED_SITRAD;
        FlagSitrad.SizeTemp = (uint16_t)StatusFlagsX->FlagSitradConnected;
        tx_queue_send(&Datalogger_Flash, &FlagSitrad, TX_WAIT_FOREVER); //envia para a thread Datalogger

        StatusFlagsX->StateSitrad = StatusFlagsX->FlagSitradConnected;

        SetStatusSitrad(StatusFlagsX->StateSitrad);
    }
}

bool RamStatusSitrad(bool In, bool SetTGetF)
{
    static bool RamStatusSitrad = false;

    if(SetTGetF)
    {
        RamStatusSitrad = In;
    }

    return RamStatusSitrad;
}

bool GetStatusSitrad(void)
{
    return RamStatusSitrad(false, false);
}

void SetStatusSitrad(bool StatusSitradCon)
{
    RamStatusSitrad(StatusSitradCon, true);
}


/**
 * Baseado na configuracao atual do equipmento e do estado, computa as flags para serem passsadas a thread
 * MainManager. Essa funcao tem muita liberdade para ser alterada, devendo manter as seguintes flags:
 * FlagFAC, FlagOTARun, FlagConnectedSitrad
 *
 * As demais devem ser alteradas ja que o caracteristica de operacao do conversor sera outra.
 * Essas flags funcionam em conjunto com a funcao StatusLed da MainManagerThread.
 *
 * @param USBControl
 * @param StatusFlagsX
 * @return
 */
_MAIN_MANAGER_FLAGS NetStatusModule(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
{
    _MAIN_MANAGER_FLAGS StatusFlag = 0;

    if(!tx_semaphore_get(&USBStatus_Semaphore, TX_NO_WAIT))
    {
//        if(USBControl->ConfNetBasic.InterfaceType == ETHERNET)
//        {
//            StatusFlag |= MM_ETH_TYPE;
//            uint8_t FlagOldNetConnected = StatusFlagsX->FlagNetConnected;
//            StatusFlagsX->FlagNetConnected = (bool)GetStatusLinkEth();
//            if((FlagOldNetConnected) && (!StatusFlagsX->FlagNetConnected)) //ocorreu uma desconexao
//            {
//                USBControl->EthWifiStatesMachine = EW_SM_RESTART_IP;
//                StatusFlagsX->StateSitrad = false;
//                StatusFlagsX->FlagErrorIp = false;
//            }
//
//            if(!StatusFlagsX->FlagNetConnected)
//            {
//                StatusFlag |= MM_ETH_NOT_CONNECTED;
//            }
//            else
//            {
//                if(USBControl->USBStatesMachine != EW_SM_WAIT_QUEUE)
//                {
//                    StatusFlag |= MM_ETH_WAITING_IP; //aguardando IP;
//                }
//                else
//                {
//                    StatusFlag |= MM_ETH_CONNECTED;
//                }
//            }
//        }
//        else
//        {
//            StatusFlag |= MM_WIFI_TYPE;
//
//            if(USBControl->ConfNetBasic.OpModeWifi == MODE_AP)
//            {
//                StatusFlag |= MM_WIFI_SERVER;
//                StatusFlag |= (_MAIN_MANAGER_FLAGS)GetStatusAPWifi();
//            }
//            else //Cliente
//            {
//                StatusFlag |= MM_WIFI_CLIENT;
//
//                if(StatusFlagsX->FlagNetConnected)
//                {
//                    StatusFlag |= MM_CONNECTED;
//
//                    EthWifiGetStatusIp(USBControl);
//                    GetStatusWiFi(USBControl);
//                    WifiStatusSignalWifi(USBControl, &StatusFlag);//carrega_flag do sinal do WiFi
//
//                }
//                else
//                {
//                    StatusFlag |= MM_DISCONNECTED;
//                    if(StatusFlagsX->FlagNetScan)
//                    {
//                        StatusFlag |= MM_SCAN_NET;
//                    }
//                }
//            }
//        }

        if(StatusFlagsX->StateSitrad)
        {
            StatusFlag |= MM_CONNECTED_SITRAD;
            USBControl->FlagConnectSitrad = true;
        }
        else
        {
            StatusFlag |= MM_DISCONNECTED_SITRAD;
            USBControl->FlagConnectSitrad = false;
        }

//        /*Erros e Status Majoritarios*/
//        if(StatusFlagsX->FlagErrorIp)
//        {
//            StatusFlag |= MM_ERRO_IP;
//        }
//
//        if(StatusFlagsX->FlagErrorInvalidStaticIp)
//        {
//            StatusFlag |= MM_ERRO_STATIC_IP;
//        }

        if(StatusFlagsX->FlagErrorPassWrd)
        {
            StatusFlag |= MM_ERRO_PWD;
        }

        if(USBControl->FlagOTARun)
        {
            StatusFlag |= MM_STATUS_OTA;
        }

        if(USBControl->FlagFAC)
        {
            StatusFlag |= MM_FAC_MODE;
        }
    }
    return StatusFlag;
}

void NetStateMachine(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
{
    static uint8_t  NumAttmptsPwd = 0;

    switch(USBControl->USBStatesMachine)
    {
        case SM_IDLE:
        {
            IdleInit(USBControl, StatusFlagsX);
        }break;

//        case SM_INIT_ETH:
//        {
//            InitEth(USBControl);
//        }break;
//
//        case EW_SM_INIT_WIFI:
//        {
//            InitWifi(USBControl, StatusFlagsX);
//
//        }break;
//
//        case EW_SM_WAIT_LINK_ETH:
//        {
//            WaitLinkEth(USBControl);
//        }break;
//
//        case EW_SM_INIT_AP:
//        {
//            InitAp(USBControl);
//        }break;
//
//        case EW_SM_READ_FLASH_NET_DEFAULT:
//        {
//            ReadFlashNetDefault(USBControl);
//        }break;
//
//        case EW_SM_SCAN:
//        {
//            WifiScan(USBControl, StatusFlagsX);
//        }break;
//
//        case EW_SM_PROVISION:
//        {
//            WifiProvision(USBControl, &NumAttmptsPwd);
//        }break;
//
//        case EW_SM_PROVISION_FAIL:
//        {
//            WifiProvisionFail(USBControl, StatusFlagsX, &NumAttmptsPwd);
//        }break;
//
//        case EW_SM_CHANGE_MODE:
//        {
//            ChangeMode(USBControl);
//        }break;
//
//        case EW_SM_DHCP:
//        {
//            InitDhcp(USBControl, StatusFlagsX);
//        }break;
//
//        case EW_SM_STATIC_IP:
//        {
//            StaticIp(USBControl, StatusFlagsX);
//        }break;
//
//        case EW_SM_STATUS_IP:
//        {
//            StatusIp(USBControl, StatusFlagsX);
//        }break;
//
//        case EW_SM_RESTART_IP:
//        {
//            RestartIp(USBControl, StatusFlagsX);
//        }break;

        case SM_RESTART_DEVICE:
        {
            RestartModule();
        }break;

        case SM_WAIT_RESET:
        {
            tx_thread_sleep(TIME_200MS);
        }break;

        case SM_DONE:
        {
            //CheckConnectivity(USBControl, StatusFlagsX);
            USBControl->USBStatesMachine = SM_WAIT_QUEUE;
            StartTimerMain(); //Inicializa Timer Main.
        }break;

        case EW_SM_WAIT_QUEUE:
        {
            //if(!tx_event_flags_get(&EthWifi_Flags, EW_FLAG_UDPTCP_ALL, TX_OR_CLEAR, &USBControl->ActualFlags, 5))
            //{
                CxAnyFlags(USBControl, StatusFlagsX);
            //}
            //else
            //{
                //CheckConnectivity(USBControl, StatusFlagsX);
            //}

        }break;

        default:
        {
            USBControl->USBStatesMachine = EW_SM_IDLE;
        }break;
    }
}


//void CheckConnectivity(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlags)
//{
//    if((USBControl->ConfNetBasic.InterfaceType == ETHERNET) ||
//       (USBControl->ConfNetBasic.OpModeWifi == MODE_CLIENT))
//    {
//        if(!StatusFlags->FlagSitradConnected && !USBControl->FlagOTARun)
//        {
//            EthWifiCheckConnectivity(USBControl, StatusFlags);
//        }
//        else
//        {// se conectado ao sitrad, remove erro de ip, que pode ocorrer no modo ip estatico
//            StatusFlags->FlagErrorIp = false;
//        }
//    }
//}

void IdleInit(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
{
    StopTimerMain(); //Para o timerMain.

    StatusFlagsX->FlagNetConnected    = false;
    StatusFlagsX->FlagNetScan         = false;
    StatusFlagsX->FlagSitradConnected = false;
    USBControl->ValidAccess       = false;
    USBControl->ValidAccessConf   = false;

    //packet_pool_init0();

//    if(USBControl->ConfNetBasic.InterfaceType == ETHERNET)
//    {
//        USBControl->EthWifiStatesMachine = EW_SM_INIT_ETH;
//    }
//    else
//    {
//        USBControl->EthWifiStatesMachine = EW_SM_INIT_WIFI;
//    }

//    tx_event_flags_set (&USB_Flags, FLAGS_NONE, TX_AND);
}

//void InitEth(_USB_CONTROL *USBControl)
//{
//    USBControl->NxIP = &g_ip_eth;
//    USBControl->NxDHCP = &g_dhcp_Eth;
//
//    ip_init_eth();
//
//    USBControl->EthWifiStatesMachine = EW_SM_WAIT_LINK_ETH;
//}

//void InitWifi(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
//{
//    ULONG IpActualStatus = 0;
//    USBControl->NxIP = &g_ip_wifi;
//    USBControl->NxDHCP = &g_dhcp_Wifi;
//
//    if(nx_ip_interface_status_check (&g_ip_wifi, 0, NX_IP_INITIALIZE_DONE,
//                                     &IpActualStatus,
//                                     500))
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_INIT, 0, false);
//#endif
//
//      ip_init_wifi();
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_INIT, 0, true);
//#endif
//
//      tx_thread_sleep(50);
//    }
//
//    if(USBControl->ConfNetBasic.OpModeWifi == MODE_AP)
//    {
//        USBControl->EthWifiStatesMachine = EW_SM_INIT_AP;
//    }
//    else
//    {
//        USBControl->EthWifiStatesMachine = EW_SM_READ_FLASH_NET_DEFAULT;
//        StatusFlagsX->FlagNetScan = true;
//    }
//}

//void WaitLinkEth(_USB_CONTROL *USBControl)
//{
//    if(!EthWifiWaitForLink(USBControl))
//    {
//        if(USBControl->ConfNetBasic.AddrMode == MODE_STATIC)
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_STATIC_IP;
//        }
//        else
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_DHCP;
//        }
//    }
//    else
//    {
//        USBControl->EthWifiStatesMachine = EW_SM_WAIT_LINK_ETH;
//        tx_thread_sleep(100);
//    }
//}

//void InitAp(_USB_CONTROL *USBControl)
//{
//    NetWifiDhcpServerStart(&g_dhcp_server1);
//    SetIpStatus(USBControl);
//    SetUdpIp(&USBControl->IpStruct);
//    UdpStart(USBControl);
//    if(!USBControl->FlagFAC)
//    {
//        TcpStart(USBControl, USBControl->IpStruct.Tcp_Port_Com);
//    }
//    TcpStart(USBControl, USBControl->IpStruct.Tcp_Port_Conf);
//
//    USBControl->EthWifiStatesMachine = EW_SM_DONE;
//}

//void ReadFlashNetDefault(_USB_CONTROL *USBControl)
//{
//    _NET_WIFI_CONF NetWifiConf;
//
//#if RECORD_DATALOGGER_EVENTS
//    ssp_err_t result =
//#endif
//    int_storage_read((uint8_t *)&NetWifiConf, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
//
//#if RECORD_DATALOGGER_EVENTS
//    if (result != SSP_SUCCESS)
//    {
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_FLASH_ERROR, result, false);
//    }
//#endif
//
//    if(NetWifiConf.ValCod == FLASH_NUMBER_MAGIC)
//    {
//        strcpy((char *)USBControl->SfProvisionWifi->key, (char *)NetWifiConf.ApPwd);
//        strcpy((char *)USBControl->SfProvisionWifi->ssid, (char *)NetWifiConf.ApSsid);
//
//        USBControl->EthWifiStatesMachine             = EW_SM_SCAN;
//    }
//    else
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_SCAN_RESULT, 0, false);
//#endif
//
//        USBControl->EthWifiStatesMachine             = EW_SM_IDLE; //nao deveria acontecer. O que fazer nesse caso?
//        tx_thread_sleep(100);
//    }
//#ifdef DEBUG_WIFI
//    strcpy((char *)USBControl->SfProvisionWifi->key, (char *)WF_KEY);
//    strcpy((char *)USBControl->SfProvisionWifi->ssid, (char *)WF_SSID);
//#endif
//}

//void WifiScan(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
//{
//    static uint8_t NumAttempts = 0;
//    _FG_ERR StatusFgErr;
//
//    StatusFgErr = ScanNets(USBControl);
//    if(FG_SUCCESS == StatusFgErr)
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_SCAN_RESULT, 1, true);
//#endif
//
//        USBControl->EthWifiStatesMachine = EW_SM_PROVISION;
//        NumAttempts = 0;
//        StatusFlagsX->FlagNumAttempts = false;
//    }
//    ///>Erro na funcao scan, tenta x vezes e reseta o modulo..Pois tem problema no hardware
//    else if(FG_WIFI_ERRO_SCAN == StatusFgErr)
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_SCAN_RESULT, (uint16_t)(NumAttempts + 1), false);
//#endif
//
//        if(NumAttempts < NUM_ATTEMPTS)
//        {
//            NumAttempts++;
//            USBControl->EthWifiStatesMachine = EW_SM_SCAN;
//            tx_thread_sleep(50);
//        }
//        else
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_RESTART_DEVICE;
//        }
//    }
//
//    else
//    {
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_SCAN_RESULT, 0, true);
//#endif
//
//        if(USBControl->ConfNetBasic.HiddenNet)  //caso rede oculta, ir para o provisionamento
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_PROVISION;
//        }
//        else
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_SCAN;
//            StatusFlagsX->FlagErrorIp = false;
//        }
//        tx_thread_sleep(50);
//    }
//}

//void WifiProvision(_USB_CONTROL *USBControl, uint8_t *NumAttmptsPwdX)
//{
//    uint32_t ActualFlags = 0;
//    static bool FlagWaitingToConnect = false;
//    static uint8_t NumAtt = 0;
//    sf_wifi_provisioning_t WifiProvisionGet;
//
//    USBControl->EthWifiStatesMachine = EW_SM_PROVISION_FAIL;
//
//    if(!FlagWaitingToConnect)
//    {
//        if(ModuleProvision(USBControl))
//        {
//            NumAtt = 0;
//            return;
//        }
//        FlagWaitingToConnect=true;
//    }
//
//    if(!tx_event_flags_get(&EthWifi_Flags, EW_FLAG_STATUS_WIFI, TX_OR_CLEAR, &ActualFlags, TX_NO_WAIT))
//    {
//        if(ActualFlags & EW_FLAG_WIFI_CONNECTED)
//        {
//
//#if RECORD_DATALOGGER_EVENTS
//            DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_CONNECTION_STATUS_CHANGE, 0, true);
//#endif
//
//            if(USBControl->ConfNetBasic.AddrMode == MODE_STATIC)
//            {
//                USBControl->EthWifiStatesMachine = EW_SM_STATIC_IP;
//            }
//            else
//            {
//                USBControl->EthWifiStatesMachine = EW_SM_DHCP;
//            }
//            //Confirmacao que o provisionamento ocorreu bem.
//            g_sf_wifi0.p_api->provisioningGet(g_sf_wifi0.p_ctrl, &WifiProvisionGet);
//
//            if( !       (!strcmp((char *)WifiProvisionGet.ssid, (char *)USBControl->SfProvisionWifi->ssid))
//                        && (WifiProvisionGet.encryption == USBControl->SfProvisionWifi->encryption)
//                        && (WifiProvisionGet.channel == USBControl->SfProvisionWifi->channel)
//                        && (WifiProvisionGet.security == USBControl->SfProvisionWifi->security)
//                        )
//            {
//                USBControl->EthWifiStatesMachine = EW_SM_PROVISION_FAIL;
//            }
//            else
//            {
//                (*NumAttmptsPwdX) = 0;
//                MacAddressList(0, false);
//                FlagWaitingToConnect = false;
//            }
//        }
//        else if(ActualFlags & EW_FLAG_WIFI_DISCONNECTED)
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_PROVISION_FAIL;
//            FlagWaitingToConnect = false;
//            (*NumAttmptsPwdX)++;
//        }
//        NumAtt = 0;
//    }
//    else
//    {
//        tx_thread_sleep(50);
//        if(NumAtt < 10)
//        {
//            NumAtt++;
//            USBControl->EthWifiStatesMachine = EW_SM_PROVISION;
//        }
//        else
//        {
//            NumAtt = 0;
//            FlagWaitingToConnect = false;
//            (*NumAttmptsPwdX)++;
//        }
//    }
//}

//void WifiProvisionFail(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX, uint8_t *NumAttmptsPwdX)
//{
//    USBControl->EthWifiStatesMachine = EW_SM_SCAN;
//    tx_thread_sleep(10);
//
//    if((*NumAttmptsPwdX) >= NUM_ATTEMPTS)
//    {
//        if(!USBControl->ConfNetBasic.HiddenNet)
//        {
//            StatusFlagsX->FlagErrorPassWrd = true;
//        }
//    }
//}

//void StaticIp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
//{
//    static uint8_t  NumAttemptsIp = 0;
//
//    if(!EthWifiStartStaticIP(USBControl->NxIP, USBControl->IpStaticMode.AddIP, USBControl->IpStaticMode.AddMask, USBControl->IpStaticMode.AddGw, USBControl->IpStaticMode.AddGw))
//    {
//        NumAttemptsIp = 0;
//        USBControl->EthWifiStatesMachine = EW_SM_STATUS_IP;
//    }
//    else
//    {
//        if(NumAttemptsIp < NUM_ATTEMPTS)
//        {
//            NumAttemptsIp++;
//            USBControl->EthWifiStatesMachine = EW_SM_RESTART_IP;
//        }
//        else
//        {
//            StatusFlagsX->FlagErrorInvalidStaticIp = true;
//            USBControl->EthWifiStatesMachine = EW_SM_WAIT_RESET;
//        }
//
//    }
//}

//void ChangeMode(_USB_CONTROL *USBControl)
//{
//    _CONF_NET_BASIC ConfNetBasic;
//
//    int_storage_read((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
//
//    ConfNetBasic.OpModeWifi = MODE_AP;
//    ConfNetBasic.InterfaceType = WIFI;
//
//    int_storage_write((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
//
//    USBControl->EthWifiStatesMachine = EW_SM_RESTART_DEVICE;
//}

//void InitDhcp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
//{
//    static uint8_t  NumAttemptsIp = 0;
//
//    if(!EthWifiStartDhcpClient(USBControl))
//    {
//        NumAttemptsIp = 0;
//        USBControl->EthWifiStatesMachine = EW_SM_STATUS_IP;
//    }
//    else
//    {
//        if(NumAttemptsIp < NUM_ATTEMPTS)
//        {
//            NumAttemptsIp++;
//        }
//        else
//        {
//            StatusFlagsX->FlagErrorIp = true;
//        }
//
//#if RECORD_DATALOGGER_EVENTS
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_IP_ERROR, USBControl->EthWifiStatesMachine, USBControl->ConfNetBasic.AddrMode);
//#endif
//
//        USBControl->EthWifiStatesMachine = EW_SM_RESTART_IP;
//    }
//}

//void WifiStatusSignalWifi(_USB_CONTROL *USBControl, _MAIN_MANAGER_FLAGS *MainManagerFlag)
//{
//    *MainManagerFlag |= MM_STATUS_WIFI;
//
//    if(USBControl->WifiRssid > STATUS_EXCELLENT)
//    {
//        *MainManagerFlag |= MM_S_WIFI_EXCELLENT;
//    }
//    else if(USBControl->WifiRssid > STATUS_GOOD)
//    {
//        *MainManagerFlag |= MM_S_WIFI_GOOD;
//    }
//    else if(USBControl->WifiRssid > STATUS_POOR)
//    {
//        *MainManagerFlag |= MM_S_WIFI_POOR;
//    }
//    else if(USBControl->WifiRssid > STATUS_VERY_POOR)
//    {
//        *MainManagerFlag |= MM_S_WIFI_VERY_POOR;
//    }
//    else
//    {
//        *MainManagerFlag |= MM_S_WIFI_NO_SIGNAL;
//    }
//}

//void StatusIp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
//{
//    static uint8_t  NumAttemptsGetIp = 0;
//
//    if(!EthWifiGetStatusIp(USBControl))
//    {
//        if(!StatusFlagsX->FlagStartUdpTcp)
//        {
//            UdpStart(USBControl);
//            if(!USBControl->FlagFAC)
//            {
//                TcpStart(USBControl, USBControl->IpStruct.Tcp_Port_Com );
//            }
//            TcpStart(USBControl, USBControl->IpStruct.Tcp_Port_Conf );
//            StatusFlagsX->FlagStartUdpTcp = true;
//        }
//
//        USBControl->FlagConnect     = true;
//        StatusFlagsX->FlagNetConnected  = true;
//        StatusFlagsX->FlagNetScan       = false;
//
//        USBControl->EthWifiStatesMachine = EW_SM_DONE;
//        NumAttemptsGetIp = 0;
//        StatusFlagsX->FlagErrorIp = false;
//        StatusFlagsX->FlagErrorPassWrd = false;
//    }
//    else
//    {
//        if(NumAttemptsGetIp < NUM_ATTEMPTS)
//        {
//            NumAttemptsGetIp++;
//        }
//        else
//        {
//            StatusFlagsX->FlagErrorIp = true;
//
//#if RECORD_DATALOGGER_EVENTS
//            DataLoggerRegisterEvent(DATALOGGER_EVENT_IP_ERROR, USBControl->EthWifiStatesMachine, USBControl->ConfNetBasic.AddrMode);
//#endif
//
//        }
//        USBControl->EthWifiStatesMachine = EW_SM_RESTART_IP;
//    }
//}

//void RestartIp(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
//{
//
//    USBControl->FlagConnect = false;
//    StatusFlagsX->FlagNetConnected = false;
//    StatusFlagsX->FlagSitradConnected = false;
//
//    //Envia para a thread DataLoggerFlash a sinalizacao quanto ao status do Sitrad.
//    _DATA_LOGGER_DATA FlagSitrad;
//    FlagSitrad.OpCod = CONNECTED_SITRAD;
//    FlagSitrad.SizeTemp = (uint16_t)StatusFlagsX->FlagSitradConnected;
//    tx_queue_send(&Datalogger_Flash, &FlagSitrad, TX_WAIT_FOREVER); //envia para a thread Datalogger
//
//    if(StatusFlagsX->FlagStartUdpTcp)
//    {
//        StatusFlagsX->FlagStartUdpTcp = false;
//        UdpFinish(USBControl);
//        if(!USBControl->FlagFAC)
//        {
//            TcpFinish(USBControl, USBControl->IpStruct.Tcp_Port_Com);
//        }
//        TcpFinish(USBControl, USBControl->IpStruct.Tcp_Port_Conf);
//    }
//
////    else
//    {
//        if(!NetStop(USBControl))
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_IDLE;
//        }
//        else
//        {
//            USBControl->EthWifiStatesMachine = EW_SM_RESTART_DEVICE;
//        }
//
//    }
//
//
//#if RECORD_DATALOGGER_EVENTS
//    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_RESTART, 0, USBControl->EthWifiStatesMachine == EW_SM_RESTART_DEVICE);
//#endif
//
//
//}

void CxAnyFlags(_USB_CONTROL *USBControl, _STATUS_FLAGS *StatusFlagsX)
{
    if(FLAG_USB_CONNECT & USBControl->ActualFlags)
    {
        if(!USBControl->FlagOTARun)
        {
//            if(!TcpAcceptConnect(USBControl, USBControl->IpStruct.Tcp_Port_Com))
//            {
//                StatusFlagsX->FlagSitradConnected = true;
//                USBControl->TimerConnectSitrad = TIME_SILENCE_SITRAD;
//            }
        }
        else
        {
            //TcpRefugeeConnection(USBControl, USBControl->IpStruct.Tcp_Port_Com);
        }
    }

    if(EW_FLAG_TCP_CONNECT_CONF & USBControl->ActualFlags)
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_CONFIG_STATUS_CHANGE, 0, true);
#endif
//        if(TcpAcceptConnect(USBControl, USBControl->IpStruct.Tcp_Port_Conf))
//        {
//            USBControl->USBStatesMachine = EW_SM_RESTART_IP;
//        }
    }

    if(EW_FLAG_TCP_RECEIVED & USBControl->ActualFlags)
    {
        if(USBControl->FlagOTARun)
        {
            USBControl->ActualFlags |= EW_FLAG_TCP_DELETE; //seta a flag para desconectar a tcp porta sitrad.
            //TcpEvent(USBControl, USBControl->IpStruct.Tcp_Port_Com); // le o pacote recebido para libear a pool
            //nx_packet_release(USBControl->PacketReceiveTCP);
        }

        else
        {
            //if(EW_TCP_REQUESTED == TcpEvent(USBControl, USBControl->IpStruct.Tcp_Port_Com))
            //{
                //_TCP_RETURN Ret_temp = AppPacketTcp(USBControl, USBControl->IpStruct.Tcp_Port_Com);
//                if(EW_TCP_REQUESTED_485 == Ret_temp)
//                {
//                    SerialPost(USBControl); //Envia pacote para a thread Serial 485
//                }
//                else if(EW_TCP_REQUESTED_CONF == Ret_temp)
//                {
//                    //TcpSend(USBControl, USBControl->IpStruct.Tcp_Port_Com); //Envia para o Sitrad o pacote TCP recheado.
//                }
                //nx_packet_release(USBControl->PacketReceiveTCP);
                StatusFlagsX->FlagSitradConnected = true; ///seta flag de conexao com o Sitrad.
            //}
            USBControl->TimerConnectSitrad = TIME_SILENCE_SITRAD;
        }
    }

    if(EW_FLAG_MFRAMEWORK & USBControl->ActualFlags)
    {
        //trata-se de uma recepção de Message FW
        if(!PendSerial485(USBControl) && StatusFlagsX->FlagSitradConnected)
        {
//            if(!TcpInstrument(USBControl))
//            {
//                //Envia para o Sitrad o pacote TCP recheado.
//                TcpSend(USBControl, USBControl->IpStruct.Tcp_Port_Com);
//            }
        }

    }

    if(EW_FLAG_TCP_RECEIVED_CONF & USBControl->ActualFlags)
    {
//        if(EW_TCP_REQUESTED == TcpEvent(USBControl, USBControl->IpStruct.Tcp_Port_Conf))
//        {
//            _TCP_RETURN Ret_temp = AppPacketTcp(USBControl, USBControl->IpStruct.Tcp_Port_Conf);
//            if(EW_TCP_REQUESTED_CONF == Ret_temp)
//            {
//                TcpSend(USBControl, USBControl->IpStruct.Tcp_Port_Conf); //Envia para o Sitrad o pacote TCP recheado.
//            }
//        }
        //nx_packet_release(USBControl->PacketReceiveTCPConf);
    }

    if(EW_FLAG_TCP_DISCONNECT & USBControl->ActualFlags)
    {
        //TcpDisconnect(USBControl, USBControl->IpStruct.Tcp_Port_Com);
        StatusFlagsX->FlagSitradConnected = false;
        USBControl->ValidAccess = false;
        USBControl->TimerConnectSitrad = 0;
        USBControl->TimerInactivity = 0;
    }

    if(EW_FLAG_TCP_DISCONNECT_CONF & USBControl->ActualFlags)
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_CONFIG_STATUS_CHANGE, 0, false);
#endif
        //TcpDisconnect(USBControl, USBControl->IpStruct.Tcp_Port_Conf);
        USBControl->ValidAccessConf = false;
        USBControl->TimerInactivity = 0;
    }

    if(EW_FLAG_TCP_DELETE & USBControl->ActualFlags)
    {
        StopTimeoutTcp();//stop o timeout para conexao tcp
        //if (NX_STILL_BOUND == nx_tcp_socket_delete(&USBControl->TcpSocket))
        //{
            //TcpFinish(USBControl, USBControl->IpStruct.Tcp_Port_Com);
        //}
        StatusFlagsX->FlagSitradConnected = false;
    }

    if(EW_FLAG_RTC_SEND & USBControl->ActualFlags)
    {
        RtcPost(USBControl);
        tx_thread_relinquish();
    }

    if(EW_FLAG_WIFI_DISCONNECTED & USBControl->ActualFlags)
    {

#if RECORD_DATALOGGER_EVENTS
        DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_CONNECTION_STATUS_CHANGE, USBControl->FlagConnect, false);
#endif

        if(USBControl->FlagConnect)
        {
            USBControl->USBStatesMachine = EW_SM_RESTART_IP;
        }
    }

    if(EW_FLAG_DETECTED_ACTIVITY & USBControl->ActualFlags)
    {
        USBControl->TimerInactivity = TIME_SILENCE_INACTIVITY;
    }

    if(FLAG_TIMEOUT_TIMER_MAIN & USBControl->ActualFlags)
    {
        EventTimerMain(USBControl, StatusFlagsX);
    }
}
