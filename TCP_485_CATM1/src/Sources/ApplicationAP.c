/*
 * ApplicationAP.c
 *
 *  Created on: 24 de jul de 2019
 *      Author: leonardo.ceolin
 */

#include "common_data.h" // include obrigatório


#include "Includes/MainManagerThreadEntry.h"
#include "Eth_Wifi_Thread.h"
#include "Includes/internal_flash.h"
#include "Includes/ApplicationAP.h"
#include "Includes/ApplicationFlash.h"
#include "Includes/Eeprom25AA02E48.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void SetStatusAPWifi(_MAIN_MANAGER_FLAGS flag);
_MAIN_MANAGER_FLAGS GetStatusAPWifi(void);
_MAIN_MANAGER_FLAGS RamStatusAPWifi(_MAIN_MANAGER_FLAGS flag, bool i);
//void NetWifiCallback(sf_wifi_callback_args_t * p_args);
//void NetWifiProvision(sf_wifi_interface_mode_t mode, uint8_t channel, sf_wifi_security_type_t Security, char const * p_ssid, char const * p_key);
//void NetWifiDhcpServerStart(NX_DHCP_SERVER * p_server);
void SetIpStatus(_ETH_WIFI_CONTROL *EthWifiControl);
void GetNameDefaultAP(_WIFI_AP_MODE_CONF *WifiApModeConf);

bool MacAddressValueWF(uint8_t *MacAddressWiFi, uint8_t Select);
_FG_ERR GetMACValueWF(uint8_t *MacAddressWF);
void SetMACValueWF(uint8_t *MacAddressWF);

#define SIZE_BUFFER_TCP 2048

void SetIpStatus(_ETH_WIFI_CONTROL *EthWifiControl)
{
    //nx_ip_interface_physical_address_get(&g_ip_wifi, 0UL, &EthWifiControl->IpStruct.IpMacAdressMs, &EthWifiControl->IpStruct.IpMacAdressLs);

    EthWifiControl->IpStruct.InterfaceIndex     = SERVER_INTERFACE_INDEX;
//    EthWifiControl->IpStruct.IpAddress          = SERVER_IP;
//    EthWifiControl->IpStruct.IpGateway          = SERVER_IP;
//    EthWifiControl->IpStruct.IpMask             = SERVER_SUBNET_MASK;
}

_MAIN_MANAGER_FLAGS RamStatusAPWifi(_MAIN_MANAGER_FLAGS flag, bool i)
{
    static _MAIN_MANAGER_FLAGS status = MM_READY_TO_CONNECT;

    if(i)
    {
        status = flag;
    }
    else
    {
        return status;
    }
    return 0;
}

_MAIN_MANAGER_FLAGS GetStatusAPWifi()
{
    return RamStatusAPWifi(MM_FLAGS_NONE, false);
}

void SetStatusAPWifi(_MAIN_MANAGER_FLAGS flag)
{
    RamStatusAPWifi(flag, true);
}

//void NetWifiCallback(sf_wifi_callback_args_t * p_args)
//{
//    switch(p_args->event)
//    {
//        case SF_WIFI_EVENT_RX:
//        case SF_WIFI_EVENT_AP_CONNECT:
//        case SF_WIFI_EVENT_AP_DISCONNECT:
//            break;
//
//        case SF_WIFI_EVENT_CLIENT_CONNECT:///< Client Associated Successfully with device AP
//
//#if RECORD_DATALOGGER_EVENTS
//            DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_AP_CLIENT_STATUS_CHANGE, 0, true);
//#endif
//
//            SetStatusAPWifi(MM_CONNECT_AND_READY);
//            break;
//
//        case SF_WIFI_EVENT_CLIENT_DISCONNECT:///< Client Disconnected from device AP
//
//#if RECORD_DATALOGGER_EVENTS
//            DataLoggerRegisterEvent(DATALOGGER_EVENT_WIFI_AP_CLIENT_STATUS_CHANGE, 0, false);
//#endif
//
//            SetStatusAPWifi(MM_READY_TO_CONNECT);
//            break;
//    }
//
//    SSP_PARAMETER_NOT_USED(p_args);
//}

//void NetWifiProvision(sf_wifi_interface_mode_t mode, uint8_t channel, sf_wifi_security_type_t Security, char const * p_ssid, char const * p_key)
//{
//    sf_wifi_provisioning_t g_provision_info;
//    ssp_err_t Result;
//
//    g_provision_info.channel    = channel;
//    g_provision_info.mode       = mode;
//    g_provision_info.security   = Security;
//    g_provision_info.encryption = SF_WIFI_ENCRYPTION_TYPE_AUTO;
//    g_provision_info.p_callback = NetWifiCallback;
//
//    strcpy((char *) &g_provision_info.ssid, (char const *) p_ssid);
//    strcpy((char *) &g_provision_info.key, (char const *) p_key);
//
//    do
//    {
//        Result = g_sf_wifi0.p_api->provisioningSet(g_sf_wifi0.p_ctrl, &g_provision_info);
//    } while (Result != SSP_SUCCESS);
//}


//void GetNameDefaultAP(_WIFI_AP_MODE_CONF *WifiApModeConf)
//{
//    char ServerWifi_SSID[32+1];
//
//    ULONG   IpMacAddressMs,
//            IpMacAddressLs;
//    uint8_t MacAddressWiFi[6];
//    uint8_t MacAddressEth[6];
//
//    WifiApModeConf->ApStaticIP.AddIP    = SERVER_IP;
//    WifiApModeConf->ApStaticIP.AddMask  = SERVER_SUBNET_MASK;
//    WifiApModeConf->ApStaticIP.AddGw    = SERVER_IP;
//    WifiApModeConf->ApStaticIP.AddDNS   = SERVER_IP;
//    WifiApModeConf->StartIpAddList      = SERVER_START_IP_ADDRESS_LIST;
//    WifiApModeConf->EndIpAddList        = SERVER_END_IP_ADDRESS_LIST;
//
//    nx_ip_interface_physical_address_get(&g_ip_wifi, 0UL, &IpMacAddressMs, &IpMacAddressLs);
//
//    MacAddressWiFi[5] = (uint8_t)((IpMacAddressMs >>8)   & 0xFF);
//    MacAddressWiFi[4] = (uint8_t)((IpMacAddressMs)       & 0xFF);
//    MacAddressWiFi[3] = (uint8_t)((IpMacAddressLs>>24)   & 0xFF);
//    MacAddressWiFi[2] = (uint8_t)((IpMacAddressLs>>16)   & 0xFF);
//    MacAddressWiFi[1] = (uint8_t)((IpMacAddressLs>>8)    & 0xFF);
//    MacAddressWiFi[0] = (uint8_t)( IpMacAddressLs        & 0xFF);
//
//    SetMACValueWF(MacAddressWiFi);
//    GetMACValue(MacAddressEth);
//
//    snprintf((char *)ServerWifi_SSID,    sizeof(ServerWifi_SSID),   "%s W-%.2X%.2X E-%.2X%.2X",
//                                        SERVER_WIFI_SSID,
//                                        (uint8_t)((IpMacAddressLs>>8)    & 0xFF),
//                                        (uint8_t)( IpMacAddressLs        & 0xFF),
//                                        MacAddressEth[4],
//                                        MacAddressEth[5]);
//
//    WifiApModeConf->Channel =       SERVER_WIFI_CHANNEL;
//    WifiApModeConf->Security =      SF_WIFI_SECURITY_TYPE_WPA2; //0-Aberta, 1-WEP, 2-WPA, 3-WPA2
//    strcpy((char *)WifiApModeConf->Ssid, (const char *)ServerWifi_SSID);
//    strcpy((char *)WifiApModeConf->Key, (const char *)SERVER_WIFI_KEY);
//}
//
//void NetWifiDhcpServerStart(NX_DHCP_SERVER * p_server)
//{
//    UINT    status;
//    UINT    addresses_added;
//    ULONG   IpMacAdressMs,
//            IpMacAdressLs;
//    uint8_t MacAdressWiFi[6];
//    _WIFI_AP_MODE_CONF WifiApModeConf; //dados para a memoria.
//
//    nx_dhcp_server_init1();
//
//    int_storage_read((uint8_t *)&WifiApModeConf, sizeof(_WIFI_AP_MODE_CONF), CONF_WIFI_AP);
//
//#if RECORD_DATALOGGER_EVENTS
//    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_INIT, 0x200, WifiApModeConf.ValCod == FLASH_NUMBER_MAGIC);
//#endif
//
//    if(WifiApModeConf.ValCod != FLASH_NUMBER_MAGIC)
//    {
//        GetNameDefaultAP(&WifiApModeConf);
//
//        WifiApModeConf.ValCod = FLASH_NUMBER_MAGIC;
//
//        int_storage_write((uint8_t*)&WifiApModeConf.ValCod, sizeof(_WIFI_AP_MODE_CONF), CONF_WIFI_AP);
//
//        _UDP_CONF ConfSitrad_UDP;
//        //Le a memoria em busca das configuracoes de UDP
//        int_storage_read((uint8_t *)&ConfSitrad_UDP, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
//
//        if((ConfSitrad_UDP.ConfDefault != FLASH_NUMBER_CUSTON) && (ConfSitrad_UDP.ConfDefault != FLASH_NUMBER_DEFAULT))
//        {   //Caso nunca tenha sido adicionado uma conf, usa o default.
//            SetConfigDefault(&ConfSitrad_UDP);
//
//            ConfSitrad_UDP.ConfDefault = FLASH_NUMBER_DEFAULT;
//            int_storage_write((uint8_t *)&ConfSitrad_UDP, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
//        }
//    }
//
//    status = nx_ip_address_set(p_server->nx_dhcp_ip_ptr, WifiApModeConf.ApStaticIP.AddIP, WifiApModeConf.ApStaticIP.AddMask);
//
//    nx_ip_interface_physical_address_get(&g_ip_wifi, 0UL, &IpMacAdressMs, &IpMacAdressLs);
//
//
//    if (status != NX_SUCCESS)
//    {
//        __BKPT(0);
//    }
//
//    MacAdressWiFi[5] = (uint8_t)((IpMacAdressMs >>8)   & 0xFF);
//    MacAdressWiFi[4] = (uint8_t)((IpMacAdressMs)       & 0xFF);
//    MacAdressWiFi[3] = (uint8_t)((IpMacAdressLs>>24)   & 0xFF);
//    MacAdressWiFi[2] = (uint8_t)((IpMacAdressLs>>16)   & 0xFF);
//    MacAdressWiFi[1] = (uint8_t)((IpMacAdressLs>>8)    & 0xFF);
//    MacAdressWiFi[0] = (uint8_t)( IpMacAdressLs        & 0xFF);
//
//    //salva na RAM o MAC atual
//    SetMACValueWF(&MacAdressWiFi[0]);
//
//    /* Assign IP addresses from the given range. */
//    status = nx_dhcp_create_server_ip_address_list(p_server,
//                                                   SERVER_INTERFACE_INDEX,
//                                                   (ULONG) WifiApModeConf.StartIpAddList,
//                                                   (ULONG) WifiApModeConf.EndIpAddList,
//                                                   &addresses_added);
//    if (status != NX_SUCCESS)
//    {
//        __BKPT(0);
//    }
//
//    /* Set network parameters returned by the DHCP server. */
//    status = nx_dhcp_set_interface_network_parameters(p_server,
//                                                      SERVER_INTERFACE_INDEX,
//                                                      WifiApModeConf.ApStaticIP.AddMask,
//                                                      WifiApModeConf.ApStaticIP.AddIP,
//                                                      WifiApModeConf.ApStaticIP.AddIP
//                                                      );
//
//    if (status != NX_SUCCESS)
//    {
//        __BKPT(0);
//    }
//
//    NetWifiProvision(   SF_WIFI_INTERFACE_MODE_AP,
//                        WifiApModeConf.Channel,
//                        WifiApModeConf.Security,
//                        (char const * )WifiApModeConf.Ssid,
//                        (char const * )WifiApModeConf.Key
//                    );
//
//    /* Start the DHCP Server. */
//    status = nx_dhcp_server_start(p_server);
//
//    if (status != NX_SUCCESS)
//    {
//        __BKPT(0);
//    }
//}
//
//bool MacAddressValueWF(uint8_t *MacAddressWiFi, uint8_t Select)
//{
//    static uint8_t MacAddress[6];
//    static bool Ret = false;
//
//    for(uint8_t i=0; i < 6; i++)
//    {
//        if(!Select)//put
//        {
//            MacAddress[i] = MacAddressWiFi[i];
//            Ret = true; //sinaliza que em algum momento o MacAddress já foi carregado/lido
//        }
//        else//get
//        {
//            MacAddressWiFi[i] = MacAddress[i];
//        }
//    }
//
//    return Ret;
//}
//
////Essa funcao escreve o endereco mac na ram.
//void SetMACValueWF(uint8_t *MacAddressWF)
//{
//    MacAddressValueWF(&MacAddressWF[0], 0U);
//}
//
////Essa funcao le o endereco mac na ram.
//_FG_ERR GetMACValueWF(uint8_t *MacAddressWF)
//{
//    if(MacAddressValueWF(&MacAddressWF[0], 1U))
//    {
//        return FG_SUCCESS;
//    }
//
//    return FG_ERROR;
//}
