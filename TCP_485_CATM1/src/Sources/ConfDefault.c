/*
 * ConfDefault.c
 *
 *  Created on: 29 de out de 2019
 *      Author: leonardo.ceolin
 */

#include "Includes/ConfDefault.h"
#include "Includes/DataLoggerFlash.h"
#include "Includes/ApplicationUSB.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void USBInitConf(_USB_CONTROL *USBData);
bool USBFAC(bool BitFac, bool SetTGetF);

/*
 * Retorna Status de FAC
 */
bool USBFAC(bool BitFac, bool SetTGetF)
{
    static bool FlagsEepromRam= false;

    if(SetTGetF)
    {
        FlagsEepromRam = BitFac;
    }

    return FlagsEepromRam;
}

void SetUSBFAC(bool bFlagFac)
{

    USBFAC(bFlagFac, true);
}

bool GetUSBFAC(void)
{

    return USBFAC(false, false);
}

/*
 * Funcao resposnsavel por inicializar o padrao de fabrica
 */
void USBInitConf(_USB_CONTROL *USBData)
{

//#if RECORD_DATALOGGER_EVENTS
//    ssp_err_t result;
//    result =
//#endif
//            int_storage_read((uint8_t *)&EthWifiData->ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
//
//#if RECORD_DATALOGGER_EVENTS
//    if (result != SSP_SUCCESS)
//    {
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_FLASH_ERROR, result, false);
//    }
//
//    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_INIT,
//            (uint16_t)((EthWifiData->ConfNetBasic.InterfaceType << 2) | (EthWifiData->ConfNetBasic.OpModeWifi << 1) | (EthWifiData->ConfNetBasic.AddrMode << 0)),
//            EthWifiData->ConfNetBasic.CodMagic == FLASH_NUMBER_CONF_BASIC);
//#endif
//
//    if(EthWifiData->ConfNetBasic.CodMagic != FLASH_NUMBER_CONF_BASIC)
//    {
//        EthWifiData->ConfNetBasic.InterfaceType = WIFI;
//        EthWifiData->ConfNetBasic.OldInterfaceType = WIFI;
//        EthWifiData->ConfNetBasic.OpModeWifi = MODE_AP;
//        EthWifiData->ConfNetBasic.FlagDNS = 0;
//        EthWifiData->ConfNetBasic.AddrMode = MODE_DHCP;
//        EthWifiData->ConfNetBasic.HiddenNet = 0;
//        EthWifiData->ConfNetBasic.CodMagic = FLASH_NUMBER_CONF_BASIC;
//
//        _NET_STATIC_MODE NetStaticWifi;
//        int_storage_read((uint8_t *)&NetStaticWifi, sizeof(_NET_STATIC_MODE), CONF_WIFI_STATIC_IP);
//        NetStaticWifi.AddDNS = 0;
//        NetStaticWifi.AddDNS2 = 0;
//        NetStaticWifi.AddIP = 0;
//        NetStaticWifi.AddGw = 0;
//        NetStaticWifi.AddMask = 0;
//
//        int_storage_write((uint8_t*)&EthWifiData->ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
//        int_storage_write((uint8_t*)&NetStaticWifi, sizeof(_NET_STATIC_MODE), CONF_WIFI_STATIC_IP);
//    }
//
//    int_storage_read((uint8_t *)&EthWifiData->IpStaticMode, sizeof(_NET_STATIC_MODE), CONF_WIFI_STATIC_IP);
//
//    _UDP_CONF ConfSitrad_UDP;
//
//    EthWifiData->IpStruct.Tcp_Port_Com  = PORT_TCP_SITRAD;
//    EthWifiData->IpStruct.Tcp_Port_Conf = PORT_TCP_SITRAD_CONF;
//    EthWifiData->IpStruct.Udp_Port      = PORT_UDP_SITRAD;
//
//    //Le a memoria em busca das configuracoes de UDP
//    int_storage_read((uint8_t *)&ConfSitrad_UDP, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
//    if((ConfSitrad_UDP.ConfDefault == FLASH_NUMBER_CUSTON) || (ConfSitrad_UDP.ConfDefault == FLASH_NUMBER_DEFAULT))
//    {
//        EthWifiData->IpStruct.Tcp_Port_Com = (uint16_t)(((ConfSitrad_UDP.ComunicationPort[1] << 8) & (0xFF00)) | ConfSitrad_UDP.ComunicationPort[0]) ;
//        EthWifiData->IpStruct.Tcp_Port_Conf = (uint16_t)(((ConfSitrad_UDP.ConfigurationPort[1] << 8) & (0xFF00)) | ConfSitrad_UDP.ConfigurationPort[0]) ;
//    }
//
//    memset((char *)EthWifiData->NetWifiConf.ApSsid, 0, sizeof(EthWifiData->NetWifiConf.ApSsid));
//    memset((char *)EthWifiData->NetWifiConf.ApPwd, 0, sizeof(EthWifiData->NetWifiConf.ApPwd));
//
//#if RECORD_DATALOGGER_EVENTS
//    result =
//#endif
//    int_storage_read((uint8_t *)&EthWifiData->NetWifiConf, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
//
//
//#if RECORD_DATALOGGER_EVENTS
//    if (result != SSP_SUCCESS)
//    {
//        DataLoggerRegisterEvent(DATALOGGER_EVENT_FLASH_ERROR, result, false);
//    }
//
//    DataLoggerRegisterEvent(DATALOGGER_EVENT_NET_INIT, 0x100, EthWifiData->NetWifiConf.ValCod == FLASH_NUMBER_MAGIC);
//#endif
//
//    if(EthWifiData->NetWifiConf.ValCod != FLASH_NUMBER_MAGIC)
//    {
//        memset((char *)EthWifiData->NetWifiConf.ApPwd, 0, sizeof(EthWifiData->NetWifiConf.ApPwd));
//        memset((char *)EthWifiData->NetWifiConf.ApSsid, 0, sizeof(EthWifiData->NetWifiConf.ApSsid));
//
//        strcpy((char *)EthWifiData->NetWifiConf.ApPwd, (const char *)/*"tplink123456"*/"");
//        strcpy((char *)EthWifiData->NetWifiConf.ApSsid, (const char *)/*"tplink123456"*/"");
//
//        EthWifiData->NetWifiConf.SecurityType = true;
//        EthWifiData->NetWifiConf.ValCod = FLASH_NUMBER_MAGIC;
//
//        int_storage_write((uint8_t*)&EthWifiData->NetWifiConf.ValCod, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
//    }
//    //seta aqui, em caso de ecal o modo de funcionamento
//    if(EthWifiData->FlagFAC)
//    {
//        EthWifiData->ConfNetBasic.InterfaceType = ETHERNET;
//        EthWifiData->ConfNetBasic.OpModeWifi = MODE_CLIENT;
//        EthWifiData->ConfNetBasic.FlagDNS = 0;
//        EthWifiData->ConfNetBasic.AddrMode = MODE_DHCP;
//        EthWifiData->ConfNetBasic.HiddenNet = 0;
//    }
}
