/*
 * ApplicationTimerMain.c
 *
 *  Created on: 18 de nov de 2019
 *      Author: leonardo.ceolin
 *
 * Faz o controle de operacoes controladas por timer.
 * Operacoes como reset do equipamento, finalizacao de atualizacao remota por timeout,
 * timeout de conexao com SITRAD, reconfiguracao do equipamento para padroes de fabrica.
 */

#include "Includes/ApplicationTimerMain.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/Thread_Monitor_entry.h"
#include "USB_HID_Device_Thread.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void EventTimerMain(_USB_CONTROL *USBData, _STATUS_FLAGS *StatusFlagsX);
void EndOta(void);

void EventTimerMain(_USB_CONTROL *USBData, _STATUS_FLAGS *StatusFlagsX)
{
    if(USBData->TimerBigFrame)
    {
        USBData->TimerBigFrame--;
        if(!USBData->TimerBigFrame)
        {
            USBData->BigFrame = 0;
            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);
        }
    }

    if(USBData->TimerOTACheckCRC)
    {
        USBData->TimerOTACheckCRC--;
        if(!USBData->TimerOTACheckCRC)
        {
            if(!CheckCRCFirmwareQSPI())
            {
                RestartModule();
            }
            else
            {
                USBData->TimerTimeOutOTA = 2;
            }
        }
    }

    if(USBData->TimerTimeOutOTA)
    {
        USBData->TimerTimeOutOTA--;
        if(!USBData->TimerTimeOutOTA)
        {
            USBData->FlagOTARun = false;
            USBData->USBStatesMachine = SM_RESTART_IP;
            g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);
            EndOta();
        }

        if(USBData->FlagConnectSitrad)
        {
            tx_event_flags_set (&USB_Flags, FLAG_USB_DELETE, TX_OR);//seta a flag para desconectar a tcp porta sitrad.
        }
    }

    if(USBData->TimerRestartModule)
    {
        USBData->TimerRestartModule--;
        if(!USBData->TimerRestartModule)
        {
            RestartModule();
        }
    }

    if(USBData->TimerRestoreFactory)
    {
        USBData->TimerRestoreFactory--;
        if(!USBData->TimerRestoreFactory)
        {
            ThreadMainManagerSetFlag(MM_RESTORE_FACTORY);
        }
    }

    if(USBData->TimerConnectSitrad)
    {
        USBData->TimerConnectSitrad--;
        if(!USBData->TimerConnectSitrad)
        {
            StatusFlagsX->FlagSitradConnected = false;
            tx_event_flags_set (&USB_Flags, FLAG_USB_DISCONNECT, TX_OR);

        }
    }

    if(USBData->TimerInactivity)
    {
        USBData->TimerInactivity--;
        if(!USBData->TimerInactivity)
        {
            if(USBData->ConfNetBasic.InterfaceType == ETHERNET)
            {
                tx_event_flags_set (&USB_Flags, FLAG_USB_DISCONNECT, TX_OR);
            }
        }
    }
}

void EndOta(void)
{

#if RECORD_DATALOGGER_EVENTS
    DataLoggerRegisterEvent(DATALOGGER_EVENT_FOTA_STATUS_CHANGE, 0, false);
#endif

    _DATA_LOGGER_DATA DataLoggerData;
    DataLoggerData.OpCod = END_OTA; //Avisa a thread datalogger que a atualização terminou.
    tx_queue_send(&Datalogger_Flash, &DataLoggerData, TX_WAIT_FOREVER); //envia para a thread Datalogger

    //ThreadMonitor_SendKeepAlive(T_MONITOR_START_MONITORING);
}
