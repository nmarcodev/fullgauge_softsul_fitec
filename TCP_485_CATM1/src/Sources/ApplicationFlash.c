/*
 * AplicationFlash.c
 *
 *  Created on: 23 de abr de 2019
 *      Author: leonardo.ceolin
 */
#include "common_data.h" // include obrigatório

#include "Includes/ApplicationFlash.h"
#include "r_flash_api.h"

#include "Includes/internal_flash.h"
#include "Includes/UdpTcpTypes.h"
#include "Includes/ApplicationAP.h"
#include "Includes/Eeprom25AA02E48.h"

_FG_ERR Flash_BootRequest(void);
void SetConfigDefault(_UDP_CONF *UdpConfig);

void SetConfigDefault(_UDP_CONF *UdpConfig)
{
    uint8_t Index = 0;
    uint8_t MacAddressWF[6];
    uint8_t MacAddressEth[6];

    UdpConfig->BaudRate485[0] = (uint8_t)(BAUD_RATE_RS485   &   0xFF);
    UdpConfig->BaudRate485[1] = (uint8_t)(BAUD_RATE_RS485 >>8)  &0xFF;

    UdpConfig->ComunicationPort[0] = (uint8_t)(PORT_TCP_SITRAD  &        0xFF);
    UdpConfig->ComunicationPort[1] = (uint8_t)(PORT_TCP_SITRAD  >>8 )    &0xFF;

    UdpConfig->ConfDefault = FLASH_NUMBER_DEFAULT;

    UdpConfig->ConfigurationPort[0] = (uint8_t)(PORT_TCP_SITRAD_CONF       &  0xFF);
    UdpConfig->ConfigurationPort[1] = (uint8_t)(PORT_TCP_SITRAD_CONF  >>8 )&  0xFF;

    UdpConfig->EnableIpFilter = 0;

    UdpConfig->EnablePassword = 0;

    UdpConfig->InitFilterIp[0] = 0;
    UdpConfig->InitFilterIp[1] = 0;
    UdpConfig->InitFilterIp[2] = 0;
    UdpConfig->InitFilterIp[3] = 0;

    UdpConfig->EndFilterIp[0] = 255;//0;
    UdpConfig->EndFilterIp[1] = 255;//0;
    UdpConfig->EndFilterIp[2] = 255;//0;
    UdpConfig->EndFilterIp[3] = 255;//0;

    UdpConfig->IpMode = 0;

    //memset(UdpConfig->ModelName, '\0', MODEL_NAME_SIZE);

    GetMACValueWF(MacAddressWF);
    GetMACValue(MacAddressEth);

    snprintf((char *)UdpConfig->ModelName,sizeof(UdpConfig->ModelName),   "%s W-%.2X%.2X E-%.2X%.2X",
                                            NAME_MODEL,
                                            //MacAddressWF[2],
                                            MacAddressWF[1],
                                            MacAddressWF[0],
                                            MacAddressEth[4],
                                            MacAddressEth[5]);

    //memcpy(UdpConfig->ModelName, NAME_MODEL, MODEL_NAME_SIZE);

    for(Index = 0; Index < PASSWORD_SIZE; Index++)
    {
        UdpConfig->Password[Index] = 0;
    }

    UdpConfig->TimeOutRs485[0] = (uint8_t)(TIME_OUT_DEFAULT_WEB     &   0xFF);
    UdpConfig->TimeOutRs485[1] = (uint8_t)(TIME_OUT_DEFAULT_WEB     >>8 )&0xFF;

}//X
