/*
 * HID_Descriptor.c
 *
 *  Created on: Apr 27, 2021
 *      Author: LAfonso01
 */
#include "common_data.h"

void g_ux_device_class_register_hid(void);
/***********************************************************************************************************************
 * USB HID Descriotor definitions
 **********************************************************************************************************************/
/* Define the size of HID Descriptor to be used */
#define USB_HID_DESCRIPTOR_TYPE      (0x01)
#define USB_HID_DESCRIPTOR_LENGTH    (0x19)   /* HID Descriptor for Keyboard Device */
#define USB_HID_DESCRIPTOR_NUM_OF_EP (1)

static UX_SLAVE_CLASS_HID_PARAMETER g_ux_device_class_hid_parameter;

/***********************************************************************************************************************
 * USB HID Interface Descriptor for FS mode g_usb_interface_descriptor_hid0
 **********************************************************************************************************************/
static volatile const unsigned char g_usb_interface_descriptor_hid0_full_speed[] BSP_PLACE_IN_SECTION_V2(".usb_interface_desc_fs") BSP_ALIGN_VARIABLE_V2(1)
= {
	/****************************************************************
	 * HID Class Keyboard Interface Descriptor Requirement 9 bytes           *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_INTERFACE_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	0x00, /* 2 bInterfaceNumber */
	0x00, /* 4 bAlternateSetting  */
	0x01, /* 5 bNumEndpoints      */
	UX_DEVICE_CLASS_HID_CLASS, /* 6 bInterfaceClass    : HID Class(0x3)  */
	UX_DEVICE_CLASS_HID_SUBCLASS, /* 7 bInterfaceSubClass : Not support boot interface FIXME */
	0x01, /* 8 bInterfaceProtocol : Protocol code(None(0)/Keyboard(1)/Mouse(2)/Keyboard+Mouse(3)) */
	0x00, /* 9 iInterface Index   */
	/****************************************************************
	 * HID Descriptor                                               *
	 ****************************************************************/
	0x9, /* 0 bLength */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_HID, /* 1 bDescriptorType : HID descriptor (0x21) */
	0x10, /* 2 bcdHID 0x0110 == 1.10 */
	0x01, /* 3 bcdHID  */
	0x21, /* 4 bCountryCode : Hardware target country */
	USB_HID_DESCRIPTOR_NUM_OF_EP, /* 5 bNumDescriptors */
	UX_DEVICE_CLASS_HID_DESCRIPTOR_REPORT, /* 6 bDescriptorType (Report descriptor type) */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH), /* 7 wItemLength */
	(UCHAR)(USB_HID_DESCRIPTOR_LENGTH >> 8), /* 8 wItemLength */
	/****************************************************************
	 * Endpoint descriptor (Interrupt-In)                           *
	 ****************************************************************/
	0x7, /* 0 bLength */
	UX_ENDPOINT_DESCRIPTOR_ITEM, /* 1 bDescriptorType */
	(UX_ENDPOINT_IN | 1), /* 2 bEndpointAddress */
	UX_INTERRUPT_ENDPOINT, /* 3 bmAttributes  */
	(UCHAR)(0x8), /* 4 wMaxPacketSize */
	(UCHAR)(0x8 >> 8), /* 5 wMaxPacketSize */
	0x8, /* 6 bInterval */
};

const UCHAR g_hid_report_descriptor_custom[] = {
	    0x06, 0x00, 0xff,              // USAGE_PAGE (Vendor Defined Page 1)
	    0x09, 0x01,                    // USAGE (Vendor Usage 1)
	    0xa1, 0x01,                    // COLLECTION (Application)
	    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
	    0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
	    0x75, 0x08,                    //   REPORT_SIZE (8)
	    //0x96, 0x84, 0x03,  //0x01,   //   REPORT_COUNT (900)
		//0x95, 0x20,			       //   REPORT_COUNT (32)
		//0x95, 0xFF,				   //   REPORT_COUNT (255)
		0x95, 0x40,					   //   REPORT_COUNT (64)
	    0x09, 0x01,                    //   USAGE (Vendor Usage 1)
	    0x81, 0x00,                    //   INPUT (Data,Ary,Abs)
	    0x09, 0x01,                    //   USAGE (Vendor Usage 1)
	    0x91, 0x00,                    //   OUTPUT (Data,Ary,Abs)
	    0xc0                           // END_COLLECTION
};

void g_ux_device_class_register_hid(void) {
	/* Initialize the HID class parameters for a keyboard.  */
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_address = (UCHAR *)g_hid_report_descriptor_custom;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_length = sizeof(g_hid_report_descriptor_custom);
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_callback = ux_hid_device_callback;
	g_ux_device_class_hid_parameter.ux_device_class_hid_parameter_report_id = UX_FALSE;

	/* Register user callback functions.  */
	g_ux_device_class_hid_parameter.ux_slave_class_hid_instance_activate = NULL;
	g_ux_device_class_hid_parameter.ux_slave_class_hid_instance_deactivate = NULL;

	/* Initialize the device HID class. This class owns both interfaces starting with 1. */
	ux_device_stack_class_register(_ux_system_slave_class_hid_name, ux_device_class_hid_entry, 1, 0x00, (VOID *)&g_ux_device_class_hid_parameter);
}

