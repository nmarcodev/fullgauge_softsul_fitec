/*
 * RX_8571 LC.c
 *
 *  Created on: 15 de jan de 2019
 *      Author: felipe.pohren
 */
#include "common_data.h" // include obrigatório

#include "Includes/TypeDefs.h"
#include "RTC_thread.h"
#include "Includes/RX_8571LC.h"
#include "Includes/DateTime.h"

// Efetua a conversão de um valor em BCD para decimal
uint8_t BCDToDecimal (uint8_t BcdByte);
// Efetua a conversão de um valor em decimal para BCD
uint8_t DecimalToBCD (uint8_t DecimalByte);

_FG_ERR RX8571LC_Init(void)
{
    ssp_err_t SSP_Status;
    __EXTERNAL_RTC_RW_TABLE ExternalRtcTable;
    ExternalRtcTable.RegisterTable.RAM = 0;
    ExternalRtcTable.RegisterTable.MinAlarm = 0;
    ExternalRtcTable.RegisterTable.HourAlarm = 0;
    ExternalRtcTable.RegisterTable.WeekDayAlarm = 0;
    ExternalRtcTable.RegisterTable.TimerCounter0 = 0;
    ExternalRtcTable.RegisterTable.TimerCounter1 = 0;
    ExternalRtcTable.RegisterTable.ExtensionRegister = 0;
    ExternalRtcTable.FirstRegister = RX_8571LC_RAM_REGISTER;

    SSP_Status =  g_i2c1.p_api->write(g_i2c1.p_ctrl,&ExternalRtcTable.FirstRegister,RX_8571LC_INIT_SIZE,false);

    if(SSP_Status == SSP_SUCCESS)
    {
        return FG_SUCCESS;
    }
    else
    {
        return FG_ERROR;
    }
}

void CloseI2C_RTC(void)
{
    // desliga a I2C
    g_i2c1.p_api->close(g_i2c1.p_ctrl);
}

void InitI2C_RTC(void)
{
    // liga a I2C
    g_i2c1.p_api->open(g_i2c1.p_ctrl, g_i2c1.p_cfg);
    g_i2c1.p_api->reset(g_i2c1.p_ctrl);
}


_FG_ERR RX8571LC_GetExternalDateTime(uint8_t* DateTimeList)
{

    SSP_PARAMETER_NOT_USED(DateTimeList);
    ssp_err_t SSP_Status;
    __EXTERNAL_RTC_RW_TABLE ExternalRtcTable;


    ExternalRtcTable.FirstRegister = RX_8571LC_INITIAL_REGISTER;

    SSP_Status =  g_i2c1.p_api->write(g_i2c1.p_ctrl,&ExternalRtcTable.FirstRegister,1,false);

    if(SSP_Status == SSP_SUCCESS)
    {
        SSP_Status =  g_i2c1.p_api->read(g_i2c1.p_ctrl,(uint8_t*)&ExternalRtcTable.RegisterTable.Second,RX_8571LC_ALL_REGISTERS_SIZE,false);
        if(SSP_Status != SSP_SUCCESS)
            return FG_ERROR;
    }
    else
    {
        return FG_ERROR;
    }

    DateTimeList[DATE_TIME_DAY]    = BCDToDecimal(ExternalRtcTable.RegisterTable.Day);
    DateTimeList[DATE_TIME_MONTH]  = BCDToDecimal(ExternalRtcTable.RegisterTable.Month);
    DateTimeList[DATE_TIME_YEAR]   = BCDToDecimal(ExternalRtcTable.RegisterTable.Year);
    DateTimeList[DATE_TIME_HOUR]   = BCDToDecimal(ExternalRtcTable.RegisterTable.Hour);
    DateTimeList[DATE_TIME_MINUTE] = BCDToDecimal(ExternalRtcTable.RegisterTable.Minute);
    DateTimeList[DATE_TIME_SECOND] = BCDToDecimal(ExternalRtcTable.RegisterTable.Second);

    return FG_SUCCESS;
}

_FG_ERR RX8571LC_SetExternalDateTime(uint8_t* const DateTimeList)
{
    SSP_PARAMETER_NOT_USED(DateTimeList);
    ssp_err_t SSP_Status;
    __EXTERNAL_RTC_RW_TABLE ExternalRtcTable;

    ExternalRtcTable.FirstRegister = RX_8571LC_INITIAL_REGISTER;
    // le os registros atuais
    SSP_Status =  g_i2c1.p_api->write(g_i2c1.p_ctrl,&ExternalRtcTable.FirstRegister,1,false);
    if(SSP_Status == SSP_SUCCESS)
    {
        SSP_Status =  g_i2c1.p_api->read(g_i2c1.p_ctrl,(uint8_t*)&ExternalRtcTable.RegisterTable.Second,RX_8571LC_ALL_REGISTERS_SIZE,false);
        if(SSP_Status != SSP_SUCCESS)
            return FG_ERROR;
    }
    else
    {
        return FG_ERROR;
    }

    // seta novo horário
    ExternalRtcTable.RegisterTable.Day = DecimalToBCD(DateTimeList[DATE_TIME_DAY]);
    ExternalRtcTable.RegisterTable.Month = DecimalToBCD(DateTimeList[DATE_TIME_MONTH]);
    ExternalRtcTable.RegisterTable.Year = DecimalToBCD(DateTimeList[DATE_TIME_YEAR]);
    ExternalRtcTable.RegisterTable.Hour = DecimalToBCD(DateTimeList[DATE_TIME_HOUR]);
    ExternalRtcTable.RegisterTable.Minute = DecimalToBCD(DateTimeList[DATE_TIME_MINUTE]);
    ExternalRtcTable.RegisterTable.Second = DecimalToBCD(DateTimeList[DATE_TIME_SECOND]);


    ExternalRtcTable.RegisterTable.FlagRegister = (uint8_t)(ExternalRtcTable.RegisterTable.FlagRegister & ~ RX_8571LC_VLF_BIT);
    ExternalRtcTable.RegisterTable.ControlRegister = (uint8_t)(ExternalRtcTable.RegisterTable.ControlRegister & ~ RX_8571LC_STOP_BIT);

    SSP_Status =  g_i2c1.p_api->write(g_i2c1.p_ctrl,&ExternalRtcTable.FirstRegister,sizeof(ExternalRtcTable),false);

    if(SSP_Status != SSP_SUCCESS)
        return FG_ERROR;
    return FG_SUCCESS;
}

//*****************************************************************************************************/
//*
//* Verifica se o relogio esta com data e hora OK
//*
//*
//* @return Retorna FG_ECLO se relogio esta em ECLO, FG_ERROR se não conseguiu ler e FG_SUCCESS se o relogio esta OK
//*
//*
//*****************************************************************************************************/
_FG_ERR RX8571LC_VerifyEclo(void)
{
    //_FG_ERR FG_Status;
    ssp_err_t SSP_Status;
    uint8_t Flagregister;

    Flagregister = RX_8571LC_FLAG_REGISTER;
    SSP_Status =  g_i2c1.p_api->write(g_i2c1.p_ctrl,&Flagregister,1,false);
    if(SSP_Status == SSP_SUCCESS)
    {
        SSP_Status =  g_i2c1.p_api->read(g_i2c1.p_ctrl,(uint8_t*)&Flagregister,1,false);
        if(SSP_Status != SSP_SUCCESS)
        {
            return FG_ERROR;
        }
    }
    else
    {
        return FG_ERROR;
    }

    if(Flagregister & RX_8571LC_VLF_BIT)
    {
        return FG_ECLO;
    }
    return FG_SUCCESS;
}

//*****************************************************************************************************/
//* Efetua a conversão de um valor em BCD para decimal
//*
//* @param[in] BcdByte: o valor em BCD a ser convertido
//*
//* @return o valor em decimal
//*
//*****************************************************************************************************/
uint8_t BCDToDecimal (uint8_t BcdByte)
{
  return (uint8_t)(((uint8_t)((BcdByte & 0xF0) >> 4) * 10) + (uint8_t)(BcdByte & 0x0F));
}

//*****************************************************************************************************/
//* Efetua a conversão de um valor em decimal para BCD
//*
//* @param[in] DecimalByte: o valor em decimal a ser convertido
//*
//* @return o valor em BCD
//*
//*****************************************************************************************************/
uint8_t DecimalToBCD (uint8_t DecimalByte)
{
  return (uint8_t)(((DecimalByte / 10) << 4) | (DecimalByte % 10));
}
