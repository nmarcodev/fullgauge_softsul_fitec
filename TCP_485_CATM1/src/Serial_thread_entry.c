/*
 * Serial Thread
 *
 * Thread responsavel pela comunicacao com os instrumentos pela rede RS-485.
 *
 * Funciona em dois modos: client e server.
 *
 * No modo cliente, recebe pelo Message Framework a mensagem SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN da Thread Eth_Wifi
 * com dados para enviar pelo barramento 485 e aguarda a resposta do instrumento. Ao receber qualquer dado do
 * instrumento, envia pelo Message Framework para a Thread Eth_Wifi o dado recebido. Apos um periodo de tempo de timeout,
 * envia pelo Message Framework a informação de que ocorreu timeout.
 *
 * No modo server, recebe pelo Message Framework a mensagem SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER da Thread Datalogger
 * com dados para enviar pelo barramento 485 e aguarda a resposta do instrumento. Ao receber dados do instrumento, faz a
 * conferencia da validade do pacote (CRC, tamanho, endereco), e envia pelo Message Framework para a Thread Datalogger
 * se o dado for valido. Se o dado nao for valido ou se ocorrer timeout, reenvia o pacote, tentando por ate 3 vezes.
 * Se todas as tentativas falharem, retorna erro atraves do Message Framework para a Thread Datalogger.
 *
 * As flags Serial_event_flags indicam quando houve comunicacao pelo Message Framework ou quando houve recepcao de um pacote
 * pela serial ou timeout.
 */
#include "Serial_thread.h"
#include "Includes/Serial_thread_entry.h"
#include "Includes/TypeDefs.h"
#include "Includes/Define_IOs.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/Eth_Wifi_Thread_Entry.h"

// semaphore's define for events
//TX_SEMAPHORE TimerSemaphore;

//TX_SEMAPHORE gSerial_Semaphore;

typedef enum
{
    TCP_CLIENT=0,
    TCP_SERVER=1,
}_TCP_MODE;

//Variavel de controle do modo
_TCP_MODE TcpMode=TCP_CLIENT;

typedef struct
{
    sf_message_header_t         *header;
    _MESSAGE_FRAMEWORK_CFG      MessageCfg;

    _SERIAL_DATA_IN_PAYLOAD     SerialDataInPayload;//sk
    _SERIAL_DATA_OUT_PAYLOAD    *SerialDataOutPayload;

    _SERIAL_DATA_IN_PAYLOAD    *DataloggerDataInPayload;

    ULONG                       SerialFlags;

    uart_callback_args_t        *p_args;

    _SERIAL_RECV                SerialRecv;

    sf_message_event_class_t    LastEventClass;
    bool                        Flag_Test485;
    bool                        Flag_Test485_CkS;

    uint8_t                     NumberOfRetrys;
    uint8_t                     CurrentEquipAddrRequested;

}_SERIAL_485;

void Timer0Open(timer_instance_t* TimerInstance);

void Serial_Message_Framework_NotifyCallback(TX_QUEUE *Serial_thread_message);
void Serial_MessageQueue_NotifyCallback(TX_QUEUE *SerialMessageQueue);
_FG_ERR SerialPend(_SERIAL_485 *Serial485);
void SerialOpen(_SERIAL_485 *Serial485);
bool SerialTransmit(_SERIAL_485 *Serial485);
void SerialPostTCP(_SERIAL_485 *Serial485);
_SERIAL_RETURN SerialReceiveMessageTreatment(_SERIAL_485 *Serial485);
void SerialReceiQueue(_SERIAL_485 *Serial485);

_SERIAL_RECV            GSerialRecv;
uint8_t                 FlagWaitDevice;

/* Serial Thread entry function */
void Serial_thread_entry(void)
{
    _SERIAL_485 Serial485;
    timer_instance_t Timer0InstanceOpen = g_timer0;

    SerialOpen(&Serial485);

    Timer0Open( &Timer0InstanceOpen);

    while (1)
    {
        if (!FlagWaitDevice) // protecao para nao trancar a aplicacao no caso de erro da serial
        {
            ThreadMonitor_SendKeepAlive(T_MONITOR_SERIAL_THREAD);
        }

        if(!tx_event_flags_get(&Serial_event_flags, SERIAL_ALL, TX_OR_CLEAR, &Serial485.SerialFlags,TIME_750MS))
        {
            if(SERIAL_MSFW == Serial485.SerialFlags)
            {
                if(FG_SUCCESS == SerialPend(&Serial485))
                {
                    if (!SerialTransmit(&Serial485))
                    {//if serial was unable to transmit it is because it was busy
                        Serial485.SerialDataInPayload.num_of_bytes = 0; // seta num of bytes para 0 para que o consumer trate como pacote vazio.
                        SerialPostTCP(&Serial485);
                        Serial485.NumberOfRetrys = 0;
                    }
                }
            }

            if(SERIAL_RCV_OK == Serial485.SerialFlags)
            {
                if(SERIAL_POST_TRUE == SerialReceiveMessageTreatment(&Serial485))
                {
                    SerialPostTCP(&Serial485);
                    tx_thread_relinquish();
                    //Serial485.NumberOfRetrys = MAX_OF_SERIAL_RETRYS;
                }
                else
                {
                    if(TcpMode == TCP_SERVER)
                    {
                        if(Serial485.NumberOfRetrys < MAX_OF_SERIAL_RETRYS)
                        {
                            FlagWaitDevice = 1;
                            if (!SerialTransmit(&Serial485))
                            {//if serial was unable to transmit it is because it was busy
                                Serial485.SerialDataInPayload.num_of_bytes = 0; // seta num of bytes para 0 para que o consumer trate como pacote vazio.
                                Serial485.NumberOfRetrys = 0;
                                SerialPostTCP(&Serial485);
                            }
                            else
                            {
                                Serial485.NumberOfRetrys++;
                            }

                        }
                        else
                        {
                            Serial485.NumberOfRetrys = 0;
                            SerialPostTCP(&Serial485);
                        }
                    }
                    else
                    {
                        Serial485.NumberOfRetrys = 0;
                    }
                }
            }

            if(SERIAL_TEST_485 == Serial485.SerialFlags)
            {
                SerialReceiQueue(&Serial485);
            }
        }
    }
}

void SerialOpen(_SERIAL_485 *Serial485)
{
    Serial485->SerialRecv.IndexPtr = 0;
    Serial485->Flag_Test485 = false;
    FlagWaitDevice      = 0U;
    Serial485->NumberOfRetrys = 0;
    Serial485->CurrentEquipAddrRequested = 0xFF;

    g_ioport.p_api->pinWrite(UART0_TX_DIR,IOPORT_LEVEL_LOW);//Habilita recepção

    g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); //Inicializa os LED
    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); //Inicializa os LED

    g_uart0.p_api->open(g_uart0.p_ctrl, g_uart0.p_cfg);

    //Used in Acquire
    Serial485->MessageCfg.acquireCfg.buffer_keep = true;   // não desaloca o buffer
    //Used in Post
    Serial485->MessageCfg.postCfg.priority = SF_MESSAGE_PRIORITY_NORMAL;
    Serial485->MessageCfg.postCfg.p_callback = NULL; // não cria callback

    g_sf_message0.p_api->bufferAcquire(g_sf_message0.p_ctrl,
                                       (sf_message_header_t **) &Serial485->SerialDataOutPayload,
                                       &Serial485->MessageCfg.acquireCfg,
                                       300);

    g_sf_message0.p_api->bufferAcquire(g_sf_message0.p_ctrl,
                                       (sf_message_header_t **) &Serial485->DataloggerDataInPayload,
                                       &Serial485->MessageCfg.acquireCfg,
                                       300);

    //tx_semaphore_create(&gSerial_Semaphore,"Serial Semaphore", 0);

    tx_queue_send_notify (&Serial_thread_message_queue, Serial_Message_Framework_NotifyCallback);

    //Inicializa notify callback para MessageQueue
    tx_queue_send_notify (&QueueTest485, Serial_MessageQueue_NotifyCallback);
}

//*************************************************************************************************
//* Configures the Timer0 and created semaphore for its uses
//*************************************************************************************************
void Timer0Open(timer_instance_t* TimerInstance)
{
    g_timer0.p_api->open(TimerInstance->p_ctrl, TimerInstance->p_cfg);

    //tx_semaphore_create(&TimerSemaphore,"TimerSemaphore", 0);
}

_FG_ERR SerialPend(_SERIAL_485 *Serial485)
{
    ssp_err_t SSPErr;

    _SERIAL_DATA_IN_PAYLOAD *SDataInPayload;

    SSPErr = g_sf_message0.p_api->pend(g_sf_message0.p_ctrl,
                                       &Serial_thread_message_queue,
                                       (sf_message_header_t **) &Serial485->header,
                                       TX_NO_WAIT);
    if (SSPErr == SSP_SUCCESS)
    {
        if(FlagWaitDevice)
        {
            return FG_ERROR;
        }
        else if(Serial485->header->event_b.class_code == SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN)
        {
            Serial485->LastEventClass = SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN;
            TcpMode = TCP_CLIENT;
        }
        else if(Serial485->header->event_b.class_code == SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER)
        {
            Serial485->LastEventClass = SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER;
            TcpMode = TCP_SERVER;
        }
        else
        {
            return FG_ERROR;
        }

        SDataInPayload = (_SERIAL_DATA_IN_PAYLOAD*)Serial485->header;

        Serial485->SerialDataInPayload.num_of_bytes = SDataInPayload->num_of_bytes;
        Serial485->SerialDataInPayload.pointer = SDataInPayload->pointer;
        FlagWaitDevice = 1;
        return FG_SUCCESS;
    }
    else
    {
        g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);
        //print_to_console("Erro - SdataInPayload - PEND\n\r");
        return FG_ERROR;
    }
}

void SerialPostTCP(_SERIAL_485 *Serial485)
{

    Serial485->SerialDataOutPayload->header.event_b.class_instance = 0;

    Serial485->SerialDataOutPayload->num_of_bytes = Serial485->SerialRecv.IndexPtr; //tamanho menos os numeros randomicos
    Serial485->SerialDataOutPayload->pointer = (uint16_t *)&Serial485->SerialRecv.RxDevice[0];

    if(Serial485->LastEventClass == SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER) //TCP eh servidor
    {
        //Envia para a thread Logger
        Serial485->SerialDataOutPayload->header.event_b.class_code = SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN; //symbol da engrenagem grava na dataflash

        g_sf_message0.p_api->post(g_sf_message0.p_ctrl,
                                  &Serial485->SerialDataOutPayload->header,//DataIpPayloadMessagePayload->header,
                                  &Serial485->MessageCfg.postCfg,//global que esta no incio desta thread;
                                  &Serial485->MessageCfg.err_postcFG,//global que esta no incio desta thread;
                                  TX_NO_WAIT);
    }

    else if(Serial485->Flag_Test485) //envia para TCPPayload@EthWifi em caso de teste da serial
    {
        Serial485->Flag_Test485 = false;
        tx_event_flags_set (&EventFlagsSerialTestF, (uint32_t)Serial485->Flag_Test485_CkS, TX_OR);
    }

    else//(Serial485->LastEventClass == SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN) //TCP eh cliente
    {
        //Envia para a thread EthWifi
        Serial485->SerialDataOutPayload->header.event_b.class_code = SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_OUT; //symbol da engrenagem
        Serial485->SerialDataOutPayload->header.event_b.code = SF_MESSAGE_EVENT_S485_RX; //Symbol Events da engrenagem

        g_sf_message0.p_api->post(g_sf_message0.p_ctrl,
                                  &Serial485->SerialDataOutPayload->header,//DataIpPayloadMessagePayload->header,
                                  &Serial485->MessageCfg.postCfg,//global que esta no incio desta thread;
                                  &Serial485->MessageCfg.err_postcFG,//global que esta no incio desta thread;
                                  TX_NO_WAIT);
    }

//    Serial485->LastEventClass = SF_MESSAGE_EVENT_CLASS_SERIAL_DATA_IN;
}

//Utilizado no teste da serial.
void SerialReceiQueue(_SERIAL_485 *Serial485)
{
    uint8_t BufferTemp[4];
    static uint16_t Send[2];

    Serial485->Flag_Test485 = true;

    tx_queue_receive(&QueueTest485, &BufferTemp[0], TX_NO_WAIT); //recebe da thread EthWifi

    Send[0] = (uint16_t)(0x0100U | BufferTemp[0]);
    Send[1] = 0x20;

    g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW);

    g_uart0.p_api->write(g_uart0.p_ctrl,
                         (uint8_t *)(&Send[0]),
                         sizeof(Send));
}

bool SerialTransmit(_SERIAL_485 *Serial485)
{
    static uint16_t VSend485[MAX_BUF_TX_485];
    uint16_t    i=0;
    uint8_t Send[MAX_BUF_TX_485];
    ssp_err_t Error;


    memcpy(Send, Serial485->SerialDataInPayload.pointer, Serial485->SerialDataInPayload.num_of_bytes);

    for(i = 0; i < Serial485->SerialDataInPayload.num_of_bytes; i++)
    {
        if((i == 0) && (Send[0] != BROADCAST))
        {
            VSend485[i] = (uint16_t)(0x0100U | Send[i]);
            Serial485->CurrentEquipAddrRequested = Send[0];
        }
        else
        {
            VSend485[i] = Send[i];
        }
    }

    //if(!FlagWaitDevice) //so envia se o canal estiver livre
    {
        //Sinaliza que foi enviado algum dado para instrumetos 485
        g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW);
        FlagWaitDevice = 1U;

        Error = g_uart0.p_api->write(g_uart0.p_ctrl,
                                     (uint8_t *)(&VSend485[0]),
                                     Serial485->SerialDataInPayload.num_of_bytes*2);
        if(Error == SSP_SUCCESS)
        {
            return true;
        }
        else
        {
            FlagWaitDevice=0;
            g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);
            return false;
        }

    }
    return false;
}

_SERIAL_RETURN SerialReceiveMessageTreatment(_SERIAL_485 *Serial485)
{
    uint32_t CheckSum = 0;
    Serial485->Flag_Test485_CkS = false;

    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);
    //verifica se estava aguardando alguma resposta de instrumentos 485
    FlagWaitDevice = 0;

    if(GSerialRecv.IndexPtr == 0) //sinaliza e sai fora
    {
        GSerialRecv.IndexPtr = 0;
        Serial485->SerialRecv.IndexPtr = 0U;

        return SERIAL_POST_VOID;
    }
    // Recebe requisição do sitrad--modo TCP eh client
    if(( Serial485->LastEventClass != SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER ) && (Serial485->LastEventClass != SF_MESSAGE_EVENT_CLASS_S485_DATALOGGER_IN))
    {
        //copia o recheio da resposta do slvae e manda para o sitrad
        memcpy(Serial485->SerialRecv.RxDevice, GSerialRecv.RxDevice, GSerialRecv.IndexPtr );
        Serial485->SerialRecv.IndexPtr = GSerialRecv.IndexPtr;
        GSerialRecv.IndexPtr = 0;
        Serial485->Flag_Test485_CkS = true;
        return SERIAL_POST_TRUE;
    }
    else //modo TCP eh o servidor na rede 485
    {

        if(GSerialRecv.IndexPtr < 2) // eh lixo, pq o datalogger soh pergunta o CMD32
        {
            memcpy(Serial485->SerialRecv.RxDevice, GSerialRecv.RxDevice, GSerialRecv.IndexPtr );
            Serial485->SerialRecv.IndexPtr = GSerialRecv.IndexPtr;
            GSerialRecv.IndexPtr = 0;
            //print_to_console("\r\n * Recebido do Instrumento 485!");
            return SERIAL_NONE;//SERIAL_POST_TRUE; //do not log incorrect answers
        }
        else // eh o CMD32
        {
            //Check for the current instrument
            if(Serial485->CurrentEquipAddrRequested != GSerialRecv.RxDevice[0])
            {
                GSerialRecv.IndexPtr = 0;
                Serial485->SerialRecv.IndexPtr = 0U;
                return SERIAL_NONE;//SERIAL_POST_VOID; //do not log empty answers
            }

            //calculo o checksum
            for(uint16_t i=0; i < (GSerialRecv.IndexPtr - 2); i++)
            {
                CheckSum = CheckSum + GSerialRecv.RxDevice[i];
            }

            //Testa o checksum esta ok.
            if(     ((uint8_t)((CheckSum & 0x0000ff00)>>8) == GSerialRecv.RxDevice[GSerialRecv.IndexPtr-2]) &&
                    ((uint8_t)CheckSum == GSerialRecv.RxDevice[GSerialRecv.IndexPtr-1]) )
            {
                memcpy(Serial485->SerialRecv.RxDevice, GSerialRecv.RxDevice, GSerialRecv.IndexPtr );
                Serial485->SerialRecv.IndexPtr = GSerialRecv.IndexPtr;
                GSerialRecv.IndexPtr = 0;
                Serial485->Flag_Test485_CkS = true;
                return SERIAL_POST_TRUE;
            }
            else
            {
                GSerialRecv.IndexPtr = 0;
                Serial485->SerialRecv.IndexPtr = 0U;
                return SERIAL_NONE;//SERIAL_POST_VOID; //do not log empty answers
            }
        }

    }


    GSerialRecv.IndexPtr = 0U;
    Serial485->SerialRecv.IndexPtr = 0U;

    return SERIAL_NONE;
}

void UserUart0Callback(uart_callback_args_t *p_args)
{
    uint8_t    TimeDiffWait;   // 10,00ms para resetar a recepção
    uint8_t    TimeWait;      //100 ms . //tempo max. para comecar a receber a resposta da serial apos envio

    //Testamos o mode da TCP
    if(TcpMode == TCP_SERVER)
    {
        TimeDiffWait = 100; //100ms
        TimeWait = 150; //150ms
    }
    else
    {
        TimeDiffWait = 10;
        TimeWait = 100;
    }

//    SEGGER_RTT_printf(0, "%x\r\n", p_args->event);
    switch (p_args->event)
    {
        case UART_EVENT_RX_CHAR:
        {
            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW);

            if(GSerialRecv.IndexPtr == DELAY_TURN_OFF_LED) //animacao do led de tx, ou desliga via timeout do timer
            {
                g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO
            }

            g_timer0.p_api->periodSet( g_timer0.p_ctrl, (uint32_t)TimeDiffWait, TIMER_UNIT_PERIOD_MSEC );
            g_timer0.p_api->start(g_timer0.p_ctrl);

            if((GSerialRecv.IndexPtr==0)&&((uint8_t)p_args->data>=BROADCAST)&&(TcpMode == TCP_SERVER))
            {
                p_args->data = 0x00;
                break;
            }
            else if((GSerialRecv.IndexPtr<2)&&((uint8_t)p_args->data==0xFF)&&(TcpMode == TCP_SERVER))
            {
                p_args->data = 0x00;
                break;
            }
            else
            {
                GSerialRecv.RxDevice[GSerialRecv.IndexPtr] = (uint8_t)p_args->data;
                GSerialRecv.IndexPtr++;
            }

        }break;

        case UART_EVENT_TX_COMPLETE:
        {
            g_timer0.p_api->periodSet( g_timer0.p_ctrl, (uint32_t)TimeWait, TIMER_UNIT_PERIOD_MSEC );
            g_timer0.p_api->start(g_timer0.p_ctrl);

        }break;

        default:
            __NOP();
            break;
    }
}

void UserTimer0Callback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&Serial_event_flags, SERIAL_RCV_OK, TX_OR);
        g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO
    }
}

///> Notificacao de recebimento de menssagem de origem Framework
void Serial_Message_Framework_NotifyCallback(TX_QUEUE *Serial_thread_message)
{
    tx_event_flags_set (&Serial_event_flags, SERIAL_MSFW, TX_OR);
    SSP_PARAMETER_NOT_USED(Serial_thread_message);
}

/////> Notificacao de recebimento de menssagem de origem queue
void Serial_MessageQueue_NotifyCallback(TX_QUEUE *SerialMessageQueue)
{
    tx_event_flags_set (&Serial_event_flags, SERIAL_TEST_485, TX_OR);
    SSP_PARAMETER_NOT_USED(SerialMessageQueue);
}
