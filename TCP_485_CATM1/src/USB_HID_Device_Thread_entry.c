#include "USB_HID_Device_Thread.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/ConfDefault.h"

const uint8_t teste_encod[]  = { 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t teste_cod[]      = { 0x11, 0x2d, 0x09, 0x00, 0x07, 0x19, 0x02, 0x13, 0x02, 0x11, 0x39, 0x0f, 0x04, 0x04, 0x04, 0x04};
uint8_t buffer_decod[]   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
const uint8_t aes_key3[]   = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
const uint8_t IVc3[]   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};


extern void g_ux_device_class_register_hid(void);
void USB_Message_Framework_NotifyCallback(TX_QUEUE *USB_HID_Device_Thread_message_queue);

void TimeoutTCP_Open(void);
void StartTimeoutTcp(timer_size_t TimeOut);
void RestartTimeoutTcp(timer_size_t TimeOut);
void StopTimeoutTcp(void);

void Timer3_Open(void);
void StopTimerMain(void);
void StartTimerMain(void);

/********* Menssage Framework *************/
void RtcPost(_USB_CONTROL *USBData);
void SerialPost(_USB_CONTROL *USBData);


UX_SLAVE_DEVICE*    device;
UX_SLAVE_INTERFACE* interface;
UX_SLAVE_CLASS_HID* hid;

UX_SLAVE_CLASS_HID_EVENT hid_event;

_USB_RETURN SendPacketUSB(_USB_CONTROL *USBData, uint16_t len)
{

	if(device->ux_slave_device_state == UX_DEVICE_CONFIGURED)
	{
		/* Get the interface.  We use the first interface, this is a simple device.  */
		interface =  device->ux_slave_device_first_interface;

		/* Form that interface, derive the HID owner.  */
		hid = interface->ux_slave_interface_class_instance;

		tx_thread_sleep(20);

		/* Then insert a key into the keyboard event.  Length is fixed to 8.  */
		hid_event.ux_device_class_hid_event_length = len;

		memcpy(&hid_event.ux_device_class_hid_event_buffer[0], &USBData->BufferUSB.HeaderPayload[0], len);
		//R_BSP_SoftwareDelay(5000U, BSP_DELAY_UNITS_MILLISECONDS);
		ux_device_class_hid_event_set(hid, &hid_event);

		//piscaLed( LED_ST_B,  500U);

		//print485("Enviando pacote via USB.\r\n", LEN("Enviando pacote via USB.\r\n") );

		return USB_SEND;
	}
	else return USB_RELISTEN;
}

/* USB HID Device Thread entry function */
void USB_HID_Device_Thread_entry(void)
{
	static _STATUS_FLAGS StatusFlags;

	tx_thread_sleep(TIME_1S);

	g_ux_device_class_register_hid();
	/* Get the pointer to the device.  */
	device =  &_ux_system_slave->ux_system_slave_device;

	/* reset the HID event structure.  */
	ux_utility_memory_set(&hid_event, 0, sizeof(UX_SLAVE_CLASS_HID_EVENT));

//	g_sce_aes_1.p_api->open(g_sce_aes_1.p_ctrl, g_sce_aes_1.p_cfg); //Open AES_0
//
//	g_sce_aes_1.p_api->encrypt(g_sce_aes_1.p_ctrl, (uint32_t *)aes_key3, (uint32_t *)IVc3, 16, (uint32_t *)teste_encod, (uint32_t *)teste_cod);
//
//	// Close AES driver
//	g_sce_aes_1.p_api->close(g_sce_aes_1.p_ctrl);

//	g_sce_aes_1.p_api->open(g_sce_aes_1.p_ctrl, g_sce_aes_1.p_cfg); //Open AES_0
//
//	g_sce_aes_1.p_api->decrypt(g_sce_aes_1.p_ctrl, (uint32_t *)aes_key2, (uint32_t *)IVc2, MessageLen, (uint32_t *)PMessage, (uint32_t *)DecMessage);//(uint32_t *)tempOut);
//
//    /* Close AES driver */
//    g_sce_aes_1.p_api->close(g_sce_aes_1.p_ctrl);

    //print485("Inicializando USB.\r\n", LEN("Inicializando USB.\r\n") );


	USBControl.FlagFAC = GetUSBFAC(); //Verifica se o modulo esta em fac
	USBOpen(&USBControl, &StatusFlags);

	Timer3_Open();
	TimeoutTCP_Open();

    while (1)
	{
    	/* Is the device configured ? */
		while (device->ux_slave_device_state != UX_DEVICE_CONFIGURED)
		{
			 /* Then wait.  */
			 tx_thread_sleep(10);
		}

		NetStateMachine(&USBControl, &StatusFlags);

		StatusSitrad(&StatusFlags);

//		if(TX_SUCCESS == tx_event_flags_set (&g_main_event_flags, NetStatusModule(&USBControl, &StatusFlags), TX_OR))
//		{
//	            ThreadMonitor_SendKeepAlive(T_MONITOR_USB_THREAD);
//		}
	}
}

//*************************************************************************************************
//* Configures the Timer03 and created semaphore for its uses
//*************************************************************************************************
void Timer3_Open(void)
{
    g_timer3.p_api->open(g_timer3.p_ctrl, g_timer3.p_cfg);

    g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
    g_timer3.p_api->start(g_timer3.p_ctrl);
}

//*************************************************************************************************
//* Configures the Timer04 and created semaphore for its uses
//*************************************************************************************************
void TimeoutTCP_Open(void)
{
    g_timer4.p_api->open(g_timer4.p_ctrl, g_timer4.p_cfg);
}

void StartTimeoutTcp(timer_size_t TimeOut)
{
    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
    g_timer4.p_api->start(g_timer4.p_ctrl);
}

void StartTimerMain(void)
{
    g_timer6Main.p_api->open(g_timer6Main.p_ctrl, g_timer6Main.p_cfg);
    g_timer6Main.p_api->periodSet( g_timer6Main.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
    g_timer6Main.p_api->start(g_timer6Main.p_ctrl);
}

void StopTimerMain(void)
{
    g_timer4.p_api->stop(g_timer6Main.p_ctrl);
}

void RestartTimeoutTcp(timer_size_t TimeOut)
{
    g_timer4.p_api->periodSet( g_timer4.p_ctrl, TimeOut, TIMER_UNIT_PERIOD_SEC );
    g_timer4.p_api->reset(g_timer4.p_ctrl);
}

void StopTimeoutTcp(void)
{
    g_timer4.p_api->stop(g_timer4.p_ctrl);
}

//Callback que sinaliza que o timer3 estourou
void UserTimer3Callback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        //Sinaliza que o evento ocorreu
        tx_semaphore_put(&USBStatus_Semaphore);

        g_timer3.p_api->periodSet( g_timer3.p_ctrl, 1, TIMER_UNIT_PERIOD_SEC );
        g_timer3.p_api->start(g_timer3.p_ctrl);
    }
}

//Callback que sinaliza que o TimeOutTCPCallback (timer4)
void TimeOutTCPCallback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&USB_Flags, FLAG_TIMEOUT_USB, TX_OR);
    }
}

void TimerMainCallback(timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&USB_Flags, FLAG_TIMEOUT_TIMER_MAIN, TX_OR);
    }
}


//Callback de recebimento de Message Framework
void USB_Message_Framework_NotifyCallback(TX_QUEUE *USB_HID_Device_Thread_message_queue)
{
    tx_event_flags_set (&USB_Flags, FLAG_MFRAMEWORK, TX_OR);
    SSP_PARAMETER_NOT_USED(USB_HID_Device_Thread_message_queue);
}
