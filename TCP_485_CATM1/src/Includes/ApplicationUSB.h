/*
 * usb.h
 *
 *  Created on: 27 de mai de 2021
 *      Author: rhenanbezerra
 */

#ifndef INCLUDES_APPLICATIONUSB_H_
#define INCLUDES_APPLICATIONUSB_H_

#include "Includes/TypeDefs.h"
#include "Includes/utility.h"
#include "messages.h"

#define SIZE_PAYLOAD_USB        61    ///> Tamanho maximo de Payload da transmissao usb
#define SIZE_HEADER_USB         3    ///> Tamanho de Header USB (Comando + comprimento do pacote)

#define HEADER_TX_USB 0x12

//Lista de comandos especiais

#define CMD_LOGIN_CONVERSOR			 	0x01
#define CMD_REINICIA_CONVERSOR			0x02
#define CMD_CONF_FABRICA 				0x03
#define CMD_ATUALIZA_CONF               0x04
#define CMD_CMD_REQ_CONF 				0x05
#define CMD_DESC_CONVERSOR 				0x06
#define CMD_LIMPAR_DATALOGGER 			0x07
#define CMD_LEITURA_LOG 				0x08
#define CMD_AJUSTE_DATA_HORA 			0x09
#define	CMD_STATUS_CONVERSOR 			0x0A
#define CMD_ATUALIZACAO_FW 				0x0B
#define CMD_TESTE_FABRICA 				0x0C

typedef union{
    uint8_t      HeaderPayload[SIZE_HEADER_USB + SIZE_PAYLOAD_USB];
    struct{
        uint8_t      Header[SIZE_HEADER_USB];
        uint8_t      Payload[SIZE_PAYLOAD_USB];
    }HP;
}_BUFFER_USB;


typedef enum
{
   USB_NONE,
   // eventos
   USB_SEND,
   USB_REQUESTED,
   USB_REQUESTED_CONFIG,
   USB_REQUESTED_485,
   USB_REQUESTED_CONF,
   USB_RELISTEN,

}_USB_RETURN;

typedef enum
{
    SM_IDLE,
    SM_INIT_ETH,
    SM_WAIT_INTERFACE,
    SM_WAIT_QUEUE,
    SM_WAIT_RESET,
    SM_EVENT_FLAG,
    SM_DHCP,
    SM_STATIC_IP,
    SM_STATUS_IP,
    SM_RESTART_IP,
    SM_VIEW_IP,
    SM_TESTE_CHANGE,

    SM_CHANGE_MODE,
    SM_WAIT_LINK_ETH,

    SM_INIT_WIFI,
    SM_READ_FLASH,
    SM_READ_FLASH_NET_DEFAULT,
    SM_SCAN,
    SM_PROVISION,
    SM_PROVISION_FAIL,

    SM_INIT_AP,

    SM_DONE,

    SM_RESTART_DEVICE,

}_USB_STATES_MACHINE;


//typedef struct
//{
//	_BUFFER_USB BufferUSB;
//	UCHAR BufferTx[SIZE_HEADER_USB + SIZE_PAYLOAD_USB];
//	UCHAR BufferRx[SIZE_HEADER_USB + SIZE_PAYLOAD_USB];
//	UCHAR BufferInfoConf[900];
//
//	UCHAR BufferRxUSBSitradOut[SIZE_HEADER_USB + SIZE_PAYLOAD_USB];
//
//    uint8_t                             TimerBigFrame;           ///> TimerBigFrameTCP
//    uint8_t                             TimerRestartModule;             ///> TimerResetConv ... Contagem para resetar o modulo conversor.
//    uint8_t                             TimerOTACheckCRC;           ///> TimerOTACheckCRC ... Contagem para verificar o CRC da flash.
//    uint8_t                             TimerTimeOutOTA;            ///> TimeOut para atualizar o firmware.
//    uint8_t                             TimerRestoreFactory;        ///> Timer responsavel por determinar a hora de restaurar as confs de fabrica.
//    uint8_t                             TimerConnectSitrad;         ///> Timer para timeout sitrad
//    uint8_t                             TimerInactivity;            ///> Timer para monitoramento de inatividade
//    bool                                FlagOTAFinished;            ///> Utilziado para sinalizar que uma atualização esta  finalizando.
//    bool                                FlagOTARun;                 ///> Utilizado para sinalizar que a atualizacao esta em curso.
//    bool                                FlagFAC;                    ///> Utilizado para determinar se está em fac ou nao.
//    bool                                FlagConnectSitrad;  ///>Utilzado para verificar se o Sitrad está conectado
//    bool                                BigFrame;                ///> Utilizado em USBPayload
//
//    uint8_t                             DateTimeValues[DATE_TIME_SIZE]; ///> Utilizado para atualizar o RTC
//
//    sf_message_header_t                 *Header;
//    _MESSAGE_FRAMEWORK_CFG              MessageCfg;
//    _FLASHEXTERNAL_PAYLOAD              *FlashExternalPaylod;
//	_DATE_TIME_MESSAGE_PAYLOAD 			*DateTimeMessagePayload;    ///> Messagem Payload - Atualiza o RTC Envio
//	_SERIAL_DATA_IN_PAYLOAD             *SerialDataInPayload;       ///> Messagem Payload - Serial 485 Recebimento
//    _SERIAL_DATA_OUT_PAYLOAD            SerialDataOutPayload;       ///> Messagem Payload - Serial 485 Envio
//}_USB_CONTROL;


typedef struct
{
	_BUFFER_USB                         BufferUSB;
	UCHAR BufferInfoConf[900];

    //sf_wifi_provisioning_t              * SfProvisionWifi;

    net_input_cfg_t                     NetCfg;

    _CONF_NET_BASIC                     ConfNetBasic;    // Contem TipoInterface, AP ou Wifi, DHCP ou Estatico.
    //_NET_WIFI_CONF                      NetWifiConf;    //Configuracoes Wifi
    //_NET_WIFI_CONF_AP                   NetWifiConfAp;  //Configuracoes Wifi Modo AP

    uint8_t                             NumNets;
    bool                                FlagConnect;  ///>Utilzado para verificar se ja conectou a rede- status
    bool                                FlagConnectSitrad;  ///>Utilzado para verificar se o Sitrad está conectado

    uint8_t                             DateTimeValues[DATE_TIME_SIZE]; ///> Utilizado para atualizar o RTC

    _IPSTRUCT                           IpStruct;
    //_NET_STATIC_MODE                    IpStaticMode;

    //NX_PACKET                           * PacketUdp;
    //NX_PACKET                           * PacketUdpSendRx;
    //NX_PACKET                           * PacketReceiveTCP;
    //NX_PACKET                           * PacketReceiveTCPConf;

    //NX_TCP_SOCKET                       TcpSocket;
    //NX_TCP_SOCKET                       TcpSocketConf;
    //NX_UDP_SOCKET                       * UdpSocket;

    //NX_IP                               * NxIP;
    //NX_DHCP                             * NxDHCP;

    uint32_t                            IpHttpRemote;

    uint32_t                            ActualFlags;
    //_PACKET_UDP                         PacketUdpSend;

    //_BUFFER_TCP                         PacketTcpSend;

    //uint8_t                             *BufferTcpRX;           ///Utilzado na recepcao TPCPayload

    _USB_STATES_MACHINE            USBStatesMachine;  ///> Contem o estado atual da maquina de estados.

    sf_message_header_t                 *Header;
    _MESSAGE_FRAMEWORK_CFG              MessageCfg; //sk

    //mensagem recebida via framework
    _DATA_IP_PAYLOAD_MESSAGE_PAYLOAD    *DataIpPayload;             ///> ???
    _DATA_TX_PAYLOAD_MESSAGE_PAYLOAD    DataTxPayloadMessagePayload;///> ???

    _DATE_TIME_MESSAGE_PAYLOAD          *DateTimeMessagePayload;    ///> Messagem Payload - Atualiza o RTC Envio
    _SERIAL_DATA_IN_PAYLOAD             *SerialDataInPayload;       ///> Messagem Payload - Serial 485 Recebimento
    _SERIAL_DATA_OUT_PAYLOAD            SerialDataOutPayload;       ///> Messagem Payload - Serial 485 Envio

    _FLASHEXTERNAL_PAYLOAD              *FlashExternalPaylod;

    uint8_t                             WifiRssid;                  ///> Contem a intensidade da rede wifi

    uint8_t                             TimerBigFrame;           ///> TimerBigFrameTCP
    uint8_t                             TimerRestartModule;             ///> TimerResetConv ... Contagem para resetar o modulo conversor.
    uint8_t                             TimerOTACheckCRC;           ///> TimerOTACheckCRC ... Contagem para verificar o CRC da flash.
    uint8_t                             TimerTimeOutOTA;            ///> TimeOut para atualizar o firmware.
    uint8_t                             TimerRestoreFactory;        ///> Timer responsavel por determinar a hora de restaurar as confs de fabrica.
    uint8_t                             TimerConnectSitrad;         ///> Timer para timeout sitrad
    uint8_t                             TimerInactivity;            ///> Timer para monitoramento de inatividade

    bool                                ValidAccess;                ///> Utilizado em TCP, valida o acesso aos dados de reposta
    bool                                ValidAccessConf;            ///> Utilizado em TCP, valida o acesso a porta configuracao
    bool                                BigFrame;                ///> Utilizado em TCPPayload
    bool                                FlagOTAFinished;            ///> Utilziado para sinalizar que uma atualização esta  finalizando.
    bool                                FlagOTARun;                 ///> Utilizado para sinalizar que a atualizacao esta em curso.
    bool                                FlagFAC;                    ///> Utilizado para determinar se está em fac ou nao.
}_USB_CONTROL;

//typedef enum
//{
//
//    FAC_TEST_START          = 0,
//
//    FAC_TEST_SUPER_CAP_ON,  // 1
//    FAC_TEST_SUPER_CAP_OFF, // 2
//    FAC_TEST_QSPI,          // 3
//
//    FAC_TEST_LEDS,          // 4
//    FAC_TEST_485,           // 5
//    FAC_TEST_WIFI,          // 6
//    FAC_TEST_MAC,           // 7
//    FAC_TEST_KEY,           // 8
//
//    FAC_TEST_END,
//
//    FAC_TEST_DO_FAC         = 0x66, //Esse id coloca o modulo em modo FAC.
//    FAC_TEST_WAIT_STEP,     // deve ser o penultimo
//    FAC_TEST_NONE           = 0xFF,
//
//}_FACTORY_TEST_STEP;

typedef struct
{
    unsigned    FlagStartUdpTcp:1,
                FlagSitradConnected:1,
                StateSitrad:1,
                FlagNetConnected:1,
                FlagNetScan:1,
                FlagNumAttempts:1,
                FlagFree01:1,
                FlagFree02:1,
                FlagFree03:1,

                FlagErrorPassWrd:1,
                FlagErrorIp:1,
                FlagErrorInvalidStaticIp:1,
                FlagErrorFree03:1,
                FlagErrorFree04:1,
                FlagErrorFree05:1,
                FlagErrorFree06:1,
                FlagErrorFree07:1;
}_STATUS_FLAGS;

typedef enum
{
    FLAGS_NONE               = 0,
   // eventos
    FLAG_DETECTED_ACTIVITY   = 0x00000001,
    FLAG_USB_RECEIVED        = 0x00000002,
    FLAG_USB_RECEIVED_CONF   = 0x00000004,
    FLAG_USB_DELETE          = 0x00000008,

    FLAG_USB_CONNECT         = 0x00000010,
    FLAG_USB_DISCONNECT      = 0x00000020,
    FLAG_USB_CONNECT_CONF    = 0x00000040,
    FLAG_USB_DISCONNECT_CONF = 0x00000080,

    FLAG_RTC_SEND            = 0x00000100,
    FLAG_FREE7               = 0x00000200,
    FLAG_FREE8               = 0x00000400,
    FLAG_QUEUE_CONSOLE       = 0x00000800,

    FLAG_MFRAMEWORK          = 0x00001000,
    FLAG_TIMEOUT_USB         = 0x00002000,
    FLAG_GET_STATUS          = 0x00004000,
    FLAG_TIMEOUT_TIMER_MAIN  = 0x00008000,

    FLAG_USB_CONNECTED      = 0x00010000,
    FLAG_USB_DISCONNECTED   = 0x00020000,
    FLAG_STATUS_USB         = 0x000F0000,

    FLAG_USB_ALL          = 0x000FFFFF,

}USB_FLAGS;

_USB_CONTROL USBControl;

_USB_RETURN SendPacketUSB(_USB_CONTROL *USBData, uint16_t len);

#endif /* INCLUDES_APPLICATIONUSB_H_ */
