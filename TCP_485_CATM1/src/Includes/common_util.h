/*
 * util.h
 *
 *  Created on: Dec 7, 2017
 *      Author: Rajkumar.Thiagarajan
 */

#ifndef UTIL_H_
#define UTIL_H_

#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"

#define APP_ERR_TRAP(a)             if(a) {__asm("BKPT #0\n");} /* trap the error location */
#define ARRAY_SIZE(x)               (sizeof(x) / sizeof(x[0]))
#define BACK_SPACE_CHAR_CTRL_H          (0x08)
#define BACK_SPACE_CHAR_CTRL_QUESTION   (0x7F)
#define CARRIAGE_RETURN                 (0x0D)

#define TIME_OUT                        (0xFFF)

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

void PrintIpv4Addr(ULONG Address, char *Str, size_t Len);
void PrintMacAddr(ULONG AddressM, ULONG AddressL, char *Str, size_t Len);
void GetUserInput(char *str);
char GetChar(UINT timeout);
void PutChar(const uint8_t *data, size_t dataLen);
void print_float(char *buffer, size_t buflen, double value);
int32_t days(int, int, int, int, int, int);
uint32_t month(int, int);
bool isleapyear(int);
int char2num(UCHAR c);
int32_t convertTimeToEpoch(const UCHAR* dateTime);
void led_on_off(ioport_port_pin_t pin, char *state);
char *ftoa(float f, char *buf);
#endif /* UTIL_H_ */
