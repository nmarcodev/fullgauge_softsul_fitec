/*
 * ApplicationAP.h
 *
 *  Created on: 24 de jul de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONAP_H_
#define INCLUDES_APPLICATIONAP_H_

#include "Includes/AP_WiFI_Thread_Entry.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/Eth_Wifi_Thread_Entry.h"
#include "Eth_Wifi_Thread.h"

#define SERVER_WIFI_CHANNEL             11
#define SERVER_WIFI_SSID                NAME_MODEL//"TCP-485 WIFI LOG"
#define SERVER_WIFI_KEY                 "admin123"

#define SERVER_IP                       IP_ADDRESS(192, 168, 2, 1)
#define SERVER_SUBNET_MASK              IP_ADDRESS(255, 255, 255, 0)
#define SERVER_INTERFACE_INDEX          0
#define SERVER_START_IP_ADDRESS_LIST    IP_ADDRESS(192, 168, 2, 10)
#define SERVER_END_IP_ADDRESS_LIST      IP_ADDRESS(192, 168, 2, 15)

//extern void NetWifiDhcpServerStart(NX_DHCP_SERVER * p_server);
extern void AP_UdpStart(_AP_SERVER_CONTROL *APData);
extern uint8_t AP_UdpEvent(_AP_SERVER_CONTROL *APData);
extern void AP_UdpSend(_AP_SERVER_CONTROL *APData);
extern void AP_TcpStart(_AP_SERVER_CONTROL *APData);
extern uint8_t AP_TcpAcceptConnect(_AP_SERVER_CONTROL *APData);
extern void AP_TcpDisconnect(_AP_SERVER_CONTROL *APData);
extern _MAIN_MANAGER_FLAGS GetStatusAPWifi(void);
extern void SetIpStatus(_ETH_WIFI_CONTROL *EthWifiControl);
extern void GetNameDefaultAP(_WIFI_AP_MODE_CONF *WifiApModeConf);
extern _FG_ERR GetMACValueWF(uint8_t *MacAddressWF);
extern void SetMACValueWF(uint8_t *MacAddressWF);

#endif /* INCLUDES_APPLICATIONAP_H_ */
