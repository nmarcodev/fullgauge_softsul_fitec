#ifndef __TYPEDEFS_H
#define __TYPEDEFS_H

#include "common_data.h" // include obrigatório

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include <time.h>

//*************************************************************************************************
// Defines

#define     STRINGIZE2(s) #s
#define     STRINGIZE(s) STRINGIZE2(s)

// versão de firmware
#define     FIRMWARE_VERSION                99
#define     FIRMWARE_RELEASE                0
//#define     FIRMWARE_PATCH                  0

#if defined(BSP_MCU_GROUP_S5D5)
#define     MCU_MODEL                       5
#elif defined(BSP_MCU_GROUP_S5D9)
#define     MCU_MODEL                       6
#endif


#define     PRODUCT_MODEL 0xEF //sk

#define     FIRST_VAL_UDP                   0xBB
#define     LAST_VAL_UDP                    0x55

#define     DEVICE_ID_SPI_CS_FLASH          0

#define     BAUD_RATE_RS485                 28800
#define     SIZE_USART_DATA_RECEPTION       560

#define     QTD_SILENCE_BYTES_DEFAULT       125
#define     QTD_SILENCE_BYTES_FULL          500
#define     QTD_SILENCE_BYTES_RS_485        QTD_SILENCE_BYTES_DEFAULT

#define     TIMER_OUT_WAIT_RX_RS485         100     // tempo que espera resposta da RS485
#define     QTD_INSTRUMENTS                 247

#ifdef FIRMWARE_PATCH
#define     VERSION                         STRINGIZE(FIRMWARE_VERSION) "." STRINGIZE(FIRMWARE_RELEASE) "." STRINGIZE(FIRMWARE_PATCH)
#else
#define     VERSION                         STRINGIZE(FIRMWARE_VERSION) "." STRINGIZE(FIRMWARE_RELEASE)
#endif
#define     MODEL                           "Wifi Log"
#define     NAME_MODEL                      "TCP-485 WIFI Log" //16 chars

#define     TIME_OUT_DEFAULT_WEB            600     // time out em segundos caso tranque em modo dados porem na porta TCP
#define     TIME_SILENCE_SITRAD             60      // Tempo max. de silencio na porta do sitrad depois de uma conexao
#define     TIME_SILENCE_INACTIVITY         70      // Tempo de inativididade

#define     SIZE_FRAG_PAYLOAD_TCP           4096 //pool size
#define     SIZE_PACKET_UDP                 255     ///> Tamanho maximo do pacote UDP para enviar.

#define     BUFFER_SIZE_RX_WINDS            40
#define     RANDOM_SIZE                     2U
#define     CRC_SIZE                        2U
#define     HEADER_CONF_SIZE                2U      // Tamanho do size para o sitrad conf
#define     HEADER_ID_SIZE                  2 //TAMANHO DO ID + CAMPO DO TAMANHO DO COMANDO
#define     HEADER_SIZE                     3U

#define     TC900E_LOG                      72
#define     MT512E_LOG                      73

#define     HEADER_TX_TCP                   0x12     ///> Cabecalho padrao da resposta TCP
#define     COMMAND_32                      0x20

#define     PORT_UDP_SITRAD                 5000    ///> Porta UDP default
#define     PORT_TCP_SITRAD                 4000    ///> Porta TCP Comunicacao  default
#define     PORT_TCP_SITRAD_CONF            5005    ///> Porta TCP Configuracao default

#define     TIME_FOR_FIRST_LOG                          30      ///> Tempo para iniciar a funcao log
#define     TIME_FOR_FIRST_LOG_AFTER_SITRAD_DISCONNECT  1      ///> Tempo para iniciar a funcao log


#define     RECORD_DATALOGGER_EVENTS        (0)

//*************************************************************************************************
// Enumerations
// definições de temporização levando em conta o TICK do RTOS de 10ms
typedef enum
{
    ZERO_MS_ONE_SHOT_TIMER  =  0,  //!< ZERO_MS_ONE_SHOT_TIMER
    TIME_10MS               =  1,  //!< TIME_10MS
    TIME_20MS               =  2,  //!< TIME_20MS
    TIME_40MS               =  4,  //!< TIME_40MS
    TIME_50MS               =  5,  //!< TIME_50MS
    TIME_100MS              =  10, //!< TIME_100MS
    TIME_200MS              =  20, //!< TIME_200MS
    TIME_250MS              =  25, //!< TIME_250MS
    TIME_300MS              =  30, //!< TIME_300MS
    TIME_500MS              =  50, //!< TIME_500MS
    TIME_750MS              =  75, //!< TIME_750MS
    TIME_1S                 = 100, //!< TIME_1S
    TIME_1S5                = 150, //!< TIME_1S5
    TIME_2S                 = 200, //!< TIME_2S
    TIME_3S                 = 300, //!< TIME_3S
    TIME_5S                 = 500, //!< TIME_5S
    TIME_10S                = 1000,//!< TIME_10S
    TIME_15S                = 1500,//!< TIME_15S
} _TIME_PARAM;


//*************************************************************************************************
// Enumerations
typedef enum
{
    START_OTA               = 0xB0,
    END_OTA                 = 0xB1,
    REQUEST_PAG             = 0xB2,
    CONNECTED_SITRAD        = 0xB3,
    TEST_START              = 0xB4,

#if RECORD_DATALOGGER_EVENTS
    REGISTER_EVENT          = 0xA0,
#endif

} _SET_EVENT;

#if RECORD_DATALOGGER_EVENTS
typedef enum
{
    DATALOGGER_EVENT_RESET,
    DATALOGGER_EVENT_SITRAD_STATUS_CHANGE,
    DATALOGGER_EVENT_CONFIG_STATUS_CHANGE,
    DATALOGGER_EVENT_FOTA_STATUS_CHANGE,
    DATALOGGER_EVENT_BUTTON,
    DATALOGGER_EVENT_DATALOGGER_ERASED,
    DATALOGGER_EVENT_TCP_COMMAND_RECEIVED,
    DATALOGGER_EVENT_WIFI_SCAN_RESULT,
    DATALOGGER_EVENT_WIFI_CONNECTION_STATUS_CHANGE,
    DATALOGGER_EVENT_WIFI_AP_CLIENT_STATUS_CHANGE,
    DATALOGGER_EVENT_NET_INIT,
    DATALOGGER_EVENT_NET_RESTART,
    DATALOGGER_EVENT_TCP_ERROR,
    DATALOGGER_EVENT_FLASH_ERROR,
    DATALOGGER_EVENT_WDT_TRIGGER,
    DATALOGGER_EVENT_IP_ERROR,
    DATALOGGER_EVENT_WIFI_INIT,
    DATALOGGER_EVENT_SOCKET_SHUTDOWN,
    DATALOGGER_EVENT_ETHERNET_CONNECTION_STATUS_CHANGE,
}_DATALOGGER_EVENTS;
#endif

typedef struct
{
    int16_t Min;
    int16_t Max;
} _PARAM_LIMITS;

typedef enum
{
	STATE_OFF			= 0,
	STATE_ON			= 1,	
} _STATE;

//*************************************************************************************************
// Defines  //MCU Model S1 = 1, S3 = 3, 4= S5D5 500k, S5D5 1M= 5, 6= S5D9, S7 = 7

typedef enum
{
    MCU_S1_FAMILY = 1,
    MCU_S3_FAMILY = 3,
    MCU_S5D5_FAMILY = 5,
    MCU_S5D9_FAMILY = 6,
    MCU_S7_FAMILY = 7,

} _MCU_MODELS;



//*************************************************************************************************
// Typedefs


typedef	int8_t				    S8;
typedef	uint8_t			        U8;

typedef	int16_t			        S16;
typedef	uint16_t			    U16;

typedef	int32_t			    	S32;
typedef	uint32_t	    		U32;

typedef int64_t			        S64;
typedef uint64_t		        U64;

typedef struct
{
    unsigned  bit0:1, bit1:1, bit2:1, bit3:1, bit4:1, bit5:1, bit6:1, bit7:1, bit8:1, bit9:1, bit10:1,
              bit11:1, bit12:1, bit13:1, bit14:1, bit15:1, bit16:1, bit17:1, bit18:1, bit19:1, bit20:1,
              bit21:1, bit22:1, bit23:1, bit24:1, bit25:1, bit26:1, bit27:1, bit28:1, bit29:1, bit30:1, bit31:1;
}_BITS32;

typedef struct
{
    unsigned  bit0:1, bit1:1, bit2:1, bit3:1, bit4:1, bit5:1, bit6:1, bit7:1, bit8:1;
}_BITS8;

typedef union
{
    U32      u32data;
    U16      u16data[2];
    _BITS32  bdata;
}_U32DATA;

typedef union
{
    U8      u8data;
    _BITS8  bdata;

}_U8DATA;

typedef union{
    struct {
        uint8_t BytHi;                 ///<MSB do valor de 16 bits
        uint8_t BytLo;                 ///<LSB do valor de 16 bits
    }BigEndian;
    struct {
        uint8_t BytLo;                 ///<MSB do valor de 16 bits
        uint8_t BytHi;                 ///<LSB do valor de 16 bits
    }LittleEndian;
    uint16_t Word16;                    ///<Valor de 16 bits
    uint8_t Bytes[2];
}ConvBytesToInt;

typedef union{
    int TwoBytes;
    struct {
        uint8_t BytLo;                 ///<MSB do valor de 16 bits
        uint8_t BytHi;                 ///<LSB do valor de 16 bits
    }BYTEHiLo;
}_Conv16ToBytes;

typedef union{
    uint32_t LongInt;                    ///<Valor de 16 bits
    uint8_t Bytes[4];
}ConvLongToBytes;

typedef struct
{
    sf_message_acquire_cfg_t acquireCfg;
    sf_message_post_cfg_t    postCfg;
    sf_message_post_err_t    err_postcFG;

}_MESSAGE_FRAMEWORK_CFG;


typedef struct
{
    uint8_t         QntDt;              //1
    uint8_t         *PayloadSerial;     //1
}_SERIAL_485_DATA; //total 4 Bytes

//*************************************************************************************************
//*
//*
//*************************************************************************************************

typedef enum
{
    OFF = 0,
    ON  = 1,
    XXX,
}_ON_OFF;


//*************************************************************************************************
//
typedef enum
{
    // OK
    FG_SUCCESS              = 0,
    FG_ERROR                = 1,
    FG_NOTHING_TO_BE_DONE   = 2,
    FG_INVALID_PARAM        = 3,

    // usado para entradas e saídas
    FG_INVALID_ADDRESS      = 10,
    FG_INVALID_NUMBER       = 11,
    FG_INVALID_TYPE         = 12,
    FG_INVALID_QUANTITY     = 13,

    // usado para parametros
    FG_OUT_OF_MIN_RANGE     = 20,
    FG_OUT_OF_MAX_RANGE     = 21,
    FG_INVALID_OFFSET       = 22,
    FG_INVALID_DATA_TYPE    = 23,
    FG_INVALID_GROUP        = 24,

    // usado para timers
    FG_TIMER_ACTIVE         = 30,
    FG_TIMER_EXPIRED        = 31,

    // usado para USB
    FG_USB_IN_PROGRESS      = 40,
    FG_USB_NO_SPACE         = 41,
    FG_USB_NO_MEDIA         = 42,
    FG_USB_FILE_NOT_FOUND   = 43,
    FG_USB_INVALID_FILE     = 44,
    FG_USB_INVALID_PATH     = 45,
    FG_USB_INVALID_PARAM    = 46,
    FG_USB_LOCKED_PARAM     = 47,
    FG_USB_WRITE_PROTECTED  = 48,


    // usado no módulo IOManager
    FG_NO_UPDATE_NEEDED     = 50,
    FG_UPDATE_NEEDED        = 51,


    // usado para habilitação de edição de parâmetro
    FG_LOCKED               = 60,
    FG_UNLOCKED             = 61,

    // usado para relógio
    FG_ECLO                 = 70,

    // Usado para a serial
    FG_TIME_OUT             = 71,

    //Usado para Wifi
    FG_WIFI_ERRO_SCAN       = 72,
    FG_WIFI_NO_NETS_SAVE    = 73,
    FG_WIFI_NO_NAME_FLASH   = 74,
    FG_WIFI_NO_NETS_PROV    = 75,

    //Usado no Datalogger
    FG_SUCCESS_485          = 76,

} _FG_ERR;

typedef enum
{
    VALIDA_ACESSO               = 1,
    REINICIA_CONV               = 2,
    RESTORE_FACTORY             = 3,
    ATUALIZA_INFO_CONF           = 4,
    REQUISITA_INFO_CONF         = 5,
    DESCONECTA_CONV             = 6,
    ERASE_DATALOGGER            = 7,
    READ_PG_DATALOGGER          = 8,
    WRITE_DATE_HOUR             = 9,
    REQUISITA_STATUS            = 10,//0x0A
    UPDATE_FIRMWARE             = 11,//0x0B
    FACTORY_TEST                = 12,//0x0C
}_ESPECIAL_COMMANDS;


typedef enum
{
    DATA_RS485_SPACE    = 0,
    DATA_RS485_MARK     = 1,
	DATA_USB_MODULE     = 2,
    DATA_WIFI_MODULE    = 3,
    DATA_UNKNOWN        = 4,
}_ID_COMMANDS;

#endif
