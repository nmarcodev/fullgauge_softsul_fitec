/*
 * Define_IOs.h
 *
 *  Created on: 24 de jul de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_DEFINE_IOS_H_
#define INCLUDES_DEFINE_IOS_H_

#define PUSH_B          IOPORT_PORT_03_PIN_04
#define PUSH_B2         IOPORT_PORT_03_PIN_05  //Vai morrer no proximo release da placa

#define UART0_TX_DIR    IOPORT_PORT_01_PIN_14

#define LED_W           //IOPORT_PORT_06_PIN_02   ///> Led de trabalho WORK

#define LED_RX          IOPORT_PORT_00_PIN_15
#define LED_TX          IOPORT_PORT_00_PIN_14

#define LED_ON_R        IOPORT_PORT_05_PIN_00
#define LED_ON_G        IOPORT_PORT_05_PIN_01
#define LED_ON_B        IOPORT_PORT_05_PIN_02

#define LED_ST_R        IOPORT_PORT_05_PIN_03
#define LED_ST_G        IOPORT_PORT_05_PIN_04
#define LED_ST_B        IOPORT_PORT_05_PIN_08

#define LED_ETH         IOPORT_PORT_04_PIN_14

#define RESET_WIFI      IOPORT_PORT_01_PIN_15

#define RTC_FAC_PIN     IOPORT_PORT_01_PIN_02

#endif /* INCLUDES_DEFINE_IOS_H_ */
