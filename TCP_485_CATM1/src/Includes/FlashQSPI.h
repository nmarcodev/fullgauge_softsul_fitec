/*
 * FlashQSPI.h
 *
 *  Created on: 13 de nov de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_FLASHQSPI_H_
#define INCLUDES_FLASHQSPI_H_

#include "Includes/DataLoggerFlash.h"
#include "Includes/ExternalFlash.h"
#include "flash_loader/app_info.h"
#include "Includes/internal_flash.h"

#define OFFSET_EXT_INFO_ADDRESS                 0x300
#define APP_EXT_INFO_ADDRESS                    (QSPI_DEVICE_ADDRESS + OFFSET_EXT_INFO_ADDRESS)
#define APP_END                                 0x080000//0x100000
#define APP_RESET                               0x010000
#define APP_EXT_END                             (QSPI_DEVICE_ADDRESS + APP_END)// - APP_RESET)

extern uint32_t SaveFirmwareQSPI(_FLASHEXTERNAL_PAYLOAD *FlashExternalPayload, uint32_t AddsFirmware);
extern bool FlashWritePageData(uint32_t Address, uint16_t Lenght, uint8_t *DataFlash);
extern void FlashReadFirmwareData(uint32_t Address, uint16_t QntPag, uint8_t *DataFlash);
extern void ClearSectorFlashQSPIFirm(uint16_t Sector);
extern void ClearFirmwareFlashQSPI(void);
extern uint8_t CheckCRCFirmwareQSPI(void);

#endif /* INCLUDES_FLASHQSPI_H_ */
