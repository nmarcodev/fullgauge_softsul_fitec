/*
 * ApplicationUdp.h
 *
 *  Created on: 16 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONUDP_H_
#define INCLUDES_APPLICATIONUDP_H_

#include "Includes/Eth_Wifi_Thread_Entry.h"

extern void UdpStart(_ETH_WIFI_CONTROL *EthWifiData);
extern void UdpUnbind(_ETH_WIFI_CONTROL *EthWifiData);
extern void UdpRelease(_ETH_WIFI_CONTROL *EthWifiData);
extern void UdpFinish(_ETH_WIFI_CONTROL *EthWifiData);

extern void ReadPacketUdpCuston(_UDP_CONF *ConfSitradUdp);
extern void XatPacketUdp(_PACKET_UDP *PacketUdp, _IPSTRUCT *PonterIpStruct);

extern void GetPacketUdpCuston(_UDP_CONF *ConfSitradUdp);
extern void SetPacketUdpCuston(_UDP_CONF *ConfSitradUdp);

extern void GetUdpIp(_IPSTRUCT *ConfSitradIPUdp);
extern void SetUdpIp(_IPSTRUCT *ConfSitradIPUdp);

//extern void UdpSendOnDemand(NX_UDP_SOCKET *UdpX);

#endif /* INCLUDES_APPLICATIONUDP_H_ */
