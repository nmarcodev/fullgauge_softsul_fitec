/*
 * ApplicationIp.h
 *
 *  Created on: 7 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONIP_H_
#define INCLUDES_APPLICATIONIP_H_

#include "Includes/ApplicationUSB.h"

extern void USBOpen(_USB_CONTROL *USBData, _STATUS_FLAGS *StatusFlagsX);
//extern uint8_t EthWifiWaitForLink(_ETH_WIFI_CONTROL *EthWifiData);
//extern uint8_t EthWifiStartDhcpClient (_ETH_WIFI_CONTROL *EthWifiData);
//extern uint8_t EthWifiStartStaticIP(NX_IP * NxIP, ULONG Address, ULONG Mask, ULONG Gtw, ULONG Dns);
//extern uint8_t EthWifiGetStatusIp(_ETH_WIFI_CONTROL *EthWifiData);
//extern UINT NetStop (_ETH_WIFI_CONTROL *EthWifiData);
//extern void set_mac_address (nx_mac_address_t*_pMacAddress);
//extern uint8_t GetStatusLinkEth(void);
//extern void SetStatusLinkEth(uint8_t StatusEth);
//extern void EthWifiCheckConnectivity(_ETH_WIFI_CONTROL *EthWifiControl, _STATUS_FLAGS *StatusFlagsX);

#endif /* INCLUDES_APPLICATIONIP_H_ */
