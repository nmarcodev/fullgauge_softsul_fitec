#ifndef __APLICATIONTIMER_H
#define __APLICATIONTIMER_H

#include "Includes/TypeDefs.h"

#include "common_data.h" // include obrigatório

typedef struct
{
    TX_TIMER timerControlBlock;
    CHAR *name;
    UINT active;
    ULONG remaining_ticks;
    ULONG reschedule_ticks;
    TX_TIMER *next_timer;
    UINT status;

}_FG_TIMER;


extern _FG_ERR  AplicationTimer_TimerVerify(_FG_TIMER* timer);

#endif
