/*
 * internal_flash.h
 *
 *  Created on: Dec 13, 2017
 *      Author: Rajkumar.Thiagarajan
 */

#ifndef INTERNAL_FLASH_H_
#define INTERNAL_FLASH_H_

#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#include "Includes/TypeDefs.h"

/* Provision info storage address ( internal flash ) */
#define FLASH_BLOCK_SIZE                (64)
#define DF_4K_OFFSET                    (0x00001000)
#define INTERNAL_DF_BASE_ADDR           (0x40100000)

#define CERT_KEYS_BASE_ADDR             (0x40104000)

#define CONF_NET_BASIC_ADDR             INTERNAL_DF_BASE_ADDR       ///> End. Base da flash Interna:    0x40100000
#define CONF_NET_BASIC_LEN              FLASH_BLOCK_SIZE            ///> 64 = 0x40 bytes                0x40100040

//por definicao, nesta etapa do projeto, teremos apenas um campo de IP_Static, manter o WIFI como o padrao para as duas interfaces
#define CONF_WIFI_STATIC_IP_ADDR        (CONF_NET_BASIC_ADDR + CONF_NET_BASIC_LEN)     ///> 0x40100040
#define CONF_WIFI_STATIC_IP_LEN         (FLASH_BLOCK_SIZE * 2)                        ///> 0x80 + 0x40100040 = 0x401000C0

#define CONF_ETH_STATIC_IP_ADDR         (CONF_WIFI_STATIC_IP_ADDR + CONF_WIFI_STATIC_IP_LEN)  ///> 0x401000C0
#define CONF_ETH_STATIC_IP_LEN          (FLASH_BLOCK_SIZE * 2)                        ///> 0x80 + 0x401000C0 = 0x40100140

#define RESERVADO_ADDR                  (CONF_ETH_STATIC_IP_ADDR + CONF_ETH_STATIC_IP_LEN)  ///> 0x40100140
#define RESERVADO_LEN                   (FLASH_BLOCK_SIZE * 6)                     ///> 0x180 + 0x40100140 = 0x401002C0

#define CONF_WIFI_AP_ADDR               (RESERVADO_ADDR + RESERVADO_LEN)  ///> 0x401002C0
#define CONF_WIFI_AP_LEN                (FLASH_BLOCK_SIZE * 5)          ///> 0x140 + 0x401002C0 = 0x40100400

#define CONF_WIFI_CLIENT_ADDR           (CONF_WIFI_AP_ADDR + CONF_WIFI_AP_LEN)    ///> 0x40100400
#define CONF_WIFI_CLIENT_LEN            (FLASH_BLOCK_SIZE * 5)                  ///> 0x140 + 0x40100400 = 0x40100540

#define CONF_ETH_ADDR                   (CONF_WIFI_CLIENT_ADDR + CONF_WIFI_CLIENT_LEN) ///> 0x40100540
#define CONF_ETH_LEN                    (FLASH_BLOCK_SIZE * 5)                    ///> 0x140 + 0x40100540 = 0x40100680

#define CONF_UDP_SITRAD_ADDR            (CONF_ETH_ADDR + CONF_ETH_LEN)        ///> 0x40100680
#define CONF_UDP_SITRAD_LEN             (FLASH_BLOCK_SIZE * 4)                ///> 0x100 + 0x40100680 = 0x40100780

///segmentacao virtual...
#define AUTO_PROV_INFO_STORAGE_ADDR     (INTERNAL_DF_BASE_ADDR + (3*DF_4K_OFFSET)) //0x40103000

#define ALL_FLAGS_256                   (AUTO_PROV_INFO_STORAGE_ADDR)                //simula eeprom girard (ALL_FLAGS_EEPROM 256)
#define ALL_FLAGS_256_LEN               (FLASH_BLOCK_SIZE * 4) ///< 256 bytes

#define DATALOGGER_INSTR_LIST           ( ALL_FLAGS_256 + ALL_FLAGS_256_LEN)       //simula eeprom girard (DATALOGGER_INSTR_LIST 256)
#define DATALOGGER_INSTR_LIST_LEN       (FLASH_BLOCK_SIZE * 4) ///< 256 bytes

//**************************reservado para Flags de bootloader: 0x40107F00 - 0x40107F40*******************************
#define FLASH_DF_BOOTLOADER_CODE_ADDRRESS           0x40107F00 ///< Endereco da flag para atualizacao de firmware
#define FLASH_BOOTLOADER_CODE_ADDR_LEN              FLASH_BLOCK_SIZE ///> 0x40 + 0x40107F00 = 0x40107F40

#define BOOTLOADER_VALID_CODE                       0xA5A55A5A ///< codigo para atualizacao de firmware

#define NUM_OF_BLOCKS(x)                ((x) / FLASH_BLOCK_SIZE)

/* Flash operations event ids */
#define ERASE_COMPLETE_EVT          1
#define WRITE_COMPLETE_EVT          2
#define ERR_FAILURE_EVT             4

#define FLASH_NUMBER_SUPER_MAGIC    0xA0
#define FLASH_NUMBER_MAGIC          0xA5
#define FLASH_NUMBER_CUSTON         0xA7
#define FLASH_NUMBER_CONF_BASIC     0xA8
#define FLASH_NUMBER_DEFAULT        0xA9

/* Info type IDs */
typedef enum e_int_storage_type {
    CONF_NET_BASIC,                 ///< CONFIG BASICA DA REDE (INTERFACE, AP/WIFI, DHCP ou STATIC)
    CONF_WIFI_STATIC_IP,            ///< CONFIG STATIC IP/MASK/GTW/DNS WIFI
    CONF_ETH_STATIC_IP,             ///< CONFIG STATIC IP/MASK/GTW/DNS ETHERNET
    CONF_WIFI_AP,                   ///< CONFIGURACOES PARA O MODO AP
    CONF_WIFI_CLIENT,               ///< CONFIGURACOES PARA MODO CLIENT
    CONF_ETH,                       ///< CONF ETHERNET
    CONF_UDP_SITRAD,                ///< CONF UDP DEFAULT
    FLASH_ALL_FLAGS_256,            ///< FLASH FSH_ALL_FLAGS_256 todos os flags necessários tem o tamanho máximo de 256 Bytes, ex.: flag calibracao
    FLASH_DATALOGGER_INSTR_LIST,    ///< FSH_DATALOGGER_INSTR_LIST  lista de equipamentos da rede RS485 que devem ser logados tem o tamanho máximo de 256 Bytes
    FLASH_BOOTLOADER_CODE_ADDRRESS  ///< REGIAO ONDE FICA ARMAZENADO O CODIGO PARA ATUALIZAR O FIRMWARE
} int_storage_type_t;

extern ssp_err_t    int_storage_init(void);
extern ssp_err_t    int_storage_deinit(void);
extern ssp_err_t    int_storage_read(uint8_t *data_buffer, unsigned int data_size, unsigned int info_type);
extern ssp_err_t    int_storage_write(uint8_t *data_buffer, unsigned int data_size, unsigned int info_type);
extern _FG_ERR      Flash_BootRequest(void);
#endif /* INTERNAL_FLASH_H_ */
