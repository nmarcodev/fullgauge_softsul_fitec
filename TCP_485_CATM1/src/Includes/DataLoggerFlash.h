/*
 * DataLoggerFlash.h
 *
 *  Created on: 27 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_DATALOGGERFLASH_H_
#define INCLUDES_DATALOGGERFLASH_H_

#include "common_data.h" // include obrigatório
#include "Includes/UdpTcpTypes.h"
#include "Includes/TypeDefs.h"

//--------------------- DEFINES ---------------------//
#define PAGE_MAX_SIZE                           256
#define INIT_BYTE_LOG_SIZE                      1
#define DATE_AND_TIME_SIZE                      6
#define LENGTH_FRAME_SIZE                       2
#define ADD_RS485_SIZE                          1
#define SIZE_ADDRESS_EEPROM                     2
#define SIZE_ADDRESS_FAC                        ADD_RS485_SIZE
#define SIZE_CKSUM_FRAME_DTL                    2

#define READY_TO_BURN                           0
#define ERASE_FINISHED                          2

#define PAYLOAD_SIZE            (2 * PAGE_MAX_SIZE + CRC_SIZE) //+2 e do CRC que pode flutuar dentro do frame
#define PAYLOAD_SIZE_ERROR      (0) //+2 e do CRC que pode flutuar dentro do frame
#define HEADER_DATALOG          (INIT_BYTE_LOG_SIZE + ADD_RS485_SIZE + LENGTH_FRAME_SIZE + DATE_AND_TIME_SIZE)  //  U8 u8LInitPacket; + U8 u8AddressRs485 + (U8 u8SizePacketMSB; U8 u8SizePacketLSB;) + U8 u8DateAndTime[DATE_AND_TIME_SIZE];
#define MAX_LENGTH_LOG          (HEADER_DATALOG + PAYLOAD)

#define DATALOGGER_TIME_STOP_START              5       //Tempo de acordo com a configuração do TIMER entre um comando de STOP e um novo START (memoria OnSemi ~5ms) CLEBER

#define PAGE_SIZE                               256

#define SIZE_MAX_FLASH_MEMORY                   SIZE_FLASH_DATALOGGER//(NUMBER_OF_PAGES * PAGE_FLASH_SIZE) //32768*256 = 8388608

#define MAX_PAGES_TO_READ                       16           //Maximo 16 pois tem o buffer preparado para isso

#define DATALOGGER_HEADER_STD                   0x1B            //Header que indica que é continuação de registro
#define DATALOGGER_HEADER_START                 0x1C            //Header que indica que novo registro foi iniciado

#if RECORD_DATALOGGER_EVENTS
#define DATALOGGER_HEADER_EVENT                 0x1D            //Header que indica registro de evento
#endif

#define MODEL_INSTRUMENTS_WITH_SIZE_OF_PACKEGE      33              // Instrumentos com o modelo abaixo e inclusive este valor não possui o size no buffer rx do comando 32
#define MODEL_INSTRUMENTS_WITH_SIZE_OF_PACKEGE_2    36              // O Instrumento 36 também não possui o size no buffer rx do comando 32



//--------------------- DEFINES EEPROM ---------------------//
#define         CONFIG_SITRAD_ADDRESS               0           //PAGINA 1: configurações do Sitrad recebida pela WEB tem o tamanho máximo de 256 Bytes
#define         ALL_FLAGS_EEPROM                    256         //PAGINA 2: todos os flags necessários tem o tamanho máximo de 256 Bytes, ex.: flag calibracao

//#define         DATALOGGER_INSTR_LIST               512         //PAGINA 3: (Flash Interna) lista de equipamentos da rede RS485 que devem ser logados estão na terceira página e tem o tamanho máximo de 256 Bytes
#define         DATALOGGER_LAST_SECTOR              768         //PAGINA 4: Salva a cada mudança de setor da memória flash

//#define         EEPROM_SIZE_PAGE                    256
//----------------------------------------------------//

//--------------------- ENUMERATOR ---------------------//
typedef enum
{
    INICIALIZA_ESCRITA,
    APAGA_MARCADOR_PAGINA,
    ESCREVE_MARCADOR_PAGINA,
    ESCREVE_REGISTRO
}_DATA_LOG_WRITE_REG;// EDatalogWriteReg;
//----------------------------------------------------//

//--------------------- UNIONS ---------------------//
typedef union
{
    struct
    {
        uint8_t          BurnFlash              : 1;
        uint8_t          DataloggerEmpty        : 1;
        uint8_t          EraseSector            : 1;
        uint8_t          Reserved               : 5;
    }Bites;
    uint8_t Bytes;
}_DATA_LOGGER_FLAGS; //UDataloggerFlags;
//----------------------------------------------------//

//--------------------- STRUCTS ---------------------//
typedef struct
{
    uint32_t                 Address;            //valor do Endereço a ser escrito considerando toda a memoria
    uint16_t                 SizeToBurnFlash;
    uint16_t                 Reserved;               //tempo de 5ms entre um stop e um novo start bit
}_DATA_LOGGER_PACKET_CONTROL;
//SDataloggerPacketControl;

typedef struct
{
    uint8_t InitPacket;
    uint8_t AddressRs485;
    uint8_t SizePacketMSB;
    uint8_t SizePacketLSB;
    uint8_t DateAndTime[DATE_AND_TIME_SIZE];   //6
    uint8_t PayloadSave[PAYLOAD_SIZE];     //256 + 2
}_DATA_LOGGER_REGISTER;

typedef struct
{
    _DATA_LOGGER_FLAGS              MDataloggerFlags;
    _DATA_LOGGER_PACKET_CONTROL     DataloggerPacketControl;
    _DATA_LOGGER_REGISTER           MDataloggerRegister;
    uint8_t                         FirstRecordControl[(QTD_INSTRUMENTS / 8) + (QTD_INSTRUMENTS % 8 ? 1 : 0)];
}_DATA_LOGGER_CONTROL;
//SDataloggerControl;

typedef union
{
    struct
    {
        uint8_t          bCalibration            : 1;
        uint8_t          bFwUpdate               : 1;
        uint8_t          bFileSystemFail         : 1;
        uint8_t          bSCapTest               : 1;
        uint8_t          bOverrun                : 1;
        uint8_t          Reserved                : 3;
    }Bites;
    uint8_t Bytes;
}_FLAGS_EEPROM;

typedef struct
{
    uint8_t NextAddress; //EEPROM MINIATURA, APENAS 190 ENDERECOS, POIS A E2PROM EH COMPARTILHA COM O MAC_ADDRESS;
}_DATA_LOGGER_EEPROM_CONTROL;

typedef struct
{
    uint16_t    LogInterval;
    uint8_t     NumberOfInst;
    uint8_t     AddressListInstruments[QTD_INSTRUMENTS];     //247
    uint16_t    CkCrc;
}_LIST_INSTRUMENTS;

typedef struct
{
    uint8_t     Retry;
    uint8_t     RequestSetup;
    uint32_t    DateHour;
}_DATA_LOG_CONTROL_BURN;

typedef struct
{
    sf_message_header_t             *header;
    _MESSAGE_FRAMEWORK_CFG          MessageCfg;

    _SERIAL_DATA_IN_PAYLOAD         *SerialDataInPayload;       ///> Messagem Payload - Serial 485 Recebimento
    _SERIAL_DATA_OUT_PAYLOAD        SerialDataOutPayload;       ///> Messagem Payload - Serial 485 Envio

    bool                            UpdateFirmware;
    bool                            ConnectSitrad;
    bool                            FlagFirwmareDownload;
    bool                            FAC;            //estado de fac na memoria
    bool                            startLog;
    bool                            isLogging;
    uint8_t                         MIndexInstrumentsToLog;
    uint8_t                         InstrumentAddress;
    uint8_t                         MEstateMachine;
    uint8_t                         OverrunFlash;
    uint16_t                        MCounterLog;
    uint16_t                        LogIntervalCounter;

    _LIST_INSTRUMENTS               MListInstruments;
    _DATA_LOGGER_CONTROL            MDataloggerControl;
    _DATA_LOGGER_EEPROM_CONTROL     MDataloggerEEpromControl;
    _FLAGS_EEPROM                   FlagsEEprom;                ///> Utilizada para salvar flags na eeprom.
    _DATA_LOG_CONTROL_BURN          MDatallogControlBurn[QTD_INSTRUMENTS];

    _FLASHEXTERNAL_PAYLOAD          FlashExternalPayload; ///> Contem dados do firmware
}_DATALOGGER_DATA;

//----------------------------------------------------//

//--------------------- FUNCTIONS ---------------------//
extern void DataloggerMountPacket(uint8_t AddInstruments, uint16_t SizePayLoad, uint8_t *pu8LPayLoad);
extern void DataloggerMountEmptyPacket(uint8_t AddInstruments);
extern void DataloggerWriteSectorEEprom(_DATALOGGER_DATA *DataLoggerData, uint16_t NextSector);
extern uint8_t DataLoggerCmdRs485(uint8_t AddressRS485, uint16_t u16LIndex);

extern uint8_t DataloggerCheckPackage(uint8_t Address, uint8_t *pu8LBufferRx);

//------------------ HOTS ----------------------------//
extern void DataloggerInit(_DATALOGGER_DATA *DatalogData);
extern void DataloggerFlashSetCmdSitrad(uint16_t Value, uint8_t *BufInstruments, uint8_t Size);
extern uint16_t  DataloggerGetTimeToLog(void);
extern uint16_t DataloggerGetListInstruments(uint8_t *ListInstruments);
extern uint8_t DataloggerGetListInstrumentsFG(uint8_t *ListInstruments);
extern void DataloggerErase(_DATALOGGER_DATA *DataLoggerData);
extern void SetValueFacEEPROM(uint8_t *Data);
extern uint16_t DataloggerGetActualPage(void);
extern uint8_t DataloggerRequisitaRS485(_DATALOGGER_DATA *DatalogData);
extern uint8_t DataloggerTimeOut(_DATALOGGER_DATA *DatalogData);
extern void DataLoggerMountPacket (_DATALOGGER_DATA *DatalogData);
extern void DataLoggerMountVoidPacket (_DATALOGGER_DATA *DatalogData);
extern void SetDataDatalogger(_DATALOGGER_DATA *DatalogData);
extern uint8_t DataloggerGetFlags(void);
extern void SetLogInterval(uint16_t Value);
extern uint8_t Datalogger(_DATALOGGER_DATA *DatalogData);

#if RECORD_DATALOGGER_EVENTS
extern void DataLoggerMountEventPacket (_DATALOGGER_DATA *DatalogData, const _DATA_LOGGER_DATA event);
#endif
//----------------------------------------------------//

extern TX_QUEUE Datalogger_Flash;

#endif /* INCLUDES_DATALOGGERFLASH_H_ */
