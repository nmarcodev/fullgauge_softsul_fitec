/*
 * ConfDefault.h
 *
 *  Created on: 29 de out de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_CONFDEFAULT_H_
#define INCLUDES_CONFDEFAULT_H_

#include "Includes/internal_flash.h"
//#include "Includes/Eth_Wifi_Thread_Entry.h"
#include "Includes/ApplicationUSB.h"
#include "Includes/TypeDefs.h"

extern void USBInitConf(_USB_CONTROL *USBData);
extern void SetUSBFAC(bool bFlagFac);
extern bool GetUSBFAC(void);

#endif /* INCLUDES_CONFDEFAULT_H_ */
