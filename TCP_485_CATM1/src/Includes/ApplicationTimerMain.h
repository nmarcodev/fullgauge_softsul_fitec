/*
 * ApplicationTimerMain.h
 *
 *  Created on: 18 de nov de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONTIMERMAIN_H_
#define INCLUDES_APPLICATIONTIMERMAIN_H_

#include "common_data.h" // include obrigatório
#include "Includes/Eth_Wifi_Thread_Entry.h"
#include "Includes/ApplicationUSB.h"
#include "Eth_Wifi_Thread.h"
#include "Includes/Define_IOs.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/FlashQSPI.h"

extern void EventTimerMain(_USB_CONTROL *USBData, _STATUS_FLAGS *StatusFlagsX);

#endif /* INCLUDES_APPLICATIONTIMERMAIN_H_ */
