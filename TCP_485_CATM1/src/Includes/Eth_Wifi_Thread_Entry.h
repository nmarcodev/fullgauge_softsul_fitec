/*
 * Eth_Wifi_Thread_Entry.h
 *
 *  Created on: 5 de jun de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_ETH_WIFI_THREAD_ENTRY_H_
#define INCLUDES_ETH_WIFI_THREAD_ENTRY_H_

#include "common_data.h"        // include obrigatório
#include "Includes/utility.h"
#include "Includes/messages.h"
#include "Includes/TypeDefs.h"
//#include "nxd_dhcp_client.h"
#include "Includes/DateTime.h"


typedef enum
{
    EW_SM_IDLE,
    EW_SM_INIT_ETH,
    EW_SM_WAIT_INTERFACE,
    EW_SM_WAIT_QUEUE,
    EW_SM_WAIT_RESET,
    EW_SM_EVENT_FLAG,
    EW_SM_DHCP,
    EW_SM_STATIC_IP,
    EW_SM_STATUS_IP,
    EW_SM_RESTART_IP,
    EW_SM_VIEW_IP,
    EW_SM_TESTE_CHANGE,

    EW_SM_CHANGE_MODE,
    EW_SM_WAIT_LINK_ETH,

    EW_SM_INIT_WIFI,
    EW_SM_READ_FLASH,
    EW_SM_READ_FLASH_NET_DEFAULT,
    EW_SM_SCAN,
    EW_SM_PROVISION,
    EW_SM_PROVISION_FAIL,

    EW_SM_INIT_AP,

    EW_SM_DONE,

    EW_SM_RESTART_DEVICE,

}_ETH_WIFI_STATES_MACHINE;

typedef enum
{
    EW_FLAGS_NONE               = 0,
   // eventos
    EW_FLAG_DETECTED_ACTIVITY   = 0x00000001,
    EW_FLAG_TCP_RECEIVED        = 0x00000002,
    EW_FLAG_TCP_RECEIVED_CONF   = 0x00000004,
    EW_FLAG_TCP_DELETE          = 0x00000008,

    EW_FLAG_TCP_CONNECT         = 0x00000010,
    EW_FLAG_TCP_DISCONNECT      = 0x00000020,
    EW_FLAG_TCP_CONNECT_CONF    = 0x00000040,
    EW_FLAG_TCP_DISCONNECT_CONF = 0x00000080,

    EW_FLAG_RTC_SEND            = 0x00000100,
    EW_FLAG_FREE7               = 0x00000200,
    EW_FLAG_FREE8               = 0x00000400,
    EW_FLAG_QUEUE_CONSOLE       = 0x00000800,

    EW_FLAG_MFRAMEWORK          = 0x00001000,
    EW_FLAG_TIMEOUT_TCP         = 0x00002000,
    EW_FLAG_GET_STATUS          = 0x00004000,
    EW_FLAG_TIMEOUT_TIMER_MAIN  = 0x00008000,

    EW_FLAG_WIFI_CONNECTED      = 0x00010000,
    EW_FLAG_WIFI_DISCONNECTED   = 0x00020000,
    EW_FLAG_STATUS_WIFI         = 0x000F0000,

    EW_FLAG_UDPTCP_ALL          = 0x000FFFFF,

}EW_TCP_UDP_FLAGS;

typedef enum
{
   EW_TCP_NONE,
   // eventos
   EW_TCP_SEND,
   EW_TCP_REQUESTED,
   EW_TCP_REQUESTED_CONFIG,
   EW_TCP_REQUESTED_485,
   EW_TCP_REQUESTED_CONF,
   EW_TCP_RELISTEN,

}_TCP_RETURN;

typedef struct
{
    unsigned  PasWrd:1, E_Ip:1, E_2:1, E_3:1, E_4:1, E_5:1, E_6:1, E_7:1, E_8:1;
}_F_Error_;

//typedef struct
//{
//    unsigned    FlagStartUdpTcp:1,
//                FlagSitradConnected:1,
//                StateSitrad:1,
//                FlagNetConnected:1,
//                FlagNetScan:1,
//                FlagNumAttempts:1,
//                FlagFree01:1,
//                FlagFree02:1,
//                FlagFree03:1,
//
//                FlagErrorPassWrd:1,
//                FlagErrorIp:1,
//                FlagErrorInvalidStaticIp:1,
//                FlagErrorFree03:1,
//                FlagErrorFree04:1,
//                FlagErrorFree05:1,
//                FlagErrorFree06:1,
//                FlagErrorFree07:1;
//}_STATUS_FLAGS;

typedef union
{
    uint8_t FlgError;
    _F_Error_ FlgE;
}_F_Error;

typedef struct
{
    _BUFFER_TCP                         BufferTcp;

    //sf_wifi_provisioning_t              * SfProvisionWifi;

    net_input_cfg_t                     NetCfg;

    _CONF_NET_BASIC                     ConfNetBasic;    // Contem TipoInterface, AP ou Wifi, DHCP ou Estatico.
    _NET_WIFI_CONF                      NetWifiConf;    //Configuracoes Wifi
    _NET_WIFI_CONF_AP                   NetWifiConfAp;  //Configuracoes Wifi Modo AP

    uint8_t                             NumNets;
    bool                                FlagConnect;  ///>Utilzado para verificar se ja conectou a rede- status
    bool                                FlagConnectSitrad;  ///>Utilzado para verificar se o Sitrad está conectado

    uint8_t                             DateTimeValues[DATE_TIME_SIZE]; ///> Utilizado para atualizar o RTC

    _IPSTRUCT                           IpStruct;
    _NET_STATIC_MODE                    IpStaticMode;

//    NX_PACKET                           * PacketUdp;
//    NX_PACKET                           * PacketUdpSendRx;
//    NX_PACKET                           * PacketReceiveTCP;
//    NX_PACKET                           * PacketReceiveTCPConf;
//
//    NX_TCP_SOCKET                       TcpSocket;
//    NX_TCP_SOCKET                       TcpSocketConf;
//    NX_UDP_SOCKET                       * UdpSocket;
//
//    NX_IP                               * NxIP;
//    NX_DHCP                             * NxDHCP;

    uint32_t                            IpHttpRemote;

    uint32_t                            ActualFlags;
    _PACKET_UDP                         PacketUdpSend;

    _BUFFER_TCP                         PacketTcpSend;

    uint8_t                             *BufferTcpRX;           ///Utilzado na recepcao TPCPayload

    _ETH_WIFI_STATES_MACHINE            EthWifiStatesMachine;  ///> Contem o estado atual da maquina de estados.

    sf_message_header_t                 *Header;
    _MESSAGE_FRAMEWORK_CFG              MessageCfg; //sk

    //mensagem recebida via framework
    _DATA_IP_PAYLOAD_MESSAGE_PAYLOAD    *DataIpPayload;             ///> ???
    _DATA_TX_PAYLOAD_MESSAGE_PAYLOAD    DataTxPayloadMessagePayload;///> ???

    _DATE_TIME_MESSAGE_PAYLOAD          *DateTimeMessagePayload;    ///> Messagem Payload - Atualiza o RTC Envio
    _SERIAL_DATA_IN_PAYLOAD             *SerialDataInPayload;       ///> Messagem Payload - Serial 485 Recebimento
    _SERIAL_DATA_OUT_PAYLOAD            SerialDataOutPayload;       ///> Messagem Payload - Serial 485 Envio

    _FLASHEXTERNAL_PAYLOAD              *FlashExternalPaylod;

    uint8_t                             WifiRssid;                  ///> Contem a intensidade da rede wifi

    uint8_t                             TimerBigFrameTCP;           ///> TimerBigFrameTCP
    uint8_t                             TimerRestartModule;             ///> TimerResetConv ... Contagem para resetar o modulo conversor.
    uint8_t                             TimerOTACheckCRC;           ///> TimerOTACheckCRC ... Contagem para verificar o CRC da flash.
    uint8_t                             TimerTimeOutOTA;            ///> TimeOut para atualizar o firmware.
    uint8_t                             TimerRestoreFactory;        ///> Timer responsavel por determinar a hora de restaurar as confs de fabrica.
    uint8_t                             TimerConnectSitrad;         ///> Timer para timeout sitrad
    uint8_t                             TimerInactivity;            ///> Timer para monitoramento de inatividade

    bool                                ValidAccess;                ///> Utilizado em TCP, valida o acesso aos dados de reposta
    bool                                ValidAccessConf;            ///> Utilizado em TCP, valida o acesso a porta configuracao
    bool                                BigFrameTCP;                ///> Utilizado em TCPPayload
    bool                                FlagOTAFinished;            ///> Utilziado para sinalizar que uma atualização esta  finalizando.
    bool                                FlagOTARun;                 ///> Utilizado para sinalizar que a atualizacao esta em curso.
    bool                                FlagFAC;                    ///> Utilizado para determinar se está em fac ou nao.
}_ETH_WIFI_CONTROL;

extern TX_QUEUE EthWifi_Flash;
extern TX_QUEUE g_TestFac_Serial;

//extern void EthWifi_UDP_Notify(NX_UDP_SOCKET *socket_ptr);
//extern void EthWifi_TCP_DisconnectRequest(NX_TCP_SOCKET *socket_ptr);
//extern void EthWifi_TCP_DisconnectRequestConf(NX_TCP_SOCKET *socket_ptr);
//extern void EthWifi_TCP_ReceiveNotify(NX_TCP_SOCKET *socket_ptr);
//extern void EthWifi_TCP_ReceiveNotifyConf(NX_TCP_SOCKET *socket_ptr);
//extern void EthWifi_TCP_ConnectRequest(NX_TCP_SOCKET *socket_ptr, UINT port);
//extern void EthWifi_TCP_ConnectRequestConf(NX_TCP_SOCKET *socket_ptr, UINT port);
extern void EthWifi_Message_Framework_NotifyCallback(TX_QUEUE *EthWifi_thread_message_queue);
extern TX_SEMAPHORE EthWifiStatus_Semaphore;
extern TX_EVENT_FLAGS_GROUP EthWifi_Flags;
extern TX_EVENT_FLAGS_GROUP UpFirmareEventFlags;
extern void RtcPost(_ETH_WIFI_CONTROL *EthWifiData);
extern void RestartTimeoutTcp(timer_t TimeOut);
extern void StartTimeoutTcp(timer_t TimeOut);
extern void StopTimeoutTcp(void);
extern void StopTimerMain(void);
extern void StartTimerMain(void);

#endif /* INCLUDES_ETH_WIFI_THREAD_ENTRY_H_ */
