/*
 * ApplicationLedsStatus.h
 *
 *  Created on: 18 de jul de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONLEDSSTATUS_H_
#define INCLUDES_APPLICATIONLEDSSTATUS_H_

//--------------------- DEFINES ---------------------//
#define RESET                   0       //Desliga o GPIO
#define SET                     1       //Liga o GPIO
#define TOGGLE                  2       //Inverte o estado do GPIO

#define LED_OFF                 0
#define LED_ON                  1
#define LED_TOGGLE              2

#define NUM_LEDS                2
#define NUM_PINS                3           // 3 pinos o RED, GREEN e BLUE

#define STATUS_EXCELLENT        70          ///> classificacao em mVolts
#define STATUS_GOOD             52
#define STATUS_POOR             42
#define STATUS_VERY_POOR        30
#define STATUS_NO_SIGNAL        0


//--------------------- ENUMERATOR ---------------------//
typedef enum
{
    WHITE                   = 0,   //   R G B       // LED1 = PC0|PC1|PC2 e LED2 = PC3|PC3|PC5
    CYAN                    = 1,   //     G B       // LED1 = PC1|PC0 e LED2 = PC4|PC3
    PURPLE                  = 2,   //   R   B       // LED1 = PC2|PC0 e LED2 = PC5|PC3
    BLUE                    = 3,   //       B       // LED1 = PC2 e LED2 = PC5
    YELLOW                  = 4,   //   R G         // LED1 = PC2|PC1 e LED2 = PC5|PC4
    GREEN                   = 5,   //     G         // LED1 = PC1 e LED2 = PC4
    RED                     = 6,   //   R           // LED1 = PC0 e LED2 = PC3
    NONE                    = 7,                    // LED de uma unica cor
}_RGB_COLLORS;

typedef enum
{
    L_RX                    = 8,
    L_TX                    = 9,
    L_OFF                   = 10,
    EXIT_TEST_LED           = 11,
    EXIT_TEST_MODULE        = 12,
}_LED_TX_RX;

typedef union{
    uint8_t         Color;
    _RGB_COLLORS    Led_RGB;
    _LED_TX_RX      Led_Tx_Rx;
}_LED_TEST;

typedef enum
{
    POWER_LED               = 0,
    STATUS_LED              = 1,
}_LED_NAME;
//----------------------------------------------------//

//--------------------- STRUCTS ---------------------//
typedef union{
    struct{
        uint8_t LedPinRed           :1;
        uint8_t LedPinGreen         :1;
        uint8_t LedPinBlue          :1;

        uint8_t LedTurnOnOff        :1;
        uint8_t LedToggle           :1;
        uint8_t LedReserved         :2;
        uint8_t LedState            :1;
    }LedsHardware;
    struct{
        uint8_t LedColor            :3;
        uint8_t LedStatus           :4;
        uint8_t LedState            :1;
    }LedProperties;
    uint8_t LedRgbT;
}_LED_RGB;
//----------------------------------------------------//

extern void DriversRgbLeds(_LED_RGB *LedRbg);

#endif /* INCLUDES_APPLICATIONLEDSSTATUS_H_ */
