/*
 * AP_WiFI_Thread_Entry.h
 *
 *  Created on: 23 de jul de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_AP_WIFI_THREAD_ENTRY_H_
#define INCLUDES_AP_WIFI_THREAD_ENTRY_H_

#include "common_data.h"

typedef enum
{
    AP_FLAGS_NONE               = 0,
   // eventos
    AP_FLAG_UDP_RECEIVED        = 0x00000001,
    AP_FLAG_TCP_RECEIVED        = 0x00000002,
    AP_FLAG_UDPandTCP_RECEIVED  = 0x00000003,
    AP_FLAG_FREE                = 0x00000004,
    AP_FLAG_FREE1               = 0x00000008,

    AP_FLAG_TCP_CONNECT         = 0x00000010,
    AP_FLAG_TCP_DISCONNECT      = 0x00000020,
    AP_FLAG_FREE4               = 0x00000040,
    AP_FLAG_DHCP_EVENT          = 0x00000080,

    AP_FLAG_RTC_SEND            = 0x00000100,
    AP_FLAG_CLIENT_CONNECT      = 0x00000200,
    AP_FLAG_CLIENT_DISCONNECT   = 0x00000400,
    AP_FLAG_QUEUE_CONSOLE       = 0X00000800,

    AP_FLAG_MFRAMEWORK          = 0x00001000,
    AP_FLAG_TIMEOUT_TCP         = 0x00002000,
    AP_FLAG_GET_STATUS          = 0x00004000,
    AP_FLAG_DISCONNECTED        = 0x00008000,

    AP_FLAG_UDPTCP_ALL          = 0x0000FFFF,

}AP_TCP_UDP_FLAGS;

typedef struct
{

    _IPSTRUCT                       IpStruct;

//    NX_PACKET                       * PacketUdp;
//    NX_PACKET                       * PacketReceiveTCP;
//
//    NX_TCP_SOCKET                   TcpSocket;
//    NX_UDP_SOCKET                   UdpSocket;
//
//    NX_IP                           * NxIP;
//    NX_PACKET_POOL                  * NxPacketPool;

    uint32_t  ActualFlags;

}_AP_SERVER_CONTROL;

//extern void AP_UDP_Notify(NX_UDP_SOCKET *socket_ptr);
//extern void AP_TCP_ConnectRequest(NX_TCP_SOCKET *socket_ptr, UINT port);
//extern void AP_TCP_ReceiveNotify(NX_TCP_SOCKET *socket_ptr);
//extern void AP_TCP_DisconnectRequest(NX_TCP_SOCKET *socket_ptr);

#endif /* INCLUDES_AP_WIFI_THREAD_ENTRY_H_ */
