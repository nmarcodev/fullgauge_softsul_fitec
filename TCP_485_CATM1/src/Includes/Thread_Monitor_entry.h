#ifndef __THREAD_MONIOTOR_THREAD_H
#define __THREAD_MONIOTOR_THREAD_H

#include "Includes/ApplicationTimer.h"

typedef struct
{
    uint32_t EventFlag;
    uint32_t KeepAliveResult;
    uint16_t TX_Status;
    uint8_t  ResetFlag;
    uint8_t  NoMonitorFlag;
    // timer de validação das threads
    _FG_TIMER ThreadMonitorTimer;
}_THREAD_MONITOR_CONTROL;


typedef enum
{
    T_MONITOR_USB_THREAD       = 0x00000001,
    T_MONITOR_MAIN_MANAGER_THREAD   = 0x00000002,
    T_MONITOR_RTC_THREAD            = 0x00000004,
    T_MONITOR_SERIAL_THREAD         = 0x00000008,
    T_MONITOR_DATALOGGER_THREAD     = 0x00000010,

    T_MONITOR_EXEC_TIMER            = 0x08000000,

    T_MONITOR_STOP_MONITORING       = 0x40000000,
    T_MONITOR_START_MONITORING      = 0x80000000,

    T_MONITOR_STOP_START            = 0xF0000000,
    T_MONITOR_ALL_THREADS           = 0x000000FF,
}_THREAD_MONITOR_FLAGS;

// numero total de threads monitoradas
#define NUMBER_OF_MONITORED_THREADS 5
#define THREAD_MONITOR_TIME         TIME_15S ///> Seta tempo em ticks

extern void ThreadMonitor_SendKeepAlive(_THREAD_MONITOR_FLAGS EventFlag);

extern TX_EVENT_FLAGS_GROUP ThreadMonitor_EventFlag;

#endif
