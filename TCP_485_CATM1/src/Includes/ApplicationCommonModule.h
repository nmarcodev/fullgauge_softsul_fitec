/*
 * ApplicationCommonModule.h
 *
 *  Created on: 21 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_APPLICATIONCOMMONMODULE_H_
#define INCLUDES_APPLICATIONCOMMONMODULE_H_

#include "common_data.h"

extern void RestartModule(void);
//extern void SortStruct(sf_wifi_scan_t *Short, uint8_t Size);

#endif /* INCLUDES_APPLICATIONCOMMONMODULE_H_ */
