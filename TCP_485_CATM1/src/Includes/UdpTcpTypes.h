/*
 * UdpTcpTypes.h
 *
 *  Created on: 21 de mai de 2019
 *      Author: leonardo.ceolin
 */

#ifndef INCLUDES_UDPTCPTYPES_H_
#define INCLUDES_UDPTCPTYPES_H_


#include "Includes/TypeDefs.h"
#include "common_data.h" // include obrigatório
//--------------------- DEFINES ---------------------//

#define MAC_ADDRESS_SIZE        6
#define IP_ADDRESS_SIZE         4
#define MASK_ADDRESS_SIZE       4
#define GTW_ADDRESS_SIZE        4
#define INIT_FILTER_IP_SIZE     4
#define END_FILTER_IP_SIZE      4
#define END_DNS_IP_SIZE         4
#define MODEL_NAME_SIZE         30
#define SETUP_PORT_SIZE         2
#define FLAG_PASSWORD_SIZE      1
#define PASSWORD_SIZE           8

#define VERSION_SIZE            12
#define COMUN_PORT_SIZE         2
#define BAUD_RATE_485_SIZE      2
#define TIME_OUT_485_SIZE       2
#define TIME_OUT_LOG_SIZE       2
#define FLAG_IP_MODE_SIZE       1
#define FLAG_T_INTERFACE_SIZE   1
#define FLAG_T_WIFI_SIZE        1
#define FLAG_ONE_BYTE_SIZE      1
#define FLAG_ENABLE_IP_FILTER   1
#define SSID_WIFI_SIZE          32
#define PWD_WIFI_SIZE           64
#define MAX_NUM_INSTRUMENTS     32

#define SIZE_PAYLOAD_TCP        4125    ///> Tamanho maximo de Payload de transmissao tcp
#define SIZE_HEADER_TCP         3       ///> Tamanho de Header TCP

typedef enum
{
    TCP_OK,    //!< TCP_OK
    TCP_NOT_OK,//!< TCP_NOT_OK
}_RETURN_TCP;


typedef enum
{
    ID_MAC_ADDRESS              = 1,
    ID_IP_ADDRESS               = 2,
    ID_MASK_ADDRESS             = 3,
    ID_GETWAY_ADDRESS           = 4,
    ID_MODEL                    = 6,
    ID_CONVERTER_NAME           = 7,
    ID_ENABLE_PASSWORD          = 8,

    ID_VERSION                  = 0x0A,
    ID_SETUP_PORT               = 0x0C,
    ID_COMUNITATION_PORT        = 0x0E,
    ID_BAUD_RATE                = 0x0F,
    ID_TIME_OUT_RS485           = 0x10,
    ID_IP_MODE                  = 0x11,
    ID_PASSWORD                 = 0x12,
    ID_ENABLE_IP_FILTER         = 0x13,
    ID_INIT_IP                  = 0x14,
    ID_END_IP                   = 0x15,
    ID_TIME_LOG                 = 0x16,
    ID_LIST_INSTRUMENTS         = 0x17,
    //***** Novos IDs   ****//
    ID_NET_INTERFACE            = 0x18,
    ID_FLAG_DNS                 = 0x19,
    ID_DNS                      = 0x1A,
    ID_DNS2                     = 0x1B,
    ID_MODE_WIFI                = 0x1C,

    ID_SSID_WIFI_AP             = 0x1D, ///SSID AP
    ID_TYPE_PWD_WIFI_AP         = 0x1E, ///TIPO DE SENHA
    ID_PWD_AP                   = 0x1F, ///SENHA AP
    ID_CHANNEL                  = 0x20,
    ID_IP_SERV_DHCP             = 0x21,
    ID_IP_MASK_SERV_DHCP        = 0x22,
    ID_IP_RANGE_INIT_SERV_DHCP  = 0x23,
    ID_IP_RANGE_END_SERV_DHCP   = 0x24,

    ID_HIDDEN_NET               = 0x25,
    ID_SSID_WIFI_CLIENT         = 0x26,
    ID_PWD_WIFI_CLIENT          = 0x27,
    ID_TYPE_PWD_WIFI_CLIENT     = 0x28,

    ID_STATUS_SITRAD            = 0x29, //Status do Sitrad (0-Desconectado 1-Conectado)

    ID_END                      = ID_STATUS_SITRAD,
}_PACKET_UDP_CONF;


typedef struct
{
    uint8_t                       Initialize;
    _PACKET_UDP_CONF              CodeMac;
    uint8_t                       SizeMac;
    uint8_t                       MacAddress[MAC_ADDRESS_SIZE];
    _PACKET_UDP_CONF              CodeIp;
    uint8_t                       SizeIp;
    uint8_t                       IpAddress[IP_ADDRESS_SIZE];
    _PACKET_UDP_CONF              CodeMask;
    uint8_t                       SizeMask;
    uint8_t                       MaskAddress[MASK_ADDRESS_SIZE];
    _PACKET_UDP_CONF              CodeGetway;
    uint8_t                       SizeGetway;
    uint8_t                       GetwayAddress[GTW_ADDRESS_SIZE];
    _PACKET_UDP_CONF              CodeModel;
    uint8_t                       SizeModel;
    uint8_t                       ModelName[MODEL_NAME_SIZE];
    _PACKET_UDP_CONF              CodeEnabPass;
    uint8_t                       SizeEnabPass;
    uint8_t                       EnablePassword;
    _PACKET_UDP_CONF              CodeVersion;
    uint8_t                       Sizeversion;
    uint8_t                       Version[VERSION_SIZE];
    _PACKET_UDP_CONF              CodeSetupPort;
    uint8_t                       SizeSetupPor;
    uint8_t                       SetupPort[SETUP_PORT_SIZE];
    _PACKET_UDP_CONF              CodeComuniPort;
    uint8_t                       SizeComuniPort;
    uint8_t                       ComunicationPort[COMUN_PORT_SIZE];
    _PACKET_UDP_CONF              CodeBaudRate;
    uint8_t                       SizeBaudRate;
    uint8_t                       BaudRate[BAUD_RATE_485_SIZE];
    _PACKET_UDP_CONF              CodeTimeOut;
    uint8_t                       SizeTimeOut;
    uint8_t                       TimeOutRs485[TIME_OUT_485_SIZE];
    _PACKET_UDP_CONF              CodeIpMode;
    uint8_t                       SizeIpMode;
    uint8_t                       IpMode;
    _PACKET_UDP_CONF              CodePass;
    uint8_t                       SizePass;
    uint8_t                       Password[PASSWORD_SIZE];
    _PACKET_UDP_CONF              CodeIpFilter;
    uint8_t                       SizeIpFilter;
    uint8_t                       EnableIpFilter;
    _PACKET_UDP_CONF              CodeInitIp;
    uint8_t                       SizeInitIp;
    uint8_t                       InitIp[INIT_FILTER_IP_SIZE];
    _PACKET_UDP_CONF              CodeEndIp;
    uint8_t                       SizeEndIp;
    uint8_t                       EndIp[END_FILTER_IP_SIZE];
    uint8_t                       CodeTerminator;
}_PACKET_UDP;

typedef struct
{
    uint8_t ConfDefault;//1 bytes
    uint8_t ModelName[MODEL_NAME_SIZE + 1]; //30bytes
    uint8_t ComunicationPort[COMUN_PORT_SIZE];//2bytes
    uint8_t EnablePassword;//1 bytes
    uint8_t Password[PASSWORD_SIZE];//8bytes
    uint8_t TimeOutRs485[TIME_OUT_485_SIZE];//2bytes
    uint8_t ConfigurationPort[COMUN_PORT_SIZE];//2bytes
    uint8_t BaudRate485[BAUD_RATE_485_SIZE];//2bytes
    uint8_t IpMode; //1Byte0000000
    uint8_t EnableIpFilter; //1Byte
    uint8_t InitFilterIp[INIT_FILTER_IP_SIZE]; //32
    uint8_t EndFilterIp[END_FILTER_IP_SIZE]; //32
}_UDP_CONF; //total 118 Bytes

typedef struct
{
    uint8_t     OpCod;          ///> Operacao Solicitada
    uint16_t    SizeTemp;       //2
    uint8_t     QntPg;          //1
    uint8_t     *PayloadTcp;    //4
    bool        status;         //1
}_DATA_LOGGER_DATA; //total 4 Bytes

typedef union{
    uint8_t      HeaderPayload[SIZE_HEADER_TCP + SIZE_PAYLOAD_TCP];
    struct{
        uint8_t      Header[SIZE_HEADER_TCP];
        uint8_t      Payload[SIZE_PAYLOAD_TCP];
    }HP;
}_BUFFER_TCP;

typedef struct
{
    uint32_t   IpAddress;
    uint32_t   IpMask;
    uint32_t   IpSubRede;
    uint32_t   IpGateway;
    uint32_t   IpMacAdressMs;
    uint32_t   IpMacAdressLs;
    uint16_t   Udp_Port;        ///Porta de comunicacao UDP
    uint16_t   Tcp_Port_Com;    ///Porta de comunicacao TCP
    uint16_t   Tcp_Port_Conf;   ///Porta de Configuracao TCP
    uint8_t    InterfaceIndex;
}_IPSTRUCT;




#endif /* INCLUDES_UDPTCPTYPES_H_ */
