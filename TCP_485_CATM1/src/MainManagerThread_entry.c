/*
 * MainManagerThread
 *
 * Responsavel pelo monitoramento da entrada digital e execucao de suas funcoes, assim como atualizacao
 * dos LEDs POWER e STATUS.
 *
 * Atraves do g_main_event_flags, recebe as flags que indicam o estado do sistema, e ajusta os LEDs nas cores correspondentes.
 *
 * Atraves do Timer_IO, periodico em 500ms, le o estado da entrada digital, e executa funcoes de acordo com o tempo de acionamento da entrada.
  */

#include "MainManagerThread.h"
#include "common_data.h" // include obrigatório
#include "Includes/ApplicationCommonModule.h"
#include "Includes/internal_flash.h"
#include "Includes/utility.h"
#include "Includes/console_config.h"
#include "Includes/TypeDefs.h"
#include "Includes/ApplicationLedsStatus.h"
#include "Includes/MainManagerThreadEntry.h"
#include "Includes/Define_IOs.h"
#include "Includes/ApplicationFlash.h"
#include "Includes/ApplicationAP.h"
#include "Includes/DataLoggerFlash.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/Eeprom25AA02E48.h"
#include "Includes/ConfDefault.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

/* Main Manager Thread entry function */

_LED_TEST                       LedTest;               ///> Utilizado no teste dos LEDs

typedef struct
{
    ULONG                           ActualFlags;           ///> Variavel com as flags utilizadas pelo controle da thread.
    _LED_RGB                        LedRgb[NUM_LEDS];
    bool                            FlagReset;
    bool                            FlagTestLeds;
    bool                            FlagTestFac;
}_MAIN_MANAGER_DATA;

#define TIME_DIFF_SEC       3 //carrega valor de para o timer2
#define TIME_OUT_FLAG_CH    4

//extern TX_THREAD AP_WiFi_Thread;
TX_THREAD   USB_HID_Device_Thread;
TX_THREAD   Thread_Datalogger;

void Timer2_Open(void);
void Timer_IO_Open(void);
void RestartTimerIO(void);
uint8_t ReadPushButton(void);
void LedsAllOnOff(bool TurnOnOff);

void MMInitLed(_MAIN_MANAGER_DATA *MainManagerData);
//void MMStatusSignalWifi(_MAIN_MANAGER_DATA *MainManagerDataF);
void StatusLeds(_MAIN_MANAGER_DATA *MainManagerData);
void TimerIO(_MAIN_MANAGER_DATA *MainManagerData);
void ThreadMainManagerSetFlag(_MAIN_MANAGER_FLAGS EventFlag);
void ThreadMainManagerSetLedColor(uint8_t LedColorTest);
bool ThreadMainManagerGetStatusButton(void);
void RestoreFactorySetting(void);
void TestLedsM(_MAIN_MANAGER_DATA *MainManagerDataF, _LED_RGB *LedRGB_P, _LED_RGB *LedRGB_S);
bool TestEnabled(bool OpSet);
bool TestKey(uint8_t Opset);


uint8_t ReadPushButton2(void);

void MainManagerThread_entry(void)
{
    _MAIN_MANAGER_DATA MainManagerData;


    MainManagerData.FlagReset       = false;
    MainManagerData.FlagTestLeds    = false;
    MainManagerData.FlagTestFac     = false;


    LedsAllOnOff(true); //Turn on all leds

    MMInitLed(&MainManagerData);
    DriversRgbLeds(&MainManagerData.LedRgb[0]);

    Timer_IO_Open();

    while (1)
    {
        ThreadMonitor_SendKeepAlive(T_MONITOR_MAIN_MANAGER_THREAD);

        if(!tx_event_flags_get(&g_main_event_flags, MM_ALL, TX_OR_CLEAR, &MainManagerData.ActualFlags, TIME_750MS))
        {
            StatusLeds(&MainManagerData);

            if(MainManagerData.ActualFlags & MM_TIMER_IO)
            {
                if(MainManagerData.FlagTestFac)
                {
                    TestKey(true);
                }
                else
                {
                    TimerIO(&MainManagerData);
                }

                if(!MainManagerData.FlagReset)
                {
                    DriversRgbLeds(&MainManagerData.LedRgb[0]);//Atualiza os leds RGB
                }

            }
            if(MainManagerData.ActualFlags & MM_RESTORE_FACTORY)
            {
                RestoreFactorySetting();
            }

            if(MainManagerData.ActualFlags & MM_FAC_TEST_LEDs)
            {
                MainManagerData.FlagTestLeds = true;
            }
        }
    }
}

/**Ex*/
void StatusLeds(_MAIN_MANAGER_DATA *MainManagerData)
{
    if(MainManagerData->ActualFlags & MM_FAC_MODE)
    {
        MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = YELLOW;
        MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;

        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = YELLOW;
        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;

        if(MainManagerData->ActualFlags & MM_ETH_CONNECTED)
        {
            MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = LED_ON;
        }
        MainManagerData->FlagTestFac = true;

    }

    else if(!MainManagerData->FlagTestFac)
    {
        if(MainManagerData->ActualFlags & MM_ETH_TYPE) //verifica se o tipo de rede eh ETH
        {
            MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = GREEN;
            MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;

            MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = RED;
            MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;

            if(MainManagerData->ActualFlags & MM_ETH_CONNECTED)
            {
                MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = BLUE; //Com IP
            }
            else if(MainManagerData->ActualFlags & MM_ETH_WAITING_IP) //com cabo, mas sem IP
            {
                MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = LED_ON;
            }

            if(MainManagerData->ActualFlags & MM_CONNECTED_SITRAD) //verifica se esta conectado com o Sitrad
            {
                MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = LED_ON;
            }
        }

//        else if(MainManagerData->ActualFlags & MM_WIFI_TYPE)
//        {//rede Wireless
//            if(MainManagerData->ActualFlags & MM_WIFI_SERVER) //verifica se eh server
//            {
//                MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = PURPLE;
//                MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;
//
//                if(MainManagerData->ActualFlags & MM_READY_TO_CONNECT)//AGUARDANDO ALGUEM SE CONECTAR
//                {
//                    MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = YELLOW;
//                    MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;
//                }
//
//                if(MainManagerData->ActualFlags & MM_CONNECT_AND_READY)//CLIENTE CONECTADO AO AP
//                {
//                    if(MainManagerData->ActualFlags & MM_CONNECTED_SITRAD) //modo ap e conectado ao sitrad
//                    {
//                        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = CYAN;
//                    }
//                    else
//                    {
//                        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = YELLOW;
//                    }
//                    MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = LED_ON;
//                }
//            }
//            else //WIFI Modo Client
//            {
//                MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = BLUE;
//                MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;
//
//                if(MainManagerData->ActualFlags & MM_SCAN_NET)
//                {
//                    MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = BLUE;
//                    MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;
//                }
//                else
//                {
//                    if(MainManagerData->ActualFlags & MM_STATUS_WIFI) //exibe o status do sinal do wifi
//                    {
//                        MMStatusSignalWifi(MainManagerData); //atualiza os leds
//                    }
//
//                    if(MainManagerData->ActualFlags & MM_CONNECTED_SITRAD) //verifica se esta conectado com o Sitrad
//                    {
//                        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = LED_ON;
//                    }
//                    else
//                    {
//                        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;
//                    }
//                }
//            }
//        }
    }

//    if(MainManagerData->ActualFlags & MM_ERRO_STATIC_IP)
//    {
//        MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = RED;
//        MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;
//
//        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = PURPLE;
//        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;
//    }
//    else if(MainManagerData->ActualFlags & MM_ERRO_IP)
//    {
//        MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = RED;
//        MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;
//
//        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = BLUE;
//        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;
//    }
//    else if(MainManagerData->ActualFlags & MM_ERRO_PWD)
//    {
//        MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = RED;
//        MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;
//
//        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = RED;
//        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;
//    }



    if(MainManagerData->ActualFlags & MM_STATUS_OTA)
    {
        MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = TOGGLE;
        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = TOGGLE;
        MainManagerData->LedRgb[POWER_LED].LedProperties.LedState = MainManagerData->LedRgb[STATUS_LED].LedProperties.LedState;
    }

    if(MainManagerData->FlagTestLeds)
    {
        _LED_RGB LedRGBPwr, LedRGBSts;
        TestLedsM(MainManagerData, &LedRGBPwr, &LedRGBSts);

        MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = LedRGBPwr.LedProperties.LedColor;
        MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LedRGBPwr.LedProperties.LedStatus;

        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = LedRGBSts.LedProperties.LedColor;
        MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = LedRGBSts.LedProperties.LedStatus;
    }
}

void TestLedsM(_MAIN_MANAGER_DATA *MainManagerDataF, _LED_RGB *LedRGB_P, _LED_RGB *LedRGB_S)
{
    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH);  // DESLIGA O LED RX
    g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH);  // DESLIGA O LED TX

    switch(LedTest.Color)
    {
        case BLUE:
            LedRGB_P->LedProperties.LedColor = BLUE;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = BLUE;
            LedRGB_S->LedProperties.LedStatus = ON;
            break;

        case RED:
            LedRGB_P->LedProperties.LedColor = RED;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = RED;
            LedRGB_S->LedProperties.LedStatus = ON;
            break;

        case GREEN:
            LedRGB_P->LedProperties.LedColor = GREEN;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = GREEN;
            LedRGB_S->LedProperties.LedStatus = ON;
            break;

        case YELLOW:
            LedRGB_P->LedProperties.LedColor = YELLOW;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = YELLOW;
            LedRGB_S->LedProperties.LedStatus = ON;
            break;

        case CYAN:
            LedRGB_P->LedProperties.LedColor = CYAN;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = CYAN;
            LedRGB_S->LedProperties.LedStatus = ON;
            break;

        case PURPLE:
            LedRGB_P->LedProperties.LedColor = PURPLE;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = PURPLE;
            LedRGB_S->LedProperties.LedStatus = ON;
            break;

        case WHITE:
            LedRGB_P->LedProperties.LedColor = WHITE;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = WHITE;
            LedRGB_S->LedProperties.LedStatus = ON;
            break;

        case L_RX:
            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW);  // LIGA O LED RX
            LedRGB_P->LedProperties.LedColor = NONE;
            LedRGB_P->LedProperties.LedStatus = OFF;
            LedRGB_S->LedProperties.LedColor = NONE;
            LedRGB_S->LedProperties.LedStatus = OFF;
            break;

        case L_TX:
            g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW); // LIGA O LED TX
            LedRGB_P->LedProperties.LedColor = NONE;
            LedRGB_P->LedProperties.LedStatus = OFF;
            LedRGB_S->LedProperties.LedColor = NONE;
            LedRGB_S->LedProperties.LedStatus = OFF;
            break;

        case EXIT_TEST_LED:
            MainManagerDataF->FlagTestLeds = false;
            LedRGB_P->LedProperties.LedColor = YELLOW;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = YELLOW;
            LedRGB_S->LedProperties.LedStatus = TOGGLE;
            break;

        case EXIT_TEST_MODULE:
            LedRGB_P->LedProperties.LedColor    = YELLOW;
            LedRGB_P->LedProperties.LedStatus   = ON;
            LedRGB_S->LedProperties.LedColor    = GREEN;
            LedRGB_S->LedProperties.LedStatus   = TOGGLE;
            g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW); // LIGA O LED TX
            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW); // LIGA O LED RX

            tx_thread_sleep(TIME_1S);
            _FLAGS_EEPROM FlagsEeprom;
            FlagsEeprom.Bytes = 0;
            SetValueFacEEPROM(&FlagsEeprom.Bytes);
            RestoreFactorySetting();
            RestartModule();
            break;

        case L_OFF:
        case NONE:
        default:
            LedRGB_P->LedProperties.LedColor = NONE;
            LedRGB_P->LedProperties.LedStatus = ON;
            LedRGB_S->LedProperties.LedColor = NONE;
            LedRGB_S->LedProperties.LedStatus = ON;
            LedTest.Color = NONE;
            break;
    }
}

bool TestKey(uint8_t OpSet)
{
    static bool BtCh = false;
    static uint8_t TimerTestBtCh = 0;

    if(OpSet)
    {
        if(!BtCh)
        {
            if(ReadPushButton())
            {
                BtCh = true;
                TimerTestBtCh = TIME_OUT_FLAG_CH; //esse valor x 500ms.
                g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW); // LIGA O LED RX
            }
        }

        else
        {
            if(TimerTestBtCh)
            {
                TimerTestBtCh--;
                if(!TimerTestBtCh)
                {
                    BtCh = false;
                    g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); // DESLIGA O LED RX
                }
            }
        }
    }

    return BtCh;
}


//void MMStatusSignalWifi(_MAIN_MANAGER_DATA *MainManagerDataF)
//{
//    if(MainManagerDataF->ActualFlags & MM_STATUS_WIFI)
//    {
//        if(MainManagerDataF->ActualFlags & MM_S_WIFI_EXCELLENT)
//        {
//            MainManagerDataF->LedRgb[STATUS_LED].LedProperties.LedColor = GREEN;
//        }
//
//        else if(MainManagerDataF->ActualFlags & MM_S_WIFI_GOOD)
//        {
//            MainManagerDataF->LedRgb[STATUS_LED].LedProperties.LedColor = YELLOW;
//        }
//
//        else if(MainManagerDataF->ActualFlags & MM_S_WIFI_POOR)
//        {
//            MainManagerDataF->LedRgb[STATUS_LED].LedProperties.LedColor = PURPLE;
//        }
//
//        else if(MainManagerDataF->ActualFlags & MM_S_WIFI_VERY_POOR)
//        {
//            MainManagerDataF->LedRgb[STATUS_LED].LedProperties.LedColor = RED;
//        }
//    }
//}


void MMInitLed(_MAIN_MANAGER_DATA *MainManagerData)
{
    g_ioport.p_api->pinWrite(LED_ON_R, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ON_G, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ON_B, IOPORT_LEVEL_LOW);

    g_ioport.p_api->pinWrite(LED_ST_R, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ST_G, IOPORT_LEVEL_LOW);
    g_ioport.p_api->pinWrite(LED_ST_B, IOPORT_LEVEL_LOW);


    MainManagerData->LedRgb[POWER_LED].LedProperties.LedStatus = LED_ON;
    MainManagerData->LedRgb[POWER_LED].LedProperties.LedColor = WHITE;
    MainManagerData->LedRgb[STATUS_LED].LedProperties.LedStatus = LED_ON;
    MainManagerData->LedRgb[STATUS_LED].LedProperties.LedColor = WHITE;
}

uint8_t ReadPushButton(void)
{
    uint8_t Cnt = 0;
    ioport_level_t IoportLevel;
    g_ioport.p_api->pinRead(PUSH_B, &IoportLevel);
    if(IoportLevel == IOPORT_LEVEL_LOW)
    {
        Cnt = 1;
    }

    return Cnt;
}

void TimerIO(_MAIN_MANAGER_DATA *MainManagerData)
{
    _CONF_NET_BASIC ConfNetBasic; //Comtem as configuracoes basicas (eth/Wifi, Ap/Client, Dhcp/Estatic)
    static bool  Flag_Tx = false,
                 Flag_Rx = false,
                 Flag_Pwr = false,
                 Flag_Sts = false;
    static uint8_t CntPushButton = 0;

    if(ReadPushButton())
    {

#if RECORD_DATALOGGER_EVENTS
        if(!CntPushButton)
        {
            DataLoggerRegisterEvent(DATALOGGER_EVENT_BUTTON, 0, true);
        }
#endif
        if(!CntPushButton)
        {
            ThreadMonitor_SendKeepAlive(T_MONITOR_STOP_MONITORING);
        }
        CntPushButton++;
        if(CntPushButton > 40)
        {
            while(1)
            {
                RestoreFactorySetting();
                RestartModule();
            }
        }

        else if(CntPushButton > 35)
        {
            if(!Flag_Sts)
            {
                g_ioport.p_api->pinWrite(LED_ON_R, IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(LED_ON_G, IOPORT_LEVEL_LOW);
                g_ioport.p_api->pinWrite(LED_ON_B, IOPORT_LEVEL_HIGH);
                Flag_Sts = true;
            }
            else
            {
                g_ioport.p_api->pinWrite(LED_ON_R, IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(LED_ON_G, IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(LED_ON_B, IOPORT_LEVEL_HIGH);
                Flag_Sts = false;
            }

            g_ioport.p_api->pinWrite(LED_ST_R, IOPORT_LEVEL_HIGH);
            g_ioport.p_api->pinWrite(LED_ST_G, IOPORT_LEVEL_LOW);
            g_ioport.p_api->pinWrite(LED_ST_B, IOPORT_LEVEL_HIGH);
        }

        else if(CntPushButton > 30)
        {
            if(!Flag_Pwr)
            {
                g_ioport.p_api->pinWrite(LED_ST_R, IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(LED_ST_G, IOPORT_LEVEL_LOW);
                g_ioport.p_api->pinWrite(LED_ST_B, IOPORT_LEVEL_HIGH);
                Flag_Pwr = true;
            }
            else
            {
                g_ioport.p_api->pinWrite(LED_ST_R, IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(LED_ST_G, IOPORT_LEVEL_HIGH);
                g_ioport.p_api->pinWrite(LED_ST_B, IOPORT_LEVEL_HIGH);
                Flag_Pwr = false;
            }
            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW); // LIGA O LED RX
            g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW); // LIGA O LED TX
        }

        else if(CntPushButton > 20)
        {
            if(!Flag_Tx)
            {
                g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_LOW); // LIGA O LED TX
                Flag_Tx = true;
            }
            else
            {
                g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED TX
                Flag_Tx = false;
            }
            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW); // LIGA O LED DE TX
        }

        else if(CntPushButton > 10)
        {
            if(!Flag_Rx)
            {
                g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_LOW); // LIGA O LED DE RX
                Flag_Rx = true;
            }
            else
            {
                g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE RX
                Flag_Rx = false;
            }
        }

        else if(CntPushButton > 5)
        {
            tx_thread_suspend(&Thread_Datalogger);
            tx_thread_suspend(&USB_HID_Device_Thread);

            g_ioport.p_api->pinWrite(LED_ON_R, IOPORT_LEVEL_HIGH);
            g_ioport.p_api->pinWrite(LED_ON_G, IOPORT_LEVEL_LOW);
            g_ioport.p_api->pinWrite(LED_ON_B, IOPORT_LEVEL_LOW);

            g_ioport.p_api->pinWrite(LED_ST_R, IOPORT_LEVEL_HIGH);
            g_ioport.p_api->pinWrite(LED_ST_G, IOPORT_LEVEL_LOW);
            g_ioport.p_api->pinWrite(LED_ST_B, IOPORT_LEVEL_LOW);

            g_ioport.p_api->pinWrite(LED_RX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE RECEPCAO
            g_ioport.p_api->pinWrite(LED_TX,IOPORT_LEVEL_HIGH); // DESLIGA O LED DE ENVIO

            MainManagerData->FlagReset = true;

        }
    }
    else if(CntPushButton > 5) //reset apenas o modo Wifi ou Eth para -> AP
    {
        int_storage_read((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);

        if(ConfNetBasic.OpModeWifi == MODE_AP)
        {
            ConfNetBasic.InterfaceType = ConfNetBasic.OldInterfaceType;
            ConfNetBasic.OpModeWifi = MODE_CLIENT;
        }
        else
        {
            ConfNetBasic.OldInterfaceType = ConfNetBasic.InterfaceType;
            ConfNetBasic.OpModeWifi = MODE_AP;
            ConfNetBasic.InterfaceType = WIFI;
        }

        ConfNetBasic.CodMagic = FLASH_NUMBER_CONF_BASIC;

        int_storage_write((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);

        tx_thread_sleep(100);
        RestartModule();
    }
    else
    {
        if(CntPushButton)
        {
            ThreadMonitor_SendKeepAlive(T_MONITOR_START_MONITORING);

#if RECORD_DATALOGGER_EVENTS
            DataLoggerRegisterEvent(DATALOGGER_EVENT_BUTTON, (uint16_t)CntPushButton, false);
#endif
            CntPushButton = 0;
        }
    }
}


void RestoreFactorySetting()
{
    _CONF_NET_BASIC ConfNetBasic; //Comtem as configuracoes basicas (eth/Wifi, Ap/Client, Dhcp/Estatic)
    //_UDP_CONF ConfSitradUdp;
    //_NET_WIFI_CONF NetWifiConf;
    //_WIFI_AP_MODE_CONF WifiApModeConf;
    _LIST_INSTRUMENTS MListInstruments;

    int_storage_read((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
    ConfNetBasic.CodMagic = 0;

    //int_storage_read((uint8_t *)&ConfSitradUdp, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
    //ConfSitradUdp.ConfDefault = 0;

    //int_storage_read((uint8_t *)&WifiApModeConf, sizeof(_WIFI_AP_MODE_CONF), CONF_WIFI_AP);
    //WifiApModeConf.ValCod = 0;

    //int_storage_read((uint8_t *)&NetWifiConf, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
    //NetWifiConf.ValCod = 0;

    memset(&MListInstruments, 0xFF, sizeof(MListInstruments));//leitura dos instrumentos

    //int_storage_write((uint8_t*)&NetWifiConf.ValCod, sizeof(_NET_WIFI_CONF), CONF_WIFI_CLIENT);
    int_storage_write((uint8_t *)&ConfNetBasic, sizeof(_CONF_NET_BASIC), CONF_NET_BASIC);
    //int_storage_write((uint8_t *)&ConfSitradUdp, sizeof(_UDP_CONF), CONF_UDP_SITRAD);
    //int_storage_write((uint8_t *)&WifiApModeConf, sizeof(_WIFI_AP_MODE_CONF), CONF_WIFI_AP);
    int_storage_write((uint8_t *)&MListInstruments, sizeof(_LIST_INSTRUMENTS), FLASH_DATALOGGER_INSTR_LIST);

    LedsAllOnOff(false);

//    RestartModule();
}

//true turn On led; False, turn Off Led.
void LedsAllOnOff(bool TurnOnOff)
{
    g_ioport.p_api->pinWrite(LED_RX,    TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_TX,    TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);

    g_ioport.p_api->pinWrite(LED_ON_R,  TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ON_G,  TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ON_B,  TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);

    g_ioport.p_api->pinWrite(LED_ST_R,  TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ST_G,  TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
    g_ioport.p_api->pinWrite(LED_ST_B,  TurnOnOff?IOPORT_LEVEL_LOW:IOPORT_LEVEL_HIGH);
}

void RestartTimerIO(void)
{
    g_timer5.p_api->periodSet( g_timer5.p_ctrl, 500, TIMER_UNIT_PERIOD_MSEC );
    g_timer5.p_api->start(g_timer5.p_ctrl);
}

//Seta flag externamente.
void ThreadMainManagerSetFlag(_MAIN_MANAGER_FLAGS EventFlag)
{
    tx_event_flags_set (&g_main_event_flags, EventFlag, TX_OR);
}

void ThreadMainManagerSetLedColor(uint8_t LedColorTest)
{
    //necessario converter, pois o enum do teste eh diferente.
    uint8_t Color[] = {NONE, WHITE, CYAN, PURPLE, BLUE, YELLOW, GREEN, RED, L_TX, L_RX, L_OFF, EXIT_TEST_LED, EXIT_TEST_MODULE};

    LedTest = (_LED_TEST)Color[LedColorTest];
}

//Funcao que eh utilizada pelo teste;
bool ThreadMainManagerGetStatusButton(void)
{
    return TestKey(false);
}

//*************************************************************************************************
//* Configures the Timer05 - utilizado para leitura do PushButton e escrita nos leds
//*************************************************************************************************
void Timer_IO_Open(void)
{
    g_timer5.p_api->open(g_timer5.p_ctrl, g_timer5.p_cfg);

    g_timer5.p_api->periodSet( g_timer5.p_ctrl, 500, TIMER_UNIT_PERIOD_MSEC );     //250ms
    g_timer5.p_api->start(g_timer5.p_ctrl);
}

void Timer_IO_Callback (timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_EXPIRED)
    {
        tx_event_flags_set (&g_main_event_flags, MM_TIMER_IO, TX_OR);
    }
}

