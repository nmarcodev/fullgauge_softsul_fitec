/*
 * Monitor Thread
 *
 * Thread responsavel por gerenciar o watchdog.
 *
 * Primeira thread a ser executada. Inicializa as demais threads e recebe um KeepAlive de cada uma delas
 * atraves da ThreadMonitor_EventFlag. Controla a periodicidade atraves do ThreadMonitorTimer, configurado
 * para 15 segundos, e Monitor_Semaphore.
 *
 * Se uma das threads monitoradas nao enviar o KeepAlive dentro do periodo de monitoramento, o WDT nao sera
 * resetado e o equipamento sera reiniciado.
 *
 * Atraves da ThreadMonitor_EventFlag, tambem recebe eventos que habilitam ou desabilitam o monitor
 * (temporariamente), que sao usados em caso de transmissao de firmware OTA, teste de fabrica ou
 * tratamento da entrada digital.
 */

#include "Monitor_Thread.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/ApplicationCommonModule.h"
#include "Includes/internal_flash.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

void ThreadMonitor_Open(_THREAD_MONITOR_CONTROL *ThreadMonitorControl);
void ThreadMonitorTimer_CallBack(ULONG input);
void CallBack_WatchDog(wdt_callback_args_t *p_args);

extern TX_THREAD RTC_thread;
extern TX_THREAD Serial_thread;
extern TX_THREAD MainManagerThread;
extern TX_THREAD Thread_Datalogger;
//extern TX_THREAD Eth_Wifi_Thread;
extern TX_THREAD USB_HID_Device_Thread;

uint32_t ThreadMonitor_GetKeepAlive(uint32_t EventFlag);

/* Monitor Thread entry function */
void Monitor_Thread_entry(void)
{
    _THREAD_MONITOR_CONTROL ThreadMonitorControl;

    /* Enable WDT counting in Debug mode */
    R_DBG->DBGSTOPCR_b.DSWDT = 0;

    ThreadMonitor_Open(&ThreadMonitorControl);

    while (1)
    {

        //Reset do watchdog
        if( ThreadMonitorControl.ResetFlag || ThreadMonitorControl.NoMonitorFlag )
        {
            g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
        }

        if(!tx_event_flags_get(&ThreadMonitor_EventFlag, T_MONITOR_STOP_START, TX_OR_CLEAR, (ULONG*)&ThreadMonitorControl.EventFlag, TX_NO_WAIT))
        {
            if(T_MONITOR_STOP_MONITORING & ThreadMonitorControl.EventFlag)
            {
                ThreadMonitorControl.NoMonitorFlag = true;
            }
            if(T_MONITOR_START_MONITORING & ThreadMonitorControl.EventFlag)
            {
                ThreadMonitorControl.NoMonitorFlag = false;
            }
        }

#if RECORD_DATALOGGER_EVENTS
        static uint16_t semaphoreGetCount = 0;
#endif
        if(!tx_semaphore_get(&g_Monitor_Semaphore, TIME_1S))
        {

#if RECORD_DATALOGGER_EVENTS
            semaphoreGetCount = 0;
#endif

            if(!tx_event_flags_get(&ThreadMonitor_EventFlag, T_MONITOR_ALL_THREADS, TX_OR_CLEAR, (ULONG*)&ThreadMonitorControl.EventFlag, TX_NO_WAIT))
            {
                ThreadMonitorControl.KeepAliveResult = ThreadMonitor_GetKeepAlive(ThreadMonitorControl.EventFlag);
                if(ThreadMonitorControl.KeepAliveResult > 0)
                {

#if RECORD_DATALOGGER_EVENTS
                    DataLoggerRegisterEvent(DATALOGGER_EVENT_WDT_TRIGGER, ThreadMonitorControl.KeepAliveResult & 0xFF, 0);
                    g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
#endif

                    ThreadMonitorControl.ResetFlag = false;
                }
            }
            else
            {

#if RECORD_DATALOGGER_EVENTS
                DataLoggerRegisterEvent(DATALOGGER_EVENT_WDT_TRIGGER, 0, 1);
                g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
#endif

                ThreadMonitorControl.ResetFlag = false;
            }
        }
#if RECORD_DATALOGGER_EVENTS
        else
        {
            semaphoreGetCount++;
            if (semaphoreGetCount >= 15)
            {
                DataLoggerRegisterEvent(DATALOGGER_EVENT_WDT_TRIGGER, (uint16_t)(semaphoreGetCount << 8), 1);
                g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
            }
        }
#endif
    }
}

/**
 * @param EventFlag
 */
void ThreadMonitor_SendKeepAlive(_THREAD_MONITOR_FLAGS EventFlag)
{
    tx_event_flags_set(&ThreadMonitor_EventFlag, EventFlag, TX_OR);
}

uint32_t ThreadMonitor_GetKeepAlive(uint32_t EventFlag)
{
    uint32_t Error = 0;
    for(uint8_t i = 0; i < NUMBER_OF_MONITORED_THREADS; i++)
    {
        if((1U << i & EventFlag ) >> i == 0)
        {
            Error |= 1U<<i;
        }
    }

    return Error;
}

/**
 * Inicializacao da thread
 * @param ThreadMonitorControl Ponteiro para a estrutura de controle
 */
void ThreadMonitor_Open(_THREAD_MONITOR_CONTROL *ThreadMonitorControl)
{
    //    ssp_err_t status;
    int_storage_init(); //Inicializa a flash Interna

    // _FG_ERR FG_Status;
    ThreadMonitorControl->ResetFlag = true;

    // monitora threads
    ThreadMonitorControl->NoMonitorFlag = false;

    // inicializa o wdt
    g_wdt0.p_api->open(g_wdt0.p_ctrl, g_wdt0.p_cfg);

    // cria timer para monitoramento das threads
    tx_timer_create (    &ThreadMonitorControl->ThreadMonitorTimer.timerControlBlock,
                        "ThreadMonitor timer",
                        ThreadMonitorTimer_CallBack,
                        (ULONG)NULL,
                        THREAD_MONITOR_TIME,
                        THREAD_MONITOR_TIME,// periódico
                        TX_AUTO_ACTIVATE );//timer inicia ativado

    // inicializa threads
    tx_thread_resume(&RTC_thread);
    tx_thread_sleep(TIME_50MS);
    tx_thread_resume(&Serial_thread);
    tx_thread_sleep(TIME_50MS);
    tx_thread_resume(&MainManagerThread);
    tx_thread_sleep(TIME_50MS);
    tx_thread_resume(&Thread_Datalogger);
    tx_thread_sleep(TIME_50MS);
    tx_thread_resume(&USB_HID_Device_Thread);
    tx_thread_sleep(TIME_50MS);
    //tx_thread_resume(&Eth_Wifi_Thread);
    //tx_thread_sleep(TIME_50MS);

}

void ThreadMonitorTimer_CallBack(ULONG input)
{
    tx_semaphore_put(&g_Monitor_Semaphore);
    SSP_PARAMETER_NOT_USED(input);
}

void CallBack_WatchDog(wdt_callback_args_t *p_args)
{
    RestartModule();

    SSP_PARAMETER_NOT_USED(p_args);
}
