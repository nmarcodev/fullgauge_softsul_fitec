/*
 * RTC Thread
 *
 * Thread responsavel por controlar os 2 RTCs presentes no sistema:
 * um RTC externo (RX8571LC) com bateria via I2C, e o RTC interno.
 *
 * Na inicializacao, carrega o horario presente no RTC externo para o interno e verifica se ha erro no RTC externo.
 * Se houver erro, seta a flag ECLO (Error Clock), o que impossibilita o registro de datalogger.
 *
 * A atualizacao do horario atual e controlado pelo timer RTC_Timer_RefreshTimer, periodico e, 1s.
 * O horario atual e registrado pela funcao SetDateTimerX() e esta disponivel para qualquer modulo do sistema
 * atraves da funcao GetDateTimerX().
 *
 * Atraves do message framework, pode receber a mensagem SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE, que ordena o
 * ajuste do horario, quando o RTC externo e atualizado com o horario passado.
 */

#include "Includes/ApplicationTimer.h"
#include "RTC_thread.h"

#include "Includes/DateTime.h"
#include "Includes/RX_8571LC.h"
#include "Includes/Thread_Monitor_entry.h"
#include "Includes/Define_IOs.h"

#if RECORD_DATALOGGER_EVENTS
#include "Includes/Thread_Datalogger_entry.h"
#endif

typedef struct
{

    // timer de refresh da interface
    _FG_TIMER                      RTC_Timer_RefreshTimer;
    rtc_time_t                     ActualTime;
    // parametros de configuração de     mensagens via message Framework
    _MESSAGE_FRAMEWORK_CFG          Configs_messageCfg;
    _DATE_TIME_MESSAGE_PAYLOAD      *mf_DateTimePayload;

}_RTC_CONTROL;

#define RTC_REFRESH_TIME TIME_1S

// definição do semaforo para sinalização de eventos
TX_SEMAPHORE g_RTC_Semaphore;
TX_SEMAPHORE g_RTC_TimerSemaphore;

// publica data e hora atualizada
void RTC_PublishDateTime(_RTC_CONTROL* RtcControl);

void RTC_Open(_RTC_CONTROL* RtcControl);

// Trata mensagens recebidas via message framework
_FG_ERR RTC_ReceiveMessagetreatment(_RTC_CONTROL *RtcControl);

// callback de notificacao de mensagens recebidas via message framework
void RTC_MessageFramework_NotifyCallback(TX_QUEUE *RTC_Thread_message_queue);

// Calback do timer de refresh na interface
void RTC_RefreshTimer_CallBack(ULONG input);

_FG_ERR RTC_Eclo(_RTC_CONTROL* RtcControl);
bool RTC_VerifyEclo(_RTC_CONTROL* RtcControl);

void RTC_StructToList(rtc_time_t* const TimeStruct, uint8_t * List);
void RTC_ListToStruct(uint8_t * const List, rtc_time_t* TimeStruct);


/* RTC Thread entry function */
void RTC_thread_entry(void)
{
    _RTC_CONTROL RtcControl;
    UINT TX_Status;

    RTC_Open(&RtcControl);

#if RECORD_DATALOGGER_EVENTS
    RTC_PublishDateTime(&RtcControl);
    DataLoggerRegisterEvent(DATALOGGER_EVENT_RESET, R_SYSTEM->RSTSR1, false);
#endif

    while (1)
    {
        ThreadMonitor_SendKeepAlive(T_MONITOR_RTC_THREAD);

        // aguarda até que um evento ocorra
        if(!tx_semaphore_get(&g_RTC_Semaphore, TIME_750MS))
        {
            RTC_ReceiveMessagetreatment(&RtcControl);

            TX_Status = tx_semaphore_get(&g_RTC_TimerSemaphore, TX_NO_WAIT);
            if(TX_Status == TX_SUCCESS)
            {
                RTC_PublishDateTime(&RtcControl);
            }
        }
    }
}


void RTC_PublishDateTime(_RTC_CONTROL* RtcControl)
{
    g_rtc0.p_api->calendarTimeGet(g_rtc0.p_ctrl, &RtcControl->ActualTime );

    RtcControl->mf_DateTimePayload->SetDateTime = false;
    RtcControl->mf_DateTimePayload->DateTimeValues[DATE_TIME_DAY]    = (uint8_t)RtcControl->ActualTime.tm_mday;
    RtcControl->mf_DateTimePayload->DateTimeValues[DATE_TIME_MONTH]  = (uint8_t)(RtcControl->ActualTime.tm_mon + 1);
    RtcControl->mf_DateTimePayload->DateTimeValues[DATE_TIME_YEAR]   = (uint8_t)(RtcControl->ActualTime.tm_year - 100);
    RtcControl->mf_DateTimePayload->DateTimeValues[DATE_TIME_HOUR]   = (uint8_t)RtcControl->ActualTime.tm_hour;
    RtcControl->mf_DateTimePayload->DateTimeValues[DATE_TIME_MINUTE] = (uint8_t)RtcControl->ActualTime.tm_min;
    RtcControl->mf_DateTimePayload->DateTimeValues[DATE_TIME_SECOND] = (uint8_t)RtcControl->ActualTime.tm_sec;

    SetDateTimerX(RtcControl->mf_DateTimePayload->DateTimeValues);
}


void RTC_Open(_RTC_CONTROL* RtcControl)
{
    uint8_t DateTimeValues[DATE_TIME_SIZE];
    _FG_ERR FG_Status;
    ssp_err_t SSP_Status;

    g_ioport.p_api->pinWrite(RTC_FAC_PIN,IOPORT_LEVEL_HIGH);
    g_i2c1.p_api->open(g_i2c1.p_ctrl, g_i2c1.p_cfg);
    g_i2c1.p_api->reset(g_i2c1.p_ctrl);

    // habilita o cristal externo e aguarda um delay para estabilização
    // isto está sendo feito porque o delay dentro da função open não é sufuciente.

    rtc_instance_ctrl_t * p_ctrl = (rtc_instance_ctrl_t *) g_rtc0.p_ctrl;
    R_RTC_Type * p_rtc_reg = (R_RTC_Type *) p_ctrl->p_reg;
    cgc_clock_cfg_t pll_cfg;
    g_cgc_on_cgc.clockStart(CGC_CLOCK_SUBCLOCK, &pll_cfg);
    p_rtc_reg->RCR4_b.RCKSEL = RTC_CLOCK_SOURCE_SUBCLK;
    R_BSP_SoftwareDelay(300U, BSP_DELAY_UNITS_MILLISECONDS);

    SSP_Status = g_rtc0.p_api->open(g_rtc0.p_ctrl, g_rtc0.p_cfg);
    if(SSP_Status == SSP_SUCCESS)
    {
        SSP_Status = g_rtc0.p_api->calendarCounterStart(g_rtc0.p_ctrl);
    }

    //cria semaforo para sinalização de eventos
    tx_semaphore_create(&g_RTC_Semaphore,"RTC Semaphore", 0);
    tx_semaphore_create(&g_RTC_TimerSemaphore,"RTC Timer Semaphore", 0);

    // cria notificação para mensagens recebidas pelo message Framework
    tx_queue_send_notify (&RTC_thread_message_queue,RTC_MessageFramework_NotifyCallback);

    // aloca buffer para mensagem com configurações atualizadas

    RtcControl->Configs_messageCfg.acquireCfg.buffer_keep = true;   // não desaloca o buffer
    RtcControl->Configs_messageCfg.postCfg.priority = SF_MESSAGE_PRIORITY_NORMAL;
    RtcControl->Configs_messageCfg.postCfg.p_callback = NULL; // não cria callback

    // aloca buffer para mensagem de data e hora
    SSP_Status = g_sf_message0.p_api->bufferAcquire( g_sf_message0.p_ctrl,
                                                    (sf_message_header_t **) &RtcControl->mf_DateTimePayload,
                                                    &RtcControl->Configs_messageCfg.acquireCfg,
                                                    TIME_250MS);


    // cria timer para refresh da interface
    tx_timer_create(&RtcControl->RTC_Timer_RefreshTimer.timerControlBlock,"RTC_refresh_timer",
                    RTC_RefreshTimer_CallBack, 0U,
                    (ULONG)TIME_10MS,
                    RTC_REFRESH_TIME, // periódico
                    TX_AUTO_ACTIVATE); //timer inicia ativado


    // inicializa RTC externo

    FG_Status = RX8571LC_Init();

    FG_Status = RX8571LC_VerifyEclo();

    if(FG_Status == FG_ECLO)
    {
        FG_Status = RTC_Eclo(RtcControl);
        SetFlagEclo(true);
    }
    else
    {
        SetFlagEclo(false);
    }
    // copia o relógio do RTC externo para o interno

    FG_Status = RX8571LC_GetExternalDateTime(&DateTimeValues[0]);

    RTC_ListToStruct((uint8_t*)&DateTimeValues, &RtcControl->ActualTime);

    SSP_Status = g_rtc0.p_api->calendarTimeSet(g_rtc0.p_ctrl, &RtcControl->ActualTime, true );
}

_FG_ERR RTC_Eclo(_RTC_CONTROL* RtcControl)
{
    SSP_PARAMETER_NOT_USED(RtcControl);

    uint8_t DateTimeValues[DATE_TIME_SIZE] = {7,3,19,0,0,0};
    _FG_ERR FG_Status;

    FG_Status =  RX8571LC_SetExternalDateTime(&DateTimeValues[0]);
//novo----------
    RX8571LC_GetExternalDateTime(&DateTimeValues[0]);
    RTC_ListToStruct((uint8_t*)&DateTimeValues, &RtcControl->ActualTime);
    g_rtc0.p_api->calendarTimeSet(g_rtc0.p_ctrl, &RtcControl->ActualTime, true );
    //fim do novo
    return FG_Status;
}

bool RTC_VerifyEclo(_RTC_CONTROL* RtcControl)
{
    _FG_ERR FG_Status;

    FG_Status = RX8571LC_VerifyEclo();

    if(FG_Status == FG_ECLO)
    {
        FG_Status = RTC_Eclo(RtcControl);
        SetFlagEclo(true);
        return true;
    }

    else
    {
        SetFlagEclo(false);
        return false;
    }
    // copia o relógio do RTC externo para o interno
}

//*************************************************************************************************
//* Calback do timer de refresh na interface
//*************************************************************************************************
void RTC_RefreshTimer_CallBack(ULONG input)
{
    //Sinaliza que o timer expirou
    tx_semaphore_ceiling_put(&g_RTC_TimerSemaphore,1);
    // sinaliza que ocorreu algum evento
    tx_semaphore_put(&g_RTC_Semaphore);

    SSP_PARAMETER_NOT_USED(input);
}

//*************************************************************************************************
//* callback de notificação de mensagem posta na queue Interface_thread_message_queue
//* via message Framework
//*************************************************************************************************
void RTC_MessageFramework_NotifyCallback(TX_QUEUE *RTC_Thread_message_queue)
{
    //Sinaliza que o evento ocorreu
    tx_semaphore_put(&g_RTC_Semaphore);
    SSP_PARAMETER_NOT_USED(RTC_Thread_message_queue);
}

//*************************************************************************************************
//* Trata mensagens recebidas via message framework
//*
//*
//*
//*************************************************************************************************
_FG_ERR RTC_ReceiveMessagetreatment(_RTC_CONTROL *RtcControl)
{
    _DATE_TIME_MESSAGE_PAYLOAD *p_DateTime;
    sf_message_header_t *pPayload;
    ssp_err_t SSP_status;
    rtc_time_t EditedTime;

    //verifica se há mensagem
    SSP_status = (ssp_err_t)g_sf_message0.p_api->pend(g_sf_message0.p_ctrl,
                                                      &RTC_thread_message_queue,
                                                      (sf_message_header_t **) &pPayload,
                                                      TX_NO_WAIT);
    // trata se recebeu
    if(SSP_status == SSP_SUCCESS)
    {
        if(pPayload->event_b.class_code == SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE)
        {
            p_DateTime = (_DATE_TIME_MESSAGE_PAYLOAD*)pPayload;
            if(p_DateTime->header.event_b.class_code == SF_MESSAGE_EVENT_CLASS_DATE_TIME_MESSAGE)
            {
                if(p_DateTime->SetDateTime == true)
                {
                    memcpy((void*)&EditedTime, (void*)&RtcControl->ActualTime, sizeof(rtc_time_t));

                    RTC_ListToStruct(p_DateTime->DateTimeValues,&EditedTime);

                    SSP_status = g_rtc0.p_api->calendarTimeSet(g_rtc0.p_ctrl, &EditedTime,true );
                    // só grava o relógio no RTC externo se o valor foi válido

                    if(SSP_status == SSP_SUCCESS)
                    {
                        RX8571LC_SetExternalDateTime(p_DateTime->DateTimeValues);
                    }
                    RTC_PublishDateTime(RtcControl);
                    SetFlagEclo(false); //atualiza o status do eclo.
                }
                return FG_SUCCESS;
            }
        }
    }
    return FG_NOTHING_TO_BE_DONE;
}

void RTC_ListToStruct(uint8_t * const List, rtc_time_t* TimeStruct)
{
    TimeStruct->tm_mday = List[DATE_TIME_DAY];
    TimeStruct->tm_mon =  List[DATE_TIME_MONTH] - 1;
    TimeStruct->tm_year = List[DATE_TIME_YEAR] + 100;
    TimeStruct->tm_hour = List[DATE_TIME_HOUR];
    TimeStruct->tm_min =  List[DATE_TIME_MINUTE];
    TimeStruct->tm_sec =  List[DATE_TIME_SECOND];
}

void RTC_StructToList(rtc_time_t* const TimeStruct, uint8_t * List)
{
    List[DATE_TIME_DAY]    = (uint8_t)TimeStruct->tm_mday;
    List[DATE_TIME_MONTH]  = (uint8_t)(TimeStruct->tm_mon +1);
    List[DATE_TIME_YEAR]   = (uint8_t)(TimeStruct->tm_year - 100);
    List[DATE_TIME_HOUR]   = (uint8_t)TimeStruct->tm_hour;
    List[DATE_TIME_MINUTE] = (uint8_t)TimeStruct->tm_min;
    List[DATE_TIME_SECOND] = (uint8_t)TimeStruct->tm_sec;
}

void RTC_callback(rtc_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
