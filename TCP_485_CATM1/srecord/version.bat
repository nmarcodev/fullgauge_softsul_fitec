for /F "tokens=1-3" %%A in ('findstr "^#.*FIRMWARE.*[0-9]$" bootloader_defs') do set BOOT_%%B=0%%C
for /F "tokens=1-3" %%A in ('findstr "^#.*FIRMWARE.*[0-9]$" firmware_defs') do set %%B=%%C
for /F "tokens=1-3" %%A in ('findstr "^#.*FIRMWARE.*[0-9]$" firmware_defs') do set %%B_DOT=.%%C
for /F "tokens=1-3" %%A in ('findstr "^#.*FIRMWARE.*[0-9]$" firmware_defs') do set %%B_0=0%%C

move TCP_485_CATM1_BV01R00_PVxxRyy.srec TCP_485_CATM1_BV%BOOT_FIRMWARE_VERSION:~-2%R%BOOT_FIRMWARE_RELEASE:~-2%_PV%FIRMWARE_VERSION_0:~-2%R%FIRMWARE_RELEASE_0:~-2%.srec
move TCP_485_CATM1_vX.Y.bin TCP_485_CATM1_v%FIRMWARE_VERSION%%FIRMWARE_RELEASE_DOT%%FIRMWARE_PATCH_DOT%.bin